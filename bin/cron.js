#!/usr/bin/env node

var http = require('http');
var config = require('../server/config/environment');

var generateStats = function() {
	console.log('Calling Generate stats api!');
	var options = {
		method: 'POST',
	    host: config.DOMAIN.replace('http://','').replace('https://',''),
	    port: 80,
	    path: '/api/v1/partner-stats/create'
	};
	console.log("======WAKUP DYNO START");
	var req = http.request(options, function(res) {
	    res.on('data', function(chunk) {
	        try {
	            // optional logging... disable after it's working
	            console.log("======WAKUP DYNO: HEROKU RESPONSE: " + chunk);
	        } catch (err) {
	            console.log(err.message);
	        }
	    });
	}).on('error', function(err) {
	    console.log("Error: " + err.message);
	});
	
	// write data to request body
	// req.write(postData);
	req.end();
};

generateStats();