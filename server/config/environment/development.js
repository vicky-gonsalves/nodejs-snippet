'use strict';

// Development specific configuration
// ==================================
module.exports = {

  // MongoDB connection options
  mongo: {
    uri: 'mongodb://localhost/ae-dev'
  },

  // Seed database on startup
  seedDB: false,
  sendgrid: {
    key: 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXX,
    sendgridIdForPassword: 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
  },
  cloudinary: {
    cloud_name: 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
    api_key: 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
    api_secret: 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
  },
  sendEmail: {
    from: 'no-reply@blah.com'
  },
  tokenExpiry: 60 * 60 * 1000 * 5, //(5 hours)
  debugAPI: true,
  DOMAIN: 'http://localhost:9000',
  appLinkAndroid: 'http://blah.com',
  appLinkIos: 'http://blah.com',
  useOrgSpecificKeyAndroid: true,
  useOrgSpecificKeyiOs: true,
  defaultAndroidKey: 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
  defaultiOSKey: './server/config/certs/key.pem',
  // elasticSearchURL: 'http://paas:e1973153ab39f9f4d9bf3ff93169cea6@dori-us-east-1.searchly.com',
  elasticSearchURL: 'http://127.0.0.1:9200',
  elasticSearchDocument: "partners",
  loggerURL: 'http://127.0.0.1:9200',
  logger: true,
  externalUrlDomain: 'http://localhost:9000',
  firebaseURL: 'https://firebasedynamiclinks.googleapis.com/v1/shortLinks',
  userRegisterDomain: 'https://influents.io/register',
  cardExcludeTag: 'AppStoreReview',
  webhook: '["https://requestb.in/1mmryzd1","https://requestb.in/12usv3x1"]',
  twilio: {
    accountSid: 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
    authToken: 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
    phoneNumber: 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
  }
};
