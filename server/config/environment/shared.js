'use strict';

exports = module.exports = {
  // List of user roles
  userRoles: ['guest', 'user', 'admin', 'influents-admin', 'organization-admin', 'superadmin']
};
