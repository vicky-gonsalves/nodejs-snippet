'use strict';

// Production specific configuration
// =================================
module.exports = {
  // Server IP
  ip: process.env.OPENSHIFT_NODEJS_IP ||
  process.env.IP ||
  undefined,

  // Server port
  port: process.env.OPENSHIFT_NODEJS_PORT ||
  process.env.PORT ||
  9000,

  // MongoDB connection options
  mongo: {
    uri: process.env.MONGOLAB_URI ||
    process.env.MONGOHQ_URL ||
    process.env.OPENSHIFT_MONGODB_DB_URL +
    process.env.OPENSHIFT_APP_NAME ||
    'mongodb://localhost/aeapp-dev'
  },
  sendgrid: {
    key: process.env.SENDGRID_KEY,
    sendgridIdForPassword: process.env.SENDGRID_ID_FOR_PASSWORD_EMAIL
  },
  sendEmail: {
    from: process.env.SEND_EMAIL_FROM
  },
  tokenExpiry: process.env.TOKEN_EXPIRY,
  debugAPI: process.env.DEBUG_API == 'true' || false,
  DOMAIN: process.env.DOMAIN,
  appLinkAndroid: process.env.ANDROID_APP_LINK,
  appLinkIos: process.env.IOS_APP_LINK,
  useOrgSpecificKeyAndroid: process.env.USE_ORG_KEY_ANDROID,
  useOrgSpecificKeyiOs: process.env.USE_ORG_KEY_IOS,
  defaultAndroidKey: process.env.DEFAULT_KEY_ANDROID,
  defaultiOSKey: process.env.DEFAULT_KEY_IOS,
  elasticSearchURL: process.env.ELASTIC_SEARCH_URL,
  elasticSearchDocument: process.env.ELASTIC_SEARCH_DOC,
  loggerURL: process.env.LOGGER_URL,
  logger: process.env.LOGGER_ENABLED,
  cloudinary: {
    cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
    api_key: process.env.CLOUDINARY_API_KEY,
    api_secret: process.env.CLOUDINARY_API_SECRET
  },
  externalUrlDomain: process.env.CLICK_TRACKING_DOMAIN,
  firebaseURL: process.env.FIREBASE_URL,
  userRegisterDomain: process.env.USER_REGISTER_DOMAIN,
  cardExcludeTag: process.env.CARD_EXCLUDE_TAG,
  webhook: process.env.WEBHOOK,
  twilio: {
    accountSid: process.env.TWILIO_ACCOUNT_SID,
    authToken: process.env.TWILIO_AUTH_TOKEN,
    phoneNumber: process.env.TWILIO_PHONE_NUMBER
  }
};
