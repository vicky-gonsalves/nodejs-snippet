/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';
import User from '../api/user/user.model';
import Organization from '../api/organization/organization.model';
import Card from '../api/card/card.model';
import Doormat from '../api/doormat/doormat.model';
import Partner from '../api/partner/partner.model';
import Sidebar from '../api/sidebar/sidebar.model';
import UserCard from '../api/user-card/user-card.model';
import Form from '../api/form/form.model';

let _ = require('lodash');

let moment = require('moment');

UserCard.find({}).remove()
.then(() => {

User.find({}).remove()
  .then(() => {
    User.create({
      provider: 'local',
      name: 'Test User',
      email: 'test@example.com'
    }, {
      provider: 'local',
      name: 'Admin',
      email: 'admin@example.com'
    })
    .then(() => {
      console.log('finished populating users');

      Card.find({}).remove()
      .then(() => {
        Form.find({}).remove()
        .then(() => {
          Partner.find({}).remove()
          .then(() => {
            Organization.find({}).remove()
            .then(() => {
              
              Organization.create({
                name: 'Radioworld Global',
                image: 'http://lorempixel.com/300/200/'
              })
              .then((organization) => {
                console.log('finished populating organizations', organization);
                var utcDate = moment.utc().toISOString();

                Form.create([{
                  organization: organization.id,
                  type: 'inquiry',
                  name: 'Ask your question.',
                  shortName: 'Support',
                  fields: [ { label: 'Subject', control: 'input', controlType: 'text' },
                            { label: 'Message', control: 'input', controlType: 'text' } ]
                }, {
                  organization: organization.id,
                  type: 'inquiry',
                  name: 'How may I help you?',
                  shortName: 'Help',
                  fields: [ { label: 'Subject', control: 'input', controlType: 'text' },
                            { label: 'Message', control: 'input', controlType: 'text' } ]
                }])
                .then((forms) => {
                  console.log('Finished populating forms', _.map(forms, function(form){ return form.id}));
                });

                Partner.create({
                  organization: organization.id,
                  partnerId: 'asdf',
                  name: 'Pats Radios and Stuff',
                  image: 'http://lorempixel.com/300/200/'
                })
                .then((partner) => {
                  console.log('Finished populating partner', partner.id);
                });

                Card.create([{
                  type: 'story',
                  organization: organization.id,
                  residue: 'text of residue',
                  icon: 'http://lorempixel.com/100/100/',
                  date: utcDate,
                  format: 'medium',
                  story: {
                    title: 'Nuns quis lectus in lingula',
                    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
                    links: [ {
                        title: 'Favourite',
                        dest: '/favourite',
                        type: 'wrapped'
                      },
                      {
                        title: 'Share',
                        dest: '/share',
                        type: 'internal'
                      } ]
                  }
                }, {
                  type: 'story',
                  organization: organization.id,
                  residue: 'text of residue',
                  icon: 'http://lorempixel.com/100/100/',
                  date: utcDate,
                  format: 'medium',
                  story: {
                    title: 'Nuns quis lectus in lingula',
                    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris id turpis quis massa maximus pulvinar. Cras vel risus id metus gravida dictum. In eget justo in nisi sollicitudin cursus. Aenean nulla ligula, consequat eu sollicitudin sed, blandit non tortor. In efficitur magna ac nunc semper dignissim. Praesent ligula justo, semper et pharetra laoreet, cursus sed mauris. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Cras vitae ligula et turpis fermentum tincidunt sit amet vitae dui. Phasellus egestas suscipit justo, sit amet faucibus orci sollicitudin vitae. Cras at ligula et odio gravida euismod nec ut arcu. Nullam vitae nisl ex. In hac habitasse platea dictumst. Nunc congue porta malesuada. Phasellus blandit cursus nisi ut pellentesque.',
                    media: {
                      image: 'http://lorempixel.com/400/250/',
                      type: 'image'
                    },
                    links: [ {
                        title: 'Favourite',
                        dest: '',
                        type: 'wrapped'
                      },
                      {
                        title: 'Share',
                        dest: '/share',
                        type: 'internal'
                      } ]
                  }
                }, {
                  type: 'story',
                  organization: organization.id,
                  residue: 'text of residue',
                  icon: 'http://lorempixel.com/100/100/',
                  date: utcDate,
                  format: 'medium',
                  story: {
                    title: 'Captain America',
                    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris id turpis quis massa maximus pulvinar. Cras vel risus id metus gravida dictum. In eget justo in nisi sollicitudin cursus. Aenean nulla ligula, consequat eu sollicitudin sed, blandit non tortor. In efficitur magna ac nunc semper dignissim. Praesent ligula justo, semper et pharetra laoreet, cursus sed mauris. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Cras vitae ligula et turpis fermentum tincidunt sit amet vitae dui. Phasellus egestas suscipit justo, sit amet faucibus orci sollicitudin vitae. Cras at ligula et odio gravida euismod nec ut arcu. Nullam vitae nisl ex. In hac habitasse platea dictumst. Nunc congue porta malesuada. Phasellus blandit cursus nisi ut pellentesque.',
                    media: {
                      image: 'https://www.youtube.com/watch?v=dKrVegVI0Us',
                      type: 'video'
                    },
                    links: [ {
                        title: 'Favourite',
                        dest: '',
                        type: 'wrapped'
                      },
                      {
                        title: 'Share',
                        dest: '/share',
                        type: 'internal'
                      }]
                  }
                }, {
                  type: 'story',
                  organization: organization.id,
                  residue: 'text of residue',
                  icon: 'http://lorempixel.com/100/100/',
                  date: utcDate,
                  format: 'small',
                  story: {
                    title: 'Nuns quis lectus in lingula',
                    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris id turpis quis massa maximus pulvinar. Cras vel risus id metus gravida dictum. In eget justo in nisi sollicitudin cursus. Aenean nulla ligula, consequat eu sollicitudin sed, blandit non tortor. In efficitur magna ac nunc semper dignissim. Praesent ligula justo, semper et pharetra laoreet, cursus sed mauris. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Cras vitae ligula et turpis fermentum tincidunt sit amet vitae dui. Phasellus egestas suscipit justo, sit amet faucibus orci sollicitudin vitae. Cras at ligula et odio gravida euismod nec ut arcu. Nullam vitae nisl ex. In hac habitasse platea dictumst. Nunc congue porta malesuada. Phasellus blandit cursus nisi ut pellentesque.',
                    link: [ ]
                  }
                }, {
                  type: 'story',
                  organization: organization.id,
                  residue: 'text of residue',
                  icon: 'http://lorempixel.com/100/100/',
                  date: utcDate,
                  format: 'small',
                  story: {
                    title: 'Nuns quis lectus in lingula',
                    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris id turpis quis massa maximus pulvinar. Cras vel risus id metus gravida dictum. In eget justo in nisi sollicitudin cursus. Aenean nulla ligula, consequat eu sollicitudin sed, blandit non tortor. In efficitur magna ac nunc semper dignissim. Praesent ligula justo, semper et pharetra laoreet, cursus sed mauris. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Cras vitae ligula et turpis fermentum tincidunt sit amet vitae dui. Phasellus egestas suscipit justo, sit amet faucibus orci sollicitudin vitae. Cras at ligula et odio gravida euismod nec ut arcu. Nullam vitae nisl ex. In hac habitasse platea dictumst. Nunc congue porta malesuada. Phasellus blandit cursus nisi ut pellentesque.',
                    media: {
                      image: 'http://lorempixel.com/400/250/',
                      type: 'image'
                    },
                    links: [ ]
                  }
                }, {
                  type: 'story',
                  organization: organization.id,
                  residue: 'text of residue',
                  icon: 'http://lorempixel.com/100/100/',
                  date: utcDate,
                  format: 'small',
                  story: {
                    title: 'Captain America',
                    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris id turpis quis massa maximus pulvinar. Cras vel risus id metus gravida dictum. In eget justo in nisi sollicitudin cursus. Aenean nulla ligula, consequat eu sollicitudin sed, blandit non tortor. In efficitur magna ac nunc semper dignissim. Praesent ligula justo, semper et pharetra laoreet, cursus sed mauris. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Cras vitae ligula et turpis fermentum tincidunt sit amet vitae dui. Phasellus egestas suscipit justo, sit amet faucibus orci sollicitudin vitae. Cras at ligula et odio gravida euismod nec ut arcu. Nullam vitae nisl ex. In hac habitasse platea dictumst. Nunc congue porta malesuada. Phasellus blandit cursus nisi ut pellentesque.',
                    media: {
                      image: 'https://www.youtube.com/watch?v=dKrVegVI0Us',
                      type: 'video'
                    },
                    links: [ ]
                  }
                }, {
                  type: 'basic',
                  organization: organization.id,
                  dismissable: false,
                  icon: 'http://lorempixel.com/100/100/',
                  date: utcDate,
                  basic: {
                    title: 'Want to Login?',
                    links: [ {
                        title: 'Dismiss',
                        dest: '',
                        type: 'wrapped'
                      },
                      {
                        title: 'Login',
                        dest: '/login',
                        type: 'wrapped'
                      } ]
                  }
                }])
                .then((cards) => {
                  console.log('finished populating cards ', _.map(cards, function(card) { return card.id }));

                  Doormat.create({
                    card: cards[0],
                    image: 'http://lorempixel.com/400/300/',
                    heading: 'Heading of Doormat'
                  })
                  .then((doormat) => {
                    console.log('Doormat created', doormat.id);
                  })

                  Sidebar.create([
                    {
                      organization: organization.id,
                      title: 'Home',
                      type: 'internal',
                      link: '_home',
                      parent: null,
                      badge: 'none',
                      icon: 'home'
                    },
                    {
                      organization: organization.id,
                      title: "Favorites",
                      type: "internal",
                      link: "_favorites",
                      parent: "null",
                      badge: "none",
                      icon: "favorite"
                    },
                    {
                      organization: organization.id,
                      title: "Products",
                      type: "internal",
                      link: "_products",
                      parent: "null",
                      badge: "none",
                      icon: "products"
                    },
                    {
                      organization: organization.id,
                      title: "Category 1",
                      type: "product_category",
                      link: "_products.product_category1",
                      parent: "3",
                      badge: "1",
                      icon: "products"
                    },
                    {
                      organization: organization.id,
                      title: "Category 2",
                      type: "product_category",
                      link: "_products.product_category2",
                      parent: "3",
                      badge: "null",
                      icon: "products"
                    },
                    {
                      organization: organization.id,
                      title: "Category 3",
                      type: "product_category",
                      link: "_products.product_category3",
                      parent: "3",
                      badge: "null",
                      icon: "products"
                    },
                    {
                      organization: organization.id,
                      title: "Support",
                      type: "web",
                      link: "http://blah.com",
                      parent: "null",
                      badge: "null",
                      icon: "support"
                    }
                  ])
                  .then((sidebars) => {
                    console.log('finished populating sidebars ', _.map(sidebars, function(sidebar) { return sidebar.id }));
                  })

                });
              });
            });
          });
        });
      });
    });
  });
});