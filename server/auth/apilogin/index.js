'use strict';

import * as config from '../../config/environment';
import express from 'express';
import passport from 'passport';
import {signToken} from '../auth.service';
import * as PartnerCtrl from '../../api/partner/partner.controller';
import * as DeviceCtrl from '../../api/device/device.controller';
const ResponseHelpers = require('../../components/responseHelpers/responseHelpers');
const Promise = require('bluebird');
Promise.config({
  // Enables all warnings except forgotten return statements.
  warnings: {
    wForgottenReturn: false
  }
});

var router = express.Router({mergeParams: true});

function saveUserRefInDeviceLog(did, userId) {
  return new Promise((resolve, reject)=> {
    return DeviceCtrl.saveUserRef(did, userId)
      .then(saved=> {
        return resolve();
      })
      .catch(err=> {
        console.log('Error in apilogin.saveUserRefInDeviceLog 1');
        return reject(err);
      })
  });
}

router.post('/', function(req, res, next) {
  let authenticateStrategy = '';
  if (req.body.email) {
    req.body.email = (req.body.email).toLowerCase();
  }
  if (req.body.token && req.body.token.length && req.body.password && req.body.password.length) {
    //First Time Login with Email/Token
    authenticateStrategy = 'apiTokenLogin';
  } else if (!req.body.token && req.body.password && req.body.password.length) {
    authenticateStrategy = 'apiPasswordLogin';
  } else {
    console.log('Error in apilogin 1');
    let err = {type: 'MissingParams', code: 422, msg: 'Missing password/token'};
    console.log(err);
    return ResponseHelpers.respondWithCustomError(res, err);
  }

  passport.authenticate(authenticateStrategy, (err, user, info)=> {
    var error = err || info;
    if (error) {
      console.log('Error in apilogin 2');
      let err = {type: 'UNAUTHORIZED', code: 401, msg: error.message || 'Not Authorized'};
      console.log(error);
      return ResponseHelpers.respondWithCustomError(res, err);
    }
    if (!user) {
      console.log('Error in apilogin 3');
      let err = {type: 'UserNotFound', code: 404, msg: error.message || 'User Not Found'};
      console.log(err);
      return ResponseHelpers.respondWithCustomError(res, err);
    }
    var token = signToken(user._id, user.role, req.deviceId, req.orgId);
    let response = {token: token, provider: user.provider, name: user.name, email: user.email, onboard: user.onboard, type: user.type, _id: user._id, active: user.active, status: user.status, role: user.role};
    return saveUserRefInDeviceLog(req.did, user._id)
      .then(()=> {
        PartnerCtrl.getUsersPartner(user._id)
          .then(partner=> {
            response.partner = {_id: partner._id, name: partner.name};
            response.organization = req.params.orgid;
            response.upgrade = req.upgrade;
            if (req.upgrade == 'force') {
              let appLink = config.appLinkAndroid;
              if (req.dos && req.dos.toLowerCase() == 'ios') {
                appLink = config.appLinkIos;
              }
              let msg = {
                meta: {
                  code: 200,
                  action: "logout",
                  message: {type: 'info', title: 'Upgrade Available', text: 'Upgrade is available', link: appLink}
                },
                data: response
              };
              return ResponseHelpers.respondWithSpecialMsg(res, msg);
            } else {
              return ResponseHelpers.respondWithCustomResult(res, response, 200);
            }
          })
          .catch(err=> {
            console.log('Error in apilogin 4');
            console.log(err);
            return ResponseHelpers.respondWithCustomError(res, err);
          });
      })
      .catch(err=> {
        console.log('Error in apilogin 5');
        console.log(err);
        return ResponseHelpers.respondWithCustomError(res, err);
      });
  })(req, res, next);
});

export default router;
