"use strict";

import passport from 'passport';
import {Strategy as LocalStrategy} from 'passport-local';
import * as UserCtrl from '../../api/user/user.controller';

function emailTokenAuthenticate(req, User, email, activationToken, done) {
  User.findOne({
    email: email.toLowerCase()
  }).exec()
    .then(user => {
      if (!user) {
        return done(null, false, {
          message: 'This email is not registered.'
        });
      } else if (user.active && user.status == 'Accepted') {
        return done(null, false, {
          message: 'User is already an active user. Please use password instead of token to login.'
        });
      }
      else {
        user.authenticateEmailToken(activationToken, function(authError, authenticated) {
          if (authError) {
            return done(authError);
          }
          if (!authenticated) {
            return done(null, false, {message: 'Email/Token is not correct.'});
          } else {
            return UserCtrl.activateUser(user, req.body.password)
              .then(updatedUser=> {
                return done(null, updatedUser);
              })
              .catch(err=> {
                return done(err);
              });
          }
        });
      }
    })
    .catch(err => done(err));
}


function localAuthenticate(User, email, password, done) {
  User.findOne({
    email: email.toLowerCase()
  }).exec()
    .then(user => {
      if (!user) {
        return done(null, false, {
          message: 'This email is not registered.'
        });
      } else if (!user.active) {
        return done(null, false, {
          message: 'User is not an active user.'
        });
      } else if (user.status == 'Invited') {
        return done(null, false, {
          message: 'Please use token instead of password to activate.'
        });
      } else if (user.status == 'Removed') {
        return done(null, false, {
          message: 'User is Removed.'
        });
      }
      user.authenticate(password, function(authError, authenticated) {
        if (authError) {
          return done(authError);
        }
        if (!authenticated) {
          return done(null, false, {message: 'This password is not correct.'});
        } else {
          return done(null, user);
        }
      });
    })
    .catch(err => done(err));
}


export function setup(User, config) {
  passport.use('apiTokenLogin', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'token',
    passReqToCallback: true
  }, function(req, email, activationToken, done) {
    return emailTokenAuthenticate(req, User, email, activationToken, done);
  }));

  passport.use('apiPasswordLogin', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password'
  }, function(email, password, done) {
    return localAuthenticate(User, email, password, done);
  }));
}
