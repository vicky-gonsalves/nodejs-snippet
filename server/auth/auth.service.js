'use strict';

import _ from 'lodash';
import config from '../config/environment';
import jwt from 'jsonwebtoken';
import expressJwt from 'express-jwt';
import compose from 'composable-middleware';
import User from '../api/user/user.model';
import ApiKey from '../api/apiKey/apiKey.model';
const moment = require('moment');

var validateJwt = expressJwt({
  secret: config.secrets.session
});

/**
 * Attaches the user object to the request if authenticated
 * Otherwise returns 403
 */
export function isAuthenticated() {
  return compose()
  // Validate jwt
    .use(function(req, res, next) {
      if (req.hasApiAccess) {
        next();
      } else {
        // allow access_token to be passed through query parameter as well
        if (req.query && req.query.hasOwnProperty('access_token')) {
          req.headers.authorization = 'Bearer ' + req.query.access_token;
        }
        validateJwt(req, res, next);
      }
    })
    // Attach user to request
    .use(function(req, res, next) {
      if (req.hasApiAccess) {
        next();
      } else {
        if (req.user.deviceId) {
          req.deviceId = _.clone(req.user.deviceId);
        }
        if (req.user.orgId) {
          req.orgId = _.clone(req.user.orgId);
        }
        User.findById(req.user._id)
          .lean()
          .exec()
          .then(user => {
            if (!user) {
              return res.status(401).end();
            }
            req.user = user;
            next();
          })
          .catch(err => next(err));
      }
    });
}

/**
 * Attaches the user object to the request if authenticated
 * Otherwise skips auth
 */
export function isAuthenticatedSeamlessly() {
  return compose()
  // Validate jwt
    .use(function(req, res, next) {
      if (req.hasApiAccess) {
        next();
      } else {
        // allow access_token to be passed through query parameter as well
        if (req.query && req.query.hasOwnProperty('access_token')) {
          req.headers.authorization = 'Bearer ' + req.query.access_token;
        }
        if (req.headers.authorization) {
          validateJwt(req, res, next);
        } else {
          next();
        }
      }
    })
    // Attach user to request
    .use(function(req, res, next) {
      if (req.hasApiAccess) {
        next();
      } else {
        if (req.user && req.user.deviceId) {
          req.deviceId = _.clone(req.user.deviceId);
        }
        if (req.user && req.user.orgId) {
          req.orgId = _.clone(req.user.orgId);
        }
        if (req.user) {
          User.findById(req.user._id)
            .lean()
            .exec()
            .then(user => {
              if (!user) {
                return res.status(401).end();
              }
              req.user = user;
              next();
            })
            .catch(err => next(err));
        }
        else {
          next();
        }
      }
    });
}

export function hasKeySecretPair() {
  return compose()
    .use(function(req, res, next) {
      console.log(req.headers);
      if (req.headers && req.headers.hasOwnProperty('apikey') && req.headers.hasOwnProperty('secret') && req.params && req.params.hasOwnProperty('orgid')) {
        ApiKey.findOne({key: req.headers['apikey'], secret: req.headers['secret'], organization: req.params.orgid, expiry: {$gt: moment()}, active: true})
          .lean()
          .exec()
          .then(apiKey => {
            if (!apiKey) {
              return res.status(401).end();
            }
            req.apiKey = apiKey;
            req.hasApiAccess = true;
            next();
          })
          .catch(err => next(err));
      } else {
        next();
      }
    })
}

export function requireKeySecretPair() {
  return compose()
    .use(function(req, res, next) {
      console.log(req.headers);
      if (req.headers && req.headers.hasOwnProperty('apikey') && req.headers.hasOwnProperty('secret') && req.params && req.params.hasOwnProperty('orgid')) {
        ApiKey.findOne({key: req.headers['apikey'], secret: req.headers['secret'], organization: req.params.orgid, expiry: {$gt: moment()}, active: true})
          .lean()
          .exec()
          .then(apiKey => {
            if (!apiKey) {
              return res.status(401).end('Invalid Key/Secret Pair');
            }
            req.hasApiAccess = true;
            next();
          })
          .catch(err => next(err));
      } else {
        return res.status(401).end('Requires Key/Secret Pair');
      }
    })
}

/**
 * Checks if the user role meets the minimum requirements of the route
 */
export function hasRole(roleRequired) {
  if (!roleRequired) {
    throw new Error('Required role needs to be set');
  }

  return compose()
    .use(isAuthenticated())
    .use(function meetsRequirements(req, res, next) {
      if (req.hasApiAccess) {
        next();
      } else {
        if (config.userRoles.indexOf(req.user.role) >=
          config.userRoles.indexOf(roleRequired)) {
          next();
        } else {
          res.status(401).send('Unauthorized');
        }
      }
    });
}

export function hasRoles(rolesRequired) {
  if (!rolesRequired && !rolesRequired.length) {
    throw new Error('Required role needs to be set');
  }

  return compose()
    .use(isAuthenticated())
    .use(function meetsRequirements(req, res, next) {
      if (req.hasApiAccess) {
        next();
      } else {
        let count = 0;
        rolesRequired.forEach(role => {
          if (config.userRoles.indexOf(req.user.role) >=
            config.userRoles.indexOf(role)) {
            ++count;
          }
        });
        if (count > 0) {
          return next();
        } else {
          res.status(401).send('Unauthorized');
        }
      }
    });
}

/**
 * Returns a jwt token signed by the app secret
 */
export function signToken(id, role, deviceId, orgId) {
  return jwt.sign({_id: id, role: role, deviceId: deviceId, orgId: orgId}, config.secrets.session, {
    expiresIn: config.tokenExpiry
  });
}

/**
 * Set token cookie directly for oAuth strategies
 */
export function setTokenCookie(req, res) {
  if (!req.user) {
    return res.status(404).send('It looks like you aren\'t logged in, please try again.');
  }
  var token = signToken(req.user._id, req.user.role, req.deviceId, req.orgId);
  res.cookie('token', token);
  res.redirect('/');
}
