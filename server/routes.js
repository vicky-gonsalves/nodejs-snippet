/**
 * Main application routes
 */

'use strict';

import errors from './components/errors';
import path from 'path';

export default function(app) {
  // Insert routes below
  app.use('/register', require('./api/register'));
  app.use('/ents', require('./api/ents'));
  app.use('/api/v1/partner-stats', require('./api/partnerStats'));
  app.use('/api/notificationLogs', require('./api/notificationLog'));
  app.use('/url', require('./api/linkAnalytic'));
  app.use('/api/audienceLinks', require('./api/audienceLink'));
  app.use('/api/coreApps', require('./api/coreApp'));
  app.use('/api/organizationApps', require('./api/organizationApp'));
  app.use('/api/apiKeys', require('./api/apiKey'));
  app.use('/api/v1/pushkeys', require('./api/pushKeys'));
  app.use('/api/v1/qrcodes', require('./api/qrcode'));
  app.use('/api/v1/audiences', require('./api/audience'));
  app.use('/api/v1/notification-status', require('./api/notificationStatus'));
  app.use('/api/v1', require('./api/organization'));

  app.use('/api/v1/cards', require('./api/card'));
  app.use('/api/v1/sidebars', require('./api/sidebar'));
  app.use('/api/v1/partners', require('./api/partner'));
  app.use('/api/v1/devices', require('./api/device'));
  app.use('/api/v1/notifications', require('./api/notification'));
  app.use('/api/v1/logs', require('./api/log'));

  app.use('/api/v1/users', require('./api/user'));

  app.use('/api/v1/auth', require('./auth'));

  // All undefined asset or api routes should return a 404
  app.route('/:url(url|api|auth|components|app|bower_components|assets)/*')
    .get(errors[404]);

  // All other routes should redirect to the index.html
  app.route('/*')
    .get((req, res) => {
      res.sendFile(path.resolve(app.get('appPath') + '/index.html'));
    });
}
