'use strict';

import _ from 'lodash';
import PushKeys from './pushKeys.model';
const Promise = require('bluebird');
const validator = require('validator');
const moment = require('moment');
const ResponseHelpers = require('../../components/responseHelpers/responseHelpers');
Promise.config({
  // Enables all warnings except forgotten return statements.
  warnings: {
    wForgottenReturn: false
  }
});

function sanitizeEntities(data, toValidate = []) {
  return new Promise((resolve, reject)=> {
    try {
      let sanitizedData = JSON.parse(JSON.stringify(data));
      if (data.name && toValidate.indexOf('name') > -1) {
        sanitizedData.name = validator.trim(data.name);
      }
      if (data.type && toValidate.indexOf('type') > -1) {
        sanitizedData.type = validator.trim(data.type);
      }
      return resolve(sanitizedData);
    } catch (e) {
      console.log('Error in pushkeys.controller.sanitizeEntities 1');
      return reject(e);
    }
  })
}

function validateEntities(toValidate = [], update = false) {
  return function(data) {
    return new Promise((resolve, reject)=> {
      if (data) {
        let errors = {type: 'InvalidParams', code: 422, msg: []};
        //Validation
        if (toValidate.indexOf('name') > -1) {
          if (!update && (!(data.name) || validator.isNull(data.name))) {
            errors.msg.push({name: 'name is required'});
          } else if (data.name && !validator.isLength(data.name, {min: 1, max: 50})) {
            errors.msg.push({name: 'name must be between 1 to 50 characters long'});
          }
        }
        if (toValidate.indexOf('type') > -1) {
          if (!update && (!(data.type) || validator.isNull(data.type))) {
            errors.msg.push({type: 'type is required'});
          } else if (data.type && !validator.isIn(data.type, ['android', 'ios'])) {
            errors.msg.push({type: 'type must be either android or ios'});
          }
        }
        if (toValidate.indexOf('key') > -1) {
          if (!update && (!(data.key) || validator.isNull(data.key))) {
            errors.msg.push({key: 'key is required'});
          }
        }

        if (errors.msg.length > 0) {
          console.log('Error in pushkeys.controller.validateEntities 1');
          return reject(errors);
        } else {
          return resolve(data);
        }
      } else {
        console.log('Error in pushkeys.controller.validateEntities 2');
        let err = {type: 'EntityNotFound', code: 422, msg: 'Entity not found.'};
        return reject(err);
      }
    });
  }
}

function saveUpdates(updates) {
  return function(entity) {
    if ((updates.schedule == 'time' && !updates.scheduleTime && entity.scheduleTime && moment(entity.scheduleTime) < moment())) {
      console.log(moment(entity.scheduleTime) < moment());
      return new Promise((resolve, reject)=> {
        console.log('Error in pushkeys.controller.saveUpdates 1');
        let err = {type: 'InvalidEntity', code: 422, msg: 'Please set scheduleTime'};
        return reject(err);
      });
    } else {
      var updated = _.merge(entity, updates);
      updated.users = updates.users;
      updated.markModified('users');
      return updated.save()
        .then(updated => {
          return updated;
        });
    }
  };
}

function removeEntity() {
  return function(entity) {
    return new Promise((resolve, reject)=> {
      if (entity) {
        return entity.remove()
          .then(() => {
            return resolve();
          })
          .catch(err=> {
            console.log('Error in pushkeys.controller.removeEntity 1');
            return reject(err);
          })
      } else {
        console.log('Error in pushkeys.controller.removeEntity 2');
        return reject({type: 'EntityNotFound', code: 404, msg: 'Notification not found'});
      }
    });
  };
}


function checkIfExists(key, id = null) {
  return new Promise((resolve, reject)=> {
    let qry = {key: key};
    if (id) {
      qry = {
        key: key,
        _id: {
          $ne: id
        }
      };
    }
    return PushKeys.find(qry).count()
      .then(count=> {
        return resolve(count > 0);
      })
      .catch(err=> {
        console.log('Error in pushkeys.controller.checkIfExists 1');
        return reject(err);
      });
  })
}


// Gets a list of PushKeyss
export function index(req, res) {
  return PushKeys.find().exec()
    .then(ResponseHelpers.respondWithResult(res))
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Gets a single PushKeys from the DB
export function show(req, res) {
  return PushKeys.findById(req.params.id).exec()
    .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'PushKey not found.'}))
    .then(ResponseHelpers.respondWithResult(res))
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Creates a new PushKeys in the DB
export function create(req, res) {
  let toValidate = ['name', 'type', 'key'];

  return sanitizeEntities(req.body, toValidate)
    .then(validateEntities(toValidate))
    .then(data=> {
      return checkIfExists(data.key)
        .then(exists=> {
          if (!exists) {
            return PushKeys.create(data)
              .then(ResponseHelpers.respondWithResult(res, 201))
              .catch(ResponseHelpers.respondWithErrorEntity(res));
          } else {
            let err = {type: 'AlreadyExists', code: 409, msg: 'Key already exists.'};
            return ResponseHelpers.respondWithCustomError(res, err);
          }
        })
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Updates an existing PushKeys in the DB
export function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  req.body.updatedAt = moment();

  let toValidate = ['name', 'type', 'key'];
  return sanitizeEntities(req.body, toValidate)
    .then(validateEntities(toValidate, true))
    .then(data=> {
      return checkIfExists(data.key, req.params.id)
        .then(exists=> {
          if (!exists) {
            return PushKeys.findById(req.params.id).exec()
              .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'PushKey not found.'}))
              .then(saveUpdates(data))
              .then(ResponseHelpers.respondWithResult(res))
              .catch(ResponseHelpers.respondWithErrorEntity(res));
          } else {
            let err = {type: 'AlreadyExists', code: 409, msg: 'Key already exists.'};
            return ResponseHelpers.respondWithCustomError(res, err);
          }
        })
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Deletes a PushKeys from the DB
export function destroy(req, res) {
  return PushKeys.findById(req.params.id).exec()
    .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'PushKey not found.'}))
    .then(removeEntity(res))
    .then(ResponseHelpers.respondWithResult(res, 204))
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}
