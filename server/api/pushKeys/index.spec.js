'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var pushKeysCtrlStub = {
  index: 'pushKeysCtrl.index',
  show: 'pushKeysCtrl.show',
  create: 'pushKeysCtrl.create',
  upsert: 'pushKeysCtrl.upsert',
  patch: 'pushKeysCtrl.patch',
  destroy: 'pushKeysCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var pushKeysIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './pushKeys.controller': pushKeysCtrlStub
});

describe('PushKeys API Router:', function() {
  it('should return an express router instance', function() {
    pushKeysIndex.should.equal(routerStub);
  });

  describe('GET /api/v1/pushkeys', function() {
    it('should route to pushKeys.controller.index', function() {
      routerStub.get
        .withArgs('/', 'pushKeysCtrl.index')
        .should.have.been.calledOnce;
    });
  });

  describe('GET /api/v1/pushkeys/:id', function() {
    it('should route to pushKeys.controller.show', function() {
      routerStub.get
        .withArgs('/:id', 'pushKeysCtrl.show')
        .should.have.been.calledOnce;
    });
  });

  describe('POST /api/v1/pushkeys', function() {
    it('should route to pushKeys.controller.create', function() {
      routerStub.post
        .withArgs('/', 'pushKeysCtrl.create')
        .should.have.been.calledOnce;
    });
  });

  describe('PUT /api/v1/pushkeys/:id', function() {
    it('should route to pushKeys.controller.upsert', function() {
      routerStub.put
        .withArgs('/:id', 'pushKeysCtrl.upsert')
        .should.have.been.calledOnce;
    });
  });

  describe('PATCH /api/v1/pushkeys/:id', function() {
    it('should route to pushKeys.controller.patch', function() {
      routerStub.patch
        .withArgs('/:id', 'pushKeysCtrl.patch')
        .should.have.been.calledOnce;
    });
  });

  describe('DELETE /api/v1/pushkeys/:id', function() {
    it('should route to pushKeys.controller.destroy', function() {
      routerStub.delete
        .withArgs('/:id', 'pushKeysCtrl.destroy')
        .should.have.been.calledOnce;
    });
  });
});
