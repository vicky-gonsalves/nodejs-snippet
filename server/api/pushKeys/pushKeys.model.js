'use strict';

import mongoose from 'mongoose';

var PushKeysSchema = new mongoose.Schema({
  name: String,
  type: {
    type: String,
    required: true
  },
  key: {
    type: String,
    required: true
  },
  active: {
    type: Boolean,
    default: true
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: {
    type: Date
  }
});

export default mongoose.model('PushKeys', PushKeysSchema);
