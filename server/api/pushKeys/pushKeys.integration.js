'use strict';

var app = require('../..');
import request from 'supertest';

var newPushKeys;

describe('PushKeys API:', function() {
  describe('GET /api/v1/pushkeys', function() {
    var pushKeyss;

    beforeEach(function(done) {
      request(app)
        .get('/api/v1/pushkeys')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          pushKeyss = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      pushKeyss.should.be.instanceOf(Array);
    });
  });

  describe('POST /api/v1/pushkeys', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/v1/pushkeys')
        .send({
          name: 'New PushKeys',
          info: 'This is the brand new pushKeys!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newPushKeys = res.body;
          done();
        });
    });

    it('should respond with the newly created pushKeys', function() {
      newPushKeys.name.should.equal('New PushKeys');
      newPushKeys.info.should.equal('This is the brand new pushKeys!!!');
    });
  });

  describe('GET /api/v1/pushkeys/:id', function() {
    var pushKeys;

    beforeEach(function(done) {
      request(app)
        .get(`/api/v1/pushkeys/${newPushKeys._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          pushKeys = res.body;
          done();
        });
    });

    afterEach(function() {
      pushKeys = {};
    });

    it('should respond with the requested pushKeys', function() {
      pushKeys.name.should.equal('New PushKeys');
      pushKeys.info.should.equal('This is the brand new pushKeys!!!');
    });
  });

  describe('PUT /api/v1/pushkeys/:id', function() {
    var updatedPushKeys;

    beforeEach(function(done) {
      request(app)
        .put(`/api/v1/pushkeys/${newPushKeys._id}`)
        .send({
          name: 'Updated PushKeys',
          info: 'This is the updated pushKeys!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedPushKeys = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedPushKeys = {};
    });

    it('should respond with the original pushKeys', function() {
      updatedPushKeys.name.should.equal('New PushKeys');
      updatedPushKeys.info.should.equal('This is the brand new pushKeys!!!');
    });

    it('should respond with the updated pushKeys on a subsequent GET', function(done) {
      request(app)
        .get(`/api/v1/pushkeys/${newPushKeys._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let pushKeys = res.body;

          pushKeys.name.should.equal('Updated PushKeys');
          pushKeys.info.should.equal('This is the updated pushKeys!!!');

          done();
        });
    });
  });

  describe('PATCH /api/v1/pushkeys/:id', function() {
    var patchedPushKeys;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/v1/pushkeys/${newPushKeys._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched PushKeys' },
          { op: 'replace', path: '/info', value: 'This is the patched pushKeys!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedPushKeys = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedPushKeys = {};
    });

    it('should respond with the patched pushKeys', function() {
      patchedPushKeys.name.should.equal('Patched PushKeys');
      patchedPushKeys.info.should.equal('This is the patched pushKeys!!!');
    });
  });

  describe('DELETE /api/v1/pushkeys/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/v1/pushkeys/${newPushKeys._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when pushKeys does not exist', function(done) {
      request(app)
        .delete(`/api/v1/pushkeys/${newPushKeys._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
