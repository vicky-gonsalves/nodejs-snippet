/**
 * PushKeys model events
 */

'use strict';

import {EventEmitter} from 'events';
import PushKeys from './pushKeys.model';
var PushKeysEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
PushKeysEvents.setMaxListeners(0);

// Model events
var events = {
  save: 'save',
  remove: 'remove'
};

// Register the event emitter to the model events
for(var e in events) {
  let event = events[e];
  PushKeys.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    PushKeysEvents.emit(event + ':' + doc._id, doc);
    PushKeysEvents.emit(event, doc);
  };
}

export default PushKeysEvents;
