'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var sidebarCtrlStub = {
  index: 'sidebarCtrl.index',
  show: 'sidebarCtrl.show',
  create: 'sidebarCtrl.create',
  update: 'sidebarCtrl.update',
  destroy: 'sidebarCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var sidebarIndex = proxyquire('./index.js', {
  'express': {
    Router: function() {
      return routerStub;
    }
  },
  './sidebar.controller': sidebarCtrlStub
});

describe('Sidebar API Router:', function() {

  it('should return an express router instance', function() {
    sidebarIndex.should.equal(routerStub);
  });

  describe('GET /api/sidebars', function() {

    it('should route to sidebar.controller.index', function() {
      routerStub.get
        .withArgs('/', 'sidebarCtrl.index')
        .should.have.been.calledOnce;
    });

  });

  describe('GET /api/sidebars/:id', function() {

    it('should route to sidebar.controller.show', function() {
      routerStub.get
        .withArgs('/:id', 'sidebarCtrl.show')
        .should.have.been.calledOnce;
    });

  });

  describe('POST /api/sidebars', function() {

    it('should route to sidebar.controller.create', function() {
      routerStub.post
        .withArgs('/', 'sidebarCtrl.create')
        .should.have.been.calledOnce;
    });

  });

  describe('PUT /api/sidebars/:id', function() {

    it('should route to sidebar.controller.update', function() {
      routerStub.put
        .withArgs('/:id', 'sidebarCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('PATCH /api/sidebars/:id', function() {

    it('should route to sidebar.controller.update', function() {
      routerStub.patch
        .withArgs('/:id', 'sidebarCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('DELETE /api/sidebars/:id', function() {

    it('should route to sidebar.controller.destroy', function() {
      routerStub.delete
        .withArgs('/:id', 'sidebarCtrl.destroy')
        .should.have.been.calledOnce;
    });

  });

});
