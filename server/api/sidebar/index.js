'use strict';

var express = require('express');
var controller = require('./sidebar.controller');
import * as logger from '../../components/logger.service';
import * as auth from '../../auth/auth.service';

var router = express.Router();

router.get('/', logger.putOtherLog(), controller.index);
router.get('/:id', logger.putOtherLog(), controller.show);
router.post('/', logger.putOtherLog(), controller.create);
router.put('/:id', logger.putOtherLog(), controller.update);
router.patch('/:id', logger.putOtherLog(), controller.update);
router.delete('/:id', logger.putOtherLog(), controller.destroy);

module.exports = router;
