'use strict';

import mongoose from 'mongoose';
mongoose.Promise = require('bluebird');
import {Schema} from 'mongoose';

var SidebarSchema = new mongoose.Schema({
  title: String,
  type: String,
  icon: String,
  dest: String,
  children: [{}],
  __v: {type: Number, select: false},
  active: {type: Boolean, default: true, select: false},
  createdAt: {
    type: Date,
    default: Date.now
  }
});

export default mongoose.model('Sidebar', SidebarSchema);
