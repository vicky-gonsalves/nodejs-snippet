'use strict';

var app = require('../..');
import request from 'supertest';

var newSidebar;

describe('Sidebar API:', function() {

  describe('GET /api/sidebars', function() {
    var sidebars;

    beforeEach(function(done) {
      request(app)
        .get('/api/sidebars')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          sidebars = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      sidebars.should.be.instanceOf(Array);
    });

  });

  describe('POST /api/sidebars', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/sidebars')
        .send({
          name: 'New Sidebar',
          info: 'This is the brand new sidebar!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newSidebar = res.body;
          done();
        });
    });

    it('should respond with the newly created sidebar', function() {
      newSidebar.name.should.equal('New Sidebar');
      newSidebar.info.should.equal('This is the brand new sidebar!!!');
    });

  });

  describe('GET /api/sidebars/:id', function() {
    var sidebar;

    beforeEach(function(done) {
      request(app)
        .get('/api/sidebars/' + newSidebar._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          sidebar = res.body;
          done();
        });
    });

    afterEach(function() {
      sidebar = {};
    });

    it('should respond with the requested sidebar', function() {
      sidebar.name.should.equal('New Sidebar');
      sidebar.info.should.equal('This is the brand new sidebar!!!');
    });

  });

  describe('PUT /api/sidebars/:id', function() {
    var updatedSidebar;

    beforeEach(function(done) {
      request(app)
        .put('/api/sidebars/' + newSidebar._id)
        .send({
          name: 'Updated Sidebar',
          info: 'This is the updated sidebar!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedSidebar = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedSidebar = {};
    });

    it('should respond with the updated sidebar', function() {
      updatedSidebar.name.should.equal('Updated Sidebar');
      updatedSidebar.info.should.equal('This is the updated sidebar!!!');
    });

  });

  describe('DELETE /api/sidebars/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/api/sidebars/' + newSidebar._id)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when sidebar does not exist', function(done) {
      request(app)
        .delete('/api/sidebars/' + newSidebar._id)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
