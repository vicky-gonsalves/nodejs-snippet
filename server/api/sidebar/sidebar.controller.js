'use strict';

import _ from 'lodash';
import Sidebar from './sidebar.model';
const validator = require('validator');
const Promise = require('bluebird');
const ResponseHelpers = require('../../components/responseHelpers/responseHelpers');
Promise.config({
  // Enables all warnings except forgotten return statements.
  warnings: {
    wForgottenReturn: false
  }
});

function saveUpdates(updates) {
  return function(entity) {
    var updated = _.merge(entity, updates);
    updated.markModified('children');
    return updated.save()
      .then(updated => {
        return updated;
      });
  };
}

function removeEntity() {
  return function(entity) {
    return new Promise((resolve, reject)=> {
      if (entity) {
        return entity.remove()
          .then(() => {
            return resolve();
          })
          .catch(err=> {
            console.log('Error in sidebar.controller.removeEntity 1');
            return reject(err);
          })
      } else {
        console.log('Error in sidebar.controller.removeEntity 2');
        return reject({type: 'EntityNotFound', code: 404, msg: 'Sidebar not found'});
      }
    });
  };
}


// Gets a list of Sidebars
export function index(req, res) {
  return Sidebar.find().exec()
    .then(ResponseHelpers.respondWithResult(res))
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Gets a single Sidebar from the DB
export function show(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.id, type: 'sidebar'}])
    .then(()=> {
      return Sidebar.findById(req.params.id).exec()
        .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Sidebar not found.'}))
        .then(ResponseHelpers.respondWithResult(res))
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Creates a new Sidebar in the DB
export function create(req, res) {
  return Sidebar.create(req.body)
    .then(ResponseHelpers.respondWithResult(res, 201))
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Updates an existing Sidebar in the DB
export function update(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.id, type: 'sidebar'}])
    .then(()=> {
      if (req.body._id) {
        delete req.body._id;
      }
      return Sidebar.findById(req.params.id).exec()
        .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Sidebar not found.'}))
        .then(saveUpdates(req.body))
        .then(ResponseHelpers.respondWithResult(res))
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Deletes a Sidebar from the DB
export function destroy(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.id, type: 'sidebar'}])
    .then(()=> {
      return Sidebar.findById(req.params.id).exec()
        .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Sidebar not found.'}))
        .then(removeEntity(res))
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Creates a new Sidebar in the DB and returns it
export function createNewSideBar(data) {
  return new Promise((resolve, reject)=> {
    if (data) {
      return Sidebar.create(data)
        .then(item => {
          return resolve(item);
        })
        .catch(err=> {
          console.log('Error in sidebar.controller.createNewSideBar 1');
          return reject(err);
        })
    } else {
      console.log('Error in sidebar.controller.createNewSideBar 2');
      return reject({type: 'MissingParams', code: 422, msg: 'Data is required'});
    }
  });
}

// Updates a Sidebar in the DB and returns it
export function updateSideBar(id, data) {
  return new Promise((resolve, reject)=> {
    if (!id) {
      console.log('Error in sidebar.controller.updateSideBar 1');
      return reject({type: 'MissingParams', code: 422, msg: 'Sidebar id not found'});
    }
    if (data) {
      return Sidebar.findById(id)
        .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Sidebar not found.'}))
        .then(saveUpdates(data))
        .then(item => {
          return resolve(item);
        })
        .catch(err=> {
          console.log('Error in sidebar.controller.updateSideBar 2');
          return reject(err);
        })
    } else {
      console.log('Error in sidebar.controller.updateSideBar 3');
      return reject({type: 'MissingParams', code: 422, msg: 'Data is required'});
    }
  });
}

// Gets a Sidebar
export function getSideBar(id) {
  return new Promise((resolve, reject)=> {
    if (id) {
      return Sidebar.findById(id)
        .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Sidebar not found.'}))
        .then(sidebar => {
          return resolve(sidebar);
        })
        .catch(err=> {
          console.log('Error in sidebar.controller.getSideBar 2');
          return reject(err);
        })
    } else {
      console.log('Error in sidebar.controller.getSideBar 2');
      return reject({type: 'MissingParams', code: 422, msg: 'Sidebar id missing'});
    }
  });
}
