'use strict';

import _ from 'lodash';
import Card from './card.model';
import * as PartnerCtrl from '../partner/partner.controller';
import * as OrgCtrl from '../organization/organization.controller';
import * as DeviceCtrl from '../device/device.controller';
import * as UserCtrl from '../user/user.controller';
import * as NotificationCtrl from '../notification/notification.controller';
import * as GCMCtrl from '../../components/gcm/gcm.controller';
import * as ES from '../../components/elasticSearch/es';
import * as config from "../../config/environment";
const cloudinary = require('cloudinary');
const async = require('async');
const Promise = require('bluebird');
const validator = require('validator');
const moment = require('moment');
const ResponseHelpers = require('../../components/responseHelpers/responseHelpers');
Promise.config({
  // Enables all warnings except forgotten return statements.
  warnings: {
    wForgottenReturn: false
  }
});

cloudinary.config({
  cloud_name: config.cloudinary.cloud_name,
  api_key: config.cloudinary.api_key,
  api_secret: config.cloudinary.api_secret
});

//Organization Specific Keys  -- Configure these constants in order to allow organization specific push keys and then setup orgMappings
const useOrgSpecificKeysForGCM = config.useOrgSpecificKeyAndroid == true || config.useOrgSpecificKeyAndroid == 'true';  //For GCM Only
const useOrgSpecificKeysForAPN = config.useOrgSpecificKeyiOs == true || config.useOrgSpecificKeyiOs == 'true';  //For APN Only

function sanitizeEntities(data, toValidate = []) {
  return new Promise((resolve, reject) => {
    try {
      let sanitizedData = JSON.parse(JSON.stringify(data));
      if (data.title && toValidate.indexOf('title') > -1) {
        sanitizedData.title = validator.trim(data.title);
      }
      if (data.status && toValidate.indexOf('status') > -1) {
        sanitizedData.status = validator.trim(data.status);
      }
      if (data.type && toValidate.indexOf('type') > -1) {
        sanitizedData.type = validator.trim(data.type);
      }
      if (data.format && toValidate.indexOf('format') > -1) {
        sanitizedData.format = validator.trim(data.format);
      }
      if (data.visibility && data.visibility.audiences && toValidate.indexOf('visibility.audiences') > -1) {
        sanitizedData.visibility.audiences = data.visibility.audiences;
      }

      if (sanitizedData && sanitizedData.data && sanitizedData.data.links && sanitizedData.data.links.length) {
        sanitizedData.data.links.forEach(link => {
          if (link.type && (link.type == '$chat' || link.type == '$share')) {
            link.dest = 'http://google.com';
          }
        });
      }

      return resolve(sanitizedData);
    } catch (e) {
      console.log('Error in card.controller.sanitizeEntities 1');
      return reject(e);
    }
  })
}

function validateEntities(toValidate = [], update = false) {
  return function(data) {
    return new Promise((resolve, reject) => {
      if (data) {
        let errors = {type: 'InvalidParams', code: 422, msg: []};
        //Validation
        if (toValidate.indexOf('title') > -1) {
          if (!update && (!(data.title) || validator.isNull(data.title))) {
            errors.msg.push({title: 'title is required'});
          } else if (data.title && !validator.isLength(data.title, {min: 1, max: 155})) {
            errors.msg.push({title: 'title must be between 1 to 155 characters long'});
          }
        }
        if (toValidate.indexOf('status') > -1) {
          if (data.status && !validator.isIn(data.status, ['draft', 'published', 'pushed', 'deleted'])) {
            errors.msg.push({status: 'Invalid status'});
          }
        }

        if (toValidate.indexOf('type') > -1) {
          if (!update && (!(data.type) || validator.isNull(data.type))) {
            errors.msg.push({type: 'type is required'});
          } else if (data.type && !validator.isIn(data.type, ['story', 'basic'])) {
            errors.msg.push({type: 'Invalid type'});
          }
        }

        if (toValidate.indexOf('format') > -1) {
          if (!update && (!(data.format) || validator.isNull(data.format))) {
            errors.msg.push({format: 'format is required'});
          } else if (data.format && !validator.isLength(data.format, {min: 1, max: 15})) {
            errors.msg.push({format: 'format must be between 1 to 15 characters long'});
          }
        }

        if (toValidate.indexOf('visibility.audiences') > -1) {
          if (data.visibility && !data.visibility.audiences) {
            errors.msg.push({visibility: `Missing audiences`});
          } else if (data.visibility && data.visibility.audiences && !_.isArray(data.visibility.audiences)) {
            errors.msg.push({audiences: `Invalid audiences. It must be an array of audiences ids.`});
          } else if (data.visibility && data.visibility.audiences && _.isArray(data.visibility.audiences)) {
            data.visibility.audiences.forEach(audience => {
              audience = audience ? audience.toString() : '';
              if (_.isObject(audience)) {
                errors.msg.push({audiences: `Invalid audiences. It must be an array of audiences ids.`});
              } else if (validator.isNull(audience)) {
                errors.msg.push({audiences: `Invalid audiences. It must be an array of audiences ids.`});
              } else if (!validator.isMongoId(audience)) {
                errors.msg.push({audiences: `${audience} audience id is not valid`});
              }
            })
          } else {
            if (data.visibility && data.visibility.audiences && data.visibility.audiences.length) {
              data.visibility.audiences.forEach(audience => {
                audience = audience ? audience.toString() : '';
                if (!validator.isMongoId(audience)) {
                  errors.msg.push({audiences: `${audience} audience id is not valid`});
                }
              });
            }
          }
        }

        if (toValidate.indexOf('data.icon') > -1) {
          if (data.data && data.data.icon && !validator.isURL(data.data.icon) && !validator.matches(data.data.icon, '\{\{(.*?)\}\}')) {
            errors.msg.push({icon: 'icon must be a valid URL'});
          }
        }

        if (toValidate.indexOf('data.media.data.url') > -1) {
          if (data && data.data && data.data.media && data.data.media.data && data.data.media.data.url && !validator.matches(data.data.media.data.url, 'market:([^\?]*)') && !validator.matches(data.data.media.data.url, 'mailto:([^\?]*)') && !validator.matches(data.data.media.data.url, '\{\{(.*?)\}\}') && !validator.isURL(data.data.media.data.url)) {
            errors.msg.push({mediaUrl: 'media url must be a valid URL'});
          }
        }

        if (toValidate.indexOf('data.actionlink.dest') > -1) {
          if (data && data.data && data.data.actionlink && data.data.actionlink.dest && !validator.matches(data.data.actionlink.dest, 'market:([^\?]*)') && !validator.matches(data.data.actionlink.dest, 'mailto:([^\?]*)') && !validator.matches(data.data.actionlink.dest, '\{\{(.*?)\}\}') && !validator.isURL(data.data.actionlink.dest)) {
            errors.msg.push({actionlinkDest: 'actionlink dest url must be a valid URL'});
          }
        }

        if (toValidate.indexOf('data.links.dest') > -1) {
          if (data && data.data && data.data.links && data.data.links.length) {
            data.data.links.forEach(link => {
              if ((!link.type || (link.type && link.type != '$chat')) && !validator.matches(link.dest, 'market:([^\?]*)') && !validator.matches(link.dest, 'mailto:([^\?]*)') && !validator.matches(link.dest, '\{\{(.*?)\}\}') && !validator.isURL(link.dest)) {
                errors.msg.push({linkUrl: 'link urls must be valid URLs'});
              }
            });
          }
        }

        if (errors.msg.length > 0) {
          console.log('Error in card.controller.validateEntities 1');
          return reject(errors);
        } else {
          return resolve(data);
        }
      } else {
        console.log('Error in card.controller.validateEntities 2');
        let err = {type: 'EntityNotFound', code: 422, msg: 'Entity not found.'};
        return reject(err);
      }
    });
  }
}

function saveUpdates(updates, patch = false) {
  return function(entity) {
    //TODO keep an eye here maybe deleting date_published
    // if (entity.date_published < moment()) {
    //   delete updates.date_published;
    // }
    if (updates.visibility && updates.visibility.audiences) {
      entity.visibility.audiences = updates.visibility.audiences;
    }
    var updated = _.merge(entity, updates, (a, b) => {
      if (_.isArray(a)) {
        return b;
      }
    });
    updated.markModified('visibility.audiences');
    updated.markModified('data');
    updated.markModified('data.links');
    updated.markModified('data.title');
    updated.markModified('data.description');
    updated.markModified('data.icon');
    updated.markModified('data.actionlink');

    if (!patch) {
      console.log(updates.data.links);
      updated.data.links = updates.data.links;
      updated.markModified('data.links');
    }

    return updated.save()
      .then(updated => {
        return Card.findById(entity._id)
          .deepPopulate('organization organization.androidKey organization.iosKey visibility.audiences')
          .exec()
          .then(card => {
            return card;
          })
          .catch(err => {
            console.log('Error in card.controller.saveUpdates 1');
            return err;
          })
      });
  };
}

function saveTags(updates) {
  return function(entity) {
    if (updates.tags) {
      /*Push*/
      // updates.tags.forEach(tag=> {
      //   if (entity.tags.indexOf(tag) < 0) {
      //     entity.tags.push(tag);
      //   }
      // });

      /*Force full update*/
      entity.tags = _.clone(updates.tags);
      if (!updates.tags.length) {
        entity.tags.length = 0;
      }
      entity.markModified('tags');
      return entity.save()
        .then(updated => {
          return entity;
        });
    } else {
      return entity;
    }
  }
}

function removeEntity() {
  return function(entity) {
    return new Promise((resolve, reject) => {
      if (entity) {
        return entity.remove()
          .then(() => {
            return resolve();
          })
          .catch(err => {
            console.log('Error in card.controller.removeEntity 1');
            return reject(err);
          })
      } else {
        console.log('Error in card.controller.removeEntity 2');
        return reject({type: 'EntityNotFound', code: 404, msg: 'Organization/Card not found'});
      }
    });
  };
}

function removeAllEntities() {
  return function(entities) {
    return new Promise((resolve, reject) => {
      return async.mapSeries(entities, function(entity, cb) {
        if (entity) {
          return entity.remove()
            .then(() => {
              cb();
            })
            .catch(err => {
              console.log('Error in card.controller.removeAllEntities 1');
              cb(err);
            })
        } else {
          console.log('Error in card.controller.removeAllEntities 2');
          cb({type: 'EntityNotFound', code: 404, msg: 'Card not found'});
        }
      }, function(err, done) {
        if (err) {
          console.log('Error in card.controller.removeAllEntities 3');
          return reject(err);
        } else {
          return resolve(done);
        }
      });
    });
  };
}

function processForPlaceholders(last) {
  return function(cards) {
    return new Promise((resolve, reject) => {
      try {
        // if (!last) {
        let dataWithPlaceholders = cards.map(card => {
          if (card.status != 'draft' && card.date_published) {
            return card;
          }
          return {status: 'unpublished'}
        });
        return resolve(dataWithPlaceholders.reverse());
        // } else {
        //   let dataWithPlaceholders = cards.filter(card=> {
        //     return card.status == 'published';
        //   });
        //   return resolve(dataWithPlaceholders);
        // }
      } catch (e) {
        console.log('Error in card.controller.processForPlaceholders 1');
        return reject({type: 'InternalError', code: 500, msg: 'Internal Error'});
      }
    });
  }
}


function attachOrgToCard(org) {
  return function(card) {
    return new Promise((resolve, reject) => {
      if (card) {
        let _card = JSON.parse(JSON.stringify(card));
        _card.organization = org;
        return resolve(_card);
      } else {
        console.log('Error in card.controller.attachOrgToCard 2');
        return reject({type: 'EntityNotFound', code: 404, msg: 'card not found'});
      }
    });
  }
}


function getAllPartnersHavingOrgId(orgId) {
  return new Promise((resolve, reject) => {
    if (orgId) {
      return PartnerCtrl.getAllUsersOfPartner(orgId)
        .then(partners => {
          return resolve(partners);
        })
        .catch(err => {
          console.log('Error in card.controller.getAllPartnersHavingOrgId 1');
          return reject(err);
        })
    } else {
      console.log('Error in card.controller.getAllPartnersHavingOrgId 2');
      return reject({type: 'EntityNotFound', code: 404, msg: 'orgId not found'});
    }
  });
}

export function getUniqueArrayOfUsers() {
  return function(partners) {
    return new Promise((resolve, reject) => {
      if (partners && partners.length) {
        let allUsers = _.uniq(_.flatten(partners.map(partner => {
          if (partner.allUsers && partner.allUsers.length) {
            return partner.allUsers;
          }
          return [];
        })));

        //Skip user type Apple
        return UserCtrl.getUsersByIds(allUsers)
          .then(usrs => {
            return resolve(usrs.map(_u => _u._id));
          })
          .catch(err => {
            console.log('Error in card.controller.getUniqueArrayOfUsers 1');
            return reject(err);
          })
      } else {
        return resolve([]);
      }
    })
  }
}

function getAllDevicesOfUsers(users) {
  return new Promise((resolve, reject) => {
    if (users && users.length) {
      return DeviceCtrl.getUsersIn(users)
        .then(devices => {
          return resolve(devices);
        })
        .catch(err => {
          console.log('Error in card.controller.getAllDevicesOfUseers 1');
          return reject(err);
        })
    } else {
      return resolve([]);
    }
  })
}

function getAllUniqueDevices() {
  return function(devices) {
    return new Promise((resolve, reject) => {
      if (devices && devices.length) {
        let allUniqueDevices = _.uniqBy(devices.filter(device => device.dpushtoken && device.dpushtoken.length), 'dpushtoken'); //get all devices with unique tokens
        return resolve(allUniqueDevices);
      } else {
        return resolve([]);
      }
    })
  }
}

function saveNotificationEntity(card, users, schedule, status, devices) {
  return new Promise((resolve, reject) => {
    if (card && users && schedule && status && devices) {
      let deviceIds = devices.map(device => device._id);
      return NotificationCtrl.saveNotification({card: card._id, users: users, schedule: schedule, status: status, recipients: deviceIds, data: card})
        .then(notification => {
          return resolve(notification);
        })
        .catch(err => {
          console.log('Error in card.controller.saveNotificationEntity 1');
          return reject(err);
        })
    } else {
      console.log('Error in card.controller.saveNotificationEntity 1');
      return reject({type: 'EntityNotFound', code: 404, msg: 'card/users/schedule/status not found'});
    }
  })
}

function sendNotification(options) {
  return new Promise((resolve, reject) => {
    if (options) {
      return GCMCtrl.sendNotification(options)
        .then(gcmResponse => {
          return resolve(gcmResponse);
        })
        .catch(err => {
          console.log('Error in card.controller.sendNotification 1');
          return reject(err);
        })
    } else {
      console.log('Error in card.controller.sendNotification 2');
      return reject({type: 'EntityNotFound', code: 404, msg: 'options not found'});
    }
  })
}

function getAllPartnersFromAudiences(queries, esIndex) {
  return new Promise((resolve, reject) => {
    if (esIndex && queries && queries.length) {
      return async.mapSeries(queries, function(query, cb) {
        return ES.perFormEsSearchByEsIndex(query.filter, esIndex)
          .then(result => {
            if (!result) {
              cb({type: 'ESTimedOut', code: 408, msg: 'ElasticSearch Timed Out'});
            } else if (result && !result.error) {
              cb(null, result);
            } else if (result && result.error) {
              cb({type: result.error.type, code: result.status, msg: result.error});
            } else {
              cb({type: 'InvalidFilter', code: 400, msg: 'Filter is invalid'});
            }
          })
          .catch(err => {
            console.log('Error in card.controller.getAllPartnersFromAudiences 1');
            cb(err);
          })
      }, function(err, allSearches) {
        if (err) {
          console.log('Error in card.controller.getAllPartnersFromAudiences 2');
          return reject(err);
        } else {
          console.log(JSON.stringify(allSearches));
          return resolve(allSearches);
        }
      })
    } else {
      return resolve([]);
    }
  })
}

function getDirectPartners(orgId, audience) {
  return function(result) {
    return new Promise((resolve, reject) => {
      if (result && result.skipped && orgId && !audience.length) {
        return getAllPartnersHavingOrgId(orgId)
          .then(response => {
            return resolve(response);
          })
          .catch(err => {
            console.log('Error in card.controller.getDirectPartners 1');
            return reject(err);
          })
      } else {
        return resolve(result);
      }
    })
  }
}

function getPartnersByPartnerIds(partners) {
  return new Promise((resolve, reject) => {
    if (partners && partners.length) {
      return PartnerCtrl.getPartnersByPartnerIds(partners)
        .then(_partners => {
          return resolve(_partners);
        })
        .catch(err => {
          console.log('Error in card.controller.getPartnersByPartnerIds 1');
          cb(err);
        })
    } else {
      return resolve({skipped: true});
    }
  });
}

function sendNotificationIfApplies(data, orgId) {
  return function(_card) {
    let card = JSON.parse(JSON.stringify(_card));
    if (card.data && card.data.media && card.data.media.data && card.data.media.data.url) {
      if (card.data.media.data.url.indexOf('res.cloudinary.com') > -1) {
        let split = card.data.media.data.url.split("/");
        let file = split[split.length - 2] + '/' + split[split.length - 1].split(".").shift();
        console.log('Parsing Thumbnail: ' + card.data.media.data.url);
        card.data.media.data.thumbnail_url = cloudinary.url(file, {format: "png", gravity: "north", height: 180, width: 320, crop: "fill"});
        console.log('Parsed Thumbnail: ' + card.data.media.data.thumbnail_url);
      } else {
        card.data.media.data.thumbnail_url = cloudinary.url(card.data.media.data.url, {fetch_format: "png", gravity: "north", height: 180, width: 320, crop: "fill", type: "fetch"});
        console.log('Parsed Thumbnail: ' + card.data.media.data.thumbnail_url);
      }
    }
    return new Promise((resolve, reject) => {
      console.log(card);
      console.log(data);
      console.log(orgId);
      console.log(card.organization);
      console.log(card.organization.es_index);
      console.log(card.visibility);
      console.log(card.visibility.audiences);
      let shdSendPush = true;
      //Skip card if card contains exclude tag
      if (card.tags && card.tags.length && card.tags.indexOf(config.cardExcludeTag) > -1) {
        shdSendPush = false;
      }
      if (card && data && orgId && card.organization && card.organization.es_index && card.visibility && card.visibility.audiences) {
        if (shdSendPush && (data.status == 'published' && (data.notify && data.notify.schedule && data.notify.schedule == 'immediate'))) {
          if (useOrgSpecificKeysForGCM && useOrgSpecificKeysForAPN) {
            console.log('Sending Notification in card.controller.sendNotificationIfApplies 1 with org Specific key');
            return getAllPartnersFromAudiences(card.visibility.audiences, card.organization.es_index)
              .then(esResults => {
                let partners = [];
                esResults.forEach(esResult => {
                  if (esResult.hits && esResult.hits.hits.length) {
                    esResult.hits.hits.forEach(partner => {
                      if (partner._source && partner._source.partner_id) {
                        partners.push(partner._source.partner_id);
                      }
                    })
                  }
                });
                console.log('ALL PARTNERS: ' + JSON.stringify(partners));
                return getPartnersByPartnerIds(partners)
                  .then(getDirectPartners(orgId, card.visibility.audiences))
                  .then(getUniqueArrayOfUsers())
                  .then(users => {
                    if (!users.length) {
                      console.log('No User to send notification, in skipping notification');
                      return resolve(card);
                    }
                    return getAllDevicesOfUsers(users)
                      .then(getAllUniqueDevices())
                      .then(allUniqueDevices => {
                        // let pushTokens = allUniqueDevices.map(device=>device.dpushtoken);
                        return saveNotificationEntity(card, users, data.notify.schedule, 'sent', allUniqueDevices) //TODO //For now setting status as sent
                          .then(notification => {
                            console.log('Notification Entity');
                            console.log(notification);  //Notification Entity Created TODO // Now background task should take care of sending notification
                            if (allUniqueDevices) {
                              return sendNotification({msg: card, devices: allUniqueDevices})
                                .then(gcmResponse => {
                                  //TODO should save status as sent once got success response this should be handles in background job
                                  console.log('gcmResponse in card.controller.sendNotificationIfApplies 2');
                                  console.log(gcmResponse);
                                  return resolve(card);
                                })
                                .catch(err => {
                                  console.log('Error in card.controller.sendNotificationIfApplies 3');
                                  return reject(err);
                                })
                            }
                          })
                          .catch(err => {
                            console.log('Error in card.controller.sendNotificationIfApplies 4');
                            return reject(err);
                          })
                      })
                      .catch(err => {
                        console.log('Error in card.controller.sendNotificationIfApplies 5');
                        return reject(err);
                      })
                  })
                  .catch(err => {
                    console.log('Error in card.controller.sendNotificationIfApplies 6');
                    return reject(err);
                  });
              })
              .catch(err => {
                console.log('Error in card.controller.sendNotificationIfApplies 8');
                return reject(err);
              });
          } else {
            console.log('Sending Notification in card.controller.sendNotificationIfApplies 2 without org Specific key');
            return getAllPartnersHavingOrgId(orgId)
              .then(getUniqueArrayOfUsers())
              .then(users => {
                return getAllDevicesOfUsers(users)
                  .then(getAllUniqueDevices())
                  .then(allUniqueDevices => {
                    // let pushTokens = allUniqueDevices.map(device=>device.dpushtoken);
                    return saveNotificationEntity(card, users, data.notify.schedule, 'sent', allUniqueDevices) //TODO //For now setting status as sent
                      .then(notification => {
                        console.log('Notification Entity');
                        console.log(notification);  //Notification Entity Created TODO // Now background task should take care of sending notification
                        if (allUniqueDevices) {
                          return sendNotification({msg: card, devices: allUniqueDevices})
                            .then(gcmResponse => {
                              //TODO should save status as sent once got success response this should be handles in background job
                              console.log('gcmResponse in card.controller.sendNotificationIfApplies 1');
                              console.log(gcmResponse);
                              return resolve(card);
                            })
                            .catch(err => {
                              console.log('Error in card.controller.sendNotificationIfApplies 1');
                              return reject(err);
                            })
                        }
                      })
                      .catch(err => {
                        console.log('Error in card.controller.sendNotificationIfApplies 2');
                        return reject(err);
                      })
                  })
                  .catch(err => {
                    console.log('Error in card.controller.sendNotificationIfApplies 3');
                    return reject(err);
                  })
              })
              .catch(err => {
                console.log('Error in card.controller.sendNotificationIfApplies 4');
                return reject(err);
              })
          }
        } else {
          console.log('Skipping Notification in card.controller.sendNotificationIfApplies 9');
          return resolve(card);
        }
      } else {
        console.log('Error in card.controller.sendNotificationIfApplies 10');
        return reject({type: 'EntityNotFound', code: 404, msg: 'card/data/orgId/Audiences/es_index not found'});
      }
    });
  }
}

function getOrgPartners() {
  return function(card) {
    return new Promise((resolve, reject) => {
      if (card && card.organization) {
        return PartnerCtrl.getOrgPartners(card.organization)
          .then(partners => {
            card.partners = partners;
            return resolve(card);
          })
          .catch(err => {
            console.log('Error in card.controller.getOrgPartners 1');
            return reject(err);
          })
      } else {
        console.log('Error in card.controller.getOrgPartners 2');
        return reject({type: 'EntityNotFound', code: 404, msg: 'card/orgId not found'});
      }
    });
  }
}

function getAllNotificationsForCard() {
  return function(card) {
    return new Promise((resolve, reject) => {
      if (card && card._id) {
        return NotificationCtrl.getNotificationsByCardId(card._id)
          .then(notifications => {
            card.notifications = notifications;
            return resolve(card);
          })
          .catch(err => {
            console.log('Error in card.controller.getAllNotificationsForCard 1');
            return reject(err);
          })
      } else {
        console.log('Error in card.controller.getAllNotificationsForCard 2');
        return reject({type: 'EntityNotFound', code: 404, msg: 'card not found'});
      }
    });
  }
}

function filterSandboxOrgs() {
  return function(cards) {
    return new Promise((resolve, reject) => {
      if (cards && cards.length) {
        let filteredCards = cards.filter(card => card.organization.level == 'sandbox');
        return resolve(filteredCards);
      } else {
        console.log('Error in card.controller.filterSandboxOrgs 2');
        return reject({type: 'EntityNotFound', code: 404, msg: 'card not found'});
      }
    });
  }
}


export function getLatestActiveCard(orgId) {
  return new Promise((resolve, reject) => {
    if (orgId) {
      return Card.findOne({active: true, status: "published", organization: orgId})
        .deepPopulate('organization organization.androidKey organization.iosKey')
        .sort({date_published: -1})
        .lean()
        .exec()
        .then(gcmResponse => {
          return resolve(gcmResponse);
        })
        .catch(err => {
          console.log('Error in card.controller.getLatestActiveCard 1');
          return reject(err);
        })
    } else {
      console.log('Error in card.controller.getLatestActiveCard 2');
      return reject({type: 'EntityNotFound', code: 404, msg: 'orgId not found'});
    }
  })
}

export function formatTags() {
  return function(cards) {
    return new Promise((resolve, reject) => {
      let allTags = [];
      cards.forEach(card => {
        card.tags.forEach(tag => {
          let exists = allTags.filter(_tag => _tag.name == tag).length ? true : false;
          if (!exists) {
            allTags.push({name: tag});
          }
        });
      });
      return resolve(allTags);
    });
  }
}

export function createCards(cards) {
  return new Promise((resolve, reject) => {
    if (cards && cards.length) {
      async.mapSeries(cards, function(card, cb) {
        return Card.create(card)
          .then(_card => {
            cb(null, _card);
          })
          .catch(err => {
            console.log('Error in card.controller.createCards 1');
            console.log(err);
            cb(err);
          });
      }, function(err, allCards) {
        if (err) {
          console.log('Error in card.controller.createCards 2');
          return reject(err);
        } else {
          return resolve(allCards);
        }
      });
    } else {
      return resolve(cards);
    }
  });
}

export function getOrgCardsToCopy(orgId) {
  return new Promise((resolve, reject) => {
    return Card.find({organization: orgId, active: true})
      .then(cards => {
        return resolve(cards);
      })
      .catch(err => {
        console.log('Error in card.controller.getOrgCardsToCopy 1');
        console.log(err);
        return reject(err);
      });
  });
}

export function getCardById(cardId) {
  return new Promise((resolve, reject) => {
    if (!cardId) {
      return resolve(null);
    }
    return Card.findById(cardId)
      .then(card => {
        return resolve(card);
      })
      .catch(err => {
        console.log('Error in card.controller.getCardById 1');
        console.log(err);
        return reject(err);
      });
  });
}

export function showNotifications(req, res) {
  return Card.findById(req.params.id)
    .select('organization')
    .lean()
    .exec()
    .then(getOrgPartners())
    .then(getAllNotificationsForCard())
    .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Organization not found.'}))
    .then(ResponseHelpers.respondWithResult(res))
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}


// Gets all tags from the DB
export function getAllTags(req, res) {
  return Card.find({organization: req.params.orgid})
    .select('tags')
    .exec()
    .then(formatTags())
    .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Organization not found.'}))
    .then(ResponseHelpers.respondWithResult(res))
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}


// Gets a list of Cards
export function index(req, res) {
  return Card.find().exec()
    .then(ResponseHelpers.respondWithResult(res))
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Gets a single Card from the DB
export function show(req, res) {
  return Card.findById(req.params.id)
    .populate('organization')
    .exec()
    .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Organization not found.'}))
    .then(ResponseHelpers.respondWithResult(res))
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Creates a new Card in the DB
export function create(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}])
    .then(() => {
      let toValidate = ['title', 'status', 'type', 'format', 'visibility.audiences', 'data.icon', /*'data.media.data.url',*/ 'data.actionlink.dest', 'data.links.dest'];
      req.body.organization = req.params.orgid;
      if (req.body.date) {
        delete req.body.date;
      }
      //forcing for validation
      if (req.body.data && req.body.data.title) {
        req.body.title = req.body.data.title;
      }

      if (req.user && req.user._id) {
        req.body.createdBy = req.user._id;
      }

      if (req.apiKey && req.apiKey._id) {
        req.body.createdByApiKey = req.apiKey._id;
      }

      return sanitizeEntities(req.body, toValidate)
        .then(validateEntities(toValidate))
        .then(data => {
          if (data.status && data.status == 'published') {
            data.date_published = moment();
          }
          if (data.title) {
            data.data.title = data.title;
          }
          return Card.create(data)
            .then(ResponseHelpers.respondWithResult(res, 201))
            .catch(ResponseHelpers.respondWithErrorEntity(res));
        })
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Creates a new Card in the DB from shortcut params
export function createFromShortcuts(req, res) {
  let toValidate = ['title', 'status', 'type', 'format', 'visibility.audiences', 'data.icon', /*'data.media.data.url',*/ 'data.actionlink.dest', 'data.links.dest'];
  let card = {data: {actionlink: {}, media: {}, links: [{}]}};
  if (req.body.residue) {
    card.residue = req.body.residue;
  }
  if (req.body.type) {
    card.type = req.body.type;
  }
  if (req.body.title) {
    card.title = req.body.title;
  }
  if (req.body.format) {
    card.format = req.body.format;
  }
  if (req.body.dismissable) {
    card.dismissable = req.body.dismissable;
  }
  if (req.body.title) {
    card.title = req.body.title;
  }
  if (req.body.description) {
    card.data.description = req.body.description;
  }
  if (req.body.icon) {
    card.icon = req.body.icon;
  }
  if (req.body.actionlink_icon) {
    card.data.actionlink.icon = req.body.actionlink_icon;
  }
  if (req.body.actionlink_dest) {
    card.data.actionlink.dest = req.body.actionlink_dest;
  }
  if (req.body.media_type) {
    card.data.media.type = req.body.media_type;
  }
  if (req.body.media_url) {
    card.data.media.data = {url: req.body.media_url};
  }
  if (req.body.media_type && req.body.media_url && (req.body.media_url).trim().length) {
    card.data.media.type = req.body.media_type;
  }
  if (req.body.media_url && (req.body.media_url).trim().length) {
    card.data.media.data = {url: req.body.media_url};
  }
  if (req.body.status) {
    card.status = req.body.status;
  }
  if (req.body.status && req.body.status == 'published') {
    card.date_published = moment();
  }
  if (req.params.orgid) {
    card.organization = req.params.orgid;
  }
  if (req.user && req.user._id) {
    card.createdBy = req.user._id;
  }

  if (req.apiKey && req.apiKey._id) {
    card.createdByApiKey = req.apiKey._id;
  }

  if ((req.body.status == 'published' && (req.body.notify && req.body.notify.schedule && req.body.notify.schedule == 'immediate'))) {
    if (req.user && req.user._id) {
      card.pushedBy = req.user._id;
    }
    if (req.apiKey && req.apiKey._id) {
      card.pushedByKey = req.apiKey._id;
    }
  }
  let linksTitles = Object.keys(req.body).filter(key => {
    return /^link\d{1,8}_title/.test(key);
  });
  let linksType = Object.keys(req.body).filter(key => {
    return /^link\d{1,8}_type/.test(key);
  });
  let linksDests = Object.keys(req.body).filter(key => {
    return /^link\d{1,8}_dest/.test(key);
  });
  linksTitles.forEach(title => {
    let index = title.substr(4, 1) - 1;
    if (card.data.links.length <= index) {
      card.data.links.push({title: req.body[title]});
    } else {
      card.data.links[index].title = req.body[title];
    }
  });

  linksType.forEach(type => {
    let index = type.substr(4, 1) - 1;
    if (card.data.links.length <= index) {
      card.data.links.push({type: req.body[type]});
    } else {
      card.data.links[index].type = req.body[type];
    }
  });

  linksDests.forEach(dest => {
    let index = dest.substr(4, 1) - 1;
    if (card.data.links.length <= index) {
      card.data.links.push({dest: req.body[dest]});
    } else {
      card.data.links[index].dest = req.body[dest];
    }
  });
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}])
    .then(() => {
      req.body.organization = req.params.orgid;
      if (req.body.date) {
        delete req.body.date;
      }
      return sanitizeEntities(card, toValidate)
        .then(validateEntities(toValidate))
        .then(data => {
          if (data.title) {
            data.data.title = data.title;
          }
          if (data.icon) {
            data.data.icon = data.icon;
          }
          if (req.body.notify) {
            data.notify = req.body.notify;
          }
          return OrgCtrl.getOrgDataById(req.params.orgid)
            .then(orgData => {
              Card.create(data)
                .then(attachOrgToCard(orgData))
                .then(sendNotificationIfApplies(data, req.params.orgid))
                .then(ResponseHelpers.respondWithResult(res, 201))
                .catch(ResponseHelpers.respondWithErrorEntity(res));
            })
            .catch(ResponseHelpers.respondWithErrorEntity(res));
        })
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Updates an existing Card in the DB
export function update(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}, {value: req.params.id, type: 'card'}])
    .then(() => {
      if (req.body._id) {
        delete req.body._id;
      }
      if (req.body.date) {
        delete req.body.date;
      }
      if (req.body.organization) {
        delete req.body.organization;
      }
      //forcing for validation
      if (req.body.data && req.body.data.title) {
        req.body.title = req.body.data.title;
      }
      req.body.date = moment();

      if (req.user && req.user._id) {
        req.body.updatedBy = req.user._id;
      }

      if (req.apiKey && req.apiKey._id) {
        req.body.updatedByApiKey = req.apiKey._id;
        req.body.updatedByApiKeyAt = moment();
      } else {
        req.body.updatedAt = moment();
      }

      if ((req.body.status == 'published' && (req.body.notify && req.body.notify.schedule && req.body.notify.schedule == 'immediate'))) {
        if (req.user && req.user._id) {
          req.body.pushedBy = req.user._id;
        }
        if (req.apiKey && req.apiKey._id) {
          req.body.pushedByKey = req.apiKey._id;
        }
      }

      let toValidate = ['title', 'status', 'type', 'format', 'visibility.audiences', 'data.icon', /*'data.media.data.url',*/ 'data.actionlink.dest', 'data.links.dest'];
      return sanitizeEntities(req.body, toValidate)
        .then(validateEntities(toValidate, true))
        .then(data => {
          if (data.title) {
            data.data.title = data.title;
          }
          if (data.icon) {
            data.data.icon = data.icon;
          }
          //if published and no date_published in req.body
          if (data.status && data.status == 'published' && !req.body.date_published) {
            data.date_published = moment();
          }
          //if date_published in req.body and date_published is past date delete it
          else if (data.status != 'published' || (data.date_published && moment(data.date_published) < moment())) {
            delete data.date_published;
          }
          console.log(req.method);
          return Card.findOne({_id: req.params.id, organization: req.params.orgid})
            .deepPopulate('organization organization.androidKey organization.iosKey visibility.audiences')
            .exec()
            .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Organization/Card not found.'}))
            .then(saveTags(data))
            .then(saveUpdates(data, req.method === 'PATCH'))
            .then(sendNotificationIfApplies(data, req.params.orgid))
            .then(ResponseHelpers.respondWithResult(res))
            .catch(ResponseHelpers.respondWithErrorEntity(res));
        })
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Deletes a Card from the DB
export function destroy(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}, {value: req.params.cardid, type: 'card'}])
    .then(() => {
      return Card.findOne({organization: req.params.orgid, _id: req.params.cardid})
        .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Organization/Card not found.'}))
        .then(removeEntity())
        .then(ResponseHelpers.respondWithResult(res, 204))
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Deletes all Sandbox Cards from the DB
export function destroyAllCards(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}])
    .then(() => {
      return Card.find({organization: req.params.orgid})
        .populate('organization')
        .exec()
        .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Card not found.'}))
        .then(filterSandboxOrgs())
        .then(removeAllEntities())
        .then(ResponseHelpers.respondWithResult(res, 204))
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

/*Gets count of user's cards*/
export function getUserCardsCount(organization, searchText, filter, user, audienceLinks) {
  return new Promise((resolve, reject) => {
    if (!organization && !organization.id) {
      console.log('Error in card.controller.getCardsCount 1');
      return reject({type: 'ParamsMissing', code: 422, msg: 'organization id missing'})
    } else {
      let vis = [{'visibility.audiences': {$in: audienceLinks}}, {'visibility.audiences': []}, {'visibility': {$exists: false}}];
      console.log("USER");
      console.log(user);
      if (user && user.type && user.type === 'Apple') {
        vis = [{'visibility.audiences': {$in: audienceLinks}}];
      }
      let query = {organization: organization.id, date_published: {$lt: moment()}, $or: vis};
      //Skip card if user role is 'superadmin' & card contains exclude tag
      if (user && user.role !== 'superadmin' && user.type !== 'Apple') {
        query.tags = {$ne: config.cardExcludeTag};
      }
      if (searchText) {
        query = {
          organization: organization.id,
          date_published: {$lt: moment()},
          $and: [
            {$or: vis},
            {$or: [{'data.title': new RegExp(searchText, "i")}, {'data.description': new RegExp(searchText, "i")}]}
          ]
        };
        //Skip card if user role is 'superadmin' & card contains exclude tag
        if (user && user.role !== 'superadmin' && user.type !== 'Apple') {
          console.log(user.role);
          query.tags = {$ne: config.cardExcludeTag};
        }
        if (filter) {
          query = filter;
          //Skip card if user role is 'superadmin' & card contains exclude tag
          if (user && user.role !== 'superadmin' && user.type !== 'Apple') {
            query.tags = {$ne: config.cardExcludeTag};
          }
          if (query.tag) {
            query.tags = {$regex: new RegExp("^" + _.clone(filter.tag).toLowerCase(), "i")};
            delete query.tag;
            //Skip card if user role is 'superadmin' & card contains exclude tag
            if (user && user.role !== 'superadmin' && user.type !== 'Apple') {
              query.tags.$ne = config.cardExcludeTag;
            }
          }
          query.organization = organization.id;
          if (!filter.date_published) {
            query.date_published = {$lt: moment()};
          }
          query.$and = [
            {$or: vis},
            {$or: [{'data.title': new RegExp(searchText, "i")}, {'data.description': new RegExp(searchText, "i")}]}
          ];
        }
      } else {
        if (filter) {
          query = filter;
          //Skip card if user role is 'superadmin' & card contains exclude tag
          if (user && user.role !== 'superadmin' && user.type !== 'Apple') {
            query.tags = {$ne: config.cardExcludeTag};
          }
          query.$or = vis;
          if (query.tag) {
            query.tags = {$regex: new RegExp("^" + _.clone(filter.tag).toLowerCase(), "i")};
            delete query.tag;
            //Skip card if user role is 'superadmin' & card contains exclude tag
            if (user && user.role !== 'superadmin' && user.type !== 'Apple') {
              query.tags.$ne = config.cardExcludeTag;
            }
          }
          if (!filter.date_published) {
            query.date_published = {$lt: moment()};
          }
          query.organization = organization.id;
        }
      }
      console.log("FEED COUNT QUERY:");
      console.log(JSON.stringify(query));
      return Card.find(query)
        .count()
        .then(count => {
          return resolve(count);
        })
        .catch(err => {
          console.log('Error in card.controller.getUserCardsCount 2');
          return reject(err);
        })
    }
  });
}

/*Gets count of the organization's cards*/
export function getOrgCardsCount(organization, searchText, filter) {
  return new Promise((resolve, reject) => {
    if (!organization && !organization.id) {
      console.log('Error in card.controller.getCardsCount 1');
      return reject({type: 'ParamsMissing', code: 422, msg: 'organization id missing'})
    } else {
      let query = {organization: organization.id};
      if (searchText) {
        query = {
          organization: organization.id,
          $or: [
            {'data.title': new RegExp(searchText, "i")},
            {'data.description': new RegExp(searchText, "i")}
          ]
        };
        if (filter) {
          query = filter;
          if (query.tag) {
            query.tags = {$regex: new RegExp("^" + _.clone(filter.tag).toLowerCase(), "i")};
            delete query.tag;
          }
          query.organization = organization.id;
          query.$or = [
            {'data.title': new RegExp(searchText, "i")},
            {'data.description': new RegExp(searchText, "i")}
          ];
        }
      } else {
        if (filter) {
          query = filter;
          if (query.tag) {
            query.tags = {$regex: new RegExp("^" + _.clone(filter.tag).toLowerCase(), "i")};
            delete query.tag;
          }
          query.organization = organization.id;
        }
      }
      return Card.find(query)
        .count()
        .then(count => {
          return resolve(count);
        })
        .catch(err => {
          console.log('Error in card.controller.getOrgCardsCount 2');
          return reject(err);
        })
    }
  });
}

/*Gets Paginated cards*/
export function getUserCards(organization, page, limit, count, searchText, sortObj, filter, user, audienceLinks) {
  return new Promise((resolve, reject) => {
    if (!organization && !organization.id) {
      console.log('Error in card.controller.getCards 1');
      return reject({type: 'ParamsMissing', code: 422, msg: 'organization id missing'})
    } else {
      let vis = [{'visibility.audiences': {$in: audienceLinks}}, {'visibility.audiences': []}, {'visibility': {$exists: false}}];
      if (user && user.type && user.type === 'Apple') {
        vis = [{'visibility.audiences': {$in: audienceLinks}}];
      }
      let query = {organization: organization.id, date_published: {$lt: moment()}, $or: vis};
      //Skip card if user role is 'superadmin' & card contains exclude tag
      if (user && user.role !== 'superadmin' && user.type !== 'Apple') {
        query.tags = {$ne: config.cardExcludeTag};
      }
      if (searchText) {
        query = {
          organization: organization.id,
          $and: [
            {$or: vis},
            {$or: [{'data.title': new RegExp(searchText, "i")}, {'data.description': new RegExp(searchText, "i")}]}
          ]
        };
        //Skip card if user role is 'superadmin' & card contains exclude tag
        if (user && user.role !== 'superadmin' && user.type !== 'Apple') {
          query.tags = {$ne: config.cardExcludeTag};
        }
        if (filter) {
          query = filter;
          //Skip card if user role is 'superadmin' & card contains exclude tag
          if (user && user.role !== 'superadmin' && user.type !== 'Apple') {
            query.tags = {$ne: config.cardExcludeTag};
          }
          if (query.tag) {
            query.tags = {$regex: new RegExp("^" + _.clone(filter.tag).toLowerCase(), "i")};
            delete query.tag;
            //Skip card if user role is 'superadmin' & card contains exclude tag
            if (user && user.role !== 'superadmin' && user.type !== 'Apple') {
              query.tags.$ne = config.cardExcludeTag;
            }
          }
          query.organization = organization.id;
          query.$and = [
            {$or: vis},
            {$or: [{'data.title': new RegExp(searchText, "i")}, {'data.description': new RegExp(searchText, "i")}]}
          ];
        }
      } else {
        if (filter) {
          query = filter;
          //Skip card if user role is 'superadmin' & card contains exclude tag
          if (user && user.role !== 'superadmin' && user.type !== 'Apple') {
            query.tags = {$ne: config.cardExcludeTag};
          }
          query.$or = vis;
          if (query.tag) {
            query.tags = {$regex: new RegExp("^" + _.clone(filter.tag).toLowerCase(), "i")};
            delete query.tag;
            //Skip card if user role is 'superadmin' & card contains exclude tag
            if (user && user.role !== 'superadmin' && user.type !== 'Apple') {
              query.tags.$ne = config.cardExcludeTag;
            }
          }
          query.organization = organization.id;
        }
      }
      let last = false;
      let maxPages = Math.ceil(count / limit) || 0;
      if (page == 'last') {
        last = true;
        page = maxPages;
      } else if (maxPages == page) {
        last = true;
      }
      console.log("FEED QUERY:");
      console.log(JSON.stringify(query));
      return Card.find(query)
        .skip(maxPages > 0 ? ((page - 1) * limit) : 0)
        .limit(limit)
        .populate('visibility.audiences')
        .sort(sortObj)
        .exec()
        .then(processForPlaceholders(last))
        .then(items => {
          return resolve({cards: items, currentPage: page});
        })
        .catch(err => {
          console.log('Error in card.controller.getUserCards 2');
          return reject(err);
        })
    }
  });
}
/*Gets Paginated cards*/
export function getOrgCards(organization, page, limit, count, searchText, sortObj, filter) {
  return new Promise((resolve, reject) => {
    if (!organization && !organization.id) {
      console.log('Error in card.controller.getCards 1');
      return reject({type: 'ParamsMissing', code: 422, msg: 'organization id missing'})
    } else {
      let maxPages = Math.ceil(count / limit) || 0;
      if (page == 'last') {
        page = maxPages;
      }
      let query = {organization: organization.id};
      if (searchText) {
        query = {
          organization: organization.id,
          $or: [
            {'data.title': new RegExp(searchText, "i")},
            {'data.description': new RegExp(searchText, "i")}
          ]
        };
        if (filter) {
          query = filter;
          if (query.tag) {
            query.tags = {$regex: new RegExp("^" + _.clone(filter.tag).toLowerCase(), "i")};
            delete query.tag;
          }
          query.organization = organization.id;
          query.$or = [
            {'data.title': new RegExp(searchText, "i")},
            {'data.description': new RegExp(searchText, "i")}
          ];
        }
      } else {
        if (filter) {
          query = filter;
          if (query.tag) {
            query.tags = {$regex: new RegExp("^" + _.clone(filter.tag).toLowerCase(), "i")};
            delete query.tag;
          }
          query.organization = organization.id;
        }
      }
      return Card.find(query)
        .skip(maxPages > 0 ? ((page - 1) * limit) : 0)
        .limit(limit)
        .populate('organization visibility.audiences')
        .sort(sortObj)
        .exec()
        .then(items => {
          return resolve({cards: items, currentPage: page});
        })
        .catch(err => {
          console.log('Error in card.controller.getOrgCards 2');
          return reject(err);
        })
    }
  });
}

/*Gets Single cards*/
export function getSingleOrgCard(organization, cardId) {
  return new Promise((resolve, reject) => {
    if (!organization && !organization.id) {
      console.log('Error in card.controller.getSingleOrgCard 1');
      return reject({type: 'ParamsMissing', code: 422, msg: 'organization id missing'})
    }

    if (!cardId) {
      console.log('Error in card.controller.getSingleOrgCard 2');
      return reject({type: 'ParamsMissing', code: 422, msg: 'card id missing'})
    }

    return Card.findOne({organization: organization.id, _id: cardId})
      .populate('organization visibility.audiences')
      .exec()
      .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Card not found.'}))
      .then(item => {
        return resolve(item);
      })
      .catch(err => {
        console.log('Error in card.controller.getSingleOrgCard 3');
        return reject(err);
      })
  });
}
