'use strict';

var express = require('express');
var controller = require('./card.controller');
import * as logger from '../../components/logger.service';
import * as auth from '../../auth/auth.service';

var router = express.Router();
router.get('/:id/notifications', controller.showNotifications);

module.exports = router;
