'use strict';

import mongoose from 'mongoose';
mongoose.Promise = require('bluebird');
import {Schema} from 'mongoose';
const deepPopulate = require('mongoose-deep-populate')(mongoose);

var CardSchema = new mongoose.Schema({
  organization: {type: Schema.Types.ObjectId, ref: 'Organization', required: true},
  /*TODO Temporary*/
  // dismissable: {type: Boolean, default: true},
  type: String,
  residue: String,
  date: {type: Date, default: Date.now},
  format: String,
  visibility: {
    audiences: [{
      type: Schema.Types.ObjectId,
      ref: 'Audience'
    }]
  },
  tags: [String],
  data: {
    // title: String,
    // description: String,
    // icon: String,
    // actionlink: {},
    // media: {},
    // links: [{}]
  },
  date_published: Date,
  status: {type: String, default: 'draft'},
  __v: {type: Number, select: false},
  active: {type: Boolean, default: true, select: false},
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: {
    type: Date
  },
  updatedByApiKeyAt: {
    type: Date
  },
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  pushedBy: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  pushedByKey: {
    type: Schema.Types.ObjectId,
    ref: 'ApiKey'
  },
  createdByApiKey: {
    type: Schema.Types.ObjectId,
    ref: 'ApiKey'
  },
  updatedByApiKey: {
    type: Schema.Types.ObjectId,
    ref: 'ApiKey'
  },
  updatedBy: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  }
});

CardSchema.set('toJSON', {
  virtuals: true
});

/*TODO Temporary*/
CardSchema.virtual('dismissable').get(function() {
  return false;
});

CardSchema.plugin(deepPopulate);
export default mongoose.model('Card', CardSchema);
