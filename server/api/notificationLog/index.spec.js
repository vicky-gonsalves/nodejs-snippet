'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var notificationLogCtrlStub = {
  index: 'notificationLogCtrl.index',
  show: 'notificationLogCtrl.show',
  create: 'notificationLogCtrl.create',
  update: 'notificationLogCtrl.update',
  destroy: 'notificationLogCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var notificationLogIndex = proxyquire('./index.js', {
  'express': {
    Router: function() {
      return routerStub;
    }
  },
  './notificationLog.controller': notificationLogCtrlStub
});

describe('NotificationLog API Router:', function() {

  it('should return an express router instance', function() {
    notificationLogIndex.should.equal(routerStub);
  });

  describe('GET /api/notificationLogs', function() {

    it('should route to notificationLog.controller.index', function() {
      routerStub.get
        .withArgs('/', 'notificationLogCtrl.index')
        .should.have.been.calledOnce;
    });

  });

  describe('GET /api/notificationLogs/:id', function() {

    it('should route to notificationLog.controller.show', function() {
      routerStub.get
        .withArgs('/:id', 'notificationLogCtrl.show')
        .should.have.been.calledOnce;
    });

  });

  describe('POST /api/notificationLogs', function() {

    it('should route to notificationLog.controller.create', function() {
      routerStub.post
        .withArgs('/', 'notificationLogCtrl.create')
        .should.have.been.calledOnce;
    });

  });

  describe('PUT /api/notificationLogs/:id', function() {

    it('should route to notificationLog.controller.update', function() {
      routerStub.put
        .withArgs('/:id', 'notificationLogCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('PATCH /api/notificationLogs/:id', function() {

    it('should route to notificationLog.controller.update', function() {
      routerStub.patch
        .withArgs('/:id', 'notificationLogCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('DELETE /api/notificationLogs/:id', function() {

    it('should route to notificationLog.controller.destroy', function() {
      routerStub.delete
        .withArgs('/:id', 'notificationLogCtrl.destroy')
        .should.have.been.calledOnce;
    });

  });

});
