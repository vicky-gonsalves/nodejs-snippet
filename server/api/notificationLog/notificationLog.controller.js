'use strict';

import * as config from '../../config/environment';
import _ from 'lodash';
import NotificationLog from './notificationLog.model';
import * as OrgCtrl from '../organization/organization.controller';
import * as PartnerCtrl from '../partner/partner.controller';
import * as ES from '../../components/elasticSearch/es';

const async = require('async');
const Promise = require('bluebird');
const validator = require('validator');
const moment = require('moment');
const ResponseHelpers = require('../../components/responseHelpers/responseHelpers');
Promise.config({
  // Enables all warnings except forgotten return statements.
  warnings: {
    wForgottenReturn: false
  }
});

function sanitizeEntities(data, toValidate = []) {
  return new Promise((resolve, reject) => {
    try {
      let sanitizedData = JSON.parse(JSON.stringify(data));
      if (data.audience && toValidate.indexOf('audience') > -1) {
        sanitizedData.audience = validator.trim(data.audience);
      }
      if (data.partner && toValidate.indexOf('partner') > -1) {
        sanitizedData.partner = validator.trim(data.partner);
      }
      return resolve(sanitizedData);
    } catch (e) {
      console.log('Error in notificationLog.controller.sanitizeEntities 1');
      return reject(e);
    }
  })
}

function validateEntities(toValidate = [], update = false) {
  return function(data) {
    return new Promise((resolve, reject) => {
      if (data) {
        let errors = {type: 'InvalidParams', code: 422, msg: []};
        //Validation
        if (toValidate.indexOf('audience') > -1) {
          if (!update && (!(data.audience) || validator.isNull(data.audience))) {
            errors.msg.push({audience: 'audience is required'});
          } else if (data.audience && !validator.isMongoId(data.audience)) {
            errors.msg.push({audience: 'audience must be Valid mongo id'});
          }
        }

        if (toValidate.indexOf('partner') > -1) {
          if (!update && (!(data.partner) || validator.isNull(data.partner))) {
            errors.msg.push({partner: 'partner is required'});
          } else if (data.partner && !validator.isMongoId(data.partner)) {
            errors.msg.push({partner: 'partner must be Valid mongo id'});
          }
        }

        if (errors.msg.length > 0) {
          console.log('Error in notificationLog.controller.validateEntities 1');
          return reject(errors);
        } else {
          return resolve(data);
        }
      } else {
        console.log('Error in notificationLog.controller.validateEntities 2');
        let err = {type: 'EntityNotFound', code: 422, msg: 'Entity not found.'};
        return reject(err);
      }
    });
  }
}

function saveUpdates(updates) {
  return function(entity) {
    let updated = _.extend(entity, updates, (a, b) => {
      if (_.isArray(a)) {
        return b;
      }
    });
    return updated.save()
      .then(updated => {
        return updated;
      });
  };
}

function removeEntity() {
  return function(entity) {
    return new Promise((resolve, reject) => {
      if (entity) {
        return entity.remove()
          .then(() => {
            return resolve();
          })
          .catch(err => {
            console.log('Error in notificationLog.controller.removeEntity 1');
            return reject(err);
          })
      } else {
        console.log('Error in notificationLog.controller.removeEntity 2');
        return reject({type: 'EntityNotFound', code: 404, msg: 'NotificationLog not found'});
      }
    });
  };
}

// save NotificationLogs
export function saveLog(data) {
  return new Promise((resolve, reject) => {
    return NotificationLog.create(data)
      .then(log => {
        return resolve(log);
      })
      .catch(err => {
        console.log('Error in notificationLog.controller.saveLog 1');
        console.log(err);
        return reject(err);
      })
  })
}
// Gets a list of NotificationLogs
export function getLogs(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}, {value: req.params.cardid, type: 'card'}])
    .then(() => {
      return NotificationLog.find({organization: req.params.orgid, card: req.params.cardid}).exec()
        .then(ResponseHelpers.respondWithResult(res))
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}
// Creates a new NotificationLog in the DB
export function create(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}])
    .then(() => {
      let toValidate = ['name', 'filter'];
      if (req.body.createdBy) {
        delete req.body.createdBy;
      }
      if (req.user) {
        req.body.createdBy = req.user._id;
      }
      if (req.body.organization) {
        delete req.body.organization;
      }
      if (req.body.lastRun) {
        delete req.body.lastRun;
      }
      if (req.body.count) {
        delete req.body.count;
      }
      if (req.body.createdAt) {
        delete req.body.createdAt;
      }
      if (req.body.updatedAt) {
        delete req.body.updatedAt;
      }
      req.body.organization = req.params.orgid;
      return sanitizeEntities(req.body, toValidate)
        .then(validateEntities(toValidate))
        .then(data => {
          return OrgCtrl.getEsIndex(req.params.orgid)
            .then(ES.perFormEsSearch(req.body.filter))
            .then(createNotificationLog(data))
            .then(ResponseHelpers.respondWithResult(res, 201))
            .catch(ResponseHelpers.respondWithErrorEntity(res));
        })
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Deletes a NotificationLog from the DB
export function destroy(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}, {value: req.params.id, type: 'notificationLog'}])
    .then(() => {
      return NotificationLog.findOne({organization: req.params.orgid, _id: req.params.id}).exec()
        .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'NotificationLog not found.'}))
        .then(removeEntity(res))
        .then(ResponseHelpers.respondWithResult(res, 204))
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}
