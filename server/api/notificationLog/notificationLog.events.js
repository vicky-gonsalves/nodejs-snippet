/**
 * NotificationLog model events
 */

'use strict';

import {EventEmitter} from 'events';
import NotificationLog from './notificationLog.model';
var NotificationLogEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
NotificationLogEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  NotificationLog.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    NotificationLogEvents.emit(event + ':' + doc._id, doc);
    NotificationLogEvents.emit(event, doc);
  }
}

export default NotificationLogEvents;
