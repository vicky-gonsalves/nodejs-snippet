'use strict';

var app = require('../..');
import request from 'supertest';

var newNotificationLog;

describe('NotificationLog API:', function() {

  describe('GET /api/notificationLogs', function() {
    var notificationLogs;

    beforeEach(function(done) {
      request(app)
        .get('/api/notificationLogs')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          notificationLogs = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      notificationLogs.should.be.instanceOf(Array);
    });

  });

  describe('POST /api/notificationLogs', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/notificationLogs')
        .send({
          name: 'New NotificationLog',
          info: 'This is the brand new notificationLog!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newNotificationLog = res.body;
          done();
        });
    });

    it('should respond with the newly created notificationLog', function() {
      newNotificationLog.name.should.equal('New NotificationLog');
      newNotificationLog.info.should.equal('This is the brand new notificationLog!!!');
    });

  });

  describe('GET /api/notificationLogs/:id', function() {
    var notificationLog;

    beforeEach(function(done) {
      request(app)
        .get('/api/notificationLogs/' + newNotificationLog._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          notificationLog = res.body;
          done();
        });
    });

    afterEach(function() {
      notificationLog = {};
    });

    it('should respond with the requested notificationLog', function() {
      notificationLog.name.should.equal('New NotificationLog');
      notificationLog.info.should.equal('This is the brand new notificationLog!!!');
    });

  });

  describe('PUT /api/notificationLogs/:id', function() {
    var updatedNotificationLog;

    beforeEach(function(done) {
      request(app)
        .put('/api/notificationLogs/' + newNotificationLog._id)
        .send({
          name: 'Updated NotificationLog',
          info: 'This is the updated notificationLog!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedNotificationLog = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedNotificationLog = {};
    });

    it('should respond with the updated notificationLog', function() {
      updatedNotificationLog.name.should.equal('Updated NotificationLog');
      updatedNotificationLog.info.should.equal('This is the updated notificationLog!!!');
    });

  });

  describe('DELETE /api/notificationLogs/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/api/notificationLogs/' + newNotificationLog._id)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when notificationLog does not exist', function(done) {
      request(app)
        .delete('/api/notificationLogs/' + newNotificationLog._id)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
