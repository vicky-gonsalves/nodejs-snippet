/**
 * NotificationStatus model events
 */

'use strict';

import {EventEmitter} from 'events';
import NotificationStatus from './notificationStatus.model';
var NotificationStatusEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
NotificationStatusEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  NotificationStatus.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    NotificationStatusEvents.emit(event + ':' + doc._id, doc);
    NotificationStatusEvents.emit(event, doc);
  }
}

export default NotificationStatusEvents;
