'use strict';

var app = require('../..');
import request from 'supertest';

var newNotificationStatus;

describe('NotificationStatus API:', function() {

  describe('GET /api/v1/notification-status', function() {
    var notificationStatuss;

    beforeEach(function(done) {
      request(app)
        .get('/api/v1/notification-status')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          notificationStatuss = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      notificationStatuss.should.be.instanceOf(Array);
    });

  });

  describe('POST /api/v1/notification-status', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/v1/notification-status')
        .send({
          name: 'New NotificationStatus',
          info: 'This is the brand new notificationStatus!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newNotificationStatus = res.body;
          done();
        });
    });

    it('should respond with the newly created notificationStatus', function() {
      newNotificationStatus.name.should.equal('New NotificationStatus');
      newNotificationStatus.info.should.equal('This is the brand new notificationStatus!!!');
    });

  });

  describe('GET /api/v1/notification-status/:id', function() {
    var notificationStatus;

    beforeEach(function(done) {
      request(app)
        .get('/api/v1/notification-status/' + newNotificationStatus._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          notificationStatus = res.body;
          done();
        });
    });

    afterEach(function() {
      notificationStatus = {};
    });

    it('should respond with the requested notificationStatus', function() {
      notificationStatus.name.should.equal('New NotificationStatus');
      notificationStatus.info.should.equal('This is the brand new notificationStatus!!!');
    });

  });

  describe('PUT /api/v1/notification-status/:id', function() {
    var updatedNotificationStatus;

    beforeEach(function(done) {
      request(app)
        .put('/api/v1/notification-status/' + newNotificationStatus._id)
        .send({
          name: 'Updated NotificationStatus',
          info: 'This is the updated notificationStatus!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedNotificationStatus = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedNotificationStatus = {};
    });

    it('should respond with the updated notificationStatus', function() {
      updatedNotificationStatus.name.should.equal('Updated NotificationStatus');
      updatedNotificationStatus.info.should.equal('This is the updated notificationStatus!!!');
    });

  });

  describe('DELETE /api/v1/notification-status/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/api/v1/notification-status/' + newNotificationStatus._id)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when notificationStatus does not exist', function(done) {
      request(app)
        .delete('/api/v1/notification-status/' + newNotificationStatus._id)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
