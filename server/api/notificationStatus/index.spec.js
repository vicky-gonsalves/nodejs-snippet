'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var notificationStatusCtrlStub = {
  index: 'notificationStatusCtrl.index',
  show: 'notificationStatusCtrl.show',
  create: 'notificationStatusCtrl.create',
  update: 'notificationStatusCtrl.update',
  destroy: 'notificationStatusCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var notificationStatusIndex = proxyquire('./index.js', {
  'express': {
    Router: function() {
      return routerStub;
    }
  },
  './notificationStatus.controller': notificationStatusCtrlStub
});

describe('NotificationStatus API Router:', function() {

  it('should return an express router instance', function() {
    notificationStatusIndex.should.equal(routerStub);
  });

  describe('GET /api/v1/notification-status', function() {

    it('should route to notificationStatus.controller.index', function() {
      routerStub.get
        .withArgs('/', 'notificationStatusCtrl.index')
        .should.have.been.calledOnce;
    });

  });

  describe('GET /api/v1/notification-status/:id', function() {

    it('should route to notificationStatus.controller.show', function() {
      routerStub.get
        .withArgs('/:id', 'notificationStatusCtrl.show')
        .should.have.been.calledOnce;
    });

  });

  describe('POST /api/v1/notification-status', function() {

    it('should route to notificationStatus.controller.create', function() {
      routerStub.post
        .withArgs('/', 'notificationStatusCtrl.create')
        .should.have.been.calledOnce;
    });

  });

  describe('PUT /api/v1/notification-status/:id', function() {

    it('should route to notificationStatus.controller.update', function() {
      routerStub.put
        .withArgs('/:id', 'notificationStatusCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('PATCH /api/v1/notification-status/:id', function() {

    it('should route to notificationStatus.controller.update', function() {
      routerStub.patch
        .withArgs('/:id', 'notificationStatusCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('DELETE /api/v1/notification-status/:id', function() {

    it('should route to notificationStatus.controller.destroy', function() {
      routerStub.delete
        .withArgs('/:id', 'notificationStatusCtrl.destroy')
        .should.have.been.calledOnce;
    });

  });

});
