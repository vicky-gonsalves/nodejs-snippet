'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var entsCtrlStub = {
  index: 'entsCtrl.index',
  show: 'entsCtrl.show',
  create: 'entsCtrl.create',
  update: 'entsCtrl.update',
  destroy: 'entsCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var entsIndex = proxyquire('./index.js', {
  'express': {
    Router: function() {
      return routerStub;
    }
  },
  './ents.controller': entsCtrlStub
});

describe('Ents API Router:', function() {

  it('should return an express router instance', function() {
    entsIndex.should.equal(routerStub);
  });

  describe('GET /ents', function() {

    it('should route to ents.controller.index', function() {
      routerStub.get
        .withArgs('/', 'entsCtrl.index')
        .should.have.been.calledOnce;
    });

  });

  describe('GET /ents/:id', function() {

    it('should route to ents.controller.show', function() {
      routerStub.get
        .withArgs('/:id', 'entsCtrl.show')
        .should.have.been.calledOnce;
    });

  });

  describe('POST /ents', function() {

    it('should route to ents.controller.create', function() {
      routerStub.post
        .withArgs('/', 'entsCtrl.create')
        .should.have.been.calledOnce;
    });

  });

  describe('PUT /ents/:id', function() {

    it('should route to ents.controller.update', function() {
      routerStub.put
        .withArgs('/:id', 'entsCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('PATCH /ents/:id', function() {

    it('should route to ents.controller.update', function() {
      routerStub.patch
        .withArgs('/:id', 'entsCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('DELETE /ents/:id', function() {

    it('should route to ents.controller.destroy', function() {
      routerStub.delete
        .withArgs('/:id', 'entsCtrl.destroy')
        .should.have.been.calledOnce;
    });

  });

});
