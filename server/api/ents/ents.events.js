/**
 * Ents model events
 */

'use strict';

import {EventEmitter} from 'events';
import Ents from './ents.model';
var EntsEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
EntsEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  Ents.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    EntsEvents.emit(event + ':' + doc._id, doc);
    EntsEvents.emit(event, doc);
  }
}

export default EntsEvents;
