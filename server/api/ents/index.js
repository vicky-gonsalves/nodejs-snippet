'use strict';

var express = require('express');
var controller = require('./ents.controller');

var router = express.Router();

router.get('/:id', controller.redirectToInviteLink);

module.exports = router;
