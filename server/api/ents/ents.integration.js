'use strict';

var app = require('../..');
import request from 'supertest';

var newEnts;

describe('Ents API:', function() {

  describe('GET /ents', function() {
    var entss;

    beforeEach(function(done) {
      request(app)
        .get('/ents')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          entss = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      entss.should.be.instanceOf(Array);
    });

  });

  describe('POST /ents', function() {
    beforeEach(function(done) {
      request(app)
        .post('/ents')
        .send({
          name: 'New Ents',
          info: 'This is the brand new ents!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newEnts = res.body;
          done();
        });
    });

    it('should respond with the newly created ents', function() {
      newEnts.name.should.equal('New Ents');
      newEnts.info.should.equal('This is the brand new ents!!!');
    });

  });

  describe('GET /ents/:id', function() {
    var ents;

    beforeEach(function(done) {
      request(app)
        .get('/ents/' + newEnts._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          ents = res.body;
          done();
        });
    });

    afterEach(function() {
      ents = {};
    });

    it('should respond with the requested ents', function() {
      ents.name.should.equal('New Ents');
      ents.info.should.equal('This is the brand new ents!!!');
    });

  });

  describe('PUT /ents/:id', function() {
    var updatedEnts;

    beforeEach(function(done) {
      request(app)
        .put('/ents/' + newEnts._id)
        .send({
          name: 'Updated Ents',
          info: 'This is the updated ents!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedEnts = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedEnts = {};
    });

    it('should respond with the updated ents', function() {
      updatedEnts.name.should.equal('Updated Ents');
      updatedEnts.info.should.equal('This is the updated ents!!!');
    });

  });

  describe('DELETE /ents/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/ents/' + newEnts._id)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when ents does not exist', function(done) {
      request(app)
        .delete('/ents/' + newEnts._id)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
