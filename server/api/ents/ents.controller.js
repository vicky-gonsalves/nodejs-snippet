'use strict';

import * as UserCtrl from "../user/user.controller";
const Promise = require('bluebird');
const ResponseHelpers = require('../../components/responseHelpers/responseHelpers');
Promise.config({
  // Enables all warnings except forgotten return statements.
  warnings: {
    wForgottenReturn: false
  }
});

export function redirectToInviteLink(req, res) {
  return UserCtrl.getInviteLinkBySearchKey(req.params.id)
    .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Invite Link not found.'}))
    .then(users => {
      let user = users.filter(usr => {
        let sub = usr.inviteLink.substring(usr.inviteLink.lastIndexOf('/') + 1);
        return sub === req.params.id;
      });
      if (user.length) {
        console.log(user[0]);
        console.log(user[0].inviteLink);
        res.redirect(user[0].inviteLink);
        return user[0];
      } else {
        return null;
      }
    })
    .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Invite Link not found.'}))
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}
