var qr = require('qr-image');

export function index(req, res) {
  let text = decodeURIComponent(req.query.q);
  console.log(text);
  var code = qr.image(text, {type: 'png', ec_level: 'H', size: 10, margin: 0});
  res.setHeader('Content-type', 'image/png');
  code.pipe(res);
}
