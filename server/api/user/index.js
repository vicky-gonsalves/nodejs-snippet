'use strict';

import {Router} from 'express';
import * as controller from './user.controller';
import * as logger from '../../components/logger.service';
import * as auth from '../../auth/auth.service';

var router = new Router();

router.patch('/:id', auth.hasRole('organization-admin'), logger.putOtherLog(), controller.update);

export default router;
