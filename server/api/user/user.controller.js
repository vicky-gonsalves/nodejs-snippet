'use strict';

import User from "./user.model";
import * as DeviceCtrl from "../device/device.controller";
import * as OrgCtrl from "../organization/organization.controller";
import * as GCMCtrl from "../../components/gcm/gcm.controller";
import _ from "lodash";
import config from "../../config/environment";
import jwt from "jsonwebtoken";
import Organization from "../organization/organization.model";
import * as Mailer from "../../components/emails/sendMail";
import path from 'path';
const validator = require('validator');
const moment = require('moment');
const async = require('async');
const PartnerCtrl = require('../partner/partner.controller');
const NotificationCtrl = require('../notification/notification.controller');
const ResponseHelpers = require('../../components/responseHelpers/responseHelpers');
const randomstring = require('randomstring');
const Promise = require('bluebird');
const request = require('request');
const requestretry = require('requestretry');
const wh = require('../../components/webhook/webhook');
Promise.config({
  // Enables all warnings except forgotten return statements.
  warnings: {
    wForgottenReturn: false
  }
});

function sanitizeEntities(data, toValidate = []) {
  return new Promise((resolve, reject) => {
    try {
      let sanitizedData = JSON.parse(JSON.stringify(data));
      if (data.phone && toValidate.indexOf('phone') > -1) {
        sanitizedData.phone = validator.trim(data.phone);
      }
      if (data.token && toValidate.indexOf('token') > -1) {
        sanitizedData.token = validator.trim(data.token);
      }
      if (data.status && toValidate.indexOf('status') > -1) {
        sanitizedData.status = validator.trim(data.status);
      }
      return resolve(sanitizedData);
    } catch (e) {
      console.log('Error in user.controller.sanitizeEntities 1');
      return reject(e);
    }
  })
}

function validateEntities(toValidate = [], update = false) {
  return function(data) {
    return new Promise((resolve, reject) => {
      if (data) {
        let errors = {type: 'InvalidParams', code: 422, msg: []};
        if (toValidate.indexOf('phone') > -1) {
          if (!data.phone || validator.isNull(data.phone)) {
            errors.msg.push({phone: 'phone must be 10 digits long, should be numeric and should not contain country code'});
          } else if (data.phone && !validator.isLength(data.phone, {min: 10, max: 10}) && !validator.isMobilePhone(data.phone, 'en-US')) {
            errors.msg.push({phone: 'phone must be 10 digits long, should be numeric and should not contain country code'});
          }
        }
        if (toValidate.indexOf('password') > -1) {
          if (data.password && !validator.isLength(data.password, {min: 6, max: 255})) {
            errors.msg.push({password: 'password should be atleast 6-20 characters long'});
          }
        }
        if (toValidate.indexOf('status') > -1) {
          if (data.status && !validator.isIn(data.status, ['Accepted'])) {
            errors.msg.push({status: 'status should be Accepted'});
          }
        }
        if (toValidate.indexOf('token') > -1) {
          if (!data.token || validator.isNull(data.token)) {
            errors.msg.push({token: 'token is required'});
          } else if (data.token && !validator.isLength(data.token, {min: 4, max: 4})) {
            errors.msg.push({token: 'Invalid token'});
          }
        }
        if (errors.msg.length > 0) {
          console.log('Error in user.controller.validateEntities 1');
          return reject(errors);
        } else {
          return resolve(data);
        }
      } else {
        console.log('Error in user.controller.validateEntities 2');
        let err = {type: 'EntityNotFound', code: 404, msg: 'Entity not found.'};
        return reject(err);
      }
    });
  }
}

function saveUpdates(updates) {
  return function(entity) {
    var updated = _.merge(entity, updates);
    return updated.save()
      .then(updated => {
        return updated;
      });
  };
}

function validationError(res, statusCode) {
  statusCode = statusCode || 422;
  return function(err) {
    res.status(statusCode).json(err);
  }
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

function saveNotificationEntity(users, schedule, status, devices) {
  return new Promise((resolve, reject) => {
    if (users && schedule && status && devices) {
      let deviceIds = devices.map(device => device._id);
      return NotificationCtrl.saveNotification({users: users, schedule: schedule, status: status, recipients: deviceIds, data: {auth: "logout"}})
        .then(notification => {
          return resolve(notification);
        })
        .catch(err => {
          console.log('Error in user.controller.saveNotificationEntity 1');
          return reject(err);
        })
    } else {
      console.log('Error in user.controller.saveNotificationEntity 1');
      return reject({type: 'EntityNotFound', code: 404, msg: 'users/schedule/status not found'});
    }
  })
}

export function deactivateAllDevicesIfUserDeactivated() {
  return function(user) {
    return new Promise((resolve, reject) => {
      if (user && !user.active) {
        console.log('Deactivating Devices');
        return DeviceCtrl.getDevicesByUserId(user._id)
          .then(devices => {
            if (devices && devices.length) {
              console.log('Devices: ');
              console.log(JSON.stringify(devices));
              return saveNotificationEntity([user._id], 'immediate', 'sent', devices) //TODO //For now setting status as sent
                .then(() => {
                  return PartnerCtrl.getPartnerByUserId(user._id)
                    .then(sendDeactivationPush(devices))
                    .then(pushSent => {
                      return resolve(user);
                    })
                    .catch(err => {
                      console.log('Error in user.controller.deactivateAllDevicesIfUserDeactivated 1');
                      return reject(err);
                    })
                })
                .catch(err => {
                  console.log('Error in user.controller.deactivateAllDevicesIfUserDeactivated 2');
                  return reject(err);
                })
            } else {
              console.log('User have no devices registered. Skipping deactivation.');
              return resolve(user);
            }
          })
          .catch(err => {
            console.log('Error in user.controller.deactivateAllDevicesIfUserDeactivated 3');
            return reject(err);
          })
      } else {
        console.log('User already Inactive. Skipping Deactivation of devices.');
        return resolve(user);
      }
    })
  }
}

function sendDeactivationPush(devices) {
  return function(partner) {
    return new Promise((resolve, reject) => {
      if (partner && partner.organization) {
        let options = {devices: devices, msg: {organization: partner.organization}};
        return GCMCtrl.sendLogoutNotification(options)
          .then(gcmResponse => {
            return resolve(gcmResponse);
          })
          .catch(err => {
            console.log('Error in user.controller.sendDeactivationPush 1');
            return reject(err);
          })
      } else {
        console.log('Error in user.controller.sendDeactivationPush 2');
        let err = {type: 'EntityNotFound', code: 404, msg: 'user\'s partner not found.'};
        console.log(err);
        return reject(err);
      }
    })
  }
}

function getUserPartner(partnerId) {
  return new Promise((resolve, reject) => {
    if (partnerId) {
      return PartnerCtrl.getPartnerByPartnerId(partnerId)
        .then(partner => {
          return resolve(partner);
        })
        .catch(err => {
          console.log('Error in user.controller.getUserPartnerByEmailAndOrg 1');
          return reject(err);
        })
    } else {
      console.log('Error in user.controller.getUserPartnerByEmailAndOrg 2');
      let err = {type: 'EntityNotFound', code: 404, msg: 'partnerId not found.'};
      console.log(err);
      return reject(err);
    }
  });
}

/**
 * Get list of users
 * restriction: 'admin'
 */
export function index(req, res) {
  return User.find({}, '-salt -password').exec()
    .then(users => {
      res.status(200).json(users);
    })
    .catch(handleError(res));
}

/**
 * Creates a new user
 */
export function create(req, res, next) {
  var newUser = new User(req.body);
  newUser.activationToken = 'abcd';
  newUser.provider = 'local';
  newUser.role = 'user';
  newUser.save()
    .then(function(user) {
      var token = jwt.sign({_id: user._id}, config.secrets.session, {
        expiresIn: 60 * 60 * 5
      });
      res.json({token});
    })
    .catch(validationError(res));
}

function getThisUser(user) {
  return new Promise((resolve, reject) => {
    return resolve(user);
  })
}

function generateInviteLink(org, email, token, partnerId = null) {
  return new Promise((resolve, reject) => {
    console.log(email);
    // return getUserPartner(partnerId)
    //   .then(partner => {
    let postData = {
      dynamicLinkInfo: {
        dynamicLinkDomain: (org.firebase.domain).replace(/(^\w+:|^)\/\//, ''),
        // link: `${config.userRegisterDomain}?email=${email}&token=${token}&org=${org._id}&partner=${partner._id}`,
        link: `${config.userRegisterDomain}?email=${email}&token=${token}`,
        androidInfo: {
          androidPackageName: org.apps.android
        },
        iosInfo: {
          iosBundleId: org.apps.ios,
          iosAppStoreId: org.apps.iosAppStoreId
        }
      }
    };
    console.log("generateInviteLink data:");
    console.log(JSON.stringify(postData));
    let url = `${config.firebaseURL}?key=${org.firebase.apiKey}`;
    let options = {
      method: 'post',
      body: postData,
      json: true,
      url: url
    };
    console.log('options:' + JSON.stringify(options));

    return requestretry(options, function(err, res, body) {
      console.log('The number of request attempts for generating invitelink: ' + res.attempts);
      console.log(body);
      if (!err && res.statusCode === 200 && body && body.shortLink && body.shortLink.length) {
        return resolve(body);
      }
      console.log('Error in user.controller.generateInviteLink 1');
      let error = {type: 'GenerateInviteLinkError', code: 400, msg: 'Error in Generate Invite Links'};
      return reject(new Error(error));
    });
    // })
    // .catch(err => {
    //   console.log('Error in user.controller.generateInviteLink 2');
    //   return reject(new Error(err));
    // });
  })
}

function sendWebhookRequest(user) {
  return new Promise((resolve, reject) => {
    return PartnerCtrl.getFullPartnerByUserId(user._id)
      .then(partner => {
        let data = JSON.parse(JSON.stringify(user));
        return OrgCtrl.getOrgDataById(partner.organization)
          .then(org => {
            delete data.salt;
            delete data.password;
            delete data.partner;
            delete data.organization;
            delete data.upgrade;
            delete data.token;
            let webhookData = {
              event: 'user_onboarded',
              payload: {
                ...data,
                partner: partner,
                org: org
              }
            };
            wh.sendWebhookRequest('webhook1', webhookData);
            return resolve(user);
          })
          .catch(err => {
            console.log('Error in user.controller.sendWebhookRequest 1');
            return reject(err);
          })
      })
      .catch(err => {
        console.log('Error in user.controller.sendWebhookRequest 2');
        return reject(err);
      })
  });
}

/**
 * Creates a new user
 */
export function createNewUser(org, data, creator, apiKey, partnerId) {
  return new Promise((resolve, reject) => {
    var newUser = new User(data);
    let token = randomstring.generate({
      length: 4,
      capitalization: 'lowercase'
    });
    newUser.activationToken = token;
    newUser.provider = 'local';
    if (creator) {
      newUser.createdBy = creator._id;
    }
    if (apiKey) {
      newUser.createdByApiKey = apiKey;
    }
    return generateInviteLink(org, data.email, token, partnerId)
      .then(links => {
        if (links && links.shortLink && links.shortLink.length) {
          newUser.inviteLink = links.shortLink;
        }
        newUser.save()
          .then((user) => {
            return resolve({user: user, token: token, links: links});
          })
          .catch(err => {
            console.log('Error in user.controller.createNewUser 1');
            return reject(err);
          });
      })
      .catch(err => {
        console.log('Error in user.controller.createNewUser 2');
        return reject(err);
      })
  });
}


function checkIfHasPasswordEmailTemplateAndReturnPartner(userId, partner) {
  return new Promise((resolve, reject) => {
    if (partner) {
      if (!config.sendgrid.sendgridIdForPassword) {
        console.log('Error in user.controller.checkIfHasPasswordEmailTemplateAndReturnPartner 1');
        let error = {type: "SendgridTemplateErr", code: 422, msg: "Sendgrid Template Id is not set to Send Password Activation Email."};
        return reject(error);  //DO NOT Make it New Error object
      }
      return Mailer.checkSendGridId(config.sendgrid.sendgridIdForPassword)
        .then(() => {
          return resolve(partner);
        })
        .catch(err => {
          console.log('Error in user.controller.checkIfHasPasswordEmailTemplateAndReturnPartner 1');
          let error = {type: "SendgridTemplateErr", code: 422, msg: "Invalid Sendgrid Template Id to Send Password Activation Email."};
          return reject(error);  //DO NOT Make it New Error object
        })
    } else {
      return PartnerCtrl.getFullPartnerAndOrgByUserId(userId)
        .then(_partner => {
          if (_partner && _partner.organization && config.sendgrid.sendgridIdForPassword) {
            return Mailer.checkSendGridId(config.sendgrid.sendgridIdForPassword)
              .then(() => {
                delete _partner.allUsers;
                return resolve(_partner);
              })
              .catch(err => {
                console.log('Error in user.controller.checkIfHasPasswordEmailTemplateAndReturnPartner 3');
                let error = 'Invalid Sendgrid Template Id to Send Password Activation Email.';
                return reject(new Error(error));
              })
          } else {
            console.log('Error in user.controller.checkIfHasPasswordEmailTemplateAndReturnPartner 4');
            let err = 'Invalid Sendgrid Template Id to Send Password Activation Email.';
            return reject(new Error(err));
          }
        })
        .catch(err => {
          console.log('Error in user.controller.checkIfHasPasswordEmailTemplateAndReturnPartner 2');
          return reject(err);
        })
    }
  })
}

function sendWebhookRequestIfInvited(user, accepted) {
  return new Promise((resolve, reject) => {
    if (!accepted) {
      console.log('Sending Webhook request for Invited user');
      return sendWebhookRequest(user)
        .then(usr => {
          return resolve(usr);
        })
        .catch(err => {
          console.log('Error in user.controller.sendWebhookRequestIfInvited 1');
          return reject(err);
        })
    } else {
      console.log('Skipping Webhook request for Already Accpted user');
      return resolve(user);
    }
  })
}

/**
 * Activates a new user
 */
export function activateUser(InactiveUser, password, partner = null, accepted = false) {
  return new Promise((resolve, reject) => {
    InactiveUser.password = password;
    InactiveUser.active = true;
    InactiveUser.status = 'Accepted';
    InactiveUser.activationToken = null;
    InactiveUser.updatedAt = moment();
    if (!accepted) {
      InactiveUser.acceptedDate = moment();
    }

    return checkIfHasPasswordEmailTemplateAndReturnPartner(InactiveUser._id, partner)
      .then(_partner => {
        return User.findById(InactiveUser._id).exec()
          .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'User not found.'}))
          .then(saveUpdates(InactiveUser))
          .then(user => {
            return sendWebhookRequestIfInvited(user, accepted)
              .then(_user => {
                let userData = {
                  user: InactiveUser
                };
                userData.user.password = password;
                userData.organization = _partner.organization;
                userData.partner = _partner;
                return sendPasswordEmail(userData)
                  .then(data => {
                    return resolve(data);
                  })
                  .catch(err => {
                    console.log('Error in user.controller.activateUser 4');
                    return reject(err);
                  });
              })
              .catch(err => {
                console.log('Error in user.controller.activateUser 2');
                return reject(err);
              });
          })
          .catch(err => {
            console.log('Error in user.controller.activateUser 1');
            return reject(err);
          });
      })
      .catch(err => {
        console.log('Error in user.controller.activateUser 6');
        return reject(err);
      });
  });
}

/**
 * Updates phone
 */
export function updatePhone(userData) {
  return new Promise((resolve, reject) => {
    return User.findOne({phone: userData.phone, email: {$ne: userData.email}})
      .then(found => {
        if (found) {
          console.log('Error in user.controller.updatePhone 1');
          let err = {type: 'AlreadyExists', code: 409, msg: 'Phone number already exists'};
          return reject(new Error(err));
        } else {
          return User.findOne({email: userData.email})
            .exec()
            .then(user => {
              return new Promise((resolve, reject) => {
                if (!user) {
                  return reject(new Error({type: 'EntityNotFound', code: 404, msg: 'User not found.'}));
                } else {
                  return resolve(user);
                }
              })
            })
            .then(saveUpdates(userData))
            .then(user => {
              return resolve(user);
            })
            .catch(err => {
              console.log('Error in user.controller.updatePhone 2');
              return reject(new Error({type: 'EntityNotFound', code: 404, msg: 'User not found.'}));
            });
        }
      })
      .catch(err => {
        console.log('Error in user.controller.updatePhone 3');
        return reject(err);
      });
  });
}

export function getUsers() {
  return function(users) {
    return new Promise((resolve, reject) => {
      return User.find({_id: {$in: users}})
        .select('status active createdAt acceptedDate')
        .lean()
        .exec()
        .then(users => {
          return resolve(users);
        })
        .catch(err => {
          console.log('Error in user.controller.getUsers 1');
          return reject(err);
        });
    });
  }
}

/**
 * Get a single user
 */
export function show(req, res, next) {
  var userId = req.params.id;

  return User.findById(userId).exec()
    .then(user => {
      if (!user) {
        return res.status(404).end();
      }
      res.json(user.profile);
    })
    .catch(err => next(err));
}

/**
 * Deletes a user
 * restriction: 'admin'
 */
export function destroy(req, res) {
  return User.findByIdAndRemove(req.params.id).exec()
    .then(function() {
      res.status(204).end();
    })
    .catch(handleError(res));
}

/**
 * Change a users password
 */
export function changePassword(req, res, next) {
  var userId = req.user._id;
  var oldPass = String(req.body.oldPassword);
  var newPass = String(req.body.newPassword);

  return User.findById(userId).exec()
    .then(user => {
      if (user.authenticate(oldPass)) {
        user.password = newPass;
        return user.save()
          .then(() => {
            res.status(204).end();
          })
          .catch(validationError(res));
      } else {
        return res.status(403).end();
      }
    });
}

/**
 * Get my info
 */
export function me(req, res, next) {
  var userId = req.user._id;
  return User.findOne({_id: userId}, '-salt -password').exec()
    .then(user => { // don't ever give out the password or salt
      if (!user) {
        console.log('Error in user.controller.me 1');
        let err = {type: 'UNAUTHORIZED', code: 401, msg: 'Not Authorized'};
        return ResponseHelpers.respondWithCustomError(res, err);
      }
      let response = {provider: user.provider, name: user.name, email: user.email, onboard: user.onboard, type: user.type, _id: user._id, active: user.active};
      return PartnerCtrl.getUsersPartner(user._id)
        .then(partner => {
          response.partner = {_id: partner._id, name: partner.name};
          response.organization = req.params.orgid;
          return ResponseHelpers.respondWithCustomResult(res, response, 200);
        })
        .catch(error => {
          console.log('Error in user.controller.me 2');
          let err = {type: 'UNAUTHORIZED', code: 401, msg: error.message || 'Not Authorized'};
          return ResponseHelpers.respondWithCustomError(res, err);
        });
    })
    .catch(err => next(err));
}

/**
 * Get User info
 */
export function getUser(userId) {
  return new Promise((resolve, reject) => {
    return User.findOne({_id: userId}, '-salt -password').exec()
      .then(user => { // don't ever give out the password or salt
        if (!user) {
          console.log('Error in user.controller.getUser 1');
          let err = {type: 'NotFound', code: 404, msg: 'User not Found'};
          return ResponseHelpers.respondWithCustomError(res, err);
        }
        let response = {provider: user.provider, name: user.name, email: user.email, onboard: user.onboard, type: user.type, _id: user._id, active: user.active};
        return PartnerCtrl.getUsersPartner(user._id)
          .then(partner => {
            response.partner = {_id: partner._id, name: partner.name};
            response.organization = partner.organization;
            console.log(partner);
            return OrgCtrl.getOrgById(partner.organization)
              .then(org => {
                response.sendgridId = org.sendgridId;
                return resolve(response);
              })
              .catch(error => {
                console.log('Error in user.controller.getUser 4');
                return reject(error);
              });
          })
          .catch(error => {
            console.log('Error in user.controller.getUser 2');
            return reject(error);
          });
      })
      .catch(error => {
        console.log('Error in user.controller.getUser 3');
        return reject(error);
      });
  });
}

/**
 * Authentication callback
 */
export function authCallback(req, res, next) {
  res.redirect('/');
}


// Updates an existing Device in the DB
export function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  if (req.body.email) {
    delete req.body.email;
  }
  if (req.body.onboard) {
    delete req.body.onboard;
  }
  if (req.body.type) {
    delete req.body.type;
  }
  if (req.body.role) {
    delete req.body.role;
  }
  if (req.body.password) {
    delete req.body.password;
  }
  if (req.body.provider) {
    delete req.body.provider;
  }
  if (req.body.activationToken) {
    delete req.body.activationToken;
  }
  if (req.body.salt) {
    delete req.body.salt;
  }
  if (req.body.facebook) {
    delete req.body.facebook;
  }
  if (req.body.twitter) {
    delete req.body.twitter;
  }
  if (req.body.google) {
    delete req.body.google;
  }
  if (req.body.github) {
    delete req.body.github;
  }
  if (req.body.status) {
    delete req.body.status;
  }
  if (req.body.createdBy) {
    delete req.body.createdBy;
  }
  if (req.body.createdAt) {
    delete req.body.createdAt;
  }
  req.body.updatedBy = req.user._id;
  req.body.updatedAt = moment();
  if (req.user._id == req.params.id) {
    return ResponseHelpers.respondWithCustomError(res, {type: 'ActionNotPermitted', code: 403, msg: 'User cannot deactivate self.'});
  }
  return PartnerCtrl.getPartnersByUserId(req.user._id)
    .then(partners => {
      let allowedOrgs = partners.map(partner => {
        return partner.organization.toString();
      });
      return User.findById(req.params.id)
        .select('_id provider name email onboard type role updatedAt updatedBy createdAt active status')
        .exec()
        .then(user => {
          return PartnerCtrl.getPartnersByUserId(user._id)
            .then(_partners => {
              let userOrgs = _partners.map(_p => {
                return _p.organization.toString();
              });
              let userExistsInAdminsOrg = userOrgs.filter(org => allowedOrgs.indexOf(org) > -1);
              if (userExistsInAdminsOrg.length) {
                return user;
              }
              return false;
            })
            .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'User not found in Organization'}))
            .then(saveUpdates(req.body))
            .then(deactivateAllDevicesIfUserDeactivated())
            .then(ResponseHelpers.respondWithResult(res))
            .catch(ResponseHelpers.respondWithErrorEntity(res));
        })
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

function generateNewToken() {
  return function(entity) {
    return new Promise((resolve, reject) => {
      if (entity && entity.generateNew) {
        return resolve({
          newToken: true,
          token: randomstring.generate({
            length: 4,
            capitalization: 'lowercase'
          })
        });
      } else {
        return resolve({newToken: false, token: entity.token});
      }
    });
  }
}

function getExistingToken() {
  return function(user) {
    return new Promise((resolve, reject) => {
      if (user.activationToken && user.activationToken.length > 15) {
        return resolve({generateNew: true});
      }
      return resolve({generateNew: false, token: user.decrypt(user.activationToken)});
    });
  }
}

function updateUserValues(user, admin) {
  return function(activationToken) {
    return new Promise((resolve, reject) => {
      user.activationToken = activationToken;
      user.status = 'Invited';
      user.active = true;
      user.onboard = true;
      user.updatedAt = moment();
      user.updatedBy = admin;
      user.save()
        .then(updated => {
          let userData = {partner: user.partner, token: activationToken, user: updated};
          return resolve(userData);
        })
        .catch(err => {
          console.log('Error in user.controller.updateUserValues');
          return reject(err);
        })
    });
  };
}

export function collectFullData(user, org, partner) {
  return function(usr) {
    return new Promise((resolve, reject) => {
      return PartnerCtrl.getFullPartner(partner, org)
        .then(_partner => {
          let fullData = {
            payload: {
              ...user,
              partner: _partner,
              org: _partner.organization
            }
          };
          return resolve({fulldata: fullData, data: user});
        })
        .catch(err => {
          console.log('Error in user.controller.collectFullData');
          return reject(err);
        })
    });
  };
}

export function activateDeactivateSingleUser(user, active, by) {
  return new Promise((resolve, reject) => {
    return User.findOne({_id: user})
      .then(_user => {
        _user.active = active;
        _user.updatedAt = moment();
        if (by) {
          _user.updatedBy = by;
        }
        _user.save()
          .then(updated => {
            return resolve(updated);
          })
          .catch(err => {
            console.log('Error in user.controller.activateDeactivateSingleUser 1');
            return reject(err);
          })
      })
      .catch(err => {
        console.log('Error in user.controller.activateDeactivateSingleUser 2');
        return reject(err);
      })
  });
}

function getToken(token) {
  return new Promise((resolve, reject) => {
    return resolve(token);
  })
}

/*Resend User token*/
export function resendUserToken(req, res) {
  if (req.user._id == req.params.id) {
    return ResponseHelpers.respondWithCustomError(res, {type: 'ActionNotPermitted', code: 403, msg: 'User cannot resend token to self.'});
  }

  return User.findOne({_id: req.params.id})
    .then(user => {
      if (user && user.status && user.status == 'Accepted') {
        return ResponseHelpers.respondWithCustomError(res, {type: 'ActionNotPermitted', code: 403, msg: 'User has already accepted the invite.'});
      }
      return Organization.findOne({_id: req.params.orgid})
        .then(org => {
          if (org.sendgridId) {
            return Mailer.checkSendGridId(org.sendgridId)
              .then(() => {
                return PartnerCtrl.getPartnersByUserId(req.user._id)
                  .then(partners => {
                    let allowedOrgs = partners.map(partner => {
                      return partner.organization.toString();
                    });
                    return User.findById(req.params.id)
                      .select('_id provider name email onboard type role updatedAt updatedBy createdAt active status inviteLink')
                      .exec()
                      .then(user => {
                        return PartnerCtrl.getPartnersByUserId(user._id)
                          .then(_partners => {
                            let userOrgs = _partners.map(_p => {
                              return _p.organization.toString();
                            });
                            let userExistsInAdminsOrg = (req.user.role == 'superadmin') ? [true] : (userOrgs.filter(org => allowedOrgs.indexOf(org) > -1));
                            if (userExistsInAdminsOrg.length) {
                              user.partner = _partners[0];
                              return user;
                            }
                            return false;
                          })
                          .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'User not found in Organization'}))
                          .then(getExistingToken())
                          .then(generateNewToken())
                          .then(token => {
                            if (token.newToken) {
                              return generateInviteLink(org, user.email, token.token, user.partner.partnerId)
                                .then(links => {
                                  if (links && links.shortLink && links.shortLink.length) {
                                    user.inviteLink = links.shortLink;
                                  }
                                  return getToken(token.token)
                                    .then(updateUserValues(user, req.user._id))
                                    .then(OrgCtrl.sendEmailIfOnboard(org))
                                    .then(ResponseHelpers.respondWithResult(res))
                                    .catch(ResponseHelpers.respondWithErrorEntity(res))
                                })
                                .catch(err => {
                                  console.log('Error in user.controller.createNewUser 2');
                                  return reject(err);
                                })
                            } else {
                              return getToken(token.token)
                                .then(updateUserValues(user, req.user._id))
                                .then(OrgCtrl.sendEmailIfOnboard(org))
                                .then(ResponseHelpers.respondWithResult(res))
                                .catch(ResponseHelpers.respondWithErrorEntity(res))
                            }
                          })
                          .catch(ResponseHelpers.respondWithErrorEntity(res))
                      })
                      .catch(ResponseHelpers.respondWithErrorEntity(res));
                  })
                  .catch(ResponseHelpers.respondWithErrorEntity(res));
              })
              .catch(ResponseHelpers.respondIfSendgridError(res));
          } else {
            let err = {type: 'SendGridError', code: 422, msg: 'Organization doesn\'t have sendgridId'};
            return ResponseHelpers.respondWithCustomError(res, err)
          }
        })
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

/*Resend User SMS*/
export function resendInviteSMS(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}, {value: req.params.partnerId, type: 'partner'}, {value: req.params.id, type: 'user'}])
    .then(() => {
      if (req.user && req.user._id === req.params.id) {
        return ResponseHelpers.respondWithCustomError(res, {type: 'ActionNotPermitted', code: 403, msg: 'User cannot resend SMS to self.'});
      }
      let toValidate = ['phone', 'token'];
      let stripEntities = ['username', 'salt', 'activationToken', '__v', 'createdBy', 'updatedBy', 'createdAt', 'updatedAt', 'password'];
      return sanitizeEntities(req.body, toValidate)
        .then(validateEntities(toValidate))
        .then(data => {
          return User.findOne({_id: req.params.id})
            .then(ResponseHelpers.handleEntityNotFound({type: 'UserNotFound', code: 404, msg: 'User not found.'}))
            .then(_user => {
              if (_user && _user.status === 'Accepted') {
                return ResponseHelpers.respondWithCustomError(res, {type: 'ActionNotPermitted', code: 403, msg: 'User already Accepted the invite.'});
              }
              return User.findOne({phone: data.phone, _id: {$ne: _user._id}})
                .then(user => {
                  if (user) {
                    return ResponseHelpers.respondWithCustomError(res, {type: 'AlreadyExists', code: 409, msg: 'Phone number already exists.'});
                  } else {
                    if (_user && _user.activationToken && _user.activationToken !== _user.encrypt(data.token)) {
                      let err = {type: 'InvalidToken', code: 422, msg: 'Invalid Token'};
                      console.log('Error in user.controller.resendInviteSMS 1');
                      console.log(err);
                      return ResponseHelpers.respondWithCustomError(res, err);
                    }
                    _user.phone = data.phone;
                    let activationToken = null;
                    if (_user.activationToken) {
                      activationToken = _user.decrypt(_user.activationToken);
                    }
                    return _user.save()
                      .then(collectFullData(_user, req.params.orgid, req.params.partnerId))
                      .then(OrgCtrl.sendSMSIfOnboard(activationToken))
                      .then(ResponseHelpers.stripResults(stripEntities))
                      .then(ResponseHelpers.respondWithResult(res))
                      .catch(ResponseHelpers.respondWithErrorEntity(res));
                  }
                })
                .catch(ResponseHelpers.respondWithErrorEntity(res));
            })
            .catch(ResponseHelpers.respondWithErrorEntity(res));
        })
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

function getUniqueArrayOfUsers() {
  return function(partners) {
    return new Promise((resolve, reject) => {
      if (partners && partners.length) {
        let allUsers = _.uniq(_.flatten(partners.map(partner => {
          if (partner.allUsers && partner.allUsers.length) {
            return partner.allUsers;
          }
          return [];
        })));
        return resolve(allUsers);
      } else {
        return resolve([]);
      }
    })
  }
}

export function sendPasswordEmail(data) {
  return new Promise((resolve, reject) => {
    let userData = JSON.parse(JSON.stringify(data));
    let emailOptions = {
      to: userData.user.email,
      from: config.sendEmail.from,
      subject: `${userData.organization.name} Account Activated`,
      data: {
        name: userData.user.name,
        email: userData.user.email,
        role: userData.user.role,
        type: userData.user.type,
        partner: userData.partner,
        organization: userData.organization,
        password: userData.user.password
      },
      sendgridId: config.sendgrid.sendgridIdForPassword,
      template: 'tmp1'
    };
    console.log('EMAIL OPTIONS:');
    console.log(JSON.stringify(emailOptions));
    return Mailer.sendMail(emailOptions)
      .then(user => {
        if (config.debugAPI) {
          userData.user.debug = {password: userData.user.password};
        }
        delete userData.user.password;
        return resolve(userData.user);
      })
      .catch(err => {
        console.log('Error in organization.controller.sendPasswordEmail 1');
        return reject(err);
      })
  });
}

/*Resend Bulk User token*/
export function resendBulkUserToken(req, res) {
  return PartnerCtrl.getAllUsersOfPartner(req.params.orgid)
    .then(getUniqueArrayOfUsers())
    .then(allUsers => {
      return Organization.findOne({_id: req.params.orgid})
        .then(org => {
          if (org.sendgridId) {
            return Mailer.checkSendGridId(org.sendgridId)
              .then(() => {
                let emails = [];
                return async.mapSeries(allUsers, function(usr, cb) {
                  console.log(usr);
                  return User.findOne({_id: usr})
                    .select('_id provider name email onboard type role updatedAt updatedBy createdAt active status inviteLink')
                    .exec()
                    .then(_user => {
                      if (!_user) {
                        cb();
                      }
                      if (_user && _user.status && _user.status === 'Accepted') {
                        cb();
                      } else if (_user && _user.status && _user.status === 'Invited') {
                        return PartnerCtrl.getPartnersByUserId(_user._id)
                          .then(partners => {
                            let allowedOrgs = partners.map(partner => {
                              return partner.organization.toString();
                            });
                            return PartnerCtrl.getPartnersByUserId(_user._id)
                              .then(_partners => {
                                let userOrgs = _partners.map(_p => {
                                  return _p.organization.toString();
                                });
                                let userExistsInAdminsOrg = (req.user.role === 'superadmin') ? [true] : (userOrgs.filter(org => allowedOrgs.indexOf(org) > -1));
                                if (userExistsInAdminsOrg.length) {
                                  emails.push({_id: _user._id, email: _user.email});
                                  _user.partner = _partners[0];
                                  return _user;
                                }
                                return false;
                              })
                              .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'User not found in Organization'}))
                              .then(getExistingToken())
                              .then(generateNewToken())
                              .then(token => {
                                if (token.newToken) {
                                  return generateInviteLink(org, _user.email, token.token, _user.partner.partnerId)
                                    .then(links => {
                                      if (links && links.shortLink && links.shortLink.length) {
                                        _user.inviteLink = links.shortLink;
                                      }
                                      return getToken(token.token)
                                        .then(updateUserValues(_user, req.user._id))
                                        .then(OrgCtrl.sendEmailIfOnboard(org))
                                        .then(ok => {
                                          cb();
                                        })
                                        .catch(err => {
                                          cb(err);
                                        })
                                    })
                                    .catch(err => {
                                      console.log('Error in user.controller.resendBulkUserToken 2');
                                      cb(err);
                                    })
                                } else {
                                  return getToken(token.token)
                                    .then(updateUserValues(_user, req.user._id))
                                    .then(OrgCtrl.sendEmailIfOnboard(org))
                                    .then(ok => {
                                      cb();
                                    })
                                    .catch(err => {
                                      cb(err);
                                    })
                                }
                              })
                              .catch(err => {
                                cb(err);
                              })
                          })
                          .catch(err => {
                            cb(err);
                          })
                      }
                    })
                    .catch(err => {
                      cb(err);
                    });

                }, function(error, done) {
                  if (error) {
                    console.log('Error in user.controller.resendBulkUserToken 1');
                    console.log(error);
                    let err = {type: 'ResendTokenError', code: 500, msg: error};
                    return ResponseHelpers.respondWithCustomError(res, err);
                  }
                  return ResponseHelpers.respondWithCustomResult(res, {reInvitedUsers: emails})
                })
              })
              .catch(ResponseHelpers.respondIfSendgridError(res));
          }
        })
        .catch(ResponseHelpers.respondWithErrorEntity(res))
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res))
}


/*Get user by Email*/
export function getUserByEmail(email) {
  return new Promise((resolve, reject) => {
    return User.findOne({email: email, role: 'superadmin'})
      .then(user => {
        return resolve(user);
      })
      .catch(err => {
        console.log('Error in user.controller.getUserByEmail');
        return reject(err);
      })
  })
}

/*Get user by Email*/
export function getUserByOnlyEmail(email) {
  return new Promise((resolve, reject) => {
    return User.findOne({email: email})
      .then(user => {
        return resolve(user);
      })
      .catch(err => {
        console.log('Error in user.controller.getUserByOnlyEmail');
        return reject(err);
      })
  })
}

/*Get user by Email*/
export function getWholeUserByEmail(email, _token = null) {
  return new Promise((resolve, reject) => {
    return User.findOne({email: email.toLowerCase()})
      .then(user => {
        if (!user) {
          let err = {type: 'EntityNotFound', code: 404, msg: 'User with email and/or token does not exists'};
          console.log('Error in user.controller.getWholeUserByEmail 1');
          console.log(err);
          return reject(err);
        }
        if (user && user.activationToken && user.activationToken !== user.encrypt(_token)) {
          let err = {type: 'EntityNotFound', code: 404, msg: 'User with email and/or token does not exists'};
          console.log('Error in user.controller.getWholeUserByEmail 2');
          console.log(err);
          return reject(err);
        }
        return resolve(user);
      })
      .catch(err => {
        console.log('Error in user.controller.getWholeUserByEmail 3');
        return reject(err);
      })
  })
}

/*Get user by ID*/
export function getUserByID(id) {
  return new Promise((resolve, reject) => {
    return User.findOne({_id: id})
      .then(user => {
        return resolve(user);
      })
      .catch(err => {
        console.log('Error in user.controller.getUserByID');
        return reject(err);
      })
  })
}

/*Get users by IDs*/
export function getUsersByIds(ids) {
  return new Promise((resolve, reject) => {
    return User.find({_id: {$in: ids}, type: {$ne: 'Apple'}})
      .select('_id')
      .then(user => {
        return resolve(user);
      })
      .catch(err => {
        console.log('Error in user.controller.getUsersByIds');
        return reject(err);
      })
  })
}

export function getInviteLinkBySearchKey(searchKey) {
  return new Promise((resolve, reject) => {
    return User.find({inviteLink: new RegExp(searchKey)})
      .select('inviteLink')
      .then(users => {
        return resolve(users);
      })
      .catch(err => {
        console.log('Error in user.controller.getInviteLinkBySearchKey');
        return reject(err);
      })
  })
}

//Sueruser can set User's Password or mark as Accepted
export function setPassword(req, res) {
  if (!req.body.password) {
    let err = {type: 'InvalidParams', code: 422, msg: 'password required'};
    return ResponseHelpers.respondWithCustomError(res, err);
  }
  if (req.body.password && !req.body.password.length) {
    let err = {type: 'InvalidParams', code: 422, msg: 'password is required.'};
    return ResponseHelpers.respondWithCustomError(res, err);
  }

  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}, {value: req.params.partnerId, type: 'partner'}, {value: req.params.id, type: 'user'}])
    .then(() => {
      let toValidate = ['password'];
      let stripEntities = ['username', 'salt', 'activationToken', '__v', 'createdBy', 'updatedBy', 'createdAt', 'updatedAt'];
      return sanitizeEntities(req.body, toValidate)
        .then(validateEntities(toValidate))
        .then(data => {
          return PartnerCtrl.getPartnerAndOrgAndUserByOrgAndUserId(req.params.orgid, req.params.id)
            .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'User not found in org/partner.'}))
            .then(partner => {
              let _u = partner.allUsers.filter(u => u._id.toString() === req.params.id);
              if (_u[0].status === 'Removed') {
                let err = {type: 'UserRemoved', code: 409, msg: `User has status ${_u[0].status}. Cannot change password/status.`};
                console.log('Error in user.controller.setPassword 1');
                console.log(err);
                return ResponseHelpers.respondWithCustomError(res, err);
              }

              delete partner.allUsers;

              return User.findById(req.params.id)
                .lean()
                .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'User not found.'}))
                .then(inActiveUser => {
                  inActiveUser.updatedBy = req.user._id;
                  return activateUser(inActiveUser, req.body.password, partner, _u[0].status === 'Accepted')
                    .then(ResponseHelpers.stripResults(stripEntities))
                    .then(ResponseHelpers.respondWithResult(res))
                    .catch(ResponseHelpers.respondWithErrorEntity(res));
                })
                .catch(ResponseHelpers.respondWithErrorEntity(res));
            })
            .catch(ResponseHelpers.respondWithErrorEntity(res));
        })
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}


function forcefulTokenGeneration() {
  return function(user) {
    return new Promise((resolve, reject) => {
      return resolve({generateNew: true});
    });
  }
}


/*Update Invites*/
export function updateInvites(req, res) {
  return PartnerCtrl.getAllUsersOfPartner(req.params.orgid)
    .then(getUniqueArrayOfUsers())
    .then(allUsers => {
      return Organization.findOne({_id: req.params.orgid})
        .then(org => {
          let emails = [];
          let itr = 0;
          let t = 1;
          return async.mapSeries(allUsers, function(usr, cb) {
            if (itr % 99 == 0) {
              t = 110 * 1000;
              itr = 0;
              console.log(`Script paused for ${t / 1000} seconds`);
            } else {
              t = 1;
            }
            setTimeout(function() {
              itr++;
              console.log(usr);
              return User.findOne({_id: usr})
                .select('_id provider name email onboard type role updatedAt updatedBy createdAt active status inviteLink')
                .exec()
                .then(_user => {
                  if (!_user) {
                    cb();
                  }
                  if (_user && _user.status && _user.status === 'Accepted') {
                    cb();
                  } else if (_user && _user.status && _user.status === 'Invited') {
                    return PartnerCtrl.getPartnersByUserId(_user._id)
                      .then(partners => {
                        let allowedOrgs = partners.map(partner => {
                          return partner.organization.toString();
                        });
                        return PartnerCtrl.getPartnersByUserId(_user._id)
                          .then(_partners => {
                            let userOrgs = _partners.map(_p => {
                              return _p.organization.toString();
                            });
                            // let userExistsInAdminsOrg = (req.user.role === 'superadmin') ? [true] : (userOrgs.filter(org => allowedOrgs.indexOf(org) > -1));
                            // if (userExistsInAdminsOrg.length) {
                            emails.push({_id: _user._id, email: _user.email});
                            _user.partner = _partners[0];
                            return _user;
                            // }
                            // return false;
                          })
                          .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'User not found in Organization'}))
                          .then(forcefulTokenGeneration())
                          .then(generateNewToken())
                          .then(token => {
                            return generateInviteLink(org, _user.email, token.token, _user.partner.partnerId)
                              .then(links => {
                                if (links && links.shortLink && links.shortLink.length) {
                                  _user.inviteLink = links.shortLink;
                                }
                                return getToken(token.token)
                                  .then(updateUserValues(_user, undefined))
                                  .then(ok => {
                                    cb();
                                  })
                                  .catch(err => {
                                    cb(err);
                                  })
                              })
                              .catch(err => {
                                console.log('Error in user.controller.updateInvites 2');
                                cb(err);
                              })
                          })
                          .catch(err => {
                            cb(err);
                          })
                      })
                      .catch(err => {
                        cb(err);
                      })
                  }
                })
                .catch(err => {
                  cb(err);
                });
            }, t);
          }, function(error, done) {
            if (error) {
              console.log('Error in user.controller.updateInvites 1');
              console.log(error);
              let err = {type: 'ResendTokenError', code: 500, msg: error};
              return ResponseHelpers.respondWithCustomError(res, err);
            }
            console.log('FORCEFUL INVITES');
            console.log(JSON.stringify({reInvitedUsers: emails}));
            return ResponseHelpers.respondWithCustomResult(res, {reInvitedUsers: emails})
          })
        })
        .catch(ResponseHelpers.respondWithErrorEntity(res))
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res))
}

export function getAllUsersInOrg(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}])
    .then(() => {
      return PartnerCtrl.getAllUsersOfPartner(req.params.orgid)
        .then(getUniqueArrayOfUsers())
        .then(ResponseHelpers.respondWithResult(res))
        .catch(ResponseHelpers.respondWithErrorEntity(res))
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

export function resendBulkInvites(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}])
    .then(() => {
      let errors = [];
      let success = [];
      return OrgCtrl.getOrgDataById(req.params.orgid)
        .then(org => {
          if (!org.sendgridId) {
            let err = {type: 'SendGridError', code: 422, msg: 'Organization doesn\'t have sendgridId'};
            return ResponseHelpers.respondWithCustomError(res, err);
          }
          return PartnerCtrl.getAllUsersOfPartner(req.params.orgid)
            .then(getUniqueArrayOfUsers())
            .then(allUsers => {
              return async.mapSeries(allUsers, function(userId, cb) {
                return User.findById(userId)
                  .then(user => {
                    if (!user) {
                      console.log('Error in user.controller.resendBulkInvites 1');
                      let err = {type: 'EntityNotFound', code: 404, msg: 'User not found in user collection'};
                      errors.push({error: err, user: userId});
                      return cb();
                    } else {
                      if (user && user.status === 'Invited') {
                        return PartnerCtrl.getUsersPartner(user)
                          .then(partner => {
                            return getThisUser(user)
                              .then(getExistingToken())
                              .then(generateNewToken())
                              .then(token => {
                                if (token.newToken) {
                                  return generateInviteLink(org, user.email, token.token)
                                    .then(links => {
                                      if (links && links.shortLink && links.shortLink.length) {
                                        user.inviteLink = links.shortLink;
                                      }
                                      return getToken(token.token)
                                        .then(updateUserValues(user, req.user._id))
                                        .then(OrgCtrl.sendEmailIfOnboard(org))
                                        .then(OrgCtrl.bypassFakeWebhookRequest(org, partner.partnerId))
                                        .then(OrgCtrl.sendSMSIfOnboard(token.token))
                                        .then(result => {
                                          let data = {user: user};
                                          if (result.hasOwnProperty('smsError') && result.smsError) {
                                            data.smsError = result.smsError;
                                          }
                                          if (result.hasOwnProperty('smsSent') && result.smsSent) {
                                            data.smsSent = result.smsSent;
                                          }
                                          return cb();
                                        })
                                        .catch(err => {
                                          console.log('Error in user.controller.resendBulkInvites 2');
                                          errors.push({error: err, user: userId});
                                          return cb();
                                        })
                                    })
                                    .catch(err => {
                                      console.log('Error in user.controller.resendBulkInvites 3');
                                      errors.push({error: err, user: userId});
                                      return cb();
                                    })
                                } else {
                                  return getToken(token.token)
                                    .then(updateUserValues(user, req.user._id))
                                    .then(OrgCtrl.sendEmailIfOnboard(org))
                                    .then(OrgCtrl.bypassFakeWebhookRequest(org, partner.partnerId))
                                    .then(OrgCtrl.sendSMSIfOnboard(token.token))
                                    .then(result => {
                                      let data = {user: user};
                                      if (result.hasOwnProperty('smsError') && result.smsError) {
                                        data.smsError = result.smsError;
                                      }
                                      if (result.hasOwnProperty('smsSent') && result.smsSent) {
                                        data.smsSent = result.smsSent;
                                      }
                                      success.push(data);
                                      return cb();
                                    })
                                    .catch(err => {
                                      console.log('Error in user.controller.resendBulkInvites 4');
                                      errors.push({error: err, user: userId});
                                      return cb();
                                    })
                                }
                              })
                          })
                          .catch(err => {
                            console.log('Error in user.controller.resendBulkInvites 5');
                            errors.push({error: err, user: userId});
                            return cb();
                          })
                      } else {
                        console.log('User already Accepted. Skipping user: ' + userId);
                        return cb();
                      }
                    }
                  })
                  .catch(err => {
                    console.log('Error in user.controller.resendBulkInvites 6');
                    console.log(err);
                    errors.push({error: err, user: userId});
                    return cb();
                  })
              }, function(err, done) {
                if (err) {
                  let error = {type: 'InternalError', code: 500, msg: err};
                  return ResponseHelpers.respondWithCustomError(res, error);
                } else {
                  console.log(JSON.stringify({count: {errors: errors.length, success: success.length}, errors: errors, success: success}));
                  return ResponseHelpers.respondWithCustomResult(res, {count: {errors: errors.length, success: success.length}, errors: errors, success: success});
                }
              });
            })
            .catch(ResponseHelpers.respondWithErrorEntity(res))
        })
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res))
}
