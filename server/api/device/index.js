'use strict';

var express = require('express');
var controller = require('./device.controller');
import * as logger from '../../components/logger.service';
import * as auth from '../../auth/auth.service';

var router = express.Router();

module.exports = router;
