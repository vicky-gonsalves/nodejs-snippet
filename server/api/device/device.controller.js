'use strict';

import _ from 'lodash';
import Device from './device.model';
const validator = require('validator');
const Promise = require('bluebird');
const ResponseHelpers = require('../../components/responseHelpers/responseHelpers');
Promise.config({
  // Enables all warnings except forgotten return statements.
  warnings: {
    wForgottenReturn: false
  }
});

function saveUpdates(updates) {
  return function(entity) {
    var updated = _.merge(entity, updates);
    return updated.save()
      .then(updated => {
        return updated;
      });
  };
}

function removeEntity() {
  return function(entity) {
    return new Promise((resolve, reject) => {
      if (entity) {
        return entity.remove()
          .then(() => {
            return resolve();
          })
          .catch(err => {
            console.log('Error in device.controller.removeEntity 1');
            return reject(err);
          })
      } else {
        console.log('Error in device.controller.removeEntity 2');
        return reject({type: 'EntityNotFound', code: 404, msg: 'Device not found'});
      }
    });
  };
}

export function saveUserRef(did, userId) {
  return new Promise((resolve, reject) => {
    return Device.findOne({did: did})
      .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Device not found.'}))
      .then(saveUpdates({user: userId}))
      .then(device => {
        return resolve(device);
      })
      .catch(err => {
        console.log('Error in device.controller.saveUserRef 1');
        return reject(err);
      })
  })
}

export function getUsersIn(users) {
  return new Promise((resolve, reject) => {
    return Device.find({user: {$in: users}})
      .select('dpushtoken dos')
      .then(devices => {
        return resolve(devices);
      })
      .catch(err => {
        console.log('Error in device.controller.getUsersIn 1');
        return reject(err);
      })
  })
}

export function getAllUsersDevices(users) {
  return new Promise((resolve, reject) => {
    return Device.find({user: {$in: users}})
      .select('-__v')
      .then(devices => {
        return resolve(devices);
      })
      .catch(err => {
        console.log('Error in device.controller.getAllUsersDevices 1');
        return reject(err);
      })
  })
}

export function getDevicesByUserId(userId) {
  return new Promise((resolve, reject) => {
    return Device.find({user: userId})
      .select('-__v')
      .then(devices => {
        return resolve(devices);
      })
      .catch(err => {
        console.log('Error in device.controller.getDevicesByUserId 1');
        return reject(err);
      })
  })
}

export function getDevice(userId, did) {
  return new Promise((resolve, reject) => {
    return Device.findOne({user: userId, did: did})
      .select('-__v')
      .then(devices => {
        return resolve(devices);
      })
      .catch(err => {
        console.log('Error in device.controller.getDevice 1');
        return reject(err);
      })
  })
}

export function getDeviceData(deviceId) {
  return new Promise((resolve, reject) => {
    if (deviceId) {
      return Device.findOne({_id: deviceId})
        .select('dtype dos')
        .then(devices => {
          return resolve(devices);
        })
        .catch(err => {
          console.log('Error in device.controller.getDevice 1');
          return reject(err);
        })
    } else {
      return resolve();
    }
  })
}

export function getDeviceInfoByPushToken(token) {
  return new Promise((resolve, reject) => {
    return Device.findOne({dpushtoken: token})
      .populate('user')
      .lean()
      .exec()
      .then(device => {
        if (device && device.user) {
          delete device.user.password;
          delete device.user.activationToken;
          delete device.user.salt;
        }
        return resolve(device);
      })
      .catch(err => {
        console.log('Error in device.controller.getDeviceInfoByPushToken 1');
        return reject(err);
      })
  })
}

// Gets a list of Devices
export function index(req, res) {
  return Device.find().exec()
    .then(ResponseHelpers.respondWithResult(res))
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Gets a single Device from the DB
export function show(req, res) {
  return Device.findById(req.params.id).exec()
    .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Device not found.'}))
    .then(ResponseHelpers.respondWithResult(res))
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Creates a new Device in the DB
export function create(req, res) {
  return Device.create(req.body)
    .then(ResponseHelpers.respondWithResult(res, 201))
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Updates an existing Device in the DB
export function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return Device.findById(req.params.id).exec()
    .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Device not found.'}))
    .then(saveUpdates(req.body))
    .then(ResponseHelpers.respondWithResult(res))
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Deletes a Device from the DB
export function destroy(req, res) {
  return Device.findById(req.params.id).exec()
    .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Device not found.'}))
    .then(removeEntity(res))
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}
