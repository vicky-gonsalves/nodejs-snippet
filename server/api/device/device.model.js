'use strict';

import mongoose from 'mongoose';
mongoose.Promise = require('bluebird');
import {Schema} from 'mongoose';

var DeviceSchema = new mongoose.Schema({
  dpushtoken: String,
  did: String,
  dtype: String,
  dmake: String,
  dmodel: String,
  dres: String,
  dos: String,
  dosv: String,
  pver: String,
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  lastLoggedInAt: {
    type: Date,
    default: Date.now
  },
  createdAt: {
    type: Date,
    default: Date.now
  }
});

export default mongoose.model('Device', DeviceSchema);
