'use strict';

var app = require('../..');
import request from 'supertest';

var newCoreApp;

describe('CoreApp API:', function() {

  describe('GET /api/coreApps', function() {
    var coreApps;

    beforeEach(function(done) {
      request(app)
        .get('/api/coreApps')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          coreApps = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      coreApps.should.be.instanceOf(Array);
    });

  });

  describe('POST /api/coreApps', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/coreApps')
        .send({
          name: 'New CoreApp',
          info: 'This is the brand new coreApp!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newCoreApp = res.body;
          done();
        });
    });

    it('should respond with the newly created coreApp', function() {
      newCoreApp.name.should.equal('New CoreApp');
      newCoreApp.info.should.equal('This is the brand new coreApp!!!');
    });

  });

  describe('GET /api/coreApps/:id', function() {
    var coreApp;

    beforeEach(function(done) {
      request(app)
        .get('/api/coreApps/' + newCoreApp._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          coreApp = res.body;
          done();
        });
    });

    afterEach(function() {
      coreApp = {};
    });

    it('should respond with the requested coreApp', function() {
      coreApp.name.should.equal('New CoreApp');
      coreApp.info.should.equal('This is the brand new coreApp!!!');
    });

  });

  describe('PUT /api/coreApps/:id', function() {
    var updatedCoreApp;

    beforeEach(function(done) {
      request(app)
        .put('/api/coreApps/' + newCoreApp._id)
        .send({
          name: 'Updated CoreApp',
          info: 'This is the updated coreApp!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedCoreApp = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedCoreApp = {};
    });

    it('should respond with the updated coreApp', function() {
      updatedCoreApp.name.should.equal('Updated CoreApp');
      updatedCoreApp.info.should.equal('This is the updated coreApp!!!');
    });

  });

  describe('DELETE /api/coreApps/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/api/coreApps/' + newCoreApp._id)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when coreApp does not exist', function(done) {
      request(app)
        .delete('/api/coreApps/' + newCoreApp._id)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
