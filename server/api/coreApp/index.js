'use strict';

var express = require('express');
var controller = require('./coreApp.controller');

var router = express.Router();

module.exports = router;
