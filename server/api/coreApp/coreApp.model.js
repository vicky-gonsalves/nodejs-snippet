'use strict';

import mongoose from 'mongoose';

var CoreAppSchema = new mongoose.Schema({
  device: String,
  version: String,
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: Date,
  createdBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  updatedBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  }
});

export default mongoose.model('CoreApp', CoreAppSchema);
