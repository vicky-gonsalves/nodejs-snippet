/**
 * CoreApp model events
 */

'use strict';

import {EventEmitter} from 'events';
import CoreApp from './coreApp.model';
var CoreAppEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
CoreAppEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  CoreApp.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    CoreAppEvents.emit(event + ':' + doc._id, doc);
    CoreAppEvents.emit(event, doc);
  }
}

export default CoreAppEvents;
