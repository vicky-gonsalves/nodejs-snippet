'use strict';

import _ from 'lodash';
import CoreApp from './coreApp.model';

const Promise = require('bluebird');
const validator = require('validator');
const moment = require('moment');
const ResponseHelpers = require('../../components/responseHelpers/responseHelpers');
Promise.config({
  // Enables all warnings except forgotten return statements.
  warnings: {
    wForgottenReturn: false
  }
});

function sanitizeEntities(data, toValidate = []) {
  return new Promise((resolve, reject)=> {
    try {
      let sanitizedData = JSON.parse(JSON.stringify(data));
      if (data.device && toValidate.indexOf('device') > -1) {
        sanitizedData.device = validator.trim(data.device.toLowerCase());
      }
      if (data.version && toValidate.indexOf('version') > -1) {
        sanitizedData.version = validator.trim(data.version.toLowerCase());
      }
      return resolve(sanitizedData);
    } catch (e) {
      console.log('Error in CoreApp.controller.sanitizeEntities 1');
      return reject(e);
    }
  })
}

function validateEntities(toValidate = [], update = false) {
  return function(data) {
    return new Promise((resolve, reject)=> {
      if (data) {
        let errors = {type: 'InvalidParams', code: 422, msg: []};
        //Validation
        if (toValidate.indexOf('version') > -1) {
          if (!update && (!(data.version) || validator.isNull(data.version))) {
            errors.msg.push({version: 'version is required'});
          } else if (data.version && !validator.isLength(data.version, {min: 1, max: 20})) {
            errors.msg.push({version: 'version must be between 1 to 20 characters long'});
          }
        }
        if (toValidate.indexOf('device') > -1) {
          if (!update && (!(data.device) || validator.isNull(data.device))) {
            errors.msg.push({device: 'device is required'});
          } else if ((data.device) && !validator.isIn(data.device, ['android', 'ios'])) {
            errors.msg.push({device: 'device can be `android` or `ios`'});
          }
        }
        if (errors.msg.length > 0) {
          console.log('Error in CoreApp.controller.validateEntities 1');
          return reject(errors);
        } else {
          return resolve(data);
        }
      } else {
        console.log('Error in CoreApp.controller.validateEntities 2');
        let err = {type: 'EntityNotFound', code: 422, msg: 'Entity not found.'};
        return reject(err);
      }
    });
  }
}

function validateCoreVersion(update = false) {
  return function(data) {
    return new Promise((resolve, reject)=> {
      if (update && data && !data.core) {
        return resolve(data);
      }
      return CoreAppCtrl.checkIfCoreVersionExists(data.core, data.device)
        .then(isValid => {
          if (isValid) {
            return resolve(true);
          } else {
            console.log('Error in CoreApp.controller.validateCoreVersion 1');
            let err = {type: 'EntityNotFound', code: 422, msg: 'Core Version not found or its not compatible with device.'};
            return reject(err);
          }
        })
        .catch(err=> {
          console.log('Error in CoreApp.controller.validateCoreVersion 2');
          return reject(err);
        });
    });
  }
}

function saveUpdates(updates) {
  return function(entity) {
    let updated = _.extend(entity, updates, (a, b)=> {
      if (_.isArray(a)) {
        return b;
      }
    });
    return updated.save()
      .then(updated => {
        return updated;
      });
  };
}

function removeEntity() {
  return function(entity) {
    return new Promise((resolve, reject)=> {
      if (entity) {
        return entity.remove()
          .then(() => {
            return resolve();
          })
          .catch(err=> {
            console.log('Error in CoreApp.controller.removeEntity 1');
            return reject(err);
          })
      } else {
        console.log('Error in CoreApp.controller.removeEntity 2');
        return reject({type: 'EntityNotFound', code: 404, msg: 'CoreApp not found'});
      }
    });
  };
}

function checkIfExists(device, version) {
  return new Promise((resolve, reject)=> {
    if (device && version) {
      let qry = {device: device.toLowerCase(), version: version.toLowerCase()};
      return CoreApp.find(qry).count()
        .then(count=> {
          return resolve(count > 0);
        })
        .catch(err=> {
          console.log('Error in CoreApp.controller.checkIfExists 1');
          return reject(err);
        });
    } else {
      return resolve(false);
    }
  })
}

function checkIfExistsForUpdate(device, version, id) {
  return new Promise((resolve, reject)=> {
    let qry = {};
    if (device || version) {
      if (id) {
        qry = {
          _id: {
            $ne: id
          }
        };
        if (device && device.length) {
          qry.device = device.toLowerCase();
        }
        if (version && version.length) {
          qry.version = version.toLowerCase();
        }
      }
      return CoreApp.find(qry).lean().exec()
        .then(versions=> {
          versions.forEach(_version=> {
            if (device && device.length) {
              _version.device = device.toLowerCase();
            }
            if (version && version.length) {
              _version.version = version.toLowerCase();
            }
          });
          let count = _.uniqBy(versions, function(elem) {
            return JSON.stringify(_.pick(elem, ['device', 'version']));
          });
          return resolve(count.length > 0);
        })
        .catch(err=> {
          console.log('Error in CoreApp.controller.checkIfExistsForUpdate 1');
          return reject(err);
        });
    } else {
      return resolve(false);
    }
  })
}

function getVersion(org, code, dos) {
  return new Promise((resolve, reject)=> {
    if (org && code && dos && code.length && dos.length) {
      let qry = {organization: org, code: code.toLowerCase(), device: dos.toLowerCase()};
      return CoreApp.findOne(qry)
        .then(version=> {
          return resolve(version);
        })
        .catch(err=> {
          console.log('Error in CoreApp.controller.getVersion 1');
          return reject(err);
        });
    } else {
      return resolve(false);
    }
  })
}


// Gets a list of CoreApps
export function getCoreVersionsList(req, res) {
  return CoreApp.find().exec()
    .then(ResponseHelpers.respondWithResult(res))
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Gets a CoreApp
export function getSingleCoreVersionsList(req, res) {
  return CoreApp.findOne({_id: req.params.id}).exec()
    .then(ResponseHelpers.respondWithResult(res))
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Gets a single CoreApp from the DB
export function checkIfCoreVersionExists(id, device) {
  return new Promise((resolve, reject)=> {
    return CoreApp.findOne({_id: id, device: device}).exec()
      .then(version=> {
        return resolve(version);
      })
      .catch(err => {
        console.log('Error in CoreApp.controller.checkIfCoreVersionExists 1');
        return reject(err);
      });
  })
}

// Creates a new CoreApp in the DB
export function createCoreVersion(req, res) {
  let toValidate = ['device', 'version'];

  return sanitizeEntities(req.body, toValidate)
    .then(validateEntities(toValidate))
    .then(data=> {
      return checkIfExists(data.device, data.version)
        .then(exists=> {
          if (!exists) {
            if (req.user) {
              data.createdBy = req.user._id;
            }
            return CoreApp.create(data)
              .then(ResponseHelpers.respondWithResult(res, 201))
              .catch(ResponseHelpers.respondWithErrorEntity(res));
          } else {
            let err = {type: 'AlreadyExists', code: 409, msg: 'version already exists.'};
            return ResponseHelpers.respondWithCustomError(res, err);
          }
        })
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Updates an existing CoreApp in the DB
export function updateVersion(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  req.body.updatedAt = moment();

  let toValidate = ['device', 'version'];
  return sanitizeEntities(req.body, toValidate)
    .then(validateEntities(toValidate, true))
    .then(data=> {
      return checkIfExistsForUpdate(data.device || null, data.version || null, req.params.id)
        .then(exists=> {
          if (!exists) {
            if (req.user) {
              data.updatedBy = req.user._id;
            }
            return CoreApp.findOne({_id: req.params.id}).exec()
              .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'CoreApp not found.'}))
              .then(saveUpdates(data))
              .then(ResponseHelpers.respondWithResult(res))
              .catch(ResponseHelpers.respondWithErrorEntity(res));
          } else {
            let err = {type: 'AlreadyExists', code: 409, msg: 'version already exists.'};
            return ResponseHelpers.respondWithCustomError(res, err);
          }
        })
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Deletes a CoreApp from the DB
export function deleteVersion(req, res) {
  return CoreApp.findOne({_id: req.params.id}).exec()
    .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'CoreApp not found.'}))
    .then(removeEntity(res))
    .then(ResponseHelpers.respondWithResult(res, 204))
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}
