'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var coreAppCtrlStub = {
  index: 'coreAppCtrl.index',
  show: 'coreAppCtrl.show',
  create: 'coreAppCtrl.create',
  update: 'coreAppCtrl.update',
  destroy: 'coreAppCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var coreAppIndex = proxyquire('./index.js', {
  'express': {
    Router: function() {
      return routerStub;
    }
  },
  './coreApp.controller': coreAppCtrlStub
});

describe('CoreApp API Router:', function() {

  it('should return an express router instance', function() {
    coreAppIndex.should.equal(routerStub);
  });

  describe('GET /api/coreApps', function() {

    it('should route to coreApp.controller.index', function() {
      routerStub.get
        .withArgs('/', 'coreAppCtrl.index')
        .should.have.been.calledOnce;
    });

  });

  describe('GET /api/coreApps/:id', function() {

    it('should route to coreApp.controller.show', function() {
      routerStub.get
        .withArgs('/:id', 'coreAppCtrl.show')
        .should.have.been.calledOnce;
    });

  });

  describe('POST /api/coreApps', function() {

    it('should route to coreApp.controller.create', function() {
      routerStub.post
        .withArgs('/', 'coreAppCtrl.create')
        .should.have.been.calledOnce;
    });

  });

  describe('PUT /api/coreApps/:id', function() {

    it('should route to coreApp.controller.update', function() {
      routerStub.put
        .withArgs('/:id', 'coreAppCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('PATCH /api/coreApps/:id', function() {

    it('should route to coreApp.controller.update', function() {
      routerStub.patch
        .withArgs('/:id', 'coreAppCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('DELETE /api/coreApps/:id', function() {

    it('should route to coreApp.controller.destroy', function() {
      routerStub.delete
        .withArgs('/:id', 'coreAppCtrl.destroy')
        .should.have.been.calledOnce;
    });

  });

});
