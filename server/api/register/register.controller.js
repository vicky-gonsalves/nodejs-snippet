"use strict";

import * as PartnerCtrl from "../partner/partner.controller";
const ejs = require('ejs');
const ResponseHelpers = require('../../components/responseHelpers/responseHelpers');
const validator = require('validator');
const Promise = require('bluebird');
Promise.config({
  // Enables all warnings except forgotten return statements.
  warnings: {
    wForgottenReturn: false
  }
});

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}


function sanitizeEntities(data, toValidate = []) {
  return new Promise((resolve, reject) => {
    try {
      let sanitizedData = JSON.parse(JSON.stringify(data));
      if (data.email && toValidate.indexOf('email') > -1) {
        sanitizedData.email = validator.trim(data.email);
      }
      if (data.org && toValidate.indexOf('org') > -1) {
        sanitizedData.org = validator.trim(data.org);
      }
      if (data.partner && toValidate.indexOf('partner') > -1) {
        sanitizedData.partner = validator.trim(data.partner);
      }
      if (data.token && toValidate.indexOf('token') > -1) {
        sanitizedData.token = validator.trim(data.token);
      }
      return resolve(sanitizedData);
    } catch (e) {
      console.log('Error in register.controller.sanitizeEntities 1');
      return reject(new Error(e));
    }
  })
}

function validateEntities(toValidate = [], update = false) {
  return function(data) {
    return new Promise((resolve, reject) => {
      if (data) {
        let errors = {type: 'InvalidParams', code: 422, msg: []};
        //Validation
        if (toValidate.indexOf('email') > -1) {
          if (!(data.email) || validator.isNull(data.email)) {
            errors.msg.push({email: 'email is required'});
          } else if (data.email && !validator.isEmail(data.email)) {
            errors.msg.push({email: 'URL doesn\'t have valid email'});
          }
        }
        if (toValidate.indexOf('org') > -1) {
          // if (!(data.org) || validator.isNull(data.org)) {
          //   errors.msg.push({org: 'org is required'});
          // }
          // else
          if (data.org && !validator.isMongoId(data.org)) {
            errors.msg.push({org: 'URL doesn\'t have valid org'});
          }
        }
        if (toValidate.indexOf('partner') > -1) {
          // if (!(data.partner) || validator.isNull(data.partner)) {
          //   errors.msg.push({partner: 'partner is required'});
          // } else
          if (data.partner && !validator.isMongoId(data.partner)) {
            errors.msg.push({partner: 'URL doesn\'t have valid partner'});
          }
        }
        if (toValidate.indexOf('token') > -1) {
          if (!(data.token) || validator.isNull(data.token)) {
            errors.msg.push({token: 'token is required'});
          }
        }
        if (errors.msg.length > 0) {
          console.log('Error in register.controller.validateEntities 1');
          return reject(errors);
        } else {
          return resolve(data);
        }
      } else {
        console.log('Error in register.controller.validateEntities 2');
        let err = {type: 'EntityNotFound', code: 422, msg: 'Entity not found.'};
        return reject(err);
      }
    });
  }
}


export function index(req, res) {
  let email = req.query.email;
  let org = req.query.org;
  let partner = req.query.partner;
  let token = req.query.token;
  let validateIds = [];
  if (org) {
    validateIds.push({value: org, type: 'organization'});
  }
  if (partner) {
    validateIds.push({value: partner, type: 'partner'});
  }

  let toValidate = ['email', 'org', 'partner', 'token'];
  return ResponseHelpers.handleValidObjectIds(validateIds)
    .then(() => {
      return sanitizeEntities(req.query, toValidate)
        .then(validateEntities(toValidate))
        .then(data => {
          return PartnerCtrl.getWholePartnerAndUser(data.org || null, data.partner || null, data.email, token)
            .then(handleEntityNotFound(res))
            .then(partner => {
              let organization = partner.organization;
              let user = partner.user;
              if (organization.hasOwnProperty('landing') && organization.landing && organization.landing.hasOwnProperty('template') && organization.landing.template) {
                res.setHeader('Content-Type', 'text/html');
                let html = ejs.render(organization.landing.template, {user: user, org: organization, partner: partner, token: token});
                res.send(html);
              } else {
                let err = {type: 'EntityNotFound', code: 404, msg: 'Organization doesn\'t have landing template.'};
                return res.status(404).send(err);
              }
            })
            .catch(handleError(res));
        })
        .catch(handleError(res));
    })
    .catch(handleError(res));
}
