'use strict';

import mongoose from 'mongoose';

var LogSchema = new mongoose.Schema({
  info: {},
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  createdAt: {
    type: Date,
    default: Date.now
  }
});

export default mongoose.model('Log', LogSchema);
