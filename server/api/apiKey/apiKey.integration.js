'use strict';

var app = require('../..');
import request from 'supertest';

var newApiKey;

describe('ApiKey API:', function() {
  describe('GET /api/apiKeys', function() {
    var apiKeys;

    beforeEach(function(done) {
      request(app)
        .get('/api/apiKeys')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          apiKeys = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      apiKeys.should.be.instanceOf(Array);
    });
  });

  describe('POST /api/apiKeys', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/apiKeys')
        .send({
          name: 'New ApiKey',
          info: 'This is the brand new apiKey!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newApiKey = res.body;
          done();
        });
    });

    it('should respond with the newly created apiKey', function() {
      newApiKey.name.should.equal('New ApiKey');
      newApiKey.info.should.equal('This is the brand new apiKey!!!');
    });
  });

  describe('GET /api/apiKeys/:id', function() {
    var apiKey;

    beforeEach(function(done) {
      request(app)
        .get(`/api/apiKeys/${newApiKey._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          apiKey = res.body;
          done();
        });
    });

    afterEach(function() {
      apiKey = {};
    });

    it('should respond with the requested apiKey', function() {
      apiKey.name.should.equal('New ApiKey');
      apiKey.info.should.equal('This is the brand new apiKey!!!');
    });
  });

  describe('PUT /api/apiKeys/:id', function() {
    var updatedApiKey;

    beforeEach(function(done) {
      request(app)
        .put(`/api/apiKeys/${newApiKey._id}`)
        .send({
          name: 'Updated ApiKey',
          info: 'This is the updated apiKey!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedApiKey = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedApiKey = {};
    });

    it('should respond with the original apiKey', function() {
      updatedApiKey.name.should.equal('New ApiKey');
      updatedApiKey.info.should.equal('This is the brand new apiKey!!!');
    });

    it('should respond with the updated apiKey on a subsequent GET', function(done) {
      request(app)
        .get(`/api/apiKeys/${newApiKey._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let apiKey = res.body;

          apiKey.name.should.equal('Updated ApiKey');
          apiKey.info.should.equal('This is the updated apiKey!!!');

          done();
        });
    });
  });

  describe('PATCH /api/apiKeys/:id', function() {
    var patchedApiKey;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/apiKeys/${newApiKey._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched ApiKey' },
          { op: 'replace', path: '/info', value: 'This is the patched apiKey!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedApiKey = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedApiKey = {};
    });

    it('should respond with the patched apiKey', function() {
      patchedApiKey.name.should.equal('Patched ApiKey');
      patchedApiKey.info.should.equal('This is the patched apiKey!!!');
    });
  });

  describe('DELETE /api/apiKeys/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/apiKeys/${newApiKey._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when apiKey does not exist', function(done) {
      request(app)
        .delete(`/api/apiKeys/${newApiKey._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
