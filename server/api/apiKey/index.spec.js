'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var apiKeyCtrlStub = {
  index: 'apiKeyCtrl.index',
  show: 'apiKeyCtrl.show',
  create: 'apiKeyCtrl.create',
  upsert: 'apiKeyCtrl.upsert',
  patch: 'apiKeyCtrl.patch',
  destroy: 'apiKeyCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var apiKeyIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './apiKey.controller': apiKeyCtrlStub
});

describe('ApiKey API Router:', function() {
  it('should return an express router instance', function() {
    apiKeyIndex.should.equal(routerStub);
  });

  describe('GET /api/apiKeys', function() {
    it('should route to apiKey.controller.index', function() {
      routerStub.get
        .withArgs('/', 'apiKeyCtrl.index')
        .should.have.been.calledOnce;
    });
  });

  describe('GET /api/apiKeys/:id', function() {
    it('should route to apiKey.controller.show', function() {
      routerStub.get
        .withArgs('/:id', 'apiKeyCtrl.show')
        .should.have.been.calledOnce;
    });
  });

  describe('POST /api/apiKeys', function() {
    it('should route to apiKey.controller.create', function() {
      routerStub.post
        .withArgs('/', 'apiKeyCtrl.create')
        .should.have.been.calledOnce;
    });
  });

  describe('PUT /api/apiKeys/:id', function() {
    it('should route to apiKey.controller.upsert', function() {
      routerStub.put
        .withArgs('/:id', 'apiKeyCtrl.upsert')
        .should.have.been.calledOnce;
    });
  });

  describe('PATCH /api/apiKeys/:id', function() {
    it('should route to apiKey.controller.patch', function() {
      routerStub.patch
        .withArgs('/:id', 'apiKeyCtrl.patch')
        .should.have.been.calledOnce;
    });
  });

  describe('DELETE /api/apiKeys/:id', function() {
    it('should route to apiKey.controller.destroy', function() {
      routerStub.delete
        .withArgs('/:id', 'apiKeyCtrl.destroy')
        .should.have.been.calledOnce;
    });
  });
});
