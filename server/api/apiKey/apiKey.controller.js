'use strict';

import * as config from '../../config/environment';
import _ from 'lodash';
import ApiKey from './apiKey.model';
import ApiKeyCounter from './apiKeyCounter.model';
import * as OrgCtrl from '../organization/organization.controller';
import crypto from 'crypto';
const Promise = require('bluebird');
const Hashids = require('hashids');
const validator = require('validator');
const moment = require('moment');
const ResponseHelpers = require('../../components/responseHelpers/responseHelpers');
Promise.config({
  // Enables all warnings except forgotten return statements.
  warnings: {
    wForgottenReturn: false
  }
});
const algorithm = 'aes-256-ctr';
const password = 'asdas@wew!~)2928343XAM@@@...@@S###@sxas';

function sanitizeEntities(data, toValidate = []) {
  return new Promise((resolve, reject) => {
    try {
      let sanitizedData = JSON.parse(JSON.stringify(data));
      if (data.name && toValidate.indexOf('name') > -1) {
        sanitizedData.name = validator.trim(data.name);
      }
      return resolve(sanitizedData);
    } catch (e) {
      console.log('Error in apiKey.controller.sanitizeEntities 1');
      return reject(e);
    }
  })
}

function validateEntities(toValidate = [], update = false) {
  return function(data) {
    return new Promise((resolve, reject) => {
      if (data) {
        let errors = {type: 'InvalidParams', code: 422, msg: []};
        //Validation
        if (toValidate.indexOf('name') > -1) {
          if (!update && (!(data.name) || validator.isNull(data.name))) {
            errors.msg.push({name: 'name is required'});
          } else if (data.name && !validator.isLength(data.name, {min: 1, max: 155})) {
            errors.msg.push({name: 'name must be between 1 to 155 characters long'});
          }
        }
        if (errors.msg.length > 0) {
          console.log('Error in apiKey.controller.validateEntities 1');
          return reject(errors);
        } else {
          return resolve(data);
        }
      } else {
        console.log('Error in apiKey.controller.validateEntities 2');
        let err = {type: 'EntityNotFound', code: 422, msg: 'Entity not found.'};
        return reject(err);
      }
    });
  }
}

function saveUpdates(updates) {
  return function(entity) {
    let updated = _.extend(entity, updates, (a, b) => {
      if (_.isArray(a)) {
        return b;
      }
    });
    return updated.save()
      .then(updated => {
        return updated;
      });
  };
}

function removeEntity() {
  return function(entity) {
    return new Promise((resolve, reject) => {
      if (entity) {
        return entity.remove()
          .then(() => {
            return resolve();
          })
          .catch(err => {
            console.log('Error in apiKey.controller.removeEntity 1');
            return reject(err);
          })
      } else {
        console.log('Error in apiKey.controller.removeEntity 2');
        return reject({type: 'EntityNotFound', code: 404, msg: 'ApiKey not found'});
      }
    });
  };
}

function getNextApiKeySequence() {
  return new Promise((resolve, reject) => {
    return ApiKeyCounter.findOne({_id: 'apiId'})
      .then(counter => {
        if (!counter) {
          return ApiKeyCounter.create({
            _id: "apiId",
            seq: 1
          })
            .then(ctr => {
              return resolve(ctr.seq);
            })
            .catch(err => {
              console.log('Error in ApiKey.controller.getNextApiKeySequence 1');
              return reject(err);
            })
        } else {
          counter.seq = ++counter.seq;
          return counter.save()
            .then(ctr => {
              return resolve(ctr.seq);
            })
            .catch(err => {
              console.log('Error in ApiKey.controller.getNextApiKeySequence 2');
              return reject(err);
            })
        }
      })
      .catch(err => {
        console.log('Error in ApiKey.controller.getNextApiKeySequence 3');
        return reject(err);
      })
  });
}

function createNewApiKey(apiKeys) {
  return new Promise((resolve, reject) => {
    return ApiKey.create(apiKeys)
      .then(apiKey => {
        return resolve(apiKey);
      })
      .catch(err => {
        console.log('Error in ApiKey.controller.createNewApiKey 1');
        return reject(err);
      })
  })
}

function encryptKey(text) {
  let cipher = crypto.createCipher(algorithm, password);
  let crypted = cipher.update(text, 'utf8', 'hex');
  crypted += cipher.final('hex');
  return crypted;
}

// decrypt(text){
//   let decipher = crypto.createDecipher(algorithm, password);
//   let dec = decipher.update(text, 'hex', 'utf8');
//   dec += decipher.final('utf8');
//   return dec;
// },

export function createApiKeyForOrg(orgid, user) {
  return new Promise((resolve, reject) => {
    return getNextApiKeySequence()
      .then(seq => {
        let key = new Hashids('', 16, 'abcdefghijklmnopqrstuvwxyz0123456789');
        let secret = new Hashids('', 64, 'abcdefghijklmnopqrstuvwxyz0123456789');
        let apiKeys = {};
        apiKeys.organization = orgid;
        apiKeys.key = key.encode(seq);
        apiKeys.secret = secret.encode(Date.now());
        apiKeys.expiry = moment().add(1, 'year');
        if (user && user._id) {
          apiKeys.createdBy = user._id;
        }
        return createNewApiKey(apiKeys)
          .then(key => {
            return resolve(key);
          })
          .catch(err => {
            console.log('Error in ApiKey.controller.createApiKeyForOrg 1');
            console.log(err);
            return reject(err);
          })
      })
      .catch(err => {
        console.log('Error in ApiKey.controller.createApiKeyForOrg 2');
        console.log(err);
        return reject(err);
      })
  })
}

export function getKeySecret(code) {
  return new Promise((resolve, reject) => {
    return OrgCtrl.getOrganizationByCode(code)
      .then(org => {
        return ApiKey.findOne({organization: org._id, expiry: {$gt: moment()}, active: true})
          .select('key secret')
          .exec()
          .then(apiKey => {
            let jsonKey = JSON.stringify(apiKey);
            let encryptedKeySecret = encryptKey(jsonKey);
            return resolve(encryptedKeySecret);
          })
          .catch(err => {
            console.log('Error in ApiKey.controller.getKeySecret 2');
            console.log(err);
            return reject(err);
          })
      })
      .catch(err => {
        console.log('Error in ApiKey.controller.getKeySecret 1');
        console.log(err);
        return reject(err);
      })

  })
}


// Creates a new Organization API key in the DB
export function createAPIKey(req, res) {
  return createApiKeyForOrg(req.params.orgid, req.user)
    .then(ResponseHelpers.respondWithResult(res, 201))
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}
