'use strict';

import mongoose from 'mongoose';

var ApiKeySchema = new mongoose.Schema({
  key: String,
  secret: String,
  organization: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  expiry: Date,
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: Date,
  createdBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  updatedBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  active: {
    type: Boolean,
    default: true
  }
});

export default mongoose.model('ApiKey', ApiKeySchema);
