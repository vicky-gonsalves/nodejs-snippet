/**
 * ApiKey model events
 */

'use strict';

import {EventEmitter} from 'events';
import ApiKey from './apiKey.model';
var ApiKeyEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
ApiKeyEvents.setMaxListeners(0);

// Model events
var events = {
  save: 'save',
  remove: 'remove'
};

// Register the event emitter to the model events
for(var e in events) {
  let event = events[e];
  ApiKey.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    ApiKeyEvents.emit(event + ':' + doc._id, doc);
    ApiKeyEvents.emit(event, doc);
  };
}

export default ApiKeyEvents;
