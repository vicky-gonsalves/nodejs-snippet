'use strict';

var express = require('express');
var controller = require('./partner.controller');
import * as logger from '../../components/logger.service';
import * as auth from '../../auth/auth.service';

var router = express.Router();

router.get('/:id', controller.getNonInvites);

module.exports = router;
