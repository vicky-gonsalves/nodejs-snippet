'use strict';

import mongoose from 'mongoose';
mongoose.Promise = require('bluebird');
import {Schema} from 'mongoose';
const deepPopulate = require('mongoose-deep-populate')(mongoose);

var PartnerSchema = new mongoose.Schema({
  name: String,
  email: {
    type: String,
    lowercase: true
  },
  partnerId: {
    type: String,
    unique: true
  },
  type: String,
  organization: {type: Schema.Types.ObjectId, ref: 'Organization'},
  allUsers: [{
    type: Schema.Types.ObjectId,
    ref: 'User'
  }],
  extended: {}, //these will only update on PATCH calls
  __v: {type: Number, select: false},
  active: {type: Boolean, default: true},
  createdAt: {
    type: Date,
    default: Date.now
  },
  createdByApiKey: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'ApiKey'
  },
  updatedByApiKey: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'ApiKey'
  }
});

// Validate partnerId doesn't exists
PartnerSchema
  .path('partnerId')
  .validate(function(value, respond) {
    var self = this;
    return this.constructor.findOne({partnerId: value}).exec()
      .then(function(partner) {
        if (partner) {
          if (self.id === partner.id) {
            return respond(true);
          }
          return respond(false);
        }
        return respond(true);
      })
      .catch(function(err) {
        throw err;
      });
  }, 'partnerId already exists');

PartnerSchema.plugin(deepPopulate);
export default mongoose.model('Partner', PartnerSchema);
