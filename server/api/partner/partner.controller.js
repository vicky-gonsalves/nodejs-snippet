'use strict';

import _ from 'lodash';
import Partner from './partner.model';
import * as OrgCtrl from '../organization/organization.controller';
import * as DeviceCtrl from '../device/device.controller';
import * as UserCtrl from '../user/user.controller';
import * as User from '../user/user.model';
import * as AudienceLinkCtrl from '../audienceLink/audienceLink.controller';
import * as AudienceCtrl from '../audience/audience.controller';
import Audience from '../audience/audience.model';
import config from "../../config/environment";
const ES = require('../../components/elasticSearch/es');
const request = require('request');
const async = require('async');
const Promise = require('bluebird');
const moment = require('moment');
const ResponseHelpers = require('../../components/responseHelpers/responseHelpers');
const validator = require('validator');
Promise.config({
  // Enables all warnings except forgotten return statements.
  warnings: {
    wForgottenReturn: false
  }
});

function sanitizeEntities(data, toValidate = []) {
  return new Promise((resolve, reject) => {
    try {
      let sanitizedData = JSON.parse(JSON.stringify(data));
      if (data.name && toValidate.indexOf('name') > -1) {
        sanitizedData.name = validator.trim(data.name);
      }
      if (data.email && toValidate.indexOf('email') > -1) {
        sanitizedData.email = validator.normalizeEmail(validator.stripLow(data.email.replace(/[^\x00-\x7F]/g, ""), false), {lowercase: true});
      }
      if (data.partnerId && toValidate.indexOf('partnerId') > -1) {
        sanitizedData.partnerId = validator.trim(data.partnerId);
      }
      if (data.type && toValidate.indexOf('type') > -1) {
        sanitizedData.type = validator.trim(data.type);
      }
      return resolve(sanitizedData);
    } catch (e) {
      console.log('Error in partner.controller.sanitizeEntities 1');
      return reject(e);
    }
  })
}

function validateEntities(toValidate = []) {
  return function(data) {
    return new Promise((resolve, reject) => {
      if (data) {
        let errors = {type: 'InvalidParams', code: 422, msg: []};
        //Validation
        if (toValidate.indexOf('name') > -1) {
          if (!(data.name) || validator.isNull(data.name)) {
            errors.msg.push({name: 'name is required'});
          } else if (data.name && !validator.isLength(data.name, {min: 1, max: 150})) {
            errors.msg.push({name: 'name must be between 1 to 150 characters long'});
          }
        }

        if (toValidate.indexOf('email') > -1) {
          if (!(data.email) || validator.isNull(data.email)) {
            errors.msg.push({email: 'email is not valid'});
          } else if (data.email && !validator.isEmail(data.email)) {
            errors.msg.push({email: 'email is not valid'});
          } else if (data.email && !validator.isLength(data.email, {min: 1, max: 150})) {
            errors.msg.push({email: 'email must be between 1 to 150 characters long'});
          }
        }

        if (toValidate.indexOf('partnerId') > -1) {
          if (!data.partnerId || validator.isNull(data.partnerId)) {
            errors.msg.push({partnerId: 'partnerId is required'});
          } else if (data.partnerId && !validator.isLength(data.partnerId, {min: 1, max: 150})) {
            errors.msg.push({partnerId: 'partnerId must be between 1 to 150 characters long'});
          }
        }

        if (toValidate.indexOf('type') > -1) {
          if (!(data.type) || validator.isNull(data.type)) {
            errors.msg.push({type: 'type is required'});
          } else if (data.type && !validator.isLength(data.type, {min: 1, max: 15})) {
            errors.msg.push({type: 'type must be between 1 to 15 characters long'});
          }
        }

        if (toValidate.indexOf('extended') > -1) {
          if (!data) {
            errors.msg.push({extended: 'extended data is required'});
          } else if (data) {
            let count = 0;
            console.log(data);
            _.each(data, function(value, prop) {
              count++;
            });
            if (!count) {
              errors.msg.push({extended: 'extended data is required'});
            }
          }
        }

        if (errors.msg.length > 0) {
          console.log('Error in partner.controller.validateEntities 1');
          return reject(errors);
        } else {
          return resolve(data);
        }
      } else {
        console.log('Error in partner.controller.validateEntities 2');
        let err = {type: 'EntityNotFound', code: 422, msg: 'Entity not found.'};
        return reject(err);
      }
    });
  }
}

function saveUpdates(updates) {
  return function(entity) {
    var updated = _.merge(entity, updates);
    return updated.save()
      .then(updated => {
        return updated;
      });
  };
}

function extendUpdates(updates) {
  return function(entity) {
    var merge = function() {
      var obj = {},
        i = 0,
        il = arguments.length,
        key;
      for (; i < il; i++) {
        for (key in arguments[i]) {
          if (arguments[i].hasOwnProperty(key)) {
            obj[key] = arguments[i][key];
          }
        }
      }
      return obj;
    };
    var updated = merge(entity.extended, updates);  //First merge all new properties in extended
    var allUpdated = _.extend(entity, {extended: updated}); // then extend all new properties with existing ones
    return allUpdated.save()
      .then(updated => {
        return updated;
      });
  };
}

function validateType(organization, type) {
  return function(data) {
    return new Promise((resolve, reject) => {
      if (!type) {
        return resolve(data);
      }
      return OrgCtrl.getOrgById(organization)
        .then(org => {
          if (org && org.partner_types) {
            let partnerTypes = org.partner_types;
            if (partnerTypes.indexOf(type) < 0) {
              let err = {type: 'InvalidPartnerType', code: 422, msg: 'Invalid partner type'};
              return reject(err);
            } else {
              return resolve(data);
            }
          } else {
            console.log("Error in partner.controller.validateType 1");
            let err = {type: 'InvalidPartnerType', code: 422, msg: 'Invalid partner type'};
            return reject(err);
          }
        })
    })
  };
}

function getOrg(audience) {
  return new Promise((resolve, reject) => {
    return resolve(audience.organization);
  })
}

function updateAudienceLinks(user) {
  return function(entity) {
    return new Promise((resolve, reject) => {
      if (entity && entity._id) {
        let partner = entity._id;
        return AudienceLinkCtrl.getAllLinks(partner)
          .then(links => {
            let audienceIds = _.map(_.uniqBy(links, 'audience'), 'audience');
            if (audienceIds && audienceIds.length) {
              return AudienceCtrl.getAudiences(audienceIds)
                .then(audiences => {
                  console.log('Audiences: ' + JSON.stringify(audiences));
                  return async.each(audiences, function(audience, cb) {
                    return getOrg(audience)
                      .then(ES.perFormEsSearch({query: {filtered: audience.filter}}))
                      .then(AudienceCtrl.updateAudience(audience._id, user))
                      .then(esResult => {
                        if (esResult && !esResult.error && esResult.hits && esResult.hits.hasOwnProperty('total')) {
                          let data = {};
                          data.lastRun = moment();
                          data.count = esResult.hits.total;
                          return Audience.findById(audience._id).exec()
                            .then(AudienceCtrl.saveUpdates(data))
                            .then(() => {
                              return cb();
                            })
                            .catch(err => {
                              console.log("Error in partner.controller.updateAudienceLinks 1");
                              console.log(err);
                              return cb(err);
                            })
                        } else {
                          let err = {type: esResult.error.type, code: esResult.status, msg: esResult};
                          return cb(err);
                        }
                      })
                      .catch(err => {
                        console.log("Error in partner.controller.updateAudienceLinks 2");
                        console.log(err);
                        return cb(err);
                      })
                  }, function(err, done) {
                    if (err) {
                      console.log("Error in partner.controller.updateAudienceLinks 3");
                      console.log(err);
                    }
                    resolve(entity);
                  });
                })
                .catch(err => {
                  console.log("Error in partner.controller.updateAudienceLinks 4");
                  console.log(err);
                  resolve(entity);
                })
            } else {
              console.log(audienceIds);
              resolve(entity);
            }
          })
          .catch(err => {
            console.log("Error in partner.controller.updateAudienceLinks 5");
            console.log(err);
            resolve(entity);
          })
      } else {
        resolve(entity);
      }
    })
  };
}
function updateAllAudienceLinksForcefully(user, orgid) {
  return function(entity) {
    return new Promise((resolve, reject) => {
      if (entity) {
        return AudienceCtrl.getAllAudiences(orgid)
          .then(audiences => {
            // console.log('Audiences: ' + JSON.stringify(audiences));
            return async.mapSeries(audiences, function(audience, cb) {
              console.log('Audience Query:' + JSON.stringify({query: {filtered: audience.filter}}));
              return getOrg(audience)
                .then(ES.perFormEsSearch({query: {filtered: audience.filter}}))
                .then(AudienceCtrl.updateAudience(audience._id, user))
                .then(esResult => {
                  if (esResult && !esResult.error && esResult.hits && esResult.hits.hasOwnProperty('total')) {
                    let data = {};
                    data.lastRun = moment();
                    data.count = esResult.hits.total;
                    return Audience.findById(audience._id).exec()
                      .then(AudienceCtrl.saveUpdates(data))
                      .then(() => {
                        return cb();
                      })
                      .catch(err => {
                        console.log("Error in partner.controller.updateAudienceLinks 1");
                        console.log(err);
                        return cb(err);
                      })
                  } else {
                    let err = {type: esResult.error.type, code: esResult.status, msg: esResult};
                    return cb(err);
                  }
                })
                .catch(err => {
                  console.log("Error in partner.controller.updateAudienceLinks 2");
                  console.log(err);
                  return cb(err);
                })
            }, function(err, done) {
              if (err) {
                console.log("Error in partner.controller.updateAudienceLinks 3");
                console.log(err);
              }
              resolve(entity);
            });
          })
          .catch(err => {
            console.log("Error in partner.controller.updateAudienceLinks 4");
            console.log(err);
            resolve(entity);
          })
      } else {
        resolve(entity);
      }
    })
  };
}

function saveESUpdates(updates) {
  return function(entity) {
    return new Promise((resolve, reject) => {
      if (entity.organization && entity.organization.es_index) {
        let query = {
          query: {
            match: {
              "partner_id": entity.partnerId
            }
          }
        };
        return ES.perFormEsSearchByEsIndex(query, entity.organization.es_index)
          .then(esObj => {
            if (!esObj.error && esObj.hits && esObj.hits.hits && esObj.hits.hits.length && esObj.hits.hits[0]._id) {
              return ES.updateDocument(entity.organization.es_index, esObj.hits.hits[0]._id, updates, entity.partnerId)
                .then(esObj => {
                  return resolve(entity);
                })
                .catch(err => {
                  console.log("Error in partner.controller.saveESUpdates 1");
                  console.log(err);
                  return reject(err);
                });
            } else {
              console.log("Partner Doesn't Exists in ElasticSearch.. Creating New Partner Record in ES");
              return Partner.findOne({_id: entity._id})
                .then(_partner => {
                  var updated = _.merge(_partner, updates);
                  return ES.createDocument(entity.organization.es_index, updated)
                    .then(esObj => {
                      console.log(esObj);
                      return resolve(entity);
                    })
                    .catch(err => {
                      console.log("Error in partner.controller.saveESUpdates 3");
                      return reject(err);
                    })
                });
            }
          })
          .catch(err => {
            console.log("Error in partner.controller.saveESUpdates 2");
            console.log('ES Object NOT Found:');
            return reject(err);
          })
      } else {
        let err = {type: 'es_indexMissing', code: 422, msg: "Organization doesnt have es_index."};
        console.log("Organization doesnt have es_index. Skipping ES update for Partner");
        return reject(err);
      }
    })
  }
}

export function savePartnerInES(updates, skip = false) {
  return function(entity) {
    return new Promise((resolve, reject) => {
      if(!entity && skip){
        return resolve(updates);
      }
      if (entity && entity.organization && entity.organization.es_index) {
        let query = {
          query: {
            match: {
              partner_id: entity.partnerId
            }
          }
        };
        return ES.perFormEsSearchByEsIndex(query, entity.organization.es_index)
          .then(esObj => {
            if (!esObj.error && esObj.hits && esObj.hits.hits && !esObj.hits.hits.length) { //prevents duplicate partners creation on es
              return ES.createDocument(entity.organization.es_index, updates)
                .then(esObj => {
                  return resolve(updates);
                })
                .catch(err => {
                  console.log("Error in partner.controller.savePartnerInES 1");
                  return Partner.findOne({_id: entity._id})
                    .then(removeEntity())
                    .then(removed => {
                      let err = {type: 'ESFail', code: 500, msg: 'Error while saving partner in ElasticSearch'};
                      return reject(err);
                    })
                    .catch(err => {
                      console.log("Error in partner.controller.savePartnerInES 2");
                      return reject(err);
                    });
                })
            } else {
              if (skip) { //for csv import
                return resolve(entity);
              }
              return Partner.findOne({_id: entity._id})
                .then(removeEntity())
                .then(removed => {
                  let err = {type: 'AlradyExists', code: 409, msg: `Partner with partnerId: ${entity.partnerId} already exists in ElasticSearch`};
                  return reject(err);
                })
                .catch(err => {
                  console.log("Error in partner.controller.savePartnerInES 3");
                  return reject(err);
                });
            }
          })
          .catch(err => {
            console.log("Error in partner.controller.savePartnerInES 2");
            return Partner.findOne({_id: entity._id})
              .then(removeEntity())
              .then(removed => {
                let err = {type: 'ESSearchFailed', code: 500, msg: 'Error while searching in ElasticSearch'};
                return reject(err);
              })
              .catch(err => {
                console.log("Error in partner.controller.savePartnerInES 5");
                return reject(err);
              });

          })
      } else {
        console.log("Organization doesnt have es_index. Skipping savePartnerInES for Partner");
        return Partner.findOne({_id: entity._id})
          .then(removeEntity())
          .then(removed => {
            let err = {type: 'ESFail', code: 500, msg: 'Organization doesnt have es_index'};
            return reject(new Error(err));
          })
          .catch(err => {
            console.log("Error in partner.controller.savePartnerInES 6");
            return reject(new Error(err));
          });
      }
    })
  }
}

function removeEntity() {
  return function(entity) {
    return new Promise((resolve, reject) => {
      if (entity) {
        return entity.remove()
          .then(() => {
            return resolve();
          })
          .catch(err => {
            console.log('Error in partner.controller.removeEntity 1');
            return reject(err);
          })
      } else {
        console.log('Error in partner.controller.removeEntity 2');
        return reject({type: 'EntityNotFound', code: 404, msg: 'Organization not found'});
      }
    });
  };
}

export function getCount(data) {
  return new Promise((resolve, reject) => {
    if (data && data.partnerId) {
      return Partner.find({partnerId: {$regex: new RegExp('^' + data.partnerId + '$', 'i')}})  //find case insensitive
        .count()
        .exec()
        .then(count => {
          return resolve(count);
        })
        .catch(err => {
          console.log('Error in partner.controller.getCount 1');
          console.log(err);
          return reject(err);
        });
    } else {
      console.log('Error in partner.controller.getCount 2');
      return reject({type: 'ParamsMissing', code: 422, msg: 'partnerId missing'});
    }
  });
}

function getUserCount(toCount) {
  return function(entity) {
    return new Promise((resolve, reject) => {

      return async.mapSeries(entity, (partner, callback) => {
        let customEntity = JSON.parse(JSON.stringify(partner));
        if (toCount) {
          customEntity.users = partner.allUsers ? partner.allUsers.length : 0;
          delete customEntity.allUsers;
          callback(null, customEntity);
        } else {
          console.log('toCount is false : Skipping count');
          delete customEntity.allUsers;
          callback(null, customEntity);
        }
      }, (err, result) => {
        if (err) {
          console.log('Error in partner.controller.getUserCount 1');
          return reject(err);
        } else {
          return resolve(result);
        }
      });
    });
  }
}

/*Deletes allUsers from array of partners*/
function getFilteredResults() {
  return function(entity) {
    return new Promise((resolve, reject) => {
      if (entity) {
        return async.mapSeries(entity, (partner, callback) => {
          let customEntity = JSON.parse(JSON.stringify(partner));
          delete customEntity.allUsers;
          delete customEntity.__v;
          delete customEntity.active;
          callback(null, customEntity);
        }, (err, result) => {
          if (err) {
            console.log('Error in partner.controller.getFilteredResults 1');
            return reject(err);
          } else {
            return resolve(result);
          }
        });
      } else {
        console.log('Error in partner.controller.getFilteredResults 2');
        return reject({type: 'EntityNotFound', code: 422, msg: 'Entity not found.'});
      }
    });
  }
}

/*Deletes allUsers from single partner*/
export function getFilterResultOne() {
  return function(entity) {
    return new Promise((resolve, reject) => {
      if (entity) {
        let customEntity = JSON.parse(JSON.stringify(entity));
        delete customEntity.allUsers;
        delete customEntity.__v;
        // delete customEntity.active;
        return resolve(customEntity);
      } else {
        console.log('Error in partner.controller.getFilterResultOne 1');
        return reject({type: 'EntityNotFound', code: 422, msg: 'Entity not found.'});
      }
    });
  }
}

/*updates allUsers from single partner with active flag*/
function updateAllUsers(active, by) {
  return function(entity) {
    return new Promise((resolve, reject) => {
      if (entity) {
        let customEntity = JSON.parse(JSON.stringify(entity));
        async.mapSeries(entity.allUsers, function(user, cb) {
          return UserCtrl.activateDeactivateSingleUser(user, active, by)
            .then(UserCtrl.deactivateAllDevicesIfUserDeactivated())
            .then(done => {
              cb();
            })
            .catch(err => {
              cb(err);
            })
        }, function(err, result) {
          if (err) {
            return reject(err);
          }
          return resolve(entity);
        });
      } else {
        console.log('Error in partner.controller.updateAllUsers 3');
        return reject({type: 'EntityNotFound', code: 422, msg: 'Entity not found.'});
      }
    });
  }
}

function saveInDBIfNew(data) {
  return function(count) {
    return new Promise((resolve, reject) => {
      if (!count) {
        if (data) {
          return Partner.create(data)
            .then(partner => {
              return resolve(partner);
            })
            .catch(err => {
              console.log('Error in partner.controller.saveInDBIfNew 1');
              console.log(err);
              return reject(err);
            });
        } else {
          console.log('Error in partner.controller.saveInDBIfNew 2');
          return reject({type: 'ParamsMissing', code: 422, msg: 'Params missing'});
        }
      } else {
        console.log('Error in partner.controller.saveInDBIfNew 3');
        return reject({type: 'AlreadyExists', code: 409, msg: 'partnerId already exists'});
      }
    });
  }
}

export function saveInDBIfNewOrSkip(data) {
  return function(count) {
    return new Promise((resolve, reject) => {
      if (!count) {
        if (data) {
          return Partner.create(data)
            .then(partner => {
              return resolve(partner);
            })
            .catch(err => {
              console.log('Error in partner.controller.saveInDBIfNew 1');
              console.log(err);
              return reject(err);
            });
        } else {
          console.log('Error in partner.controller.saveInDBIfNew 2');
          return reject({type: 'ParamsMissing', code: 422, msg: 'Params missing'});
        }
      } else {
        return resolve(data);
      }
    });
  }
}

export function getTypes() {
  return function(entities) {
    return new Promise((resolve, reject) => {
      if (entities && entities.length) {
        let types = _.uniqBy(entities.filter(_p => _p.type), 'type').map(_pa => _pa.type);
        return resolve(types);
      } else {
        return resolve([]);
      }
    });
  }
}

export function doesUserExists(orgId, email, phone = null) {
  return new Promise((resolve, reject) => {
    return Partner.find({organization: orgId})
      .populate('allUsers')
      .select('allUsers')
      .then(partners => {
        let existingUsers = 0;
        let existingPhones = 0;
        partners.forEach(partner => {
          //check if email exists
          existingUsers += partner.allUsers.filter((doc) => {
            return doc && (doc.email === email);
          }).length;

          if (phone) {
            //check if phone exists
            existingPhones += partner.allUsers.filter((doc) => {
              return doc && (doc.phone === phone);
            }).length;
          }
        });
        return resolve({email: existingUsers, phone: existingPhones});
      })
      .catch(err => {
        console.log('Error in partner.controller.doesUserExists 1');
        console.log(err);
        return reject(err);
      })
  });
}

export function addUser(userData, orgId, partnerId) {
  return new Promise((resolve, reject) => {
    return Partner.findOne({organization: orgId, partnerId: partnerId})
      .then(partners => {
        if (partners) {
          if (!partners.allUsers) {
            partners.allUsers = [];
          }
          partners.allUsers.push(userData.user._id);
          partners.markModified('allUsers');
          partners.save()
            .then(updated => {
              return resolve(userData);
            })
            .catch(err => {
              console.log('Error in partner.controller.addUser 1');
              console.log(err);
              return reject(err);
            })
        } else {
          let err = {type: 'EntityNotFound', code: 404, msg: 'Partner not found.'};
          console.log(err);
          return reject(err);
        }
      })
      .catch(err => {
        console.log('Error in partner.controller.addUser 2');
        console.log(err);
        return reject(err);
      })
  });
}

function getAllUsersDevices(withUserDevices) {
  return function(partner) {
    return new Promise((resolve, reject) => {
      if (withUserDevices) {
        if (partner) {
          if (partner.allUsers && partner.allUsers.length) {
            let allUsers = partner.allUsers.map(user => {
              return user._id.toString();
            });
            return DeviceCtrl.getAllUsersDevices(allUsers)
              .then(devices => {
                if (devices && devices.length) {
                  partner.allUsers.forEach(user => {
                    user.devices = devices.filter(device => device.user.toString() == user._id.toString());
                  });
                  return resolve(partner);
                } else {
                  return resolve(partner);
                }
              })
              .catch(err => {
                console.log('Error in partner.controller.getAllUsersDevices 1');
                console.log(err);
                return reject(err);
              })
          } else {
            return resolve(partner);
          }
        } else {
          console.log('Error in partner.controller.getAllUsersDevices 2');
          let err = {type: 'EntityNotFound', code: 404, msg: 'Partner not found.'};
          console.log(err);
          return reject(err);
        }
      } else {
        return resolve(partner);
      }
    })
  }
}

function getActivationToken() {
  return function(partner) {
    return new Promise((resolve, reject) => {
      if (partner && partner.allUsers && partner.allUsers.length) {
        async.mapSeries(partner.allUsers, function(user, cb) {
          return UserCtrl.getUserByID(user._id)
            .then(_user => {
              user.activationToken = _user.decrypt(user.activationToken);
              cb();
            })
            .catch(err => {
              cb();
            })
        }, function(err, done) {
          return resolve(partner);
        });
      } else {
        return resolve(partner);
      }
    })
  }
}

export function createPartners(partners) {
  return new Promise((resolve, reject) => {
    if (partners && partners.length) {
      async.mapSeries(partners, function(partner, cb) {
        return Partner.create(partner)
          .then(part => {
            cb(null, part);
          })
          .catch(err => {
            console.log('Error in partner.controller.createPartners 1');
            console.log(err);
            cb(err);
          });
      }, function(err, allPartners) {
        if (err) {
          console.log('Error in partner.controller.createPartners 2');
          return reject(err);
        } else {
          return resolve(allPartners);
        }
      });
    } else {
      return resolve(partners);
    }
  });
}

// Gets a list of Organization's Partners
export function getOrgPartners(orgId) {
  return new Promise((resolve, reject) => {
    return Partner.find({organization: orgId, active: true})
      .then(partners => {
        return resolve(partners);
      })
      .catch(err => {
        console.log('Error in partner.controller.getOrgPartners 1');
        console.log(err);
        return reject(err);
      });
  });
}

// Gets a list of Organization's Partners
export function getOrgsAllPartners(orgId) {
  return new Promise((resolve, reject) => {
    return Partner.find({organization: orgId})
      .select('createdAt active')
      .lean()
      .exec()
      .then(partners => {
        return resolve(partners);
      })
      .catch(err => {
        console.log('Error in partner.controller.getOrgsAllPartners 1');
        console.log(err);
        return reject(err);
      });
  });
}

export function getPartnerByUserId(userId) {
  return new Promise((resolve, reject) => {
    return Partner.findOne({allUsers: userId})
      .deepPopulate('organization.androidKey organization.iosKey')
      .exec()
      .then(partner => {
        return resolve(partner);
      })
      .catch(err => {
        console.log('Error in partner.controller.getPartnerByUserId 1');
        console.log(err);
        return reject(err);
      });
  });
}

export function getPartnerByOrgAndUserId(orgId, userId) {
  return new Promise((resolve, reject) => {
    return Partner.findOne({allUsers: userId, organization: orgId})
      .exec()
      .then(partner => {
        return resolve(partner);
      })
      .catch(err => {
        console.log('Error in partner.controller.getPartnerByOrgAndUserId 1');
        console.log(err);
        return reject(err);
      });
  });
}

export function getPartnerAndOrgAndUserByOrgAndUserId(orgId, userId) {
  return new Promise((resolve, reject) => {
    return Partner.findOne({allUsers: userId, organization: orgId})
      .populate('organization allUsers')
      .lean()
      .then(partner => {
        return resolve(partner);
      })
      .catch(err => {
        console.log('Error in partner.controller.getPartnerAndUserByOrgAndUserId 1');
        console.log(err);
        return reject(err);
      });
  });
}

export function getPartnersByUserId(userId) {
  return new Promise((resolve, reject) => {
    return Partner.find({allUsers: userId})
      .then(partner => {
        return resolve(partner);
      })
      .catch(err => {
        console.log('Error in partner.controller.getPartnerByUserId 1');
        console.log(err);
        return reject(err);
      });
  });
}

export function getFullPartnerByPartnerId(partnerId) {
  return new Promise((resolve, reject) => {
    return Partner.findOne({partnerId: partnerId})
      .then(partner => {
        return resolve(partner);
      })
      .catch(err => {
        console.log('Error in partner.controller.getPartnerByPartnerId 1');
        console.log(err);
        return reject(err);
      });
  });
}

export function getFullPartnerAndOrgByUserId(id) {
  return new Promise((resolve, reject) => {
    return Partner.findOne({allUsers: id})
      .populate('organization')
      .lean()
      .then(partner => {
        return resolve(partner);
      })
      .catch(err => {
        console.log('Error in partner.controller.getPartnerByPartnerId 1');
        console.log(err);
        return reject(err);
      });
  });
}

export function getFullPartnerByUserId(userId) {
  return new Promise((resolve, reject) => {
    return Partner.findOne({allUsers: userId})
      .exec()
      .then(partner => {
        return resolve(partner);
      })
      .catch(err => {
        console.log('Error in partner.controller.getFullPartnerByUserId 1');
        console.log(err);
        return reject(err);
      });
  });
}

export function getWholePartnerAndUser(org = null, partner = null, email, _token = null) {
  return new Promise((resolve, reject) => {
    return UserCtrl.getWholeUserByEmail(email, _token)
      .then(_user => {
        console.log('INVITE DATA');
        console.log(email);
        console.log(_user);
        console.log(partner);
        console.log(org);
        let query = {allUsers: _user._id, active: true};
        if (org) {
          query.organization = org;
        }
        if (partner) {
          query._id = partner;
        }
        return Partner.findOne(query)
          .populate('organization')
          .lean()
          .then(partner => {
            if (partner) {
              partner.user = _user;
              return resolve(partner);
            } else {
              return reject({type: 'EntityNotFound', code: 404, msg: 'partner not found'});
            }
          })
          .catch(err => {
            console.log('Error in partner.controller.getWholePartnerAndUser 1');
            console.log(err);
            return reject(err);
          });
      })
      .catch(err => {
        console.log('Error in partner.controller.getWholePartnerAndUser 2');
        console.log(err);
        return reject(err);
      });
  });
}

export function getPartnersByUserIdAndOrg(userId, org) {
  return new Promise((resolve, reject) => {
    return Partner.find({allUsers: userId, organization: org})
      .select('_id')
      .then(partners => {
        return resolve(partners);
      })
      .catch(err => {
        console.log('Error in partner.controller.getPartnersByUserIdAndOrg 1');
        console.log(err);
        return reject(err);
      });
  });
}

export function getPartnersByPartnerIds(partnerIds) {
  return new Promise((resolve, reject) => {
    return Partner.find({partnerId: {$in: partnerIds}})
      .select('_id allUsers')
      .exec()
      .then(partner => {
        return resolve(partner);
      })
      .catch(err => {
        console.log('Error in partner.controller.getPartnersByPartnerIds 1');
        console.log(err);
        return reject(err);
      });
  });
}

export function getPartnerByPartnerId(partnerId) {
  return new Promise((resolve, reject) => {
    console.log('PartnerID: ' + partnerId);
    return Partner.findOne({partnerId: partnerId})
      .select('_id')
      .exec()
      .then(partner => {
        console.log('partner:');
        console.log(partner);
        return resolve(partner);
      })
      .catch(err => {
        console.log('Error in partner.controller.getPartnerByPartnerId 1');
        console.log(err);
        return reject(err);
      });
  });
}

export function getFullPartner(id, org) {
  return new Promise((resolve, reject) => {
    return Partner.findOne({_id: id, organization: org})
      .populate('organization')
      .exec()
      .then(partner => {
        return resolve(partner);
      })
      .catch(err => {
        console.log('Error in partner.controller.getFullPartner 1');
        console.log(err);
        return reject(err);
      });
  });
}

export function getFullPartnerWithoutUsers(id, org) {
  return new Promise((resolve, reject) => {
    return Partner.findOne({_id: id, organization: org})
      .select('-allUsers')
      .populate('organization')
      .exec()
      .then(partner => {
        return resolve(partner);
      })
      .catch(err => {
        console.log('Error in partner.controller.getFullPartner 1');
        console.log(err);
        return reject(err);
      });
  });
}

// Gets a list of Organization's Partners
export function getOrganizationPartners(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}])
    .then(() => {
      let withUsersCount = false;
      if (req.params && req.params.hasOwnProperty('withusers') && req.params.withusers && req.params.withusers === 'withusers') {
        withUsersCount = true;
      }
      let query = {organization: req.params.orgid, active: true};
      if (req.query.audienceid && req.query.audienceid.length) {
        return ResponseHelpers.handleValidObjectIds([{value: req.query.audienceid, type: 'audience'}])
          .then(() => {
            return AudienceCtrl.getAudienceByOrgAndId(req.params.orgid, req.query.audienceid)
              .then(audience => {
                if (!audience) {
                  let err = {type: 'AudienceMissing', code: 422, msg: 'audience is not attached to this organization'};
                  console.log(err);
                  console.log('Error in partner.controller.getOrganizationPartners 1');
                  return ResponseHelpers.respondWithCustomError(res, err);
                }
                if (audience && !audience.organization) {
                  let err = {type: 'OrgMissing', code: 422, msg: 'organization is not attached to this audience'};
                  console.log(err);
                  console.log('Error in partner.controller.getOrganizationPartners 1');
                  return ResponseHelpers.respondWithCustomError(res, err);
                }
                if (audience && audience.organization && !audience.organization.es_index) {
                  let err = {type: 'ESIndexMissing', code: 422, msg: 'Organization doesn\'t have es_index'};
                  console.log(err);
                  console.log('Error in partner.controller.getOrganizationPartners 2');
                  return ResponseHelpers.respondWithCustomError(res, err);
                }
                return ES.perFormEsSearchByEsIndex(audience.filter, audience.organization.es_index)
                  .then(esResult => {
                    console.log(esResult);
                    if (esResult && !esResult.error && esResult.hits && esResult.hits.total) {
                      let partners = _.map(esResult.hits.hits, function(partner) {
                        return partner._source.partner_id;
                      });
                      if (partners && partners.length) {
                        query.partnerId = {$in: partners};
                      }
                      return Partner.find(query)
                        .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Partners not found.'}))
                        .then(getUserCount(withUsersCount))
                        .then(ResponseHelpers.respondWithResult(res))
                        .catch(ResponseHelpers.respondWithErrorEntity(res));
                    } else if (esResult && esResult.error) {
                      let err = {type: esResult.error.type, code: esResult.status, msg: esResult};
                      return ResponseHelpers.respondWithCustomError(res, err);
                    } else if (esResult && !esResult.error && esResult.hits && !esResult.hits.total) {
                      return ResponseHelpers.respondWithCustomResult(res, []);
                    }
                  })
                  .catch(ResponseHelpers.respondWithErrorEntity(res));
              })
          })
          .catch(ResponseHelpers.respondWithErrorEntity(res));
      } else {
        return Partner.find(query)
          .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Partners not found.'}))
          .then(getUserCount(withUsersCount))
          .then(ResponseHelpers.respondWithResult(res))
          .catch(ResponseHelpers.respondWithErrorEntity(res));
      }
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Gets a list of Organization's Partner's Types
export function getAllPartnerTypes(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}])
    .then(() => {
      return Partner.find({organization: req.params.orgid, active: true})
        .select('type')
        .lean()
        .exec()
        .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Partners not found.'}))
        .then(getTypes())
        .then(ResponseHelpers.respondWithResult(res))
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Gets Organization's Single Partner
export function getSingleOrganizationPartner(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}])
    .then(() => {
      return Partner.findOne({organization: req.params.orgid, partnerId: req.params.partnerId})
        .select('_id name partnerId email organization type extended')
        .populate('organization')
        .exec()
        .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Organization/Partner not found.'}))
        .then(ResponseHelpers.respondWithResult(res))
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Gets Partner's users
export function getPartnerUsers(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}, {value: req.params.id, type: 'partner'}])
    .then(() => {
      let withUserDevices = false;
      if (req.query.withDevices && req.query.withDevices == '1') {
        withUserDevices = true;
      }
      return Partner.findOne({organization: req.params.orgid, _id: req.params.id})
        .select('-_id allUsers')
        .populate('allUsers', '-password -salt -createdAt')
        .lean()
        .exec()
        .then(getActivationToken())
        .then(getAllUsersDevices(withUserDevices))
        .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Organization/Partner not found.'}))
        .then(ResponseHelpers.respondWithResult(res))
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Gets a User's Partner
export function getUsersPartner(user) {
  return new Promise((resolve, reject) => {
    return Partner.findOne({allUsers: user})
      .select('_id name organization')
      .exec()
      .then(partner => {
        return resolve(partner);
      })
      .catch(err => {
        console.log('Error in partner.controller.getUsersPartner 1');
        console.log(err);
        return reject(err);
      });
  });
}

// Gets a User's Partner
export function getUsersPartnerByOrgAndUserId(user, org) {
  return new Promise((resolve, reject) => {
    return Partner.findOne({allUsers: user, organization: org})
      .select('_id')
      .exec()
      .then(partner => {
        return resolve(partner);
      })
      .catch(err => {
        console.log('Error in partner.controller.getUsersPartnerByOrgAndUserId 1');
        console.log(err);
        return reject(err);
      });
  });
}

// Gets Organization's all Users
export function getAllUsersOfPartner(orgId) {
  return new Promise((resolve, reject) => {
    return Partner.find({organization: orgId})
      .select('_id allUsers')
      .exec()
      .then(partners => {
        return resolve(partners);
      })
      .catch(err => {
        console.log('Error in partner.controller.getAllUsersOfPartner 1');
        console.log(err);
        return reject(err);
      });
  });
}

// Creates a new Partner in the DB
export function create(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}])
    .then(() => {
      if (req.body.active) {
        delete req.body.active;
      }
      if (req.body.organization) {
        delete req.body.organization;
      }
      if (req.body.allUsers) {
        delete req.body.allUsers;
      }
      if (req.apiKey && req.apiKey._id) {
        req.body.createdByApiKey = req.apiKey._id;
      }

      let params = req.body;
      params.organization = req.params.orgid;

      let toValidate = ['name', 'email', 'partnerId', 'type'];

      return sanitizeEntities(params, toValidate)
        .then(validateEntities(toValidate))
        .then(data => {
          return getCount(data)
            .then(validateType(params.organization, data.type))
            .then(saveInDBIfNew(data))
            .then(data => {
              return Partner.findOne({organization: params.organization, _id: data.id})
                .populate('organization')
                .select('name email partnerId organization type active')
                .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Organization/Partner not found.'}))
                .then(savePartnerInES(data))
                .then(getFilterResultOne())
                .then(updateAllAudienceLinksForcefully(req.user, req.params.orgid))
                .then(ResponseHelpers.respondWithResult(res, 201))
                .catch(ResponseHelpers.respondWithErrorEntity(res));
            })
            // .then(getFilterResultOne())
            // .then(ResponseHelpers.respondWithResult(res, 201))
            .catch(ResponseHelpers.respondWithErrorEntity(res));
        })
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Updates an existing Partner in the DB
export function update(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}])
    .then(() => {
      if (req.body.active) {
        delete req.body.active;
      }
      if (req.body.organization) {
        delete req.body.organization;
      }
      if (req.body.partnerId) {
        delete req.body.partnerId;
      }
      if (req.body.allUsers) {
        delete req.body.allUsers;
      }
      if (req.body._id) {
        delete req.body._id;
      }
      if (req.body.extended) {
        delete req.body.extended;
      }
      if (req.apiKey && req.apiKey._id) {
        req.body.updatedByApiKey = req.apiKey._id;
      }

      let params = req.body;
      let toValidate = ['name', 'email', 'type'];

      return sanitizeEntities(params, toValidate)
        .then(validateEntities(toValidate))
        .then(data => {
          let type = null;
          if (data && data.type) {
            type = data.type;
          }
          return Partner.findOne({organization: req.params.orgid, partnerId: req.params.partnerId})
            .populate('organization')
            .select('name email partnerId organization type active')
            .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Organization/Partner not found.'}))
            .then(validateType(req.params.orgid, type))
            .then(saveESUpdates(data))
            .then(saveUpdates(data))
            .then(getFilterResultOne())
            .then(updateAllAudienceLinksForcefully(req.user, req.params.orgid))
            .then(ResponseHelpers.respondWithResult(res))
            .catch(ResponseHelpers.respondWithErrorEntity(res));
        })
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Patches an existing Partner in the DB
export function patch(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}])
    .then(() => {
      let active = req.body.active;
      let by = null;
      if (req.user) {
        by = req.user;
      }
      if (req.body.extended) {
        delete req.body.extended;
      }
      let type = null;
      if (req.body && req.body.type) {
        type = req.body.type;
      }
      return Partner.findOne({organization: req.params.orgid, partnerId: req.params.partnerId})
        .populate('organization')
        .select('name email partnerId organization type active')
        .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Organization/Partner not found.'}))
        .then(validateType(req.params.orgid, type))
        .then(saveESUpdates(req.body))
        .then(saveUpdates({active: active}))
        .then(updateAllUsers(active, by))
        .then(getFilterResultOne())
        .then(ResponseHelpers.respondWithResult(res))
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Patches an existing Partner in the DB with Extended fields
export function patchExtended(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}, {value: req.params.id, type: 'partner'}])
    .then(() => {
      let params = req.body;
      let toValidate = [];

      return sanitizeEntities(params, toValidate)
        .then(validateEntities(toValidate))
        .then(data => {
          return Partner.findOne({organization: req.params.orgid, _id: req.params.id})
            .populate('organization')
            .select('name email partnerId organization type active extended')
            .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Organization/Partner not found.'}))
            .then(saveESUpdates(data))
            .then(extendUpdates(data))
            .then(getFilterResultOne())
            .then(updateAllAudienceLinksForcefully(req.user, req.params.orgid))
            .then(ResponseHelpers.respondWithResult(res))
            .catch(ResponseHelpers.respondWithErrorEntity(res));
        })
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Deletes a Partner from the DB
export function destroy(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}])
    .then(() => {
      return Partner.findOne({organization: req.params.orgid, partnerId: req.params.partnerId})
        .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Organization/Partner not found.'}))
        .then(removeEntity())
        .then(ResponseHelpers.respondWithResult(res, 204))
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Checks if User exists
export function checkUser(orgId, email) {
  return new Promise((resolve, reject) => {
    return Partner.find({organization: orgId})
      .populate('allUsers')
      .then(partners => {
        let existingUsers = 0;
        partners.forEach(partner => {
          //check if email exists
          existingUsers += partner.allUsers.filter((doc) => {
            return doc && doc.email === email;
          }).length;
        });
        return resolve(existingUsers === 1);
      })
      .catch(err => {
        console.log('Error in partner.controller.checkUser 2');
        console.log(err);
        return reject(err);
      })
  });
}

export function getNonInvites(req, res) {
  return Partner.find({organization: req.params.id})
    .populate('allUsers')
    .exec()
    .then(partners => {
      if (partners) {
        let r = [];
        let a = partners.forEach(partner => {
          partner.allUsers.forEach(user => function(usr) {
            if (!usr.inviteLink) {
              r.push(usr._id);
            }
          })
        });
        console.log('without inviteLink');
        console.log(JSON.stringify(r));
        res.json(r)
      } else {
        res.send('no users found');
      }
    })
    .catch(err => {
      console.log(err);
      res.send(err);
    })
}


// Partner.find({organization: '589c8b585d4c0b001d16fa57'})
//   .populate('allUsers')
//   .exec()
//   .then(users => {
//     var usercount=0;
//     var count=0;
//     users.forEach(partner=>{
//       usercount+=partner.allUsers.length;
//       count+=(partner.allUsers.filter(u=>{
//         return u.phone && u.phone.length && u.status=='Invited';
//       }).length)
//     })
//     console.log(usercount);
//     console.log(count);
//   })
