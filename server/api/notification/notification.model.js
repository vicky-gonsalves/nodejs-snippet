'use strict';

import mongoose from 'mongoose';
const deepPopulate = require('mongoose-deep-populate')(mongoose);

var NotificationSchema = new mongoose.Schema({
  card: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Card'
  },
  users: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  }],
  data: {},
  recipients: [{  //Final recipients who are going to receive the push
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Device'
  }],
  schedule: {
    type: String,
    enum: ['immediate', 'time']
  },
  scheduleTime: {
    type: Date
  },
  status: {
    type: String,
    enum: ['scheduled', 'draft', 'sent', 'failed']
  },
  active: {
    type: Boolean,
    default: true
  },
  date: {
    type: Date,
    default: Date.now
  }
});

var validatePresenceOf = function(value) {
  return value && value.length;
};

NotificationSchema
  .pre('save', function(next) {
    if (!this.isModified('schedule')) {
      return next();
    }
    if (validatePresenceOf(this.schedule) && this.schedule == 'immediate') {
      this.scheduleTime = Date.now();
      next();
    } else {
      next();
    }
  });

NotificationSchema.plugin(deepPopulate, {
  populate: {
    'recipients.user': {
      select: 'email _id name onboard type'
    },
    'users': {
      select: 'email _id name onboard type'
    }
  }
});
export default mongoose.model('Notification', NotificationSchema);
