'use strict';

import _ from "lodash";
import Notification from "./notification.model";
import * as DeviceCtrl from "../device/device.controller";
import * as CardCtrl from "../card/card.controller";
import * as GCMCtrl from "../../components/gcm/gcm.controller";
const Promise = require('bluebird');
const validator = require('validator');
const moment = require('moment');
const ResponseHelpers = require('../../components/responseHelpers/responseHelpers');
Promise.config({
  // Enables all warnings except forgotten return statements.
  warnings: {
    wForgottenReturn: false
  }
});

function sanitizeEntities(data, toValidate = []) {
  return new Promise((resolve, reject) => {
    try {
      let sanitizedData = JSON.parse(JSON.stringify(data));
      if (data.card && toValidate.indexOf('card') > -1) {
        sanitizedData.card = validator.trim(data.card);
      }
      if (data.users && toValidate.indexOf('users') > -1) {
        sanitizedData.users = data.users.map(user => validator.trim(user));
      }
      if (data.schedule && toValidate.indexOf('schedule') > -1) {
        sanitizedData.schedule = validator.trim(data.schedule);
      }
      if (data.scheduleTime && toValidate.indexOf('scheduleTime') > -1) {
        sanitizedData.scheduleTime = validator.trim(data.scheduleTime);
      }
      if (data.status && toValidate.indexOf('status') > -1) {
        sanitizedData.status = validator.trim(data.status);
      }
      if (data.active && toValidate.indexOf('active') > -1) {
        sanitizedData.active = validator.trim(data.active);
      }
      return resolve(sanitizedData);
    } catch (e) {
      console.log('Error in notification.controller.sanitizeEntities 1');
      return reject(e);
    }
  })
}

function validateEntities(toValidate = [], update = false) {
  return function(data) {
    return new Promise((resolve, reject) => {
      if (data) {
        let errors = {type: 'InvalidParams', code: 422, msg: []};
        //Validation
        if (toValidate.indexOf('card') > -1) {
          if (!update && (!(data.card) || validator.isNull(data.card))) {
            errors.msg.push({card: 'card is required'});
          } else if (((data.card) && !validator.isMongoId(data.card))) {
            errors.msg.push({card: 'card id is not valid'});
          }
        }
        if (toValidate.indexOf('users') > -1) {
          if (data.users && data.users.length) {
            data.users.forEach(user => {
              if (!validator.isMongoId(user)) {
                errors.msg.push({users: `${user} user id is not valid`});
              }
            });
          }
        }
        if (toValidate.indexOf('schedule') > -1) {
          if (!update && (!(data.schedule) || validator.isNull(data.schedule))) {
            errors.msg.push({schedule: 'schedule is required'});
          } else if (data.schedule && !validator.isIn(data.schedule, ['immediate', 'time'])) {
            errors.msg.push({schedule: 'Invalid schedule'});
          } else if (data.schedule && validator.isIn(data.schedule, ['time'])) {
            if (!update && (!(data.scheduleTime) || validator.isNull(data.scheduleTime))) {
              errors.msg.push({scheduleTime: 'scheduleTime is required'});
            } else if ((!(data.status) || validator.isNull(data.status))) {
              errors.msg.push({status: 'status is required'});
            } else if (((data.status) && !validator.isIn(data.status, ['scheduled', 'draft']))) {
              errors.msg.push({status: 'Invalid status'});
            }
          }
        }
        if (toValidate.indexOf('scheduleTime') > -1) {
          if (data.scheduleTime && !validator.isDate(data.scheduleTime)) {
            errors.msg.push({scheduleTime: 'scheduleTime must be valid datetime'});
          } else if (data.scheduleTime && validator.isDate(data.scheduleTime) && moment(data.scheduleTime) < moment()) {
            errors.msg.push({scheduleTime: 'scheduleTime cannot be past datetime'});
          }
        }
        if (toValidate.indexOf('status') > -1) {
          if (!update && (!data.schedule && !validator.isIn(data.schedule, ['time'])) && (!(data.status) || validator.isNull(data.status))) {
            errors.msg.push({status: 'status is required'});
          } else if (data.status && (!data.schedule && !validator.isIn(data.schedule, ['time'])) && !validator.isIn(data.status, ['scheduled', 'draft', 'sent', 'failed'])) {
            errors.msg.push({status: 'Invalid status'});
          }
        }

        if (errors.msg.length > 0) {
          console.log('Error in notification.controller.validateEntities 1');
          return reject(errors);
        } else {
          return resolve(data);
        }
      } else {
        console.log('Error in notification.controller.validateEntities 2');
        let err = {type: 'EntityNotFound', code: 422, msg: 'Entity not found.'};
        return reject(err);
      }
    });
  }
}

function saveUpdates(updates) {
  return function(entity) {
    if ((updates.schedule == 'time' && !updates.scheduleTime && entity.scheduleTime && moment(entity.scheduleTime) < moment())) {
      console.log(moment(entity.scheduleTime) < moment());
      return new Promise((resolve, reject) => {
        console.log('Error in notification.controller.saveUpdates 1');
        let err = {type: 'InvalidEntity', code: 422, msg: 'Please set scheduleTime'};
        return reject(err);
      });
    } else {
      var updated = _.merge(entity, updates);
      updated.users = updates.users;
      updated.markModified('users');
      return updated.save()
        .then(updated => {
          return updated;
        });
    }
  };
}

function removeEntity() {
  return function(entity) {
    return new Promise((resolve, reject) => {
      if (entity) {
        return entity.remove()
          .then(() => {
            return resolve();
          })
          .catch(err => {
            console.log('Error in notification.controller.removeEntity 1');
            return reject(err);
          })
      } else {
        console.log('Error in notification.controller.removeEntity 2');
        return reject({type: 'EntityNotFound', code: 404, msg: 'Notification not found'});
      }
    });
  };
}

function getDevice(userId, did) {
  return new Promise((resolve, reject) => {
    if (userId && did) {
      return DeviceCtrl.getDevice(userId, did)
        .then(device => {
          if (device) {
            return resolve(device);
          }
          return reject({type: 'DeviceNotFound', code: 404, msg: 'Device not found'});
        })
        .catch(err => {
          console.log('Error in notification.controller.getDevice 1');
          return reject(err);
        })
    } else {
      console.log('Error in notification.controller.getDevice 2');
      return reject({type: 'EntityNotFound', code: 400, msg: 'userId/did not found'});
    }
  })
}

function getLatestActiveCard(orgId) {
  return new Promise((resolve, reject) => {
    return CardCtrl.getLatestActiveCard(orgId)
      .then(card => {
        return resolve(card);
      })
      .catch(err => {
        console.log('Error in notification.controller.getDummyCard 1');
        return reject(err);
      })
  })
}

// Gets a list of Notifications
export function index(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}])
    .then(() => {
      return Notification.find().exec()
        .then(ResponseHelpers.respondWithResult(res))
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Gets a list of sent Notifications
export function getSentNotifications(org, startDate, endDate) {
  return new Promise((resolve, reject) => {
    return Notification.find({'data.organization._id': org, status: 'sent', "date": {"$gte": startDate, "$lt": endDate}})
      .count()
      .exec()
      .then(notifications => {
        return resolve(notifications);
      })
      .catch(err => {
        console.log('Error in notification.controller.getSentNotifications 1');
        return reject(err);
      })
  })
}

// Gets a single Notification from the DB
export function show(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}, {value: req.params.id, type: 'notification'}])
    .then(() => {
      return Notification.findById(req.params.id).exec()
        .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Notification not found.'}))
        .then(ResponseHelpers.respondWithResult(res))
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Saves a new Notification in the DB
export function saveNotification(notificationData) {
  return new Promise((resolve, reject) => {
    if (notificationData.schedule == 'immediate' && notificationData.scheduleTime) {
      delete notificationData.scheduleTime;  //scheduleTime is set automatically in model
      notificationData.status = 'scheduled';
    }
    return Notification.create(notificationData)
      .then(notification => {
        //TODO Should add instance in notificationStatus db once notification created
        return resolve(notification);
      })
      .catch(err => {
        console.log('Error in notification.controller.saveNotification 1');
        return reject(err);
      })
  })
}

export function getNotificationsByCardId(cardId) {
  return new Promise((resolve, reject) => {
    return Notification.find({card: cardId})
      .select('-_id')
      .deepPopulate('users recipients.user')
      .lean()
      .then(notifications => {
        notifications.forEach(notification => {
          notification.users.forEach(user => {
            delete user.activationToken;
          });
          notification.recipients.forEach(recipient => {
            delete recipient.user.activationToken;
          });
        });
        return resolve(notifications);
      })
      .catch(err => {
        console.log(err);
        return reject(err);
      });
  })
}

// Creates a new Notification in the DB
export function create(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}])
    .then(() => {
      let toValidate = ['card', 'users', 'schedule', 'scheduleTime', 'status'];
      if (req.body.schedule == 'immediate' && req.body.scheduleTime) {
        delete req.body.scheduleTime;  //scheduleTime is set automatically in model
        req.body.status = 'scheduled';
      }
      return sanitizeEntities(req.body, toValidate)
        .then(validateEntities(toValidate))
        .then(data => {
          return Notification.create(data) //TODO Should add instance in notificationStatus db once notification created
            .then(ResponseHelpers.respondWithResult(res, 201))
            .catch(ResponseHelpers.respondWithErrorEntity(res));
        })
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Ping Request for Push Notifications
export function pingPush(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}])
    .then(() => {
      let userId = req.user._id;
      return getDevice(userId, req.params.did)
        .then(device => {
          return getLatestActiveCard(req.params.orgid)
            .then(card => {
              let options = {msg: card, devices: [device]};
              return GCMCtrl.sendNotification(options)
                .then(gcmResponse => {
                  if (gcmResponse.code && gcmResponse.code != 200) {
                    return ResponseHelpers.respondWithCustomError(res, gcmResponse);
                  }
                  console.log('Ping pushed: ' + JSON.stringify(device));
                  console.log('Card: ' + JSON.stringify(card));
                  return ResponseHelpers.respondWithCustomResult(res, gcmResponse);
                })
                .catch(err => {
                  console.log('Error in card.controller.sendNotification 1');
                  return ResponseHelpers.respondWithCustomError(res, err);
                });
            })
            .catch(ResponseHelpers.respondWithErrorEntity(res));
        })
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Updates an existing Notification in the DB
export function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  if (req.body.schedule == 'immediate' && req.body.scheduleTime) {
    delete req.body.scheduleTime;  //scheduleTime is set automatically in model
    req.body.status = 'scheduled';
  }
  req.body.date = moment();
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}, {value: req.params.id, type: 'notification'}])
    .then(() => {
      let toValidate = ['card', 'users', 'schedule', 'scheduleTime', 'status'];
      return sanitizeEntities(req.body, toValidate)
        .then(validateEntities(toValidate, true))
        .then(data => {
          return Notification.findById(req.params.id).exec()
            .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Notification not found.'}))
            .then(saveUpdates(data))
            .then(ResponseHelpers.respondWithResult(res))
            .catch(ResponseHelpers.respondWithErrorEntity(res));
        })
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Deletes a Notification from the DB
export function destroy(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}, {value: req.params.id, type: 'notification'}])
    .then(() => {
      return Notification.findById(req.params.id).exec()
        .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Notification not found.'}))
        .then(removeEntity(res))
        .then(ResponseHelpers.respondWithResult(res, 204))
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}
