'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var organizationAppCtrlStub = {
  index: 'organizationAppCtrl.index',
  show: 'organizationAppCtrl.show',
  create: 'organizationAppCtrl.create',
  update: 'organizationAppCtrl.update',
  destroy: 'organizationAppCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var organizationAppIndex = proxyquire('./index.js', {
  'express': {
    Router: function() {
      return routerStub;
    }
  },
  './organizationApp.controller': organizationAppCtrlStub
});

describe('OrganizationApp API Router:', function() {

  it('should return an express router instance', function() {
    organizationAppIndex.should.equal(routerStub);
  });

  describe('GET /api/organizationApps', function() {

    it('should route to organizationApp.controller.index', function() {
      routerStub.get
        .withArgs('/', 'organizationAppCtrl.index')
        .should.have.been.calledOnce;
    });

  });

  describe('GET /api/organizationApps/:id', function() {

    it('should route to organizationApp.controller.show', function() {
      routerStub.get
        .withArgs('/:id', 'organizationAppCtrl.show')
        .should.have.been.calledOnce;
    });

  });

  describe('POST /api/organizationApps', function() {

    it('should route to organizationApp.controller.create', function() {
      routerStub.post
        .withArgs('/', 'organizationAppCtrl.create')
        .should.have.been.calledOnce;
    });

  });

  describe('PUT /api/organizationApps/:id', function() {

    it('should route to organizationApp.controller.update', function() {
      routerStub.put
        .withArgs('/:id', 'organizationAppCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('PATCH /api/organizationApps/:id', function() {

    it('should route to organizationApp.controller.update', function() {
      routerStub.patch
        .withArgs('/:id', 'organizationAppCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('DELETE /api/organizationApps/:id', function() {

    it('should route to organizationApp.controller.destroy', function() {
      routerStub.delete
        .withArgs('/:id', 'organizationAppCtrl.destroy')
        .should.have.been.calledOnce;
    });

  });

});
