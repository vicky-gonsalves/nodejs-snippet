'use strict';

var app = require('../..');
import request from 'supertest';

var newOrganizationApp;

describe('OrganizationApp API:', function() {

  describe('GET /api/organizationApps', function() {
    var organizationApps;

    beforeEach(function(done) {
      request(app)
        .get('/api/organizationApps')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          organizationApps = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      organizationApps.should.be.instanceOf(Array);
    });

  });

  describe('POST /api/organizationApps', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/organizationApps')
        .send({
          name: 'New OrganizationApp',
          info: 'This is the brand new organizationApp!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newOrganizationApp = res.body;
          done();
        });
    });

    it('should respond with the newly created organizationApp', function() {
      newOrganizationApp.name.should.equal('New OrganizationApp');
      newOrganizationApp.info.should.equal('This is the brand new organizationApp!!!');
    });

  });

  describe('GET /api/organizationApps/:id', function() {
    var organizationApp;

    beforeEach(function(done) {
      request(app)
        .get('/api/organizationApps/' + newOrganizationApp._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          organizationApp = res.body;
          done();
        });
    });

    afterEach(function() {
      organizationApp = {};
    });

    it('should respond with the requested organizationApp', function() {
      organizationApp.name.should.equal('New OrganizationApp');
      organizationApp.info.should.equal('This is the brand new organizationApp!!!');
    });

  });

  describe('PUT /api/organizationApps/:id', function() {
    var updatedOrganizationApp;

    beforeEach(function(done) {
      request(app)
        .put('/api/organizationApps/' + newOrganizationApp._id)
        .send({
          name: 'Updated OrganizationApp',
          info: 'This is the updated organizationApp!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedOrganizationApp = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedOrganizationApp = {};
    });

    it('should respond with the updated organizationApp', function() {
      updatedOrganizationApp.name.should.equal('Updated OrganizationApp');
      updatedOrganizationApp.info.should.equal('This is the updated organizationApp!!!');
    });

  });

  describe('DELETE /api/organizationApps/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/api/organizationApps/' + newOrganizationApp._id)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when organizationApp does not exist', function(done) {
      request(app)
        .delete('/api/organizationApps/' + newOrganizationApp._id)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
