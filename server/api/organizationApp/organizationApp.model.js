'use strict';

import mongoose from 'mongoose';

var OrganizationAppSchema = new mongoose.Schema({
  organization: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Organization'
  },
  code: String,     //version code
  upgrade: String,  //null, optional or force
  device: String,  //android | ios
  notes: String,
  core: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'CoreApp'
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: Date,
  createdBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  updatedBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  }
});

export default mongoose.model('OrganizationApp', OrganizationAppSchema);
