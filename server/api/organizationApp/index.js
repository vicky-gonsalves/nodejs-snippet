'use strict';

var express = require('express');
var controller = require('./organizationApp.controller');

var router = express.Router();

module.exports = router;
