/**
 * OrganizationApp model events
 */

'use strict';

import {EventEmitter} from 'events';
import OrganizationApp from './organizationApp.model';
var OrganizationAppEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
OrganizationAppEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  OrganizationApp.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    OrganizationAppEvents.emit(event + ':' + doc._id, doc);
    OrganizationAppEvents.emit(event, doc);
  }
}

export default OrganizationAppEvents;
