'use strict';

import * as config from '../../config/environment';
import _ from 'lodash';
import OrganizationApp from './organizationApp.model';
import * as OrgCtrl from '../organization/organization.controller';
import * as CoreAppCtrl from '../coreApp/coreApp.controller';
import compose from 'composable-middleware';

const Promise = require('bluebird');
const validator = require('validator');
const moment = require('moment');
const ResponseHelpers = require('../../components/responseHelpers/responseHelpers');
Promise.config({
  // Enables all warnings except forgotten return statements.
  warnings: {
    wForgottenReturn: false
  }
});

function sanitizeEntities(data, toValidate = []) {
  return new Promise((resolve, reject) => {
    try {
      let sanitizedData = JSON.parse(JSON.stringify(data));
      if (data.code && toValidate.indexOf('code') > -1) {
        sanitizedData.code = validator.trim(data.code.toLowerCase());
      }
      if (data.upgrade && toValidate.indexOf('upgrade') > -1) {
        sanitizedData.upgrade = validator.trim(data.upgrade);
      }
      if (data.upgrade && toValidate.indexOf('device') > -1) {
        sanitizedData.device = validator.trim(data.device.toLowerCase());
      }
      if (data.notes && toValidate.indexOf('notes') > -1) {
        sanitizedData.notes = validator.trim(data.notes);
      }
      return resolve(sanitizedData);
    } catch (e) {
      console.log('Error in OrganizationApp.controller.sanitizeEntities 1');
      return reject(e);
    }
  })
}

function validateEntities(toValidate = [], update = false) {
  return function(data) {
    return new Promise((resolve, reject) => {
      if (data) {
        let errors = {type: 'InvalidParams', code: 422, msg: []};
        //Validation
        if (toValidate.indexOf('code') > -1) {
          if (!update && (!(data.code) || validator.isNull(data.code))) {
            errors.msg.push({code: 'code is required'});
          } else if (data.code && !validator.isLength(data.code, {min: 1, max: 20})) {
            errors.msg.push({code: 'code must be between 1 to 20 characters long'});
          }
        }
        if (toValidate.indexOf('notes') > -1) {
          if (!update && (!(data.notes) || validator.isNull(data.notes))) {
            errors.msg.push({notes: 'notes is required'});
          } else if (data.notes && !validator.isLength(data.notes, {min: 1, max: 10000})) {
            errors.msg.push({notes: 'notes must be between 1 to 10000 characters long'});
          }
        }
        if (toValidate.indexOf('upgrade') > -1) {
          if ((data.upgrade) && !validator.isIn(data.upgrade, ['force', 'optional', null])) {
            errors.msg.push({upgrade: 'upgrade can be `force`, `optional` or null'});
          }
        }
        if (toValidate.indexOf('device') > -1) {
          if (!update && (!(data.device) || validator.isNull(data.device))) {
            errors.msg.push({device: 'device is required'});
          } else if ((data.device) && !validator.isIn(data.device, ['android', 'ios'])) {
            errors.msg.push({device: 'device can be `android` or `ios`'});
          }
        }
        if (toValidate.indexOf('core') > -1) {
          if (!update && (!(data.core) || validator.isNull(data.core))) {
            errors.msg.push({core: 'core is required'});
          } else if (data.core && !validator.isMongoId(data.core)) {
            errors.msg.push({core: 'core must be valid mongo id'});
          }
        }
        if (errors.msg.length > 0) {
          console.log('Error in OrganizationApp.controller.validateEntities 1');
          return reject(errors);
        } else {
          return resolve(data);
        }
      } else {
        console.log('Error in OrganizationApp.controller.validateEntities 2');
        let err = {type: 'EntityNotFound', code: 422, msg: 'Entity not found.'};
        return reject(err);
      }
    });
  }
}

function validateCoreVersion(update = false) {
  return function(data) {
    return new Promise((resolve, reject) => {
      if (update && data && !data.core) {
        return resolve(data);
      }
      return CoreAppCtrl.checkIfCoreVersionExists(data.core, data.device)
        .then(isValid => {
          if (isValid) {
            return resolve(data);
          } else {
            console.log('Error in OrganizationApp.controller.validateCoreVersion 1');
            let err = {type: 'EntityNotFound', code: 422, msg: 'Core Version not found or its not compatible with device.'};
            return reject(err);
          }
        })
        .catch(err => {
          console.log('Error in OrganizationApp.controller.validateCoreVersion 2');
          return reject(err);
        });
    });
  }
}

function saveUpdates(updates) {
  return function(entity) {
    let updated = _.extend(entity, updates, (a, b) => {
      if (_.isArray(a)) {
        return b;
      }
    });
    return updated.save()
      .then(updated => {
        return updated;
      });
  };
}

function removeEntity() {
  return function(entity) {
    return new Promise((resolve, reject) => {
      if (entity) {
        return entity.remove()
          .then(() => {
            return resolve();
          })
          .catch(err => {
            console.log('Error in OrganizationApp.controller.removeEntity 1');
            return reject(err);
          })
      } else {
        console.log('Error in OrganizationApp.controller.removeEntity 2');
        return reject({type: 'EntityNotFound', code: 404, msg: 'OrganizationApp not found'});
      }
    });
  };
}

function checkIfExists(org, device, code, id = null) {
  return new Promise((resolve, reject) => {
    if (code && code.length) {
      let qry = {organization: org, code: code.toLowerCase(), device: device};
      if (id) {
        qry = {
          organization: org,
          code: code.toLowerCase(),
          device: device,
          _id: {
            $ne: id
          }
        };
      }
      return OrganizationApp.find(qry).count()
        .then(count => {
          return resolve(count > 0);
        })
        .catch(err => {
          console.log('Error in OrganizationApp.controller.checkIfExists 1');
          return reject(err);
        });
    } else {
      return resolve(false);
    }
  })
}

function getVersion(org, code, dos) {
  return new Promise((resolve, reject) => {
    if (org && code && dos && code.length && dos.length) {
      let qry = {organization: org, code: code.toLowerCase(), device: dos.toLowerCase()};
      return OrganizationApp.findOne(qry)
        .then(version => {
          return resolve(version);
        })
        .catch(err => {
          console.log('Error in OrganizationApp.controller.getVersion 1');
          return reject(err);
        });
    } else {
      return resolve(false);
    }
  })
}

export function checkUpgrade() {
  return compose()
    .use(function(req, res, next) {
      return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}])
        .then(() => {
          if (req.body && req.params.orgid && req.pver) {
            return getVersion(req.params.orgid, req.pver, req.dos)
              .then(version => {
                if (req.dos && (req.dos.toLowerCase() == 'ios' || req.dos.toLowerCase() == 'android')) {
                  if (version) {
                    if (version.upgrade && version.upgrade == 'force') {
                      req.upgrade = 'force';
                      next();
                    } else if (version.upgrade && version.upgrade == 'optional') {
                      req.upgrade = 'optional';
                      next();
                    } else if (!version.upgrade) {
                      req.upgrade = null;
                      next();
                    }
                  } else {
                    let err = {type: 'VersionNotExists', code: 404, msg: 'App version does not exists'};
                    return ResponseHelpers.respondWithCustomError(res, err);
                  }
                } else {
                  req.upgrade = null;
                  next();
                }
              })
              .catch(err => {
                console.log('Error in OrganizationApp.controller.checkUpgrade 1');
                return ResponseHelpers.respondWithCustomError(res, err);
              });
          } else {
            next();
          }
        })
        .catch(err => {
          console.log('Error in OrganizationApp.controller.checkUpgrade 21');
          return ResponseHelpers.respondWithCustomError(res, err);
        });
    });
}

// Gets a list of OrganizationApps
export function getVersionsList(req, res) {
  return OrganizationApp.find({organization: req.params.orgid})
    .populate('core')
    .exec()
    .then(ResponseHelpers.respondWithResult(res))
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Gets a single OrganizationApp from the DB
export function getSingleVersion(org, pver, dos) {
  return new Promise((resolve, reject) => {
    return OrganizationApp.findOne({code: pver, organization: org, device: dos}).exec()
      .then(version => {
        return resolve(version);
      })
      .catch(err => {
        console.log('Error in OrganizationApp.controller.getVersionsList 1');
        return reject(err);
      });
  })
}

// Gets a single OrganizationApp from the DB
export function getSingleVersionById(req, res) {
  return OrganizationApp.findOne({_id: req.params.id, organization: req.params.orgid})
    .populate('core')
    .exec()
    .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'OrganizationApp not found.'}))
    .then(ResponseHelpers.respondWithResult(res, 200))
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Creates a new OrganizationApp in the DB
export function createVersion(req, res) {
  let toValidate = ['code', 'upgrade', 'device', 'notes', 'core'];
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}])
    .then(() => {
      return sanitizeEntities(req.body, toValidate)
        .then(validateEntities(toValidate))
        .then(validateCoreVersion())
        .then(data => {
          return checkIfExists(req.params.orgid, data.device, data.code)
            .then(exists => {
              if (!exists) {
                data.organization = req.params.orgid;
                if (req.user) {
                  data.createdBy = req.user._id;
                }
                return OrganizationApp.create(data)
                  .then(ResponseHelpers.respondWithResult(res, 201))
                  .catch(ResponseHelpers.respondWithErrorEntity(res));
              } else {
                let err = {type: 'AlreadyExists', code: 409, msg: 'Version Code already exists.'};
                return ResponseHelpers.respondWithCustomError(res, err);
              }
            })
            .catch(ResponseHelpers.respondWithErrorEntity(res));
        })
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Updates an existing OrganizationApp in the DB
export function updateVersion(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  req.body.updatedAt = moment();

  let toValidate = ['code', 'upgrade', 'device', 'notes', 'core'];
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}])
    .then(() => {
      return sanitizeEntities(req.body, toValidate)
        .then(validateEntities(toValidate, true))
        .then(validateCoreVersion(true))
        .then(data => {
          return checkIfExists(req.params.orgid, data.device, data.code, req.params.id)
            .then(exists => {
              if (!exists) {
                if (req.user) {
                  data.updatedBy = req.user._id;
                }
                return OrganizationApp.findOne({_id: req.params.id, organization: req.params.orgid}).exec()
                  .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'OrganizationApp not found.'}))
                  .then(saveUpdates(data))
                  .then(ResponseHelpers.respondWithResult(res))
                  .catch(ResponseHelpers.respondWithErrorEntity(res));
              } else {
                let err = {type: 'AlreadyExists', code: 409, msg: 'Code already exists.'};
                return ResponseHelpers.respondWithCustomError(res, err);
              }
            })
            .catch(ResponseHelpers.respondWithErrorEntity(res));
        })
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Deletes a OrganizationApp from the DB
export function deleteVersion(req, res) {
  return OrganizationApp.findOne({_id: req.params.id, organization: req.params.orgid}).exec()
    .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'OrganizationApp not found.'}))
    .then(removeEntity(res))
    .then(ResponseHelpers.respondWithResult(res, 204))
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}
