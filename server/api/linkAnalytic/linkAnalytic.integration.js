'use strict';

var app = require('../..');
import request from 'supertest';

var newLinkAnalytic;

describe('LinkAnalytic API:', function() {

  describe('GET /url', function() {
    var linkAnalytics;

    beforeEach(function(done) {
      request(app)
        .get('/url')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          linkAnalytics = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      linkAnalytics.should.be.instanceOf(Array);
    });

  });

  describe('POST /url', function() {
    beforeEach(function(done) {
      request(app)
        .post('/url')
        .send({
          name: 'New LinkAnalytic',
          info: 'This is the brand new linkAnalytic!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newLinkAnalytic = res.body;
          done();
        });
    });

    it('should respond with the newly created linkAnalytic', function() {
      newLinkAnalytic.name.should.equal('New LinkAnalytic');
      newLinkAnalytic.info.should.equal('This is the brand new linkAnalytic!!!');
    });

  });

  describe('GET /url/:id', function() {
    var linkAnalytic;

    beforeEach(function(done) {
      request(app)
        .get('/url/' + newLinkAnalytic._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          linkAnalytic = res.body;
          done();
        });
    });

    afterEach(function() {
      linkAnalytic = {};
    });

    it('should respond with the requested linkAnalytic', function() {
      linkAnalytic.name.should.equal('New LinkAnalytic');
      linkAnalytic.info.should.equal('This is the brand new linkAnalytic!!!');
    });

  });

  describe('PUT /url/:id', function() {
    var updatedLinkAnalytic;

    beforeEach(function(done) {
      request(app)
        .put('/url/' + newLinkAnalytic._id)
        .send({
          name: 'Updated LinkAnalytic',
          info: 'This is the updated linkAnalytic!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedLinkAnalytic = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedLinkAnalytic = {};
    });

    it('should respond with the updated linkAnalytic', function() {
      updatedLinkAnalytic.name.should.equal('Updated LinkAnalytic');
      updatedLinkAnalytic.info.should.equal('This is the updated linkAnalytic!!!');
    });

  });

  describe('DELETE /url/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/url/' + newLinkAnalytic._id)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when linkAnalytic does not exist', function(done) {
      request(app)
        .delete('/url/' + newLinkAnalytic._id)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
