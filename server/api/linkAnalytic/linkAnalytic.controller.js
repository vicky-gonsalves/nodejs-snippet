'use strict';

import _ from 'lodash';
import LinkAnalytic from './linkAnalytic.model';
import * as PartnerCtrl from '../partner/partner.controller';
import * as CardCtrl from '../card/card.controller';
import config from "../../config/environment";
const lzutf8 = require('lzutf8');

const async = require('async');
const Promise = require('bluebird');
const validator = require('validator');
const moment = require('moment');
const ResponseHelpers = require('../../components/responseHelpers/responseHelpers');
Promise.config({
  // Enables all warnings except forgotten return statements.
  warnings: {
    wForgottenReturn: false
  }
});


function sanitizeEntities(data, toValidate = []) {
  return new Promise((resolve, reject) => {
    try {
      let sanitizedData = JSON.parse(JSON.stringify(data));
      if (data.audience && toValidate.indexOf('audience') > -1) {
        sanitizedData.audience = validator.trim(data.audience);
      }
      if (data.partner && toValidate.indexOf('partner') > -1) {
        sanitizedData.partner = validator.trim(data.partner);
      }
      return resolve(sanitizedData);
    } catch (e) {
      console.log('Error in linkAnalytic.controller.sanitizeEntities 1');
      return reject(e);
    }
  })
}

function validateEntities(toValidate = [], update = false) {
  return function(data) {
    return new Promise((resolve, reject) => {
      if (data) {
        let errors = {type: 'InvalidParams', code: 422, msg: []};
        //Validation
        if (toValidate.indexOf('audience') > -1) {
          if (!update && (!(data.audience) || validator.isNull(data.audience))) {
            errors.msg.push({audience: 'audience is required'});
          } else if (data.audience && !validator.isMongoId(data.audience)) {
            errors.msg.push({audience: 'audience must be Valid mongo id'});
          }
        }

        if (toValidate.indexOf('partner') > -1) {
          if (!update && (!(data.partner) || validator.isNull(data.partner))) {
            errors.msg.push({partner: 'partner is required'});
          } else if (data.partner && !validator.isMongoId(data.partner)) {
            errors.msg.push({partner: 'partner must be Valid mongo id'});
          }
        }

        if (errors.msg.length > 0) {
          console.log('Error in linkAnalytic.controller.validateEntities 1');
          return reject(errors);
        } else {
          return resolve(data);
        }
      } else {
        console.log('Error in linkAnalytic.controller.validateEntities 2');
        let err = {type: 'EntityNotFound', code: 422, msg: 'Entity not found.'};
        return reject(err);
      }
    });
  }
}

function saveUpdates(updates) {
  return function(entity) {
    let updated = _.extend(entity, updates, (a, b) => {
      if (_.isArray(a)) {
        return b;
      }
    });
    return updated.save()
      .then(updated => {
        return updated;
      });
  };
}

function removeEntity() {
  return function(entity) {
    return new Promise((resolve, reject) => {
      if (entity) {
        return entity.remove()
          .then(() => {
            return resolve();
          })
          .catch(err => {
            console.log('Error in linkAnalytic.controller.removeEntity 1');
            return reject(err);
          })
      } else {
        console.log('Error in linkAnalytic.controller.removeEntity 2');
        return reject({type: 'EntityNotFound', code: 404, msg: 'LinkAnalytic not found'});
      }
    });
  };
}

export function getLinkStats(card, index, skipCount) {
  return new Promise((resolve, reject) => {
    if (skipCount) {
      return resolve({skipped: true});
    }
    return LinkAnalytic.find({card: card, index: index})
      .count()
      .exec()
      .then(totalCount => {
        return LinkAnalytic.aggregate([
          {
            $match: {
              card: card, index: index
            }
          },
          {
            $group: {_id: '$user'}
          },
          {
            $project: {
              "_id": '$_id'
            }
          }
        ])
          .exec()
          .then(uniqueCount => {
            return resolve({total_clicks: totalCount, unique_clicks: uniqueCount.length});
          })
          .catch(err => {
            console.log('Error in linkAnalytic.controller.getLinkStats 1');
            return reject(err);
          })
      })
      .catch(err => {
        console.log('Error in linkAnalytic.controller.getLinkStats 2');
        return reject(err);
      })
  });
}

// Gets a list of LinkAnalytics
export function index(req, res) {
  let output = lzutf8.decompress(req.params.encrypted, {inputEncoding: 'Base64'});
  console.log("GOT EXTERNAL URL");
  console.log(output);
  let parsed = JSON.parse(output);
  console.log('Redirecting to ' + parsed.l);
  let data = {
    user: parsed.u,
    organization: parsed.o,
    url: parsed.l,
    device: parsed.d,
    card: parsed.c,
    index: parsed.i
  };
  return CardCtrl.getCardById(data.card)
    .then(_card => {
      let redirectLink = parsed.l;
      if (_card) {
        if (parsed.l.indexOf('?') > -1) {
          redirectLink = `${parsed.l}&utm_source=influents&utm_medium=${data.card ? 'card' : 'sidebar'}&utm_term=${_card.data['title']}&utm_content=${parsed.t == 'actionlink' ? _card.data.actionlink['icon'] : _card.data.links[data.index - 1].title}${_card.data.links[data.index - 1] && _card.data.links[data.index - 1]['utm_campaign'] ? '&utm_campaign=' + _card.data.links[data.index - 1]['utm_campaign'] : ''}`;
        } else {
          redirectLink = `${parsed.l}?utm_source=influents&utm_medium=${data.card ? 'card' : 'sidebar'}&utm_term=${_card.data['title']}&utm_content=${parsed.t == 'actionlink' ? _card.data.actionlink['icon'] : _card.data.links[data.index - 1].title}${_card.data.links[data.index - 1] && _card.data.links[data.index - 1]['utm_campaign'] ? '&utm_campaign=' + _card.data.links[data.index - 1]['utm_campaign'] : ''}`;
        }
      }
      return PartnerCtrl.getUsersPartnerByOrgAndUserId(parsed.u, parsed.o)
        .then(partner => {
          if (partner && partner._id) {
            data.partner = partner._id;
            return LinkAnalytic.create(data)
              .then(link => {
                console.log('SAVED linkClick :' + JSON.stringify(link));
                console.log('Final redirection: ' + redirectLink);
                res.writeHead(302, {
                  'Location': encodeURI(redirectLink)
                });
                return res.end();
              })
              .catch(ResponseHelpers.respondWithErrorEntity(res));
          } else {
            return PartnerCtrl.getUsersPartner(parsed.user)
              .then(_p => {
                if (_p && _p._id) {
                  data.partner = _p._id;
                }
                return LinkAnalytic.create(data)
                  .then(link => {
                    console.log('SAVED linkClick :' + JSON.stringify(link));
                    console.log('Final redirection: ' + redirectLink);
                    res.writeHead(302, {
                      'Location': encodeURI(redirectLink)
                    });
                    return res.end();
                  })
                  .catch(ResponseHelpers.respondWithErrorEntity(res));
              })
              .catch(ResponseHelpers.respondWithErrorEntity(res));
          }
        })
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Gets a single LinkAnalytic from the DB
export function show(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}, {value: req.params.id, type: 'linkAnalytic'}])
    .then(() => {
      return LinkAnalytic.findOne({organization: req.params.orgid, _id: req.params.id}).exec()
        .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'LinkAnalytic not found.'}))
        .then(ResponseHelpers.respondWithResult(res))
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Deletes a LinkAnalytic from the DB
export function destroy(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}, {value: req.params.id, type: 'linkAnalytic'}])
    .then(() => {
      return LinkAnalytic.findOne({organization: req.params.orgid, _id: req.params.id}).exec()
        .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'LinkAnalytic not found.'}))
        .then(removeEntity(res))
        .then(ResponseHelpers.respondWithResult(res, 204))
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}
