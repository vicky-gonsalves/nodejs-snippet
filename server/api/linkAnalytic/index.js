'use strict';

var express = require('express');
var controller = require('./linkAnalytic.controller');

var router = express.Router();

router.get('/:encrypted', controller.index);

module.exports = router;
