/**
 * LinkAnalytic model events
 */

'use strict';

import {EventEmitter} from 'events';
import LinkAnalytic from './linkAnalytic.model';
var LinkAnalyticEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
LinkAnalyticEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  LinkAnalytic.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    LinkAnalyticEvents.emit(event + ':' + doc._id, doc);
    LinkAnalyticEvents.emit(event, doc);
  }
}

export default LinkAnalyticEvents;
