'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var linkAnalyticCtrlStub = {
  index: 'linkAnalyticCtrl.index',
  show: 'linkAnalyticCtrl.show',
  create: 'linkAnalyticCtrl.create',
  update: 'linkAnalyticCtrl.update',
  destroy: 'linkAnalyticCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var linkAnalyticIndex = proxyquire('./index.js', {
  'express': {
    Router: function() {
      return routerStub;
    }
  },
  './linkAnalytic.controller': linkAnalyticCtrlStub
});

describe('LinkAnalytic API Router:', function() {

  it('should return an express router instance', function() {
    linkAnalyticIndex.should.equal(routerStub);
  });

  describe('GET /url', function() {

    it('should route to linkAnalytic.controller.index', function() {
      routerStub.get
        .withArgs('/', 'linkAnalyticCtrl.index')
        .should.have.been.calledOnce;
    });

  });

  describe('GET /url/:id', function() {

    it('should route to linkAnalytic.controller.show', function() {
      routerStub.get
        .withArgs('/:id', 'linkAnalyticCtrl.show')
        .should.have.been.calledOnce;
    });

  });

  describe('POST /url', function() {

    it('should route to linkAnalytic.controller.create', function() {
      routerStub.post
        .withArgs('/', 'linkAnalyticCtrl.create')
        .should.have.been.calledOnce;
    });

  });

  describe('PUT /url/:id', function() {

    it('should route to linkAnalytic.controller.update', function() {
      routerStub.put
        .withArgs('/:id', 'linkAnalyticCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('PATCH /url/:id', function() {

    it('should route to linkAnalytic.controller.update', function() {
      routerStub.patch
        .withArgs('/:id', 'linkAnalyticCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('DELETE /url/:id', function() {

    it('should route to linkAnalytic.controller.destroy', function() {
      routerStub.delete
        .withArgs('/:id', 'linkAnalyticCtrl.destroy')
        .should.have.been.calledOnce;
    });

  });

});
