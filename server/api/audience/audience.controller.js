'use strict';

import * as config from '../../config/environment';
import _ from 'lodash';
import Audience from './audience.model';
import * as OrgCtrl from '../organization/organization.controller';
import * as AudienceLinkCtrl from '../audienceLink/audienceLink.controller';
import * as ES from '../../components/elasticSearch/es';

const Promise = require('bluebird');
const validator = require('validator');
const moment = require('moment');
const ResponseHelpers = require('../../components/responseHelpers/responseHelpers');
Promise.config({
  // Enables all warnings except forgotten return statements.
  warnings: {
    wForgottenReturn: false
  }
});

function sanitizeEntities(data, toValidate = []) {
  return new Promise((resolve, reject) => {
    try {
      let sanitizedData = JSON.parse(JSON.stringify(data));
      if (data.name && toValidate.indexOf('name') > -1) {
        sanitizedData.name = validator.trim(data.name);
      }
      return resolve(sanitizedData);
    } catch (e) {
      console.log('Error in audience.controller.sanitizeEntities 1');
      return reject(e);
    }
  })
}

function validateEntities(toValidate = [], update = false) {
  return function(data) {
    return new Promise((resolve, reject) => {
      if (data) {
        let errors = {type: 'InvalidParams', code: 422, msg: []};
        //Validation
        if (toValidate.indexOf('name') > -1) {
          if (!update && (!(data.name) || validator.isNull(data.name))) {
            errors.msg.push({name: 'name is required'});
          } else if (data.name && !validator.isLength(data.name, {min: 1, max: 155})) {
            errors.msg.push({name: 'name must be between 1 to 155 characters long'});
          }
        }
        if (toValidate.indexOf('filter') > -1) {
          if ((!(data.filter) && !_.isObject(data.filter))) {
            errors.msg.push({filter: 'filter object is required'});
          }
        }
        if (errors.msg.length > 0) {
          console.log('Error in audience.controller.validateEntities 1');
          return reject(errors);
        } else {
          return resolve(data);
        }
      } else {
        console.log('Error in audience.controller.validateEntities 2');
        let err = {type: 'EntityNotFound', code: 422, msg: 'Entity not found.'};
        return reject(err);
      }
    });
  }
}

export function saveUpdates(updates) {
  return function(entity) {
    let updated = _.extend(entity, updates, (a, b) => {
      if (_.isArray(a)) {
        return b;
      }
    });
    return updated.save()
      .then(updated => {
        return updated;
      });
  };
}

function removeEntity() {
  return function(entity) {
    return new Promise((resolve, reject) => {
      if (entity) {
        return entity.remove()
          .then(() => {
            return resolve();
          })
          .catch(err => {
            console.log('Error in audience.controller.removeEntity 1');
            return reject(err);
          })
      } else {
        console.log('Error in audience.controller.removeEntity 2');
        return reject({type: 'EntityNotFound', code: 404, msg: 'Audience not found'});
      }
    });
  };
}

function linkAudience(esResult, audienceId, user) {
  return new Promise((resolve, reject) => {
    if (!audienceId) {
      return reject({type: 'EntityNotFound', code: 404, msg: 'audienceId not valid'});
    }
    return AudienceLinkCtrl.removeLinks(audienceId)
      .then(() => {
        if (esResult && !esResult.error && esResult.hits) {
          return AudienceLinkCtrl.createLinks(esResult.hits.hits, audienceId, user)
            .then(() => {
              return resolve();
            })
            .catch(err => {
              console.log('Error in audience.controller.linkAudience 1');
              return reject(err);
            })
        } else {
          console.log('Error in audience.controller.linkAudience 2');
          return resolve({type: 'EntityNotFound', code: 404, msg: 'EsResult not valid'});
        }
      })
      .catch(err => {
        console.log('Error in audience.controller.linkAudience 3');
        return reject(err);
      })
  });
}

function createAudience(data, user) {
  return function(esResult) {
    return new Promise((resolve, reject) => {
      if (esResult && !esResult.error && esResult.hits && esResult.hits.hasOwnProperty('total')) {
        data.lastRun = moment();
        data.count = esResult.hits.total;
        console.log("ES RUN RESULT:" + JSON.stringify(esResult));
        return Audience.create(data)
          .then(audience => {
            return linkAudience(esResult, audience._id, user)
              .then(() => {
                return resolve(audience);
              })
              .catch(err => {
                console.log('Error in audience.controller.createAudience 1');
                return reject(err);
              });
          })
          .catch(err => {
            console.log('Error in audience.controller.createAudience 2');
            return reject(err);
          })
      } else {
        console.log('Error in audience.controller.createAudience 3');
        return reject({type: esResult.error && esResult.error.type ? esResult.error.type : 'hits not available', code: esResult.status || 500, msg: esResult});
      }
    });
  }
}

export function updateAudience(id, user) {
  return function(esResult) {
    return new Promise((resolve, reject) => {
      if (esResult && !esResult.error && esResult.hits && esResult.hits.hasOwnProperty('total')) {
        console.log("ES RUN RESULT:" + JSON.stringify(esResult));
        return linkAudience(esResult, id, user)
          .then(() => {
            return resolve(esResult);
          })
          .catch(err => {
            console.log('Error in audience.controller.createAudience 1');
            return reject(err);
          });
      } else {
        console.log('Error in audience.controller.createAudience 2');
        return reject({type: esResult.error && esResult.error.type ? esResult.error.type : 'hits not available', code: esResult.status || 500, msg: esResult});
      }
    });
  }
}

export function getAudiences(ids) {
  return new Promise((resolve, reject) => {
    return Audience.find({_id: {$in: ids}})
      .populate('organization')
      .then(audiences => {
        return resolve(audiences);
      })
      .catch(err => {
        console.log('Error in audience.controller.getAudiences 1');
        return reject(err);
      });
  });
}

export function getAllAudiences(orgid) {
  return new Promise((resolve, reject) => {
    return Audience.find({organization: orgid})
      .populate('organization')
      .then(audiences => {
        return resolve(audiences);
      })
      .catch(err => {
        console.log('Error in audience.controller.getAllAudiences 1');
        return reject(err);
      });
  });
}

export function getAudienceByOrgAndId(orgid, audience) {
  return new Promise((resolve, reject) => {
    return Audience.findOne({organization: orgid, _id: audience})
      .populate('organization')
      .then(audiences => {
        return resolve(audiences);
      })
      .catch(err => {
        console.log('Error in audience.controller.getAudienceByOrgAndId 1');
        return reject(err);
      });
  });
}

export function getAudience(ids) {
  return new Promise((resolve, reject) => {
    return Audience.find({_id: {$in: ids}})
      .then(auds => {
        return resolve(auds);
      })
      .catch(err => {
        return resolve();
      });
  });
}

// Gets a list of Audiences
export function index(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}])
    .then(() => {
      return Audience.find({organization: req.params.orgid}).exec()
        .then(ResponseHelpers.respondWithResult(res))
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Gets a single Audience from the DB
export function show(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}, {value: req.params.id, type: 'audience'}])
    .then(() => {
      return Audience.findOne({organization: req.params.orgid, _id: req.params.id}).exec()
        .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Audience not found.'}))
        .then(ResponseHelpers.respondWithResult(res))
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Creates a new Audience in the DB
export function create(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}])
    .then(() => {
      let toValidate = ['name', 'filter'];
      if (req.body.createdBy) {
        delete req.body.createdBy;
      }
      if (req.user) {
        req.body.createdBy = req.user._id;
      }
      if (req.body.organization) {
        delete req.body.organization;
      }
      if (req.body.lastRun) {
        delete req.body.lastRun;
      }
      if (req.body.count) {
        delete req.body.count;
      }
      if (req.body.createdAt) {
        delete req.body.createdAt;
      }
      if (req.body.updatedAt) {
        delete req.body.updatedAt;
      }
      req.body.organization = req.params.orgid;
      return sanitizeEntities(req.body, toValidate)
        .then(validateEntities(toValidate))
        .then(data => {
          return OrgCtrl.getEsIndex(req.params.orgid)
            .then(ES.perFormEsSearch({query: {filtered: req.body.filter}}))
            .then(createAudience(data, req.user))
            .then(ResponseHelpers.respondWithResult(res, 201))
            .catch(ResponseHelpers.respondWithErrorEntity(res));
        })
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Preview Elastic Search
export function previewElasticSearch(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}])
    .then(() => {
      return OrgCtrl.getEsIndex(req.params.orgid)
        .then(ES.perFormEsSearch(req.body))
        .then(ResponseHelpers.respondRawResults(res))
        .catch(ResponseHelpers.respondRawErrors(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}


// Updates an existing Audience in the DB
export function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  if (req.body.createdBy) {
    delete req.body.createdBy;
  }
  if (req.user) {
    req.updatedBy = req.user;
  }
  if (req.body.organization) {
    delete req.body.organization;
  }
  if (req.body.lastRun) {
    delete req.body.lastRun;
  }
  if (req.body.count) {
    delete req.body.count;
  }
  if (req.body.createdAt) {
    delete req.body.createdAt;
  }
  if (req.body.updatedAt) {
    delete req.body.updatedAt;
  }
  req.body.updatedAt = moment();
  req.body.organization = req.params.orgid;
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}, {value: req.params.id, type: 'audience'}])
    .then(() => {
      let toValidate = ['name'];
      return sanitizeEntities(req.body, toValidate)
        .then(validateEntities(toValidate, true))
        .then(data => {
          return OrgCtrl.getEsIndex(req.params.orgid)
            .then(ES.perFormEsSearch({query: {filtered: req.body.filter}}))
            .then(updateAudience(req.params.id, req.user))
            .then(esResult => {
              if (esResult && !esResult.error && esResult.hits && esResult.hits.hasOwnProperty('total')) {
                data.lastRun = moment();
                data.count = esResult.hits.total;
                return Audience.findOne({organization: req.params.orgid, _id: req.params.id}).exec()
                  .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Audience not found.'}))
                  .then(saveUpdates(data))
                  .then(ResponseHelpers.respondWithResult(res))
                  .catch(ResponseHelpers.respondWithErrorEntity(res));
              } else {
                let err = {type: esResult.error.type, code: esResult.status, msg: esResult};
                return ResponseHelpers.respondWithCustomError(res, err);
              }
            })
            .catch(ResponseHelpers.respondWithErrorEntity(res));
        })
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Deletes a Audience from the DB
export function destroy(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}, {value: req.params.id, type: 'audience'}])
    .then(() => {
      return Audience.findOne({organization: req.params.orgid, _id: req.params.id}).exec()
        .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Audience not found.'}))
        .then(removeEntity(res))
        .then(ResponseHelpers.respondWithResult(res, 204))
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}
