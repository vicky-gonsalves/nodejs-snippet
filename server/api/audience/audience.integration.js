'use strict';

var app = require('../..');
import request from 'supertest';

var newAudience;

describe('Audience API:', function() {

  describe('GET /api/v1/audiences', function() {
    var audiences;

    beforeEach(function(done) {
      request(app)
        .get('/api/v1/audiences')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          audiences = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      audiences.should.be.instanceOf(Array);
    });

  });

  describe('POST /api/v1/audiences', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/v1/audiences')
        .send({
          name: 'New Audience',
          info: 'This is the brand new audience!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newAudience = res.body;
          done();
        });
    });

    it('should respond with the newly created audience', function() {
      newAudience.name.should.equal('New Audience');
      newAudience.info.should.equal('This is the brand new audience!!!');
    });

  });

  describe('GET /api/v1/audiences/:id', function() {
    var audience;

    beforeEach(function(done) {
      request(app)
        .get('/api/v1/audiences/' + newAudience._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          audience = res.body;
          done();
        });
    });

    afterEach(function() {
      audience = {};
    });

    it('should respond with the requested audience', function() {
      audience.name.should.equal('New Audience');
      audience.info.should.equal('This is the brand new audience!!!');
    });

  });

  describe('PUT /api/v1/audiences/:id', function() {
    var updatedAudience;

    beforeEach(function(done) {
      request(app)
        .put('/api/v1/audiences/' + newAudience._id)
        .send({
          name: 'Updated Audience',
          info: 'This is the updated audience!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedAudience = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedAudience = {};
    });

    it('should respond with the updated audience', function() {
      updatedAudience.name.should.equal('Updated Audience');
      updatedAudience.info.should.equal('This is the updated audience!!!');
    });

  });

  describe('DELETE /api/v1/audiences/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/api/v1/audiences/' + newAudience._id)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when audience does not exist', function(done) {
      request(app)
        .delete('/api/v1/audiences/' + newAudience._id)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
