'use strict';

var express = require('express');
var controller = require('./audience.controller');

var router = express.Router();

module.exports = router;
