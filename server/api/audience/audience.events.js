/**
 * Audience model events
 */

'use strict';

import {EventEmitter} from 'events';
import Audience from './audience.model';
var AudienceEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
AudienceEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  Audience.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    AudienceEvents.emit(event + ':' + doc._id, doc);
    AudienceEvents.emit(event, doc);
  }
}

export default AudienceEvents;
