'use strict';

import mongoose from 'mongoose';

var AudienceSchema = new mongoose.Schema({
  name: String,
  filter: {},
  count: Number,
  organization: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Organization'
  },
  lastRun: Date,
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: {
    type: Date
  },
  createdBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  updatedBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  }
});

export default mongoose.model('Audience', AudienceSchema);
