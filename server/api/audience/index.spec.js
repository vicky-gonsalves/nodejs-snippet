'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var audienceCtrlStub = {
  index: 'audienceCtrl.index',
  show: 'audienceCtrl.show',
  create: 'audienceCtrl.create',
  update: 'audienceCtrl.update',
  destroy: 'audienceCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var audienceIndex = proxyquire('./index.js', {
  'express': {
    Router: function() {
      return routerStub;
    }
  },
  './audience.controller': audienceCtrlStub
});

describe('Audience API Router:', function() {

  it('should return an express router instance', function() {
    audienceIndex.should.equal(routerStub);
  });

  describe('GET /api/v1/audiences', function() {

    it('should route to audience.controller.index', function() {
      routerStub.get
        .withArgs('/', 'audienceCtrl.index')
        .should.have.been.calledOnce;
    });

  });

  describe('GET /api/v1/audiences/:id', function() {

    it('should route to audience.controller.show', function() {
      routerStub.get
        .withArgs('/:id', 'audienceCtrl.show')
        .should.have.been.calledOnce;
    });

  });

  describe('POST /api/v1/audiences', function() {

    it('should route to audience.controller.create', function() {
      routerStub.post
        .withArgs('/', 'audienceCtrl.create')
        .should.have.been.calledOnce;
    });

  });

  describe('PUT /api/v1/audiences/:id', function() {

    it('should route to audience.controller.update', function() {
      routerStub.put
        .withArgs('/:id', 'audienceCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('PATCH /api/v1/audiences/:id', function() {

    it('should route to audience.controller.update', function() {
      routerStub.patch
        .withArgs('/:id', 'audienceCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('DELETE /api/v1/audiences/:id', function() {

    it('should route to audience.controller.destroy', function() {
      routerStub.delete
        .withArgs('/:id', 'audienceCtrl.destroy')
        .should.have.been.calledOnce;
    });

  });

});
