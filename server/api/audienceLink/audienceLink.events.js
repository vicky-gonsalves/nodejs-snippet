/**
 * AudienceLink model events
 */

'use strict';

import {EventEmitter} from 'events';
import AudienceLink from './audienceLink.model';
var AudienceLinkEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
AudienceLinkEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  AudienceLink.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    AudienceLinkEvents.emit(event + ':' + doc._id, doc);
    AudienceLinkEvents.emit(event, doc);
  }
}

export default AudienceLinkEvents;
