'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var audienceLinkCtrlStub = {
  index: 'audienceLinkCtrl.index',
  show: 'audienceLinkCtrl.show',
  create: 'audienceLinkCtrl.create',
  update: 'audienceLinkCtrl.update',
  destroy: 'audienceLinkCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var audienceLinkIndex = proxyquire('./index.js', {
  'express': {
    Router: function() {
      return routerStub;
    }
  },
  './audienceLink.controller': audienceLinkCtrlStub
});

describe('AudienceLink API Router:', function() {

  it('should return an express router instance', function() {
    audienceLinkIndex.should.equal(routerStub);
  });

  describe('GET /api/audienceLinks', function() {

    it('should route to audienceLink.controller.index', function() {
      routerStub.get
        .withArgs('/', 'audienceLinkCtrl.index')
        .should.have.been.calledOnce;
    });

  });

  describe('GET /api/audienceLinks/:id', function() {

    it('should route to audienceLink.controller.show', function() {
      routerStub.get
        .withArgs('/:id', 'audienceLinkCtrl.show')
        .should.have.been.calledOnce;
    });

  });

  describe('POST /api/audienceLinks', function() {

    it('should route to audienceLink.controller.create', function() {
      routerStub.post
        .withArgs('/', 'audienceLinkCtrl.create')
        .should.have.been.calledOnce;
    });

  });

  describe('PUT /api/audienceLinks/:id', function() {

    it('should route to audienceLink.controller.update', function() {
      routerStub.put
        .withArgs('/:id', 'audienceLinkCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('PATCH /api/audienceLinks/:id', function() {

    it('should route to audienceLink.controller.update', function() {
      routerStub.patch
        .withArgs('/:id', 'audienceLinkCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('DELETE /api/audienceLinks/:id', function() {

    it('should route to audienceLink.controller.destroy', function() {
      routerStub.delete
        .withArgs('/:id', 'audienceLinkCtrl.destroy')
        .should.have.been.calledOnce;
    });

  });

});
