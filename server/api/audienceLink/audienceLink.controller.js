'use strict';

import * as config from '../../config/environment';
import _ from 'lodash';
import AudienceLink from './audienceLink.model';
import * as OrgCtrl from '../organization/organization.controller';
import * as PartnerCtrl from '../partner/partner.controller';
import * as ES from '../../components/elasticSearch/es';

const async = require('async');
const Promise = require('bluebird');
const validator = require('validator');
const moment = require('moment');
const ResponseHelpers = require('../../components/responseHelpers/responseHelpers');
Promise.config({
  // Enables all warnings except forgotten return statements.
  warnings: {
    wForgottenReturn: false
  }
});

function sanitizeEntities(data, toValidate = []) {
  return new Promise((resolve, reject) => {
    try {
      let sanitizedData = JSON.parse(JSON.stringify(data));
      if (data.audience && toValidate.indexOf('audience') > -1) {
        sanitizedData.audience = validator.trim(data.audience);
      }
      if (data.partner && toValidate.indexOf('partner') > -1) {
        sanitizedData.partner = validator.trim(data.partner);
      }
      return resolve(sanitizedData);
    } catch (e) {
      console.log('Error in audienceLink.controller.sanitizeEntities 1');
      return reject(e);
    }
  })
}

function validateEntities(toValidate = [], update = false) {
  return function(data) {
    return new Promise((resolve, reject) => {
      if (data) {
        let errors = {type: 'InvalidParams', code: 422, msg: []};
        //Validation
        if (toValidate.indexOf('audience') > -1) {
          if (!update && (!(data.audience) || validator.isNull(data.audience))) {
            errors.msg.push({audience: 'audience is required'});
          } else if (data.audience && !validator.isMongoId(data.audience)) {
            errors.msg.push({audience: 'audience must be Valid mongo id'});
          }
        }

        if (toValidate.indexOf('partner') > -1) {
          if (!update && (!(data.partner) || validator.isNull(data.partner))) {
            errors.msg.push({partner: 'partner is required'});
          } else if (data.partner && !validator.isMongoId(data.partner)) {
            errors.msg.push({partner: 'partner must be Valid mongo id'});
          }
        }

        if (errors.msg.length > 0) {
          console.log('Error in audienceLink.controller.validateEntities 1');
          return reject(errors);
        } else {
          return resolve(data);
        }
      } else {
        console.log('Error in audienceLink.controller.validateEntities 2');
        let err = {type: 'EntityNotFound', code: 422, msg: 'Entity not found.'};
        return reject(err);
      }
    });
  }
}

function saveUpdates(updates) {
  return function(entity) {
    let updated = _.extend(entity, updates, (a, b) => {
      if (_.isArray(a)) {
        return b;
      }
    });
    return updated.save()
      .then(updated => {
        return updated;
      });
  };
}

function removeEntity() {
  return function(entity) {
    return new Promise((resolve, reject) => {
      if (entity) {
        return entity.remove()
          .then(() => {
            return resolve();
          })
          .catch(err => {
            console.log('Error in audienceLink.controller.removeEntity 1');
            return reject(err);
          })
      } else {
        console.log('Error in audienceLink.controller.removeEntity 2');
        return reject({type: 'EntityNotFound', code: 404, msg: 'AudienceLink not found'});
      }
    });
  };
}

export function createLinks(esData, audienceId, user) {
  return new Promise((resolve, reject) => {
    let partners = esData.map(_partner => _partner._source.partner_id).filter(_pa => _pa != null);
    console.log("Es Partners: " + JSON.stringify(partners));
    if (!partners.length) {
      return resolve();
    }
    return async.mapSeries(partners, function(_partner, cb) {
      return PartnerCtrl.getPartnerByPartnerId(_partner)
        .then(_p => {
          if (_p && _p._id) {
            let q = {partner: _p._id, audience: audienceId};
            if (user && user._id) {
              q.createdBy = user._id
            }
            return AudienceLink.create(q)
              .then(audienceLink => {
                console.log('AudienceLink:' + JSON.stringify(audienceLink));
                return cb();
              })
              .catch(err => {
                console.log('Error in audienceLink.controller.createAudienceLink 2');
                return cb(err);
              });
          } else {
            return cb({type: 'ParnerNotFound', code: 404, msg: 'Partner not found'});
          }
        })
        .catch(err => {
          return cb(err);
        })

    }, function(err, done) {
      if (err) {
        console.log('Error in audienceLink.controller.createUpdateLinks 1');
        return reject(err);
      }
      return resolve();
    });
  });
}

export function removeLinks(audienceId) {
  return new Promise((resolve, reject) => {
    console.log('Removing Existing Links for audience id:' + audienceId);
    return AudienceLink.remove({audience: audienceId})
      .then(() => {
        return resolve();
      })
      .catch(() => {
        return resolve();
      });
  });
}

export function getPartnersByAudienceLinkIds(audienceIds) {
  return new Promise((resolve, reject) => {
    if (audienceIds && audienceIds.length) {
      return AudienceLink.find({audience: {$in: audienceIds}})
        .populate('partner')
        .then(allLinks => {
          return resolve(allLinks.map(link => link.partner));
        })
        .catch(err => {
          console.log('Error in audienceLink.controller.getUsersByAudienceLinkIds 1');
          console.log(err);
          return reject(err);
        });
    }
  });
}

export function getLinksByPartner(audienceId) {
  return function(partners) {
    return new Promise((resolve, reject) => {
      if (!partners && !partners.length) {
        let err = {type: 'NoPartners', code: 400, msg: 'No partners available for this user'};
        console.log('Error in audienceLink.controller.getLinksByPartner 1');
        console.log(err);
        return reject(err);
      }
      let partnerIds = partners.map(_p => _p._id);
      console.log('Users partners: ' + JSON.stringify(partnerIds));
      return AudienceLink.find({partner: {$in: partnerIds}})
        .select('audience')
        .then(aud => {
          if (aud && aud.length) {
            return resolve(aud.map(_a => _a.audience));
          } else {
            return resolve([]);
          }
        })
        .catch(err => {
          console.log('Error in audienceLink.controller.getLinksByPartner 2');
          console.log(err);
          return reject(err);
        });
    });
  }
}

export function getAllLinks(partnerId) {
  return new Promise((resolve, reject) => {
    return AudienceLink.find({partner: partnerId})
      .then(partners => {
        return resolve(partners);
      })
      .catch(err => {
        return resolve();
      });
  });
}

// Gets a list of AudienceLinks
export function index(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}])
    .then(() => {
      return AudienceLink.find({organization: req.params.orgid}).exec()
        .then(ResponseHelpers.respondWithResult(res))
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Gets a single AudienceLink from the DB
export function show(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}, {value: req.params.id, type: 'audienceLink'}])
    .then(() => {
      return AudienceLink.findOne({organization: req.params.orgid, _id: req.params.id}).exec()
        .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'AudienceLink not found.'}))
        .then(ResponseHelpers.respondWithResult(res))
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Creates a new AudienceLink in the DB
export function create(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}])
    .then(() => {
      let toValidate = ['name', 'filter'];
      if (req.body.createdBy) {
        delete req.body.createdBy;
      }
      if (req.user) {
        req.body.createdBy = req.user._id;
      }
      if (req.body.organization) {
        delete req.body.organization;
      }
      if (req.body.lastRun) {
        delete req.body.lastRun;
      }
      if (req.body.count) {
        delete req.body.count;
      }
      if (req.body.createdAt) {
        delete req.body.createdAt;
      }
      if (req.body.updatedAt) {
        delete req.body.updatedAt;
      }
      req.body.organization = req.params.orgid;
      return sanitizeEntities(req.body, toValidate)
        .then(validateEntities(toValidate))
        .then(data => {
          return OrgCtrl.getEsIndex(req.params.orgid)
            .then(ES.perFormEsSearch(req.body.filter))
            .then(createAudienceLink(data))
            .then(ResponseHelpers.respondWithResult(res, 201))
            .catch(ResponseHelpers.respondWithErrorEntity(res));
        })
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Preview Elastic Search
export function previewElasticSearch(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}])
    .then(() => {
      return OrgCtrl.getEsIndex(req.params.orgid)
        .then(ES.perFormEsSearch(req.body))
        .then(ResponseHelpers.respondRawResults(res))
        .catch(ResponseHelpers.respondRawErrors(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}


// Updates an existing AudienceLink in the DB
export function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  if (req.body.createdBy) {
    delete req.body.createdBy;
  }
  if (req.user) {
    req.updatedBy = req.user;
  }
  if (req.body.organization) {
    delete req.body.organization;
  }
  if (req.body.lastRun) {
    delete req.body.lastRun;
  }
  if (req.body.count) {
    delete req.body.count;
  }
  if (req.body.createdAt) {
    delete req.body.createdAt;
  }
  if (req.body.updatedAt) {
    delete req.body.updatedAt;
  }
  req.body.updatedAt = moment();
  req.body.organization = req.params.orgid;
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}, {value: req.params.id, type: 'audienceLink'}])
    .then(() => {
      let toValidate = ['name'];
      return sanitizeEntities(req.body, toValidate)
        .then(validateEntities(toValidate, true))
        .then(data => {
          return OrgCtrl.getEsIndex(req.params.orgid)
            .then(ES.perFormEsSearch(req.body.filter))
            .then(esResult => {
              if (esResult && !esResult.error && esResult.hits && esResult.hits.total) {
                data.lastRun = moment();
                data.count = esResult.hits.total;
                return AudienceLink.findOne({organization: req.params.orgid, _id: req.params.id}).exec()
                  .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'AudienceLink not found.'}))
                  .then(saveUpdates(data))
                  .then(ResponseHelpers.respondWithResult(res))
                  .catch(ResponseHelpers.respondWithErrorEntity(res));
              } else {
                let err = {type: esResult.error.type, code: esResult.status, msg: esResult};
                return ResponseHelpers.respondWithCustomError(res, err);
              }
            })
            .catch(ResponseHelpers.respondWithErrorEntity(res));
        })
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Deletes a AudienceLink from the DB
export function destroy(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}, {value: req.params.id, type: 'audienceLink'}])
    .then(() => {
      return AudienceLink.findOne({organization: req.params.orgid, _id: req.params.id}).exec()
        .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'AudienceLink not found.'}))
        .then(removeEntity(res))
        .then(ResponseHelpers.respondWithResult(res, 204))
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

export function getList(req, res) {
  return AudienceLink.find({partner: req.params.partnerid})
    .populate('audience')
    .exec()
    .then(ResponseHelpers.respondWithResult(res, 200))
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}
