'use strict';

var app = require('../..');
import request from 'supertest';

var newAudienceLink;

describe('AudienceLink API:', function() {

  describe('GET /api/audienceLinks', function() {
    var audienceLinks;

    beforeEach(function(done) {
      request(app)
        .get('/api/audienceLinks')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          audienceLinks = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      audienceLinks.should.be.instanceOf(Array);
    });

  });

  describe('POST /api/audienceLinks', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/audienceLinks')
        .send({
          name: 'New AudienceLink',
          info: 'This is the brand new audienceLink!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newAudienceLink = res.body;
          done();
        });
    });

    it('should respond with the newly created audienceLink', function() {
      newAudienceLink.name.should.equal('New AudienceLink');
      newAudienceLink.info.should.equal('This is the brand new audienceLink!!!');
    });

  });

  describe('GET /api/audienceLinks/:id', function() {
    var audienceLink;

    beforeEach(function(done) {
      request(app)
        .get('/api/audienceLinks/' + newAudienceLink._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          audienceLink = res.body;
          done();
        });
    });

    afterEach(function() {
      audienceLink = {};
    });

    it('should respond with the requested audienceLink', function() {
      audienceLink.name.should.equal('New AudienceLink');
      audienceLink.info.should.equal('This is the brand new audienceLink!!!');
    });

  });

  describe('PUT /api/audienceLinks/:id', function() {
    var updatedAudienceLink;

    beforeEach(function(done) {
      request(app)
        .put('/api/audienceLinks/' + newAudienceLink._id)
        .send({
          name: 'Updated AudienceLink',
          info: 'This is the updated audienceLink!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedAudienceLink = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedAudienceLink = {};
    });

    it('should respond with the updated audienceLink', function() {
      updatedAudienceLink.name.should.equal('Updated AudienceLink');
      updatedAudienceLink.info.should.equal('This is the updated audienceLink!!!');
    });

  });

  describe('DELETE /api/audienceLinks/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/api/audienceLinks/' + newAudienceLink._id)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when audienceLink does not exist', function(done) {
      request(app)
        .delete('/api/audienceLinks/' + newAudienceLink._id)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
