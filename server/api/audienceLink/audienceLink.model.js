'use strict';

import mongoose from 'mongoose';

var AudienceLinkSchema = new mongoose.Schema({
  audience: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Audience'
  },
  partner: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Partner'
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: {
    type: Date
  },
  createdBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  updatedBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  }
});

export default mongoose.model('AudienceLink', AudienceLinkSchema);
