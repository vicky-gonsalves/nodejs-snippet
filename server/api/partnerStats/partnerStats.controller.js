/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/partner-stats              ->  index
 * POST    /api/partner-stats              ->  create
 * GET     /api/partner-stats/:id          ->  show
 * PUT     /api/partner-stats/:id          ->  update
 * DELETE  /api/partner-stats/:id          ->  destroy
 */

'use strict';

import _ from 'lodash';
import PartnerStats from './partnerStats.model';
import Partner from '../partner/partner.model';
import * as PartnerCtrl from '../partner/partner.controller';
import Organization from '../organization/organization.model';
const async = require('async');
const Promise = require('bluebird');
const ResponseHelpers = require('../../components/responseHelpers/responseHelpers');
const millis24H = 1000 * 60 * 60 * 24;

Promise.config({
  // Enables all warnings except forgotten return statements.
  warnings: {
    wForgottenReturn: false
  }
});

function saveUpdates(updates) {
  return function(entity) {
    var updated = _.merge(entity, updates);
    return updated.save()
      .then(updated => {
        return updated;
      });
  };
}

function removeEntity() {
  return function(entity) {
    return new Promise((resolve, reject) => {
      if (entity) {
        return entity.remove()
          .then(() => {
            return resolve();
          })
          .catch(err => {
            console.log('Error in partnerStats.controller.removeEntity 1');
            return reject(err);
          })
      } else {
        console.log('Error in partnerStats.controller.removeEntity 2');
        return reject({type: 'EntityNotFound', code: 404, msg: 'PartnerStats not found'});
      }
    });
  };
}

function createPartnerStats(organization, days) {
  return new Promise((resolve, reject) => {
    if(days.length == 0) {
      console.log('empty days passed to createdPartnerStats');
      return resolve({ });
    }
    // var startOfDay = new Date(date.valueOf() - ( date.valueOf() % millis24H ) );
    // var endOfDay = new Date(date.valueOf() - ( date.valueOf() % millis24H ) + millis24H - 1 );
    // console.log('day', startOfDay, endOfDay);
    let mrObject = {};
    // map function
    mrObject.map = function(){
      if(this.createdAt) {
        var date = new Date(this.createdAt.valueOf() - ( this.createdAt.valueOf() % millis24H ) );
        emit( date, 1 );
      }
    };
    // reduce function
    mrObject.reduce = function(key, values){
      return Array.sum( values );
    };

    mrObject.query = { organization: organization._id, createdAt: { $exists: true }, active: true };
    mrObject.scope = { total: 0, millis24H: millis24H };
    mrObject.finalize = function(key, value) {
      total += value;
      return total;
    };

    Partner.mapReduce(mrObject)
    .then((results)=>{
      // console.log('res', results);
      var partnerStats = [];
      var currentCount = 0;
      
      _.each(days, (day)=>{
        var targetResult = _.find(results, (result)=>{
          return (new Date(result._id).getTime() >= (day.getTime() - (day.getTime() % millis24H))) 
            && (new Date(result._id).getTime() < (day.getTime() - (day.getTime() % millis24H) + millis24H)) 
        });
        if(targetResult) {
          currentCount = targetResult.value;
        }
        partnerStats.push({
          organization: organization._id,
          date: new Date((day.getTime() - (day.getTime() % millis24H))),
          partnersInvited: currentCount
        });
      });
      PartnerStats.create(partnerStats)
        .then(createdPartnerStats=>{
          console.log('created partnerStats for org: ', organization.name, createdPartnerStats.length);
          return resolve({results: partnerStats});
        })
        .catch(err=>{
          console.log('Error in partnerStats.controller.createPartnerStats 1');
          console.log(err);
          return reject(err);
        });
    })
    .catch(err=>{
      console.log('Error in partnerStats.controller.createPartnerStats 2');
      console.log(err);
      return reject(err);
    });
  });
}

function updatePartnerStats(organization, date) {
  return new Promise((resolve, reject) => {
    var startOfDay = new Date(date.valueOf() - ( date.valueOf() % millis24H ) );
    var endOfDay = new Date(date.valueOf() - ( date.valueOf() % millis24H ) + millis24H - 1 );
    console.log('PartnerStats.controller.updatePartnerStats day', startOfDay, endOfDay);
    let mrObject = {};
    // map function
    mrObject.map = function(){
      if(this.createdAt) {
        var date = new Date(this.createdAt.valueOf() - ( this.createdAt.valueOf() % millis24H ) );
        emit( date, 1 );
      }
    };
    // reduce function
    mrObject.reduce = function(key, values){
      return Array.sum( values );
    };

    mrObject.query = { 
      organization: organization._id, active: true,
      createdAt: { $lte: endOfDay, $gte: startOfDay }
    };
    mrObject.scope = { total: 0, millis24H: millis24H };
    mrObject.finalize = function(key, value) {
      total += value;
      return total;
    };

    Partner.mapReduce(mrObject)
    .then((results)=>{
      // console.log('res', results);
      PartnerStats.findOne({
        organization: organization._id,
        date: { $lt: date }
      })
      .sort({ date: -1 }).exec()
      .then(lastPartnerStat=>{
        console.log('PartnerStats.controller.updatePartnerStats lastPartnerStat', lastPartnerStat);
        if(lastPartnerStat.date.getTime() < (date.getTime() - date.getTime() % millis24H)) {
          var count = lastPartnerStat.partnersInvited + (results.length>0?results[0].value:0);
          PartnerStats.create({
            organization: organization._id,
            date: new Date((date.getTime() - (date.getTime() % millis24H))),
            partnersInvited: count
          })
          .then(createdPartnerStat=>{
            console.log('PartnerStats.controller.updatePartnerStats created partnerStat for org: ', organization.name, createdPartnerStat.date);
            return resolve({result: createdPartnerStat});
          })
          .catch(err=>{
            console.log('Error in partnerStats.controller.updatePartnerStats 1');
            console.log(err);
            return reject(err);
          });
        } else {
          return resolve({ message: 'stats already created for day'});
        }
      });
      
    })
    .catch(err=>{
      console.log('Error in partnerStats.controller.updatePartnerStats 2');
      console.log(err);
      return reject(err);
    });
  });
}

function createPartnerAcceptedStats(organization, days) {
  return new Promise((resolve, reject) => {
    Partner.find({ organization: organization._id, 'allUsers.0': {$exists: true }})
    .populate('allUsers')
    // .sort({ createdAt: 1})
    .then(partners=>{
      console.log('partnerStats.controller.createPartnerAcceptedStats partners length before filter', partners.length);
      partners = _.filter(partners, partner=>{
        // console.log(partner.allUsers[0]);
        var accepted = false;
        _.each(partner.allUsers, (user)=>{
          if(user.acceptedDate) {
            accepted = true;
          }
        });
        return accepted;
      });
      console.log('partnerStats.controller.createPartnerAcceptedStats partners length after filter', partners.length);
      let partnersGroupByDate = _.groupBy(partners, (partner)=>{
        var firstAcceptedUser = _.minBy(partner.allUsers, (user)=>{
          return new Date(user.acceptedDate).getTime();
        });
        var date = new Date(firstAcceptedUser.acceptedDate.valueOf() -
            ( firstAcceptedUser.acceptedDate.valueOf() % ( 1000 * 60 * 60 * 24 ) )
        );
        // console.log('partner date', date.toISOString());
        return date.toISOString();
      });
      let results = [];
      let cumulativeCount = 0;
      // console.log('partnersGroupByDate before', partnersGroupByDate);
      partnersGroupByDate = _(partnersGroupByDate).toPairs().sortBy(0).fromPairs().value();
      // console.log('partnersGroupByDate after', partnersGroupByDate);
      _.each(partnersGroupByDate, (group, key)=>{
        cumulativeCount += group.length;
        results.push({_id: key, value: cumulativeCount});
      });
      if(results.length == 0) {
        PartnerStats.update({ organization: organization._id }, { $set: { partnersAccepted: 0} }, {multi: true}).exec()
        .then(partnerStatsUpdated=>{
          // console.log('partnerStatsUpdated 1', partnerStatsUpdated);
          return resolve(partnerStatsUpdated);
        })
        .catch(err=>{
          console.log('Error in partnerStats.controller.createPartnerAcceptedStats 1');
          console.log(err);
          return resolve({ message: 'failed for org'+ organization.name})
        })
      } else {
        _.each(results, (result, index)=>{
          if(index == 0) {
            PartnerStats.update({ 
              organization: organization._id,
              date: { $lt: new Date(result._id) }
            },{ 
              $set: { partnersAccepted: 0 } 
            }, 
            { multi: true }).exec()
            .then(partnerStatsUpdated=>{
              // console.log('partnerStatsUpdated 2', partnerStatsUpdated);
              return resolve(partnerStatsUpdated);
            })
            .catch(err=>{
              console.log('Error in partnerStats.controller.createPartnerAcceptedStats 2');
              console.log(err);
              return resolve({ message: 'failed for org'+ organization.name})
            })
          }
          if(index == results.length -1) {
            PartnerStats.update({ 
              organization: organization._id,
              date: { $gte: new Date(result._id) }
            },{ 
              $set: { partnersAccepted: result.value }
            },
            { multi: true }).exec()
            .then(partnerStatsUpdated=>{
              // console.log('partnerStatsUpdated 3', partnerStatsUpdated);
              return resolve(partnerStatsUpdated);
            })
            .catch(err=>{
              console.log('Error in partnerStats.controller.createPartnerAcceptedStats 3');
              console.log(err);
              return resolve({ message: 'failed for org'+ organization.name})
            })
          } else {
            PartnerStats.update({ 
              organization: organization._id,
              date: { $gte: new Date(result._id), $lt: new Date(results[index+1]._id) }
            },{ 
              $set: { partnersAccepted: result.value } 
            }, 
            { multi: true }).exec()
            .then(partnerStatsUpdated=>{
              // console.log('partnerStatsUpdated 4', partnerStatsUpdated);
              return resolve(partnerStatsUpdated);
            })
            .catch(err=>{
              console.log('Error in partnerStats.controller.createPartnerAcceptedStats 4');
              console.log(err);
              return resolve({ message: 'failed for org'+ organization.name})
            })
          }
        });
      }
    })
  });
}

function updatePartnerAcceptedStats(organization, date) {
  return new Promise((resolve, reject) => {
    var startOfDay = new Date(date.valueOf() - ( date.valueOf() % millis24H ) );
    var endOfDay = new Date(date.valueOf() - ( date.valueOf() % millis24H ) + millis24H - 1 );
    console.log('PartnerStats.controller.updatePartnerAcceptedStats day', date, startOfDay, endOfDay);
    Partner.find({ 
      organization: organization._id, 
      'allUsers.0': {$exists: true }, 
      // createdAt: { $lte: endOfDay, $gte: startOfDay }
    })
    .populate('allUsers')
    // .sort({ createdAt: 1})
    .then(partners=>{
      console.log('PartnerStats.controller.updatePartnerAcceptedStats partners 1', partners.length);
      partners = _.filter(partners, partner=>{
        var accepted = false;
        _.each(partner.allUsers, (user)=>{
          if(user.acceptedDate && user.acceptedDate.valueOf() >= startOfDay.getTime() && user.acceptedDate.valueOf() <= endOfDay.getTime()) {
            accepted = true;
          }
        });
        console.log('PartnerStats.controller.updatePartnerAcceptedStats partner accepted', partner.name);
        return accepted;
      });
      console.log('PartnerStats.controller.updatePartnerAcceptedStats partners accepted filtered', partners.length, startOfDay, endOfDay);
      let partnersGroupByDate = _.groupBy(partners, (partner)=>{
        var firstAcceptedUser = _.minBy(partner.allUsers, (user)=>{
          return new Date(user.acceptedDate).getTime();
        });
        var date = new Date(firstAcceptedUser.acceptedDate.valueOf() -
            ( firstAcceptedUser.acceptedDate.valueOf() % ( 1000 * 60 * 60 * 24 ) )
        );
        return date.toISOString();
      });
      // console.log('partnersGroupByDate before', partnersGroupByDate);
      partnersGroupByDate = _(partnersGroupByDate).toPairs().sortBy(0).fromPairs().value();
      // console.log('partnersGroupByDate after', partnersGroupByDate);
      PartnerStats.findOne({organization: organization._id, date: { $lt: startOfDay }})
      .sort({ date: -1 }).exec()
      .then(lastPartnerStat=>{
        console.log('PartnerStats.controller.updatePartnerAcceptedStats lastPartnerStat', lastPartnerStat);
        if(!lastPartnerStat) {
          return resolve({message: 'stats for prev day not found'});
        }
        if(lastPartnerStat.date.getTime() <= (date.getTime() - date.getTime() % millis24H)) {
          var currentCount = partnersGroupByDate[Object.keys(partnersGroupByDate)[0]]?partnersGroupByDate[Object.keys(partnersGroupByDate)[0]].length:0;
          let cumulativeCount = lastPartnerStat.partnersAccepted + currentCount;
        
          PartnerStats.update({
            organization: organization._id,
            date: new Date((date.getTime() - (date.getTime() % millis24H)))
          }, {
            $set: { partnersAccepted: cumulativeCount }
          })
          .then(updatedPartnerStat=>{
            // console.log('updated accepted partnerStat for org: ', organization.name, updatedPartnerStat.date);
            return resolve({result: updatedPartnerStat});
          })
          .catch(err=>{
            console.log('Error in partnerStats.controller.updatePartnerAcceptedStats 1');
            console.log(err);
            return reject(err);
          });
        } else {
          return resolve({ message: 'stats already created for day'});
        }
      });
    });
  });
}

function generateStatsForOrgs(req) {
  return function(organizations) {
    return new Promise((resolve, reject)=>{
      let yesterday = new Date(Date.now() - millis24H );
      if(req.query.millis) {
        yesterday = new Date(parseInt(req.query.millis));
        console.log('custom date', yesterday, req.query.millis);
        if(yesterday.getTime() == NaN) {
          return reject({ message: 'Invalid Date' });
        }
      }
      // console.log('yesterday', yesterday);
      if(organizations.length == 0) {
        return reject({ message: 'organizations not found' });
      }
      return async.mapSeries(organizations, function(organization, callback) {
        PartnerStats.count({organization: organization._id}).exec()
        .then(count=>{
          // console.log('count', count);
          if(count == 0) {
            // loop from partner.createdAt date till yesterday
            let days = [];
            let day = new Date(organization.createdAt.getTime());
            // console.log('organization.createdAt', organization.createdAt, yesterday, (yesterday.getTime() - yesterday.getTime() % millis24H), day.getTime());
            while((yesterday.getTime() - yesterday.getTime() % millis24H + millis24H) > day.getTime()) {
              days.push(day);
              day = new Date(day.getTime() + millis24H); 
            }
            console.log('partnerStats.controller.generateStatsForOrgs creating partnerStats for ', days[0], days[days.length-1]);
            createPartnerStats(organization, days)
            .then(result=>{
              if(result) {
                console.log('partnerStats.controller.generateStatsForOrgs generated for org: ', organization._id, organization.name, result);
                createPartnerAcceptedStats(organization, days)
                .then(acceptedResult=>{
                  if(acceptedResult) {
                    console.log('partnerStats.controller.generateStatsForOrgs partnersAccepted updated for org: ', organization._id, organization.name, result);
                    return callback(null, acceptedResult);
                  } else {
                    console.log('partnerStats.controller.generateStatsForOrgs partnersAccepted update failed for org: ', organization._id, organization.name);
                    return callback(null, acceptedResult)
                  }
                })
              } else {
                console.log('partnerStats.controller.generateStatsForOrgs generation failed for org: ', organization._id, organization.name);
                return callback(null, result);
              }
            })
            .catch(err=>{
              console.log('Error in partnerStats.controller.generateStatsForOrgs 1');
              console.log(err);
              return callback(err);
            });
          } else {
            updatePartnerStats(organization, yesterday)
            .then(result=>{
              if(result) {
                console.log('partnerStats.controller.generateStatsForOrgs generated for org: ', organization._id, organization.name, result);
                updatePartnerAcceptedStats(organization, yesterday)
                .then(acceptedResult=>{
                  if(acceptedResult) {
                    console.log('partnerStats.controller.generateStatsForOrgs partnersAccepted updated for org: ', organization._id, organization.name, acceptedResult);
                    return callback(null, acceptedResult);
                  } else {
                    console.log('partnerStats.controller.generateStatsForOrgs partnersAccepted update failed for org: ', organization._id, organization.name, acceptedResult);
                    return callback(acceptedResult);
                  }
                });
              } else {
                console.log('partnerStats.controller.generateStatsForOrgs generation failed for org: ', organization._id, organization.name);
                return callback(result);
              }
            })
            .catch(err=>{
              console.log('Error in partnerStats.controller.generateStatsForOrgs 2');
              console.log(err);
              return callback(err);
            });
          }
        })
        .catch(err => {
          console.log('Error in partnerStats.controller.generateStatsForOrgs 3');
          console.log(err);
          return callback(err);
        })
      }, function(err, results) {
        if (err) {
          console.log('Error in partnerStats.controller.generateStatsForOrgs 4');
          console.log(err);
          return reject(err);
        } else {
          console.log('partnerStats.controller.generateStatsForOrgs success');
          return resolve({ message: 'partnerStats.controller generateStatsForOrgs success'});
        }
      });
    });
  }
}

// ran everyday to store invited vs accepted stats
export function generateStats(req, res) {
  Organization.find({ active: true }).exec()
    .then(generateStatsForOrgs(req))
    .then(ResponseHelpers.respondWithResult(res))
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

function appendTodaysStatsIfRequired(req) {
  return function(stats) {
    return new Promise((resolve, reject)=>{
      var todayMillis = Date.now();
      console.log('partnerStats.controller.appendTodaysStatsIfRequired todayMillis, req.query.end ', todayMillis, req.query.end);
      if(req.query.end && (todayMillis - (todayMillis%millis24H)) > req.query.end) {
        console.log('partnerStats.controller.appendTodaysStatsIfRequired not appending todays stats ', todayMillis, req.query.end);
        return resolve(stats);
      } else {
        let mrObject = {};
        // map function
        mrObject.map = function(){
          if(this.createdAt) {
            var date = new Date(this.createdAt.valueOf() - ( this.createdAt.valueOf() % millis24H ) );
            emit( date, 1 );
          }
        };
        // reduce function
        mrObject.reduce = function(key, values){
          return Array.sum( values );
        };

        mrObject.query = { 
          organization: req.params.orgid, active: true,
          createdAt: { $gte: new Date(todayMillis - (todayMillis%millis24H)) }
        };
        mrObject.scope = { total: 0, millis24H: millis24H };
        mrObject.finalize = function(key, value) {
          total += value;
          return total;
        };

        return Partner.mapReduce(mrObject)
          .then((results)=>{
            let statsObj = {
              _id: 'NEW',
              organization: req.params.orgid,
              date: new Date(Date.now() - (Date.now()%millis24H)).toISOString()
            };
            if(results.length >0) {
              statsObj.partnersInvited = (stats[stats.length - 1]?stats[stats.length - 1].partnersInvited:0) + results[0].value;
            } else {
              statsObj.partnersInvited = stats[stats.length - 1]?stats[stats.length - 1].partnersInvited:0;
            }
            console.log('partnerStats.controller.appendTodaysStatsIfRequired res', results);
            return Partner.find({
              organization: req.params.orgid, active: true,
              'allUsers.0': {$exists: true }, 
              // createdAt: { $lte: new Date(todayMillis), $gte: new Date(todayMillis - (todayMillis%millis24H)) }
            })
            .populate('allUsers')
            // .sort({ createdAt: 1})
            .then(partners=>{
              console.log('partnerStats.controller.appendTodaysStatsIfRequired partners before filter', partners.length);
              partners = _.filter(partners, partner=>{
                // console.log('appendTodaysStatsIfRequired', partner.allUsers[0]);
                var accepted = false;
                _.each(partner.allUsers, (user)=>{
                  if(user.acceptedDate && user.acceptedDate.valueOf() >= (todayMillis - (todayMillis%millis24H)) && user.acceptedDate.valueOf() <= todayMillis) {
                    accepted = true;
                  }
                })
                return accepted;
              });
              console.log('partnerStats.controller.appendTodaysStatsIfRequired partners after filter', partners.length);
              let partnersGroupByDate = _.groupBy(partners, (partner)=>{
                var firstAcceptedUser = _.minBy(partner.allUsers, (user)=>{
                  return new Date(user.acceptedDate).getTime();
                });
                var date = new Date(firstAcceptedUser.acceptedDate.valueOf() -
                    ( firstAcceptedUser.acceptedDate.valueOf() % ( 1000 * 60 * 60 * 24 ) )
                );
                return date.toISOString();
              });
              // console.log('partnersGroupByDate before', partnersGroupByDate);
              partnersGroupByDate = _(partnersGroupByDate).toPairs().sortBy(0).fromPairs().value();
              // console.log('partnersGroupByDate after', partnersGroupByDate);
              if(partnersGroupByDate && partnersGroupByDate[Object.keys(partnersGroupByDate)[0]] && partnersGroupByDate[Object.keys(partnersGroupByDate)[0]].length>0) {
                let count = partnersGroupByDate[Object.keys(partnersGroupByDate)[0]].length;
                let cumulativeCount = count + (stats[stats.length - 1]?stats[stats.length - 1].partnersAccepted:0);
                statsObj.partnersAccepted = cumulativeCount;
              } else {
                statsObj.partnersAccepted = stats[stats.length - 1]?stats[stats.length - 1].partnersAccepted:0;
              }
              stats.push(statsObj);
              return resolve(stats);
            })
            .catch(err=>{
              console.log('Error in partnerStats.controller.appendTodaysStatsIfRequired 1');
              console.log(err);
              return reject(err);
            });
        });
      }
    });
  }
}

// Gets a list of PartnerStats for org
export function getPartnerStats(req, res) {
  var query = {
    organization: req.params.orgid
  };
  if(req.query.start && req.query.end) {
    var startDay = new Date(req.query.start - req.query.start % millis24H);
    var endDay = new Date(req.query.end - (req.query.end % millis24H) + millis24H - 1);
    query.date = { $gte: startDay, $lte: endDay };
    console.log('partnerStats.controller.getPartnerStats startDay', req.query.start, startDay, endDay);
  } 
  // else {
  //   var startDay = new Date((Date.now() - Date.now() % millis24H) - (30 * millis24H));
  //   query.date = { $gte: startDay };
  // }
  return PartnerStats.find(query)
  .sort({ date: 1 }).lean().exec()
  .then(appendTodaysStatsIfRequired(req))
  .then(ResponseHelpers.respondWithResult(res))
  .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Gets a list of PartnerStats
export function index(req, res) {
  //return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}])
  //   .then(() => {
      return PartnerStats.find().exec()
        .then(ResponseHelpers.respondWithResult(res))
        .catch(ResponseHelpers.respondWithErrorEntity(res));
   // })
   // .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Gets a single PartnerStats from the DB
export function show(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.id, type: 'partnerStats'}])
    .then(() => {
      return PartnerStats.findById(req.params.id).exec()
        .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'partnerStats not found.'}))
        .then(ResponseHelpers.respondWithResult(res))
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Creates a new PartnerStats in the DB
export function create(req, res) {
  return PartnerStats.create(req.body)
    .then(ResponseHelpers.respondWithResult(res, 201))
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Updates an existing PartnerStats in the DB
export function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return ResponseHelpers.handleValidObjectIds([{value: req.params.id, type: 'partnerStats'}])
    .then(() => {
      return PartnerStats.findById(req.params.id).exec()
        .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Partner statistics not found.'})(res))
        .then(saveUpdates(req.body))
        .then(ResponseHelpers.respondWithResult(res))
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Deletes a PartnerStats from the DB
export function destroy(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.id, type: 'partnerStats'}])
    .then(() => {
      return PartnerStats.findById(req.params.id).exec()
        .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'partnerStats not found.'}))
        .then(removeEntity(res))
        .then(ResponseHelpers.respondWithResult(res, 204))
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}
