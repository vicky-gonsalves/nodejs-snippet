'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var partnerStatsCtrlStub = {
  index: 'partnerStatsCtrl.index',
  show: 'partnerStatsCtrl.show',
  create: 'partnerStatsCtrl.create',
  update: 'partnerStatsCtrl.update',
  destroy: 'partnerStatsCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var partnerStatsIndex = proxyquire('./index.js', {
  'express': {
    Router: function() {
      return routerStub;
    }
  },
  './partnerStats.controller': partnerStatsCtrlStub
});

describe('PartnerStats API Router:', function() {

  it('should return an express router instance', function() {
    partnerStatsIndex.should.equal(routerStub);
  });

  describe('GET /api/partner-stats', function() {

    it('should route to partnerStats.controller.index', function() {
      routerStub.get
        .withArgs('/', 'partnerStatsCtrl.index')
        .should.have.been.calledOnce;
    });

  });

  describe('GET /api/partner-stats/:id', function() {

    it('should route to partnerStats.controller.show', function() {
      routerStub.get
        .withArgs('/:id', 'partnerStatsCtrl.show')
        .should.have.been.calledOnce;
    });

  });

  describe('POST /api/partner-stats', function() {

    it('should route to partnerStats.controller.create', function() {
      routerStub.post
        .withArgs('/', 'partnerStatsCtrl.create')
        .should.have.been.calledOnce;
    });

  });

  describe('PUT /api/partner-stats/:id', function() {

    it('should route to partnerStats.controller.update', function() {
      routerStub.put
        .withArgs('/:id', 'partnerStatsCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('PATCH /api/partner-stats/:id', function() {

    it('should route to partnerStats.controller.update', function() {
      routerStub.patch
        .withArgs('/:id', 'partnerStatsCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('DELETE /api/partner-stats/:id', function() {

    it('should route to partnerStats.controller.destroy', function() {
      routerStub.delete
        .withArgs('/:id', 'partnerStatsCtrl.destroy')
        .should.have.been.calledOnce;
    });

  });

});
