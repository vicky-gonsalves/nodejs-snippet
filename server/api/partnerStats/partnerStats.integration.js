'use strict';

var app = require('../..');
import request from 'supertest';

var newPartnerStats;

describe('PartnerStats API:', function() {

  describe('GET /api/partner-stats', function() {
    var partnerStatss;

    beforeEach(function(done) {
      request(app)
        .get('/api/partner-stats')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          partnerStatss = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      partnerStatss.should.be.instanceOf(Array);
    });

  });

  describe('POST /api/partner-stats', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/partner-stats')
        .send({
          name: 'New PartnerStats',
          info: 'This is the brand new partnerStats!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newPartnerStats = res.body;
          done();
        });
    });

    it('should respond with the newly created partnerStats', function() {
      newPartnerStats.name.should.equal('New PartnerStats');
      newPartnerStats.info.should.equal('This is the brand new partnerStats!!!');
    });

  });

  describe('GET /api/partner-stats/:id', function() {
    var partnerStats;

    beforeEach(function(done) {
      request(app)
        .get('/api/partner-stats/' + newPartnerStats._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          partnerStats = res.body;
          done();
        });
    });

    afterEach(function() {
      partnerStats = {};
    });

    it('should respond with the requested partnerStats', function() {
      partnerStats.name.should.equal('New PartnerStats');
      partnerStats.info.should.equal('This is the brand new partnerStats!!!');
    });

  });

  describe('PUT /api/partner-stats/:id', function() {
    var updatedPartnerStats;

    beforeEach(function(done) {
      request(app)
        .put('/api/partner-stats/' + newPartnerStats._id)
        .send({
          name: 'Updated PartnerStats',
          info: 'This is the updated partnerStats!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedPartnerStats = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedPartnerStats = {};
    });

    it('should respond with the updated partnerStats', function() {
      updatedPartnerStats.name.should.equal('Updated PartnerStats');
      updatedPartnerStats.info.should.equal('This is the updated partnerStats!!!');
    });

  });

  describe('DELETE /api/partner-stats/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/api/partner-stats/' + newPartnerStats._id)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when partnerStats does not exist', function(done) {
      request(app)
        .delete('/api/partner-stats/' + newPartnerStats._id)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
