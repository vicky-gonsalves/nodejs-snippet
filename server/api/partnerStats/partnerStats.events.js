/**
 * PartnerStats model events
 */

'use strict';

import {EventEmitter} from 'events';
import PartnerStats from './partnerStats.model';
var PartnerStatsEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
PartnerStatsEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  PartnerStats.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    PartnerStatsEvents.emit(event + ':' + doc._id, doc);
    PartnerStatsEvents.emit(event, doc);
  }
}

export default PartnerStatsEvents;
