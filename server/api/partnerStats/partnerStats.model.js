'use strict';

import mongoose from 'mongoose';
mongoose.Promise = require('bluebird');
import {Schema} from 'mongoose';

var PartnerStatsSchema = new mongoose.Schema({
  date: {
    type: Date,
    default: Date.now
  },
  organization: {type: Schema.Types.ObjectId, ref: 'Organization'},
  partnersInvited: Number,
  partnersAccepted: Number
}, {
  timestamps: true
});

export default mongoose.model('PartnerStats', PartnerStatsSchema);
