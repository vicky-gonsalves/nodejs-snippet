'use strict';

var express = require('express');
var controller = require('./partnerStats.controller');

var router = express.Router();

router.post('/create', controller.generateStats);

module.exports = router;
