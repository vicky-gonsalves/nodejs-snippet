'use strict';

import config from "../../config/environment";
import User from "../../api/user/user.model";
import * as logger from "../../components/logger.service";
import * as auth from "../../auth/auth.service";

const express = require('express');
const controller = require('./organization.controller');
const CardCtrl = require('../card/card.controller');
const PartnerCtrl = require('../partner/partner.controller');
const PartnerStatsCtrl = require('../partnerStats/partnerStats.controller');
const UserCtrl = require('../user/user.controller');
const NotificationCtrl = require('../notification/notification.controller');
const NotificationLogCtrl = require('../notificationLog/notificationLog.controller');
const AudienceCtrl = require('../audience/audience.controller');
const AudienceLinkCtrl = require('../audienceLink/audienceLink.controller');
const ApiKeyCtrl = require('../apiKey/apiKey.controller');
const OrgVersionCtrl = require('../organizationApp/organizationApp.controller');
const CoreAppCtrl = require('../coreApp/coreApp.controller');

// Passport Configuration
require('../../auth/apilogin/passport').setup(User, config);

var router = express.Router();

/*Organization Generic Routes*/
router.get('/orgs/:id/keys', logger.putOtherLog(), controller.getKeys);
router.get('/orgs/:id/config', logger.putOtherLog(), controller.getConfig);
router.get('/orgs/:id/orgwithconfig/:partnerId?', logger.putOtherLog(), controller.getOrgWithConfig);
router.get('/orgs', logger.putOtherLog(), controller.index);
router.get('/orgs/code/:code', logger.putOtherLog(), controller.getOrgByCode);
router.get('/orgs/:id', logger.putOtherLog(), controller.show);
router.get('/orgs/:orgid/index/mappings', logger.putOtherLog(), controller.checkOrgExistance(), controller.getIndexMappings);
router.get('/orgs/:orgid/style', logger.putOtherLog(), auth.isAuthenticated(), controller.checkOrgExistance(), controller.getStyle);
router.post('/orgs/', auth.hasKeySecretPair(), auth.hasRole('superadmin'), logger.putOtherLog(), controller.create);
router.post('/orgs/:orgid/style', logger.putOtherLog(), auth.hasRole('superadmin'), controller.checkOrgExistance(), controller.saveStyle);
router.patch('/orgs/:orgid/style', logger.putOtherLog(), auth.hasRole('superadmin'), controller.checkOrgExistance(), controller.saveStyle);
router.put('/orgs/:orgid/style', logger.putOtherLog(), auth.hasRole('superadmin'), controller.checkOrgExistance(), controller.saveStyle);
router.put('/orgs/:id', auth.hasKeySecretPair(), auth.hasRole('superadmin'), logger.putOtherLog(), controller.update);
router.patch('/orgs/:id', auth.hasKeySecretPair(), auth.hasRole('superadmin'), logger.putOtherLog(), controller.update);
router.delete('/orgs/:id', auth.hasKeySecretPair(), auth.hasRole('superadmin'), logger.putOtherLog(), controller.destroy);
router.get('/orgs/:orgid/stats', /* auth.hasKeySecretPair(), auth.hasRole('organization-admin'), logger.putOtherLog(), controller.checkOrgExistance(),  controller.checkUserExistanceInOrgByID(),*/controller.getStats);

/*Custom External Module routes*/
/*Partners Routes*/
/* TODO Order is important for hasKeySecretPair & isAuthenticated auth.isAuthenticated(): isAuthenticated must come after hasKeySecretPair*/
router.get('/orgs/:orgid/partners/stats', auth.hasKeySecretPair(), auth.hasRoles(['superadmin', 'organization-admin']), logger.putOtherLog(), controller.checkOrgExistance(), PartnerStatsCtrl.getPartnerStats);
router.get('/orgs/:orgid/partners/:withusers?', auth.hasKeySecretPair(), auth.hasRoles(['superadmin', 'organization-admin']), logger.putOtherLog(), controller.checkOrgExistance(), PartnerCtrl.getOrganizationPartners);
router.get('/orgs/:orgid/partners/all/types', auth.hasKeySecretPair(), auth.hasRoles(['superadmin', 'organization-admin']), logger.putOtherLog(), controller.checkOrgExistance(), PartnerCtrl.getAllPartnerTypes);
router.get('/orgs/:orgid/partner/:partnerId', auth.hasKeySecretPair(), auth.hasRoles(['superadmin', 'organization-admin']), controller.checkOrgExistance(), PartnerCtrl.getSingleOrganizationPartner);
router.get('/orgs/:orgid/partners/:id/users', auth.hasKeySecretPair(), auth.hasRoles(['superadmin', 'organization-admin']), controller.checkOrgExistance(), PartnerCtrl.getPartnerUsers);
router.get('/orgs/:orgid/partnertypes', auth.hasKeySecretPair(), auth.isAuthenticated(), controller.checkOrgExistance(), controller.getPartnerTypes);
router.put('/orgs/:orgid/partnertypes', auth.hasKeySecretPair(), auth.hasRoles(['superadmin', 'organization-admin']), controller.checkOrgExistance(), controller.postPartnerTypes);
router.post('/orgs/:orgid/partners/', auth.hasKeySecretPair(), auth.hasRoles(['superadmin', 'organization-admin']), controller.checkOrgExistance(), PartnerCtrl.create);
router.put('/orgs/:orgid/partners/:partnerId', auth.hasKeySecretPair(), auth.hasRoles(['superadmin', 'organization-admin']), controller.checkOrgExistance(), PartnerCtrl.update);
router.patch('/orgs/:orgid/partners/:partnerId', auth.hasKeySecretPair(), auth.hasRoles(['superadmin', 'organization-admin']), controller.checkOrgExistance(), PartnerCtrl.patch);
router.delete('/orgs/:orgid/partners/:partnerId', auth.hasKeySecretPair(), auth.hasRoles(['superadmin', 'organization-admin']), logger.putOtherLog(), controller.checkOrgExistance(), PartnerCtrl.destroy);
router.patch('/orgs/:orgid/partners/:id/extended', auth.hasKeySecretPair(), auth.hasRoles(['superadmin', 'organization-admin']), controller.checkOrgExistance(), PartnerCtrl.patchExtended);

/*Cards Routes*/
router.post('/orgs/:orgid/cards/', auth.hasKeySecretPair(), auth.hasRoles(['superadmin', 'organization-admin']), logger.putOtherLog(), controller.checkOrgExistance(), CardCtrl.create);
router.post('/orgs/:orgid/cards/int', auth.hasKeySecretPair(), auth.hasRoles(['superadmin', 'organization-admin']), logger.putOtherLog(), controller.checkOrgExistance(), CardCtrl.createFromShortcuts);
router.get('/orgs/:orgid/cards/:start/:limit', auth.hasKeySecretPair(), logger.saveRequestData({type: 'feed'}), logger.putOtherLog(), controller.checkOrgExistance(), controller.orgFeed);
router.post('/orgs/:orgid/cards/:start/:limit', auth.hasKeySecretPair(), logger.saveRequestData({type: 'feed'}), logger.putOtherLog(), controller.checkOrgExistance(), controller.orgFeed);
router.get('/orgs/:orgid/users/me/feed/:start/:limit', auth.hasKeySecretPair(), auth.isAuthenticated(), logger.saveRequestData({type: 'feed'}), logger.putOtherLog(), controller.checkOrgExistance(), controller.userFeed);
router.post('/orgs/:orgid/users/me/feed/:start/:limit', auth.hasKeySecretPair(), auth.isAuthenticated(), logger.saveRequestData({type: 'feed'}), logger.putOtherLog(), controller.checkOrgExistance(), controller.userFeed);
router.get('/orgs/:orgid/users/:userid/feed/:start/:limit', auth.hasKeySecretPair(), auth.hasRoles(['superadmin', 'organization-admin']), logger.saveRequestData({type: 'feed'}), logger.putOtherLog(), controller.checkOrgExistance(), controller.userFeed);
router.post('/orgs/:orgid/users/:userid/feed/:start/:limit', auth.hasKeySecretPair(), auth.hasRoles(['superadmin', 'organization-admin']), logger.saveRequestData({type: 'feed'}), logger.putOtherLog(), controller.checkOrgExistance(), controller.userFeed);
router.get('/orgs/:orgid/cards/:cardid', /* auth.hasKeySecretPair(),*/ auth.isAuthenticatedSeamlessly(), logger.saveRequestData({type: 'view'}), logger.putOtherLog(), controller.checkOrgExistance(), controller.getSingleOrgCard); //hasKeySecretPair shouldnt be used here
router.put('/orgs/:orgid/cards/:id', auth.hasKeySecretPair(), auth.hasRoles(['superadmin', 'organization-admin']), logger.putOtherLog(), controller.checkOrgExistance(), CardCtrl.update);
router.patch('/orgs/:orgid/cards/:id', auth.hasKeySecretPair(), auth.hasRoles(['superadmin', 'organization-admin']), logger.putOtherLog(), controller.checkOrgExistance(), CardCtrl.update); //Can also be used to send Notifications { status:published, notify: { schedule:immediate } }
router.delete('/orgs/:orgid/cards/:cardid', auth.hasKeySecretPair(), auth.hasRoles(['superadmin', 'organization-admin']), logger.putOtherLog(), controller.checkOrgExistance(), CardCtrl.destroy);
router.delete('/orgs/:orgid/cards', auth.hasKeySecretPair(), auth.hasRoles(['superadmin', 'organization-admin']), logger.putOtherLog(), controller.checkOrgExistance(), CardCtrl.destroyAllCards);
router.get('/orgs/:orgid/tags', auth.hasKeySecretPair(), logger.putOtherLog(), controller.checkOrgExistance(), CardCtrl.getAllTags);

/*Sidebars Routes*/
router.get('/orgs/:orgid/sidebar', auth.hasKeySecretPair(), auth.isAuthenticated(), logger.putOtherLog(), controller.checkOrgExistance(), controller.getOrgSidebars);
router.post('/orgs/:orgid/sidebar', auth.hasKeySecretPair(), logger.putOtherLog(), controller.checkOrgExistance(), controller.createUpdateOrgSidebars);

/*Authentication Routes*/
router.use('/orgs/:orgid/users/me/login', logger.putDeviceLog(), logger.putOtherLog(), controller.checkOrgExistance(), controller.checkUserExistanceInOrgByEmail(), OrgVersionCtrl.checkUpgrade(), require('../../auth/apilogin'));
router.use('/orgs/:orgid/users/me', logger.saveRequestData({type: 'user info'}), auth.isAuthenticated(), logger.putOtherLog(), controller.checkOrgExistance(), controller.checkUserExistanceInOrgByToken(), UserCtrl.me);  //TODO add controller.checkUserExistanceInOrgByToken in all apis when auth.isAuthenticated is added in all apis
router.post('/orgs/:orgid/partners/:partnerId/user', logger.saveRequestData({type: 'register'}), auth.hasKeySecretPair(), /*auth.isAuthenticated(),*/ /*logger.putDeviceLog(), */logger.putOtherLog(), controller.checkOrgExistance(), controller.createUser);
router.patch('/orgs/:orgid/partners/:partnerId/users/:id/resend-token', auth.hasRoles(['superadmin', 'organization-admin']), logger.putDeviceLog(), logger.putOtherLog(), controller.checkOrgExistance(), controller.checkUserExistanceInOrgByID(), UserCtrl.resendUserToken);
router.patch('/orgs/:orgid/resend-token', auth.hasRoles(['superadmin', 'organization-admin']), logger.putOtherLog(), controller.checkOrgExistance(), controller.checkUserExistanceInOrgByToken(), UserCtrl.resendBulkUserToken);
router.post('/orgs/:orgid/partner/:partnerId/user/:id/resend-sms-invite', logger.putOtherLog(), controller.checkOrgExistance(), controller.checkUserExistanceInOrgByID(), UserCtrl.resendInviteSMS);
router.post('/orgs/:orgid/partner/:partnerId/user/:id', auth.hasRoles(['superadmin']), logger.putOtherLog(), UserCtrl.setPassword);  //superuser can set users password or mark as Accepted

/*Notifications*/
router.post('/orgs/:orgid/notifications', auth.hasKeySecretPair(), logger.putOtherLog(), controller.checkOrgExistance(), NotificationCtrl.create);
router.put('/orgs/:orgid/notifications/:id', auth.hasKeySecretPair(), logger.putOtherLog(), controller.checkOrgExistance(), NotificationCtrl.update);
router.delete('/orgs/:orgid/notifications/:id', auth.hasKeySecretPair(), logger.putOtherLog(), controller.checkOrgExistance(), NotificationCtrl.destroy);
router.post('/orgs/:orgid/users/devices/:did', auth.isAuthenticated(), logger.putOtherLog(), controller.checkOrgExistance(), controller.checkUserExistanceInOrgByToken(), NotificationCtrl.pingPush);

/*Audience Routes*/
router.post('/orgs/:orgid/audiences/', auth.hasKeySecretPair(), logger.putOtherLog(), controller.checkOrgExistance(), AudienceCtrl.create);
router.get('/orgs/:orgid/audiences/:ids/stats', auth.hasKeySecretPair(), logger.putOtherLog(), controller.checkOrgExistance(), controller.getAudienceStats);
router.get('/orgs/:orgid/audiences/', auth.hasKeySecretPair(), logger.putOtherLog(), controller.checkOrgExistance(), AudienceCtrl.index);
router.get('/orgs/:orgid/audiences/:id', auth.hasKeySecretPair(), logger.putOtherLog(), controller.checkOrgExistance(), AudienceCtrl.show);
router.put('/orgs/:orgid/audiences/:id', auth.hasKeySecretPair(), logger.putOtherLog(), controller.checkOrgExistance(), AudienceCtrl.update);
router.patch('/orgs/:orgid/audiences/:id', auth.hasKeySecretPair(), logger.putOtherLog(), controller.checkOrgExistance(), AudienceCtrl.update);
router.delete('/orgs/:orgid/audiences/:id', auth.hasKeySecretPair(), logger.putOtherLog(), controller.checkOrgExistance(), AudienceCtrl.destroy);


/*Audiencelink*/
router.get('/orgs/partner/:partnerid/audiencelinks/', AudienceLinkCtrl.getList);

/*Audience Routes for ElasticSearch*/
router.post('/orgs/:orgid/audiences/preview', auth.hasKeySecretPair(), logger.putOtherLog(), controller.checkOrgExistance(), AudienceCtrl.previewElasticSearch);

/*API Keys*/
router.post('/orgs/:orgid/apikeys', auth.hasRole('superadmin'), logger.putOtherLog(), controller.checkOrgExistance(), controller.checkUserExistanceInOrgByToken(), ApiKeyCtrl.createAPIKey);
// router.put('/orgs/:orgid/apikeys', auth.isAuthenticated(), logger.putOtherLog(), controller.checkOrgExistance(), controller.checkUserExistanceInOrgByToken(), ApiKeyCtrl.updateAPIKey);


/*App Versions*/
router.get('/orgs/:orgid/versions', auth.hasKeySecretPair(), auth.hasRole('superadmin'), logger.putOtherLog(), controller.checkOrgExistance(), OrgVersionCtrl.getVersionsList);
router.get('/orgs/:orgid/versions/:id', auth.hasKeySecretPair(), auth.hasRole('superadmin'), logger.putOtherLog(), controller.checkOrgExistance(), OrgVersionCtrl.getSingleVersionById);
router.post('/orgs/:orgid/versions', auth.hasKeySecretPair(), auth.hasRole('superadmin'), logger.putOtherLog(), controller.checkOrgExistance(), OrgVersionCtrl.createVersion);
router.put('/orgs/:orgid/versions/:id', auth.hasKeySecretPair(), auth.hasRole('superadmin'), logger.putOtherLog(), controller.checkOrgExistance(), OrgVersionCtrl.updateVersion);
router.patch('/orgs/:orgid/versions/:id', auth.hasKeySecretPair(), auth.hasRole('superadmin'), logger.putOtherLog(), controller.checkOrgExistance(), OrgVersionCtrl.updateVersion);
router.delete('/orgs/:orgid/versions/:id', auth.hasKeySecretPair(), auth.hasRole('superadmin'), logger.putOtherLog(), controller.checkOrgExistance(), OrgVersionCtrl.deleteVersion);


/*Core App Versions*/
router.get('/orgs/versions/core', auth.hasKeySecretPair(), auth.hasRole('superadmin'), logger.putOtherLog(), CoreAppCtrl.getCoreVersionsList);
router.post('/orgs/versions/core', auth.hasKeySecretPair(), auth.hasRole('superadmin'), logger.putOtherLog(), CoreAppCtrl.createCoreVersion);
router.get('/orgs/versions/core/:id', auth.hasKeySecretPair(), auth.hasRole('superadmin'), logger.putOtherLog(), CoreAppCtrl.getSingleCoreVersionsList);
router.put('/orgs/versions/core/:id', auth.hasKeySecretPair(), auth.hasRole('superadmin'), logger.putOtherLog(), CoreAppCtrl.updateVersion);
router.patch('/orgs/versions/core/:id', auth.hasKeySecretPair(), auth.hasRole('superadmin'), logger.putOtherLog(), CoreAppCtrl.updateVersion);
router.delete('/orgs/versions/core/:id', auth.hasKeySecretPair(), auth.hasRole('superadmin'), logger.putOtherLog(), CoreAppCtrl.deleteVersion);

/*CSV import users*/
router.post('/orgs/:orgid/partners/bulk', auth.hasKeySecretPair(), auth.isAuthenticated(), logger.putOtherLog(), controller.createBulkUser);
router.post('/user-phone/bulk', auth.hasKeySecretPair(), auth.isAuthenticated(), logger.putOtherLog(), controller.updateBulkPhones);


/*Notification Logs*/
router.get('/orgs/:orgid/:cardid/notification-logs', auth.hasKeySecretPair(), logger.putOtherLog(), NotificationLogCtrl.getLogs);

/*Resync All Partners in ES*/
router.get('/orgs/resync/all/es/indexes', controller.resyncAll);


/*InviteLinks correction call*/
router.get('/orgs/:orgid/updateInvites', logger.putOtherLog(), controller.checkOrgExistance(), UserCtrl.updateInvites);

/*Resend SMS/Email Invites to Invited users- Does not uses CSV*/
router.post('/orgs/:orgid/resendInvites', auth.hasRole('superadmin'), logger.putOtherLog(), controller.checkOrgExistance(), UserCtrl.resendBulkInvites);

/*Get All org Users*/
router.get('/orgs/:orgid/getAllUsersInOrg', auth.hasRole('superadmin'), logger.putOtherLog(), controller.checkOrgExistance(), UserCtrl.getAllUsersInOrg);

module.exports = router;
