'use strict';

import _ from "lodash";
import compose from "composable-middleware";
import Organization from "./organization.model";
import OrganizationCounter from "./organizationCounter.model";
import * as OrgVersionCtrl from "../organizationApp/organizationApp.controller";
import * as PartnerCtrl from "../partner/partner.controller";
import * as CardCtrl from "../card/card.controller";
import * as SidebarCtrl from "../sidebar/sidebar.controller";
import * as UserCtrl from "../user/user.controller";
import * as AudienceCtrl from "../audience/audience.controller";
import * as AudienceLinkCtrl from "../audienceLink/audienceLink.controller";
import * as NotificationCtrl from "../notification/notification.controller";
import * as APIKeyCtrl from "../apiKey/apiKey.controller";
import * as LinkAnalyticsCtrl from "../linkAnalytic/linkAnalytic.controller";
import * as DeviceCtrl from "../device/device.controller";
import config from "../../config/environment";
import * as Mailer from "../../components/emails/sendMail";
import Partner from "../partner/partner.model";
const mongoose = require('mongoose');
const es = require('../../components/elasticSearch/es');
const handlebars = require('handlebars');
const cloudinary = require('cloudinary');
const moment = require('moment');
const Hashids = require('hashids');
const validator = require('validator');
const Promise = require('bluebird');
const ResponseHelpers = require('../../components/responseHelpers/responseHelpers');
const qrImageAPIPath = config.DOMAIN + '/api/v1/qrcodes';
const shortid = require('shortid');
const request = require('request');
const parse = require('csv-parse');
const async = require('async');
const uuid = require('node-uuid');
const lzutf8 = require('lzutf8');
const tv4 = require('tv4');
const fs = require('fs');
const path = require('path');
const wh = require('../../components/webhook/webhook');
const sms = require('../../components/twilio/sms');
let workerFarm = require('worker-farm')
  , workers = workerFarm(require.resolve('./child'))
  , ret = 0;

Promise.config({
  // Enables all warnings except forgotten return statements.
  warnings: {
    wForgottenReturn: false
  }
});

cloudinary.config({
  cloud_name: config.cloudinary.cloud_name,
  api_key: config.cloudinary.api_key,
  api_secret: config.cloudinary.api_secret
});

function sanitizeEntities(data, toValidate = []) {
  return new Promise((resolve, reject) => {
    try {
      let sanitizedData = JSON.parse(JSON.stringify(data));
      if (data.name && toValidate.indexOf('name') > -1) {
        sanitizedData.name = validator.trim(data.name);
      }
      if (data.role && toValidate.indexOf('role') > -1) {
        sanitizedData.role = validator.trim(data.role);
      }
      if (data.phone && toValidate.indexOf('phone') > -1) {
        sanitizedData.phone = validator.trim(data.phone);
      }
      if (data.email && toValidate.indexOf('email') > -1) {
        sanitizedData.email = validator.normalizeEmail(validator.stripLow(data.email.replace(/[^\x00-\x7F]/g, ""), false), {lowercase: true});
      }
      if (data.partnerId && toValidate.indexOf('partnerId') > -1) {
        sanitizedData.partnerId = validator.trim(validator.stripLow(data.partnerId, false));
      }
      if (data.partnerEmail && toValidate.indexOf('partnerEmail') > -1) {
        sanitizedData.partnerEmail = validator.normalizeEmail(validator.stripLow(data.partnerEmail.replace(/[^\x00-\x7F]/g, ""), false), {lowercase: true});
      }
      if (data.partnerName && toValidate.indexOf('partnerName') > -1) {
        sanitizedData.partnerName = validator.trim(data.partnerName);
      }
      if (data.sendgridId && toValidate.indexOf('sendgridId') > -1) {
        sanitizedData.sendgridId = validator.trim(data.sendgridId);
      }
      if (data.sendgridIdForPassword && toValidate.indexOf('sendgridIdForPassword') > -1) {
        sanitizedData.sendgridIdForPassword = validator.trim(data.sendgridIdForPassword);
      }
      if (data.apps) {
        sanitizedData.apps = {};
        if (data.apps.android && toValidate.indexOf('apps.android') > -1) {
          sanitizedData.apps.android = validator.trim(data.apps.android);
        }
        if (data.apps.ios && toValidate.indexOf('apps.ios') > -1) {
          sanitizedData.apps.ios = validator.trim(data.apps.ios);
        }
        if (data.apps.iosAppStoreId && toValidate.indexOf('apps.iosAppStoreId') > -1) {
          sanitizedData.apps.iosAppStoreId = validator.trim(data.apps.iosAppStoreId);
        }
      }
      if (data.firebase) {
        sanitizedData.firebase = {};
        if (data.firebase.domain && toValidate.indexOf('firebase.domain') > -1) {
          sanitizedData.firebase.domain = validator.trim(data.firebase.domain);
        }
        if (data.firebase.apiKey && toValidate.indexOf('firebase.apiKey') > -1) {
          sanitizedData.firebase.apiKey = validator.trim(data.firebase.apiKey);
        }
      }
      if (!data.import) {
        if (data.image && toValidate.indexOf('image') > -1) {
          sanitizedData.image = validator.trim(data.image);
        }
        if (data.intro_video && toValidate.indexOf('intro_video') > -1) {
          sanitizedData.intro_video = validator.trim(data.intro_video);
        }
        if (data.es_index && toValidate.indexOf('es_index') > -1) {
          sanitizedData.es_index = validator.trim(data.es_index);
        }
        if (data.androidKey && toValidate.indexOf('androidKey') > -1) {
          sanitizedData.androidKey = validator.trim(data.androidKey);
        }
        if (data.iosKey && toValidate.indexOf('iosKey') > -1) {
          sanitizedData.iosKey = validator.trim(data.iosKey);
        }
      } else if (data.import && toValidate.indexOf('import') > -1) {
        sanitizedData.import = {source: '', objects: []};
        if (data.import.source) {
          sanitizedData.import.source = validator.trim(data.import.source);
        }
        if (data.import.objects && data.import.objects.length) {
          sanitizedData.import.objects = data.import.objects.map(obj => validator.trim(obj));
        }
      }

      return resolve(sanitizedData);
    } catch (e) {
      console.log('Error in organization.controller.sanitizeEntities 1');
      return reject(e);
    }
  })
}

function validateEntities(toValidate = [], update = false) {
  return function(data) {
    return new Promise((resolve, reject) => {
      if (data) {
        let errors = {type: 'InvalidParams', code: 422, msg: []};
        //Validation
        if (toValidate.indexOf('name') > -1) {
          if (!update && (!(data.name) || validator.isNull(data.name))) {
            errors.msg.push({name: 'name is required'});
          } else if (data.name && !validator.isLength(data.name, {min: 1, max: 150})) {
            errors.msg.push({name: 'name must be between 1 to 150 characters long'});
          }
        }
        if (toValidate.indexOf('partnerName') > -1) {
          if (!update && (!(data.partnerName) || validator.isNull(data.partnerName))) {
            errors.msg.push({partnerName: 'partnerName is required'});
          } else if (data.partnerName && !validator.isLength(data.partnerName, {min: 1, max: 150})) {
            errors.msg.push({partnerName: 'partnerName must be between 1 to 150 characters long'});
          }
        }
        if (toValidate.indexOf('partnerId') > -1) {
          if (!update && (!(data.partnerId) || validator.isNull(data.partnerId))) {
            errors.msg.push({partnerId: 'partnerId is required'});
          }
        }
        if (toValidate.indexOf('role') > -1) {
          if (data.role && !validator.isIn(data.role, ['superadmin', 'admin', 'user'])) {
            errors.msg.push({role: 'invalid role. role can be `superadmin` or `admin`or `user`'});
          }
        }
        if (toValidate.indexOf('phone') > -1) {
          if (data.phone && !validator.isLength(data.phone, {min: 10, max: 10}) && !validator.isMobilePhone(data.phone, 'en-US')) {
            errors.msg.push({phone: 'phone must be 10 digits long, should be numeric and should not contain country code'});
          }
        }
        if (toValidate.indexOf('email') > -1) {
          if (data.email && !validator.isEmail(data.email)) {
            errors.msg.push({email: 'email should be valid email address'});
          }
        }

        if (toValidate.indexOf('partnerEmail') > -1) {
          if (data.partnerEmail && !validator.isEmail(data.partnerEmail)) {
            errors.msg.push({partnerEmail: 'partnerEmail should be valid email address'});
          }
        }

        if (toValidate.indexOf('level') > -1) {
          if (!update && (!(data.level) || validator.isNull(data.level))) {
            errors.msg.push({level: 'level is required'});
          } else if (data.level && !validator.isIn(data.level, ['production', 'sandbox'])) {
            errors.msg.push({level: 'level should be either production or sandbox'});
          }
        }

        if (toValidate.indexOf('sendgridId') > -1) {
          if (!update && (!(data.sendgridId) || validator.isNull(data.sendgridId))) {
            errors.msg.push({sendgridId: 'Sendgrid Template Id is required'});
          } else if (data.sendgridId && !validator.isLength(data.sendgridId, {min: 1, max: 150})) {
            errors.msg.push({sendgridId: 'sendgridId must be between 1 to 150 characters long'});
          }
        }

        if (toValidate.indexOf('sendgridIdForPassword') > -1) {
          if (!update && (!(data.sendgridIdForPassword) || validator.isNull(data.sendgridIdForPassword))) {
            errors.msg.push({sendgridIdForPassword: 'sendgridIdForPassword Template Id is required'});
          } else if (data.sendgridIdForPassword && !validator.isLength(data.sendgridIdForPassword, {min: 1, max: 150})) {
            errors.msg.push({sendgridIdForPassword: 'sendgridIdForPassword must be between 1 to 150 characters long'});
          }
        }

        if (toValidate.indexOf('apps.android') > -1) {
          if (!update && (!(data.apps && data.apps.android) || validator.isNull(data.apps.android))) {
            errors.msg.push({android: 'Android package name is required'});
          } else if (data.apps && data.apps.android && !validator.isLength(data.apps.android, {min: 1, max: 150})) {
            errors.msg.push({android: 'Android package name must be between 1 to 150 characters long'});
          }
        }

        if (toValidate.indexOf('apps.ios') > -1) {
          if (!update && (!(data.apps && data.apps.ios) || validator.isNull(data.apps.ios))) {
            errors.msg.push({ios: 'iOS package name is required'});
          } else if (data.apps && data.apps.ios && !validator.isLength(data.apps.ios, {min: 1, max: 150})) {
            errors.msg.push({ios: 'iOS package name must be between 1 to 150 characters long'});
          }
        }

        if (toValidate.indexOf('apps.iosAppStoreId') > -1) {
          if (!update && (!(data.apps && data.apps.iosAppStoreId) || validator.isNull(data.apps.iosAppStoreId))) {
            errors.msg.push({iosAppStoreId: 'ios AppStoreId is required'});
          } else if (data.apps && data.apps.iosAppStoreId && !validator.isLength(data.apps.iosAppStoreId, {min: 1, max: 15})) {
            errors.msg.push({iosAppStoreId: 'ios AppStoreId name must be between 1 to 15 characters long'});
          }
        }

        if (toValidate.indexOf('firebase.domain') > -1) {
          if (!update && (!(data.firebase && data.firebase.domain) || validator.isNull(data.firebase.domain))) {
            errors.msg.push({ios: 'Firebase domain name is required'});
          } else if (data.firebase && data.firebase.domain && !validator.isLength(data.firebase.domain, {min: 1, max: 150})) {
            errors.msg.push({domain: 'Firebase domain name must be between 1 to 150 characters long'});
          }
        }

        if (toValidate.indexOf('firebase.apiKey') > -1) {
          if (!update && (!(data.firebase && data.firebase.apiKey) || validator.isNull(data.firebase.apiKey))) {
            errors.msg.push({apiKey: 'Firebase apiKey is required'});
          } else if (data.firebase && data.firebase.apiKey && !validator.isLength(data.firebase.apiKey, {min: 1, max: 150})) {
            errors.msg.push({apiKey: 'apiKey name must be between 1 to 150 characters long'});
          }
        }

        if (toValidate.indexOf('smsInvite') > -1) {
          if (data.smsInvite && data.smsInvite.length) {
            data.smsInvite.forEach(sms => {
              if ((!sms.from || (sms.from && !validator.isMobilePhone(sms.from, 'en-US')))) {
                errors.msg.push({from: 'from is required for smsInvite'});
              }
              if (!sms.text && !sms.media) {
                errors.msg.push({smsInvite: 'text or media is required for smsInvite'});
              } else if ((sms.text && !validator.isLength(sms.text, {min: 1, max: 160}))) {
                errors.msg.push({text: 'text is invalid. Max 160 characters are allowed'});
              } else if ((sms.media && !validator.isLength(sms.media, {min: 1, max: 255}))) {
                errors.msg.push({media: 'media is invalid. Max 255 characters are allowed'});
              }
            })
          }
        }

        if (!data.import) {
          if (toValidate.indexOf('es_index') > -1) {
            if (!update && !(data.es_index) || validator.isNull(data.es_index)) {
              errors.msg.push({es_index: 'es_index is required'});
            } else if (data.es_index && !validator.isLength(data.es_index, {min: 1, max: 20})) {
              errors.msg.push({es_index: 'es_index must be between 1 to 20 characters long'});
            }
          }

          if (toValidate.indexOf('image') > -1) {
            if (!update && !(data.image) || validator.isNull(data.image)) {
              errors.msg.push({image: 'image is not valid'});
            } else if (data.image && !validator.isURL(data.image)) {
              errors.msg.push({image: 'image is not a valid url'});
            }
          }

          if (toValidate.indexOf('intro_video') > -1) {
            if (!update && !(data.intro_video) || validator.isNull(data.intro_video)) {
              errors.msg.push({intro_video: 'intro_video is not valid'});
            } else if (data.intro_video && !validator.isURL(data.intro_video)) {
              errors.msg.push({intro_video: 'intro_video is not a valid url'});
            }
          }

          if (toValidate.indexOf('iosKey') > -1) {
            if (!update && (!(data.iosKey) || validator.isNull(data.iosKey))) {
              errors.msg.push({iosKey: 'iosKey is not valid'});
            } else if (data.iosKey && !validator.isMongoId(data.iosKey)) {
              errors.msg.push({iosKey: 'iosKey is not valid Mongo Id'});
            }
          }

          if (toValidate.indexOf('androidKey') > -1) {
            if (!update && (!(data.androidKey) || validator.isNull(data.androidKey))) {
              errors.msg.push({androidKey: 'androidKey is not valid'});
            } else if (data.androidKey && !validator.isMongoId(data.androidKey)) {
              errors.msg.push({androidKey: 'androidKey is not valid Mongo Id'});
            }
          }

          if (toValidate.indexOf('partner_types') > -1) {
            if (!update && !(data.partner_types) || validator.isNull(data.partner_types)) {
              errors.msg.push({partner_types: 'partner_types is not valid'});
            } else if (data.partner_types && !_.isArray(data.partner_types) && !data.partner_types.length) {
              errors.msg.push({partner_types: 'partner_types is not valid'});
            }
          }

          if (toValidate.indexOf('user_types') > -1) {
            if (!update && !(data.user_types) || validator.isNull(data.user_types)) {
              errors.msg.push({user_types: 'user_types is not valid'});
            } else if (data.user_types && !_.isArray(data.user_types) && !data.user_types.length) {
              errors.msg.push({user_types: 'user_types is not valid'});
            }
          }
        } else if (data.import && toValidate.indexOf('import') > -1) {
          if (data.import && !_.isObject(data.import)) {
            errors.msg.push({import: 'import is not a valid object'});
          } else if (!data.import.source) {
            errors.msg.push({'import.source': 'import.source is required'});
          } else if (data.import.source && !validator.isMongoId(data.import.source)) {
            errors.msg.push({'import.source': 'import.source is not a valid organization mongo id'});
          } else if (!data.import.objects && !data.import.objects.length) {
            errors.msg.push({'import.objects': 'import.objects is required'});
          } else if (data.import.objects && !data.import.objects.length) {
            errors.msg.push({'import.objects': 'import.objects is required'});
          } else if (data.import.objects && data.import.objects.length) {
            data.import.objects.forEach(obj => {
              if (!validator.isIn(obj, ['cards', 'partners', 'sidebars'])) {
                errors.msg.push({'import.objects': 'import.objects is not valid. Possible values are [\'cards\', \'partners\', \'sidebars\']'});
              }
            });
          }
          data.isImport = true;
        }
        if (errors.msg.length > 0) {
          console.log('Error in organization.controller.validateEntities 1');
          return reject(errors);
        } else {
          return resolve(data);
        }
      } else {
        console.log('Error in organization.controller.validateEntities 2');
        let err = {type: 'EntityNotFound', code: 422, msg: 'Entity not found.'};
        return reject(err);
      }
    });
  }
}

function getNextOrgSequence() {
  return new Promise((resolve, reject) => {
    return OrganizationCounter.findOne({_id: 'orgId'})
      .then(counter => {
        if (!counter) {
          return OrganizationCounter.create({
            _id: "orgId",
            seq: 1
          })
            .then(ctr => {
              return resolve(ctr.seq);
            })
            .catch(err => {
              console.log('Error in organization.controller.getNextSequence 1');
              return reject(err);
            })
        } else {
          counter.seq = ++counter.seq;
          return counter.save()
            .then(ctr => {
              return resolve(ctr.seq);
            })
            .catch(err => {
              console.log('Error in organization.controller.getNextSequence 2');
              return reject(err);
            })
        }
      })
      .catch(err => {
        console.log('Error in organization.controller.getNextSequence 3');
        return reject(err);
      })
  });
}

function saveUpdates(updates) {
  return function(entity) {
    var updated = _.merge(entity, updates, (a, b) => {
      if (_.isArray(a)) {
        return b;
      }
    });
    if (updates.smsInvite && !updates.smsInvite.length) {
      updated.smsInvite.length = 0;
    }
    if (updates.sidebars && !updates.sidebars.length) {
      updated.sidebars.length = 0;
    }
    updated.markModified('partner_types');
    updated.markModified('user_types');
    updated.markModified('sidebars');
    updated.markModified('smsInvite');
    updated.markModified('level');
    return updated.save()
      .then(updated => {
        return updated;
      });
  };
}

function removeEntity() {
  return function(entity) {
    return new Promise((resolve, reject) => {
      if (entity) {
        return entity.remove()
          .then(() => {
            return resolve();
          })
          .catch(err => {
            console.log('Error in organization.controller.removeEntity 1');
            return reject(err);
          })
      } else {
        console.log('Error in organization.controller.removeEntity 2');
        return reject({type: 'EntityNotFound', code: 404, msg: 'Organization not found'});
      }
    });
  };
}

function getOrgInstanceWithAPIKey(org) {
  return function(APIKey) {
    let _org = JSON.parse(JSON.stringify(org));
    _org.apiKey = APIKey;
    return new Promise((resolve, reject) => {
      return resolve(_org);
    });
  }
}

function getSideBar() {
  return function(entity) {
    return new Promise((resolve, reject) => {
      return SidebarCtrl.getSideBar(entity._id)
        .then(sidebar => {
          return resolve(sidebar);
        })
        .catch(err => {
          console.log('Error in organization.controller.getSideBar 2');
          return reject(err);
        })
    })
  }
}

function insertSidebarInOrganization(organization) {
  return function(entity) {
    return new Promise((resolve, reject) => {
      if (!entity) {
        console.log('Error in organization.controller.insertSidebarInOrganization 1');
        return reject({type: 'EntityMissing', code: 404, msg: 'Sidebar not found'});
      }
      if (organization) {
        organization.sidebars.push(entity._id);
        organization.markModified('sidebars');
        return organization.save()
          .then(updated => {
            return resolve(updated);
          })
          .catch(err => {
            console.log('Error in organization.controller.insertSidebarInOrganization 2');
            return reject(err);
          })
      } else {
        console.log('Error in organization.controller.insertSidebarInOrganization 3');
        return reject({type: 'EntityMissing', code: 404, msg: 'Organization not found'});
      }
    })
  }
}

function getOrganization(id) {
  return function(entity) {
    return new Promise((resolve, reject) => {
      return Organization.findById(id)
        .select('-_id sidebars')
        // .populate('sidebars')
        .then((org) => {
          return resolve(org);
        })
        .catch(error => {
          console.log('Error in organization.controller.getOrganization 1');
          return reject(error);
        })
    });
  }
}

function createUpdateSidebar(data) {
  return function(organization) {
    return new Promise((resolve, reject) => {
      //Sidebar exists update it
      if (organization && organization.sidebars && organization.sidebars.length) {
        let sidebarId = organization.sidebars[0];  //considering currently organizations can have only one sidebar
        return SidebarCtrl.updateSideBar(sidebarId, data)
          .then(getOrganization(organization._id))
          .then(org => {
            return resolve(org);
          })
          .catch(error => {
            console.log('Error in organization.controller.updateSidebar 1');
            return reject(error);
          })
      } else if (organization && organization.sidebars && !organization.sidebars.length) {
        //Sidebar Doesn't exists insert a new one
        return SidebarCtrl.createNewSideBar(data)
          .then(insertSidebarInOrganization(organization))
          .then(getOrganization(organization._id))
          .then((org) => {
            return resolve(org);
          })
          .catch(error => {
            console.log('Error in organization.controller.updateSidebar 2');
            return reject(error);
          })
      } else {
        console.log('Error in organization.controller.updateSidebar 3');
        return reject({type: 'EntityMissing', code: 500, msg: 'Entity missing'});
      }
    });
  }
}

function getItems(count) {
  return function(entity) {
    return new Promise((resolve, reject) => {
      if (entity) {
        return resolve({items: entity.cards, __count: count, __page: entity.currentPage});
      } else {
        console.log('Error in organization.controller.getItems 1');
        return reject({type: 'EntityMissing', code: 500, msg: 'cards missing'});
      }
    });
  }
}

function getVersionIfPverExists(pver, dos) {
  return function(entity) {
    return new Promise((resolve, reject) => {
      if (pver && pver.length && dos && dos.length) {
        return OrgVersionCtrl.getSingleVersion(entity._id, pver, dos)
          .then(version => {
            entity.version = version;
            return resolve(entity);
          })
          .catch(err => {
            console.log('Error in organization.controller.getVersionIfPverExists 1');
            return reject(err);
          })
      } else {
        return resolve(entity);
      }
    });
  }
}

function sendUserFeed(start, limit, searchText, sortObj, filter, user) {
  return function(organization) {
    return new Promise((resolve, reject) => {
      return UserCtrl.getUserByID(user)
        .then(_user => {
          return PartnerCtrl.getPartnersByUserIdAndOrg(user, organization._id)
            .then(AudienceLinkCtrl.getLinksByPartner())
            .then(audienceLinks => {
              console.log('AudienceLinks: ' + JSON.stringify(audienceLinks));
              if (_user && _user.type && _user.type === 'Apple' && audienceLinks.length) {
                return AudienceCtrl.getAudience(audienceLinks)
                  .then(auds => {
                    let filteredLinks = auds.filter(audience => audience.name === 'inreview').map(_auds => _auds._id);
                    console.log('Filtered AudienceLinks: ' + JSON.stringify(filteredLinks));
                    return CardCtrl.getUserCardsCount(organization, searchText, filter, _user, filteredLinks)
                      .then(count => {
                        return CardCtrl.getUserCards(organization, start, limit, count, searchText, sortObj, filter, _user, filteredLinks)
                          .then(getItems(count))
                          .then(item => {
                            return resolve(item);
                          })
                          .catch(err => {
                            console.log('Error in organization.controller.sendUserFeed 1');
                            return reject(err);
                          })
                      })
                      .catch(err => {
                        console.log('Error in organization.controller.sendUserFeed 2');
                        return reject(err);
                      })
                  })
                  .catch(err => {
                    console.log('Error in organization.controller.sendUserFeed 3');
                    return reject(err);
                  })
              } else {
                return CardCtrl.getUserCardsCount(organization, searchText, filter, _user, audienceLinks)
                  .then(count => {
                    return CardCtrl.getUserCards(organization, start, limit, count, searchText, sortObj, filter, _user, audienceLinks)
                      .then(getItems(count))
                      .then(item => {
                        return resolve(item);
                      })
                      .catch(err => {
                        console.log('Error in organization.controller.sendUserFeed 4');
                        return reject(err);
                      })
                  })
                  .catch(err => {
                    console.log('Error in organization.controller.sendUserFeed 5');
                    return reject(err);
                  })
              }
            })
            .catch(err => {
              console.log('Error in organization.controller.sendUserFeed 6');
              return reject(err);
            })
        })
        .catch(err => {
          console.log('Error in organization.controller.sendUserFeed 7');
          return reject(err);
        })
    });
  }
}

function getOrganizationById(id) {
  return new Promise((resolve, reject) => {
    if (id) {
      return Organization.findById(id)
        .then(org => {
          return resolve(org);
        })
        .catch(err => {
          console.log('Error in organization.controller.getOrganizationById 1');
          return reject(err);
        })
    } else {
      console.log('Error in organization.controller.getOrganizationById 2');
      return reject({type: 'MissingParams', code: 422, msg: 'orgid missing'});
    }
  });
}

function getOrganizationDetails(id) {
  return function(entity) {
    return new Promise((resolve, reject) => {
      if (id) {
        return Organization.findById(id)
          .then(org => {
            return resolve(org);
          })
          .catch(err => {
            console.log('Error in organization.controller.getOrganizationById 1');
            return reject(err);
          })
      } else {
        console.log('Error in organization.controller.getOrganizationById 2');
        return reject({type: 'MissingParams', code: 422, msg: 'orgid missing'});
      }
    });
  }
}

function getUsersPartner(orgId, user) {
  return new Promise((resolve, reject) => {
    if (orgId && user && user._id) {
      return PartnerCtrl.getPartnerByOrgAndUserId(orgId, user._id)
        .then(partner => {
          return resolve(partner);
        })
        .catch(err => {
          console.log('Error in organization.controller.getUsersPartner 1');
          return reject(err);
        })
    } else {
      return resolve();
    }
  });
}

function reformatStyle() {
  return function(entity) {
    return new Promise((resolve, reject) => {
      if (entity && entity.style) {
        return resolve({style: entity.style});
      } else {
        return resolve({style: {}});
      }
    });
  }
}

function reformatTypes() {
  return function(entity) {
    return new Promise((resolve, reject) => {
      if (entity && entity.partner_types && entity.partner_types.length) {
        return resolve(entity.partner_types);
      } else {
        return resolve([]);
      }
    });
  }
}

function reformatResponse() {
  return function(entity) {
    return new Promise((resolve, reject) => {
      return resolve({"meta": {"code": 200}, "stats": {"reach": {"partners": entity.partner, "users": entity.user}}});
    });
  }
}

function parseItems(item, data, sidebar = false) {
  _.each(item, function(value, prop) {
    if (item[prop] && !_.isArray(item[prop]) && !_.isObject(item[prop])) {
      let template = handlebars.compile(item[prop]);
      item[prop] = template(data);
      if (prop == 'dest' && validator.isURL(encodeURI(item[prop])) && sidebar) {
        if (item[prop].indexOf('?') > -1) {
          item[prop] = `${item[prop]}&utm_source=influents&utm_medium=sidebar&utm_term=${item['title']}&utm_content=${item['title']}`;
        } else {
          item[prop] = `${item[prop]}?utm_source=influents&utm_medium=sidebar&utm_term=${item['title']}&utm_content=${item['title']}`;
        }
      }
    } else if (item[prop] && _.isArray(item[prop])) {
      item[prop] = item[prop].map(child => parseItems(child, data, sidebar));
    } else if (item[prop] && _.isObject(item[prop])) {
      item[prop] = parseItems(item[prop], data, sidebar);
    }
  });
  return item;
}

function parseSMSTemplate(items, data) {
  items.forEach(item => {
    let text = handlebars.compile(item.text);
    item.text = text(data);
    let media = handlebars.compile(item.media);
    item.media = media(data);
  });
  return items;
}

function parseItemsData(data, allItems, isSidebar = false) {
  return new Promise((resolve, reject) => {
    if (data && allItems && allItems.items && allItems.items.length) {
      allItems.items.forEach(itemData => {
        if (!isSidebar && itemData.data) {
          itemData.data = parseItems(itemData.data, data);

          // if (itemData.data.title) {
          //   let title = handlebars.compile(itemData.data.title);
          //   itemData.data.title = title(data);
          // }
          //
          // if (itemData.data.description) {
          //   let description = handlebars.compile(itemData.data.description);
          //   itemData.data.description = description(data);
          // }
          //
          // if (itemData.data.media && itemData.data.media.data) {
          //   if (itemData.data.media.data.url) {
          //     let mediaURL = handlebars.compile(itemData.data.media.data.url);
          //     itemData.data.media.data.url = mediaURL(data);
          //   }
          //   if (itemData.data.media.data.title) {
          //     let mediaTitle = handlebars.compile(itemData.data.media.data.title);
          //     itemData.data.media.data.title = mediaTitle(data);
          //   }
          //   if (itemData.data.media.data.icon) {
          //     let mediaIcon = handlebars.compile(itemData.data.media.data.icon);
          //     itemData.data.media.data.icon = mediaIcon(data);
          //   }
          // }
          //
          // if (itemData.data.actionlink) {
          //   if (itemData.data.actionlink.dest) {
          //     let actionLink = handlebars.compile(itemData.data.actionlink.dest);
          //     itemData.data.actionlink.dest = actionLink(data);
          //   }
          //   if (itemData.data.actionlink.title) {
          //     let actionTitle = handlebars.compile(itemData.data.actionlink.title);
          //     itemData.data.actionlink.title = actionTitle(data);
          //   }
          //   if (itemData.data.actionlink.icon) {
          //     let actionIcon = handlebars.compile(itemData.data.actionlink.icon);
          //     itemData.data.actionlink.icon = actionIcon(data);
          //   }
          // }
          //
          // if (itemData.data.links) {
          //   if (itemData.data.links.dest) {
          //     let linksURL = handlebars.compile(itemData.data.links.dest);
          //     itemData.data.links.dest = linksURL(data);
          //   }
          //   if (itemData.data.links.title) {
          //     let linksTitle = handlebars.compile(itemData.data.links.title);
          //     itemData.data.links.title = linksTitle(data);
          //   }
          //   if (itemData.data.links.icon) {
          //     let linksIcon = handlebars.compile(itemData.data.links.icon);
          //     itemData.data.links.icon = linksIcon(data);
          //   }
          // }
          //
          // if (itemData.data.icon) {
          //   let icon = handlebars.compile(itemData.data.icon);
          //   itemData.data.icon = icon(data);
          // }
          //
          if (itemData.organization && itemData.organization.sidebars) {
            itemData.organization.sidebars = parseItems(itemData.organization.sidebars, data, true);
          }
        } else if (isSidebar && itemData) {
          itemData = parseItems(itemData, data, true);
        }
      });
      return resolve(allItems);
    } else {
      return resolve(allItems);
    }
  });
}

function parseHandleBars(req, parse, isSidebar = false) {
  return function(allItems) {
    return new Promise((resolve, reject) => {
      if (allItems && allItems.items && allItems.items.length && parse) {
        let data = {
          user: null,
          organization: null,
          partner: null
        };
        if (req.user) {
          data.user = {
            _id: req.user._id,
            name: req.user.name,
            email: req.user.email,
            onboard: req.user.onboard,
            type: req.user.type,
            role: req.user.role,
            active: req.user.active,
            status: req.user.status
          }
        }
        let orgId = req.params.orgid;

        return getOrganizationById(orgId)
          .then(org => {
            if (org) {
              data.organization = {
                _id: org._id,
                name: org.name,
                intro_video: org.intro_video,
                level: org.level,
                es_index: org.es_index,
                code: org.code,
                user_types: org.user_types,
                partner_types: org.partner_types,
                sidebars: org.sidebars
              }
            }
            return getUsersPartner(orgId, data.user)
              .then(partner => {
                if (partner) {
                  data.partner = {
                    _id: partner._id,
                    name: partner.name,
                    partnerId: partner.partnerId,
                    email: partner.email,
                    type: partner.type,
                    allUsers: partner.allUsers,
                    extended: partner.extended
                  };
                }
                return parseItemsData(data, allItems, isSidebar)
                  .then(allItems => {
                    return resolve(allItems);
                  })
                  .catch(err => {
                    return resolve(allItems);
                  });
              })
              .catch(err => {
                return resolve(allItems);
              });
          })
          .catch(err => {
            return resolve(allItems);
          });
      } else {
        return resolve(allItems);
      }
    });
  }
}

function parseThumbNails() {
  return function(allItems) {
    return new Promise((resolve, reject) => {
      if (allItems && allItems.items && allItems.items.length) {
        let parsed = allItems.items.map(item => {
          if (item.data && item.data.media && item.data.media.data && item.data.media.data.url) {
            if (item.data.media.data.url.indexOf('res.cloudinary.com') > -1) {
              let split = item.data.media.data.url.split("/");
              let file = split[split.length - 2] + '/' + split[split.length - 1].split(".").shift();
              console.log('Parsing Thumbnail: ' + item.data.media.data.url);
              item.data.media.data.thumbnail_url = cloudinary.url(file, {format: "png", gravity: "north", height: 180, width: 320, crop: "fill"});
              console.log('Parsed Thumbnail: ' + item.data.media.data.thumbnail_url);
            } else {
              item.data.media.data.thumbnail_url = cloudinary.url(item.data.media.data.url, {fetch_format: "png", gravity: "north", height: 180, width: 320, crop: "fill", type: "fetch"});
            }
          }
          return item;
        });
        allItems.items = parsed;
      }
      return resolve(allItems);
    });
  }
}

function createEncryptedURL(cardId, index, userId, deviceId, linkType, url, org) {
  let obj = {c: cardId, i: index, u: userId, d: deviceId, t: linkType, l: url, o: org};
  let output = encodeURIComponent(lzutf8.compress(JSON.stringify(obj), {outputEncoding: 'Base64'}));
  return `${config.externalUrlDomain}/url/${output}`;
}

function parseExtrenalURLs(userId, orgId, deviceId) {
  return function(allItems) {
    return new Promise((resolve, reject) => {
      if (allItems && allItems.items && allItems.items.length) {
        let parsed = allItems.items.map(item => {
          if (item.status == 'published') {
            if (item.data && item.data.actionlink && item.data.actionlink.dest && validator.isURL(item.data.actionlink.dest)) {
              item.data.actionlink.dest = createEncryptedURL(item._id, 0, userId, deviceId, 'actionlink', item.data.actionlink.dest, orgId);
            }
            if (item.data && item.data.links && item.data.links.length) {
              item.data.links.forEach((link, index) => {
                if (link.dest && validator.isURL(link.dest)) {
                  link.dest = createEncryptedURL(item._id, (index + 1), userId, deviceId, 'link', link.dest, orgId);
                }
              })
            }
          }
          return item;
        });
        allItems.items = parsed;
      }
      return resolve(allItems);
    });
  }
}

function sendOrgFeed(start, limit, searchText, sortObj, filter) {
  return function(organization) {
    return new Promise((resolve, reject) => {
      return CardCtrl.getOrgCardsCount(organization, searchText, filter)
        .then(count => {
          return CardCtrl.getOrgCards(organization, start, limit, count, searchText, sortObj, filter)
            .then(getItems(count))
            .then(item => {
              return resolve(item);
            })
            .catch(err => {
              console.log('Error in organization.controller.sendOrgFeed 1');
              return reject(err);
            })
        })
        .catch(err => {
          console.log('Error in organization.controller.sendOrgFeed 2');
          return reject(err);
        })
    });
  }
}
function getSingleOrgFeed(cardId) {
  return function(organization) {
    return new Promise((resolve, reject) => {
      return CardCtrl.getSingleOrgCard(organization, cardId)
        .then(item => {
          return resolve(item);
        })
        .catch(err => {
          console.log('Error in organization.controller.getSingleOrgFeed 1');
          return reject(err);
        })
    });
  }
}

function checkIfUserExistsInOrg(orgId, email, phone = null) {
  return new Promise((resolve, reject) => {
    return PartnerCtrl.doesUserExists(orgId, email, phone)
      .then(existance => {
        let exists = false;
        if (existance.hasOwnProperty('email') && existance.email > 0) {
          console.log('Error in organization.controller.checkIfUserExists 1');
          exists = true;
          let err = {type: 'AlreadyExists', code: 409, msg: 'Email already exists.'};
          return reject(err);
        } else if (existance.hasOwnProperty('phone') && existance.phone > 0) {
          exists = true;
          console.log('Error in organization.controller.checkIfUserExists 2');
          let err = {type: 'AlreadyExists', code: 409, msg: 'Phone already exists.'};
          return reject(new Error(err));
        }
        return resolve(exists);
      })
      .catch(err => {
        console.log('Error in organization.controller.checkIfUserExists 3');
        console.log(err);
        return reject(err);
      })
  });
}

function createNewUser(org, data, creator, apiKey, partnerId) {
  return function(userExists) {
    return new Promise((resolve, reject) => {
      if (!userExists) {
        return UserCtrl.createNewUser(org, data, creator, apiKey, partnerId)
          .then(userData => {
            return resolve(userData);
          })
          .catch(err => {
            console.log('Error in organization.controller.createNewUser 1');
            return reject(new Error(err));
          })
      } else {
        console.log('Error in organization.controller.createNewUser 2');
        let err = {type: 'AlreadyExists', code: 409, msg: 'Email already exists.'};
        return reject(new Error(err));
      }
    });
  }
}

export function sendEmailIfOnboard(org, isBulk = false) {
  return function(userData) {
    return new Promise((resolve, reject) => {
      if (userData) {
        userData = JSON.parse(JSON.stringify(userData));
        if (userData.user.onboard && userData.token) {
          return UserCtrl.getUser(userData.user._id)
            .then(usr => {
              console.log('token: ' + userData.token);
              //Send Email
              let qrImageText = `influents://register?email=${userData.user.email}&token=${userData.token}&ref=1`;
              let encodedQRPath = `${qrImageAPIPath}?q=` + encodeURIComponent(`${qrImageText}`);
              console.log(encodedQRPath);
              console.log(userData);
              console.log('sendgrid Id:');
              console.log(usr.sendgridId);
              console.log('Organization');
              console.log(org);
              console.log('Invite Link');
              console.log(userData.user.inviteLink);
              let emailOptions = {
                to: userData.user.email,
                from: config.sendEmail.from,
                subject: `${org.name} Account Activation`,
                data: {
                  name: userData.user.name,
                  email: userData.user.email,
                  role: userData.user.role,
                  type: userData.user.type,
                  partner: usr.partner,
                  token: userData.token,
                  organization: org,
                  qrPath: encodedQRPath,
                  inviteLink: userData.user.inviteLink
                },
                sendgridId: usr.sendgridId,
                template: 'tmp1',
                customArgs: {
                  influents_userid: (userData.user._id).toString(),
                  influents_orgid: (org._id).toString(),
                  influents_partnerid: (usr.partner._id).toString(),
                  influents_event: isBulk ? 'bulkinvite' : 'invite'
                }
              };
              console.log('EMAIL OPTIONS:');
              console.log(JSON.stringify(emailOptions));
              return Mailer.sendMail(emailOptions)
                .then(user => {
                  userData.user.activationToken = userData.token;
                  if (config.debugAPI) {
                    userData.user.debug = {activationToken: userData.token};
                  }
                  return resolve(userData.user);
                })
                .catch(err => {
                  console.log('Error in organization.controller.sendEmailIfOnboard 2');
                  return reject(err);
                })
            })
            .catch(err => {
              console.log('Error in organization.controller.sendEmailIfOnboard 1');
              return reject(err);
            })
        } else {
          console.log('user not onboard skipping welcome email');
          return resolve(userData.user);
        }
      } else {
        console.log('Error in organization.controller.sendEmailIfOnboard 3');
        let err = {type: 'EntityNotFound', code: 404, msg: 'user not found.'};
        return reject(err);
      }
    });
  }
}

function saveUserInPartner(orgId, partnerId) {
  return function(userData) {
    return new Promise((resolve, reject) => {
      if (userData) {
        return PartnerCtrl.addUser(userData, orgId, partnerId)
          .then(() => {
            return resolve(userData);
          })
          .catch(err => {
            console.log('Error in organization.controller.saveUserInPartner 1');
            return reject(err);
          })
      } else {
        console.log('Error in organization.controller.saveUserInPartner 2');
        let err = {type: 'EntityNotFound', code: 404, msg: 'user not found.'};
        return reject(err);
      }
    });
  }
}

function updateSidebarDirectlyInOrg(sidebarData) {
  return function(organization) {
    return new Promise((resolve, reject) => {
      if (organization && sidebarData) {
        organization.sidebars = sidebarData;
        organization.markModified('sidebars');
        return organization.save()
          .then(getOrganization(organization._id))
          .then(org => {
            return resolve(org.sidebars);
          })
          .catch(err => {
            console.log('Error in organization.controller.updateSidebarDirectlyInOrg 1');
            return reject(err);
          });
      } else {
        console.log('Error in organization.controller.updateSidebarDirectlyInOrg 2');
        let err = {type: 'EntityNotFound', code: 404, msg: 'organization/sidebar not found.'};
        return reject(err);
      }
    });
  }
}

function copyPartners(copyPartners, sourceOrg, destOrg) {
  return new Promise((resolve, reject) => {
    if (copyPartners && sourceOrg && destOrg) {
      destOrg.partners = [];
      return PartnerCtrl.getOrgPartners(sourceOrg)
        .then(partners => {
          let allPartners = partners.map(partner => {
            return {
              partnerId: shortid.generate(),
              name: partner.name,
              email: partner.email,
              type: partner.type,
              organization: destOrg._id,
              active: partner.active,
              allUsers: partner.allUsers
            };
          });
          return PartnerCtrl.createPartners(allPartners)
            .then(pts => {
              destOrg.partners = pts.slice();
              return resolve(destOrg);
            })
            .catch(err => {
              console.log('Error in organization.controller.copyPartners 1');
              return reject(err);
            });
        })
        .catch(err => {
          console.log('Error in organization.controller.copyPartners 2');
          return reject(err);
        });
    } else {
      return resolve(destOrg);
    }
  });
}

function copyCards(copyCards, sourceOrg, destOrg) {
  return new Promise((resolve, reject) => {
    if (copyCards && sourceOrg && destOrg) {
      return CardCtrl.getOrgCardsToCopy(sourceOrg)
        .then(cards => {
          let allCards = cards.map(card => {
            return {
              organization: destOrg._id,
              type: card.type,
              residue: card.residue,
              date: card.date,
              format: card.format,
              visibility: card.visibility,
              data: card.data,
              date_published: card.date_published,
              status: card.status,
              active: card.active
            };
          });
          return CardCtrl.createCards(allCards)
            .then(cards => {
              return resolve(destOrg);
            })
            .catch(err => {
              console.log('Error in organization.controller.copyCards 1');
              return reject(err);
            });
        })
        .catch(err => {
          console.log('Error in organization.controller.copyCards 2');
          return reject(err);
        });
    } else {
      return resolve(destOrg);
    }
  });
}

function duplicateObjects(data) {
  return function(organization) {
    return new Promise((resolve, reject) => {
      if (data && data.isImport && organization && data.import) {
        let org = _.cloneDeep(organization);
        return copyPartners(data.import.objects.indexOf('partners') > -1, data.import.source, org)
          .then(_org => {
            return copyCards(data.import.objects.indexOf('cards') > -1, data.import.source, org)
              .then(_corg => {
                return resolve(_org);
              })
              .catch(err => {
                console.log('Error in organization.controller.duplicateObjects 1');
                return reject(err);
              });
          })
          .catch(err => {
            console.log('Error in organization.controller.duplicateObjects 2');
            return reject(err);
          });
      } else {
        return resolve(organization);
      }
    });
  }
}

function getOrgSidebarsIfHasToImport(data) {
  return new Promise((resolve, reject) => {
    if (data && data.isImport && data.import.objects.indexOf('sidebars') > -1 && (!data.sidebars || (data.sidebars && !data.sidebars.length))) {
      return Organization.findById(data.import.source)
        .lean()
        .then(org => {
          data.sidebars = org.sidebars;
          return resolve(data);
        })
        .catch(err => {
          console.log('Error in organization.controller.getOrgIfHasToImport 1');
          return reject(err);
        })
    } else {
      return resolve(data);
    }
  });
}

function createOrg() {
  return function(organization) {
    return new Promise((resolve, reject) => {
      if (organization) {
        return Organization.create(organization)
          .then(org => {
            return resolve(org);
          })
          .catch(err => {
            console.log('Error in organization.controller.createOrg 1');
            return reject(err);
          })
      } else {
        console.log('Error in organization.controller.createOrg 2');
        let err = {type: 'EntityNotFound', code: 422, msg: 'data not found.'};
        return reject(err);
      }
    });
  }
}

function createEsIndex() {
  return function(_organization) {
    return new Promise((resolve, reject) => {
      if (_organization) {
        let organization = JSON.parse(JSON.stringify(_organization));
        return es.createIndex(organization.code)
          .then(org => {
            organization.es_index = organization.code;
            return resolve(organization);
          })
          .catch(err => {
            console.log('Error in organization.controller.createOrg 1');
            return reject(err);
          })
      } else {
        console.log('Error in organization.controller.createOrg 2');
        let err = {type: 'EntityNotFound', code: 422, msg: 'data not found.'};
        return reject(err);
      }
    });
  }
}

function deleteEsIndex() {
  return function(_organization) {
    return new Promise((resolve, reject) => {
      if (_organization) {
        let organization = JSON.parse(JSON.stringify(_organization));
        return es.deleteIndex(organization.code)
          .then(org => {
            organization.es_index = organization.code;
            return resolve(organization);
          })
          .catch(err => {
            console.log('Error in organization.controller.deleteEsIndex 1');
            return reject(err);
          })
      } else {
        console.log('Error in organization.controller.deleteEsIndex 2');
        let err = {type: 'EntityNotFound', code: 422, msg: 'data not found.'};
        return reject(err);
      }
    });
  }
}

function getNotificationsCount(org, startDate, endDate) {
  return new Promise((resolve, reject) => {
    if (org && startDate && endDate) {
      return NotificationCtrl.getSentNotifications(org, startDate, endDate)
        .then(notifications => {
          return resolve(notifications);
        })
        .catch(err => {
          console.log('Error in organization.controller.getNotificationsCount 1');
          return reject(err);
        })
    } else {
      console.log('Error in organization.controller.getNotificationsCount 2');
      let err = {type: 'EntityNotFound', code: 422, msg: 'org not found.'};
      return reject(err);
    }
  });
}

function allUsersCount(org, startDate, endDate) {
  return new Promise((resolve, reject) => {
    if (org && startDate && endDate) {
      return PartnerCtrl.getAllUsersOfPartner(org)
        .then(CardCtrl.getUniqueArrayOfUsers())
        .then(UserCtrl.getUsers())
        .then(users => {
          let counts = {
            total: _.filter(users, function(data) {
              return data.createdAt >= startDate && data.createdAt <= endDate
            }).length,
            invited: _.filter(users, function(data) {
              return data.createdAt >= startDate && data.createdAt <= endDate && data.status == 'Invited';
            }).length,
            accepted: _.filter(users, function(data) {
              return data.createdAt >= startDate && data.createdAt <= endDate && data.status == 'Accepted';
            }).length
          };
          return resolve(counts);
        })
        .catch(err => {
          console.log('Error in organization.controller.allUsersCount 1');
          return reject(err);
        })
    } else {
      console.log('Error in organization.controller.allUsersCount 2');
      let err = {type: 'EntityNotFound', code: 422, msg: 'org not found.'};
      return reject(err);
    }
  });
}

function getPartnersCount(org, startDate, endDate) {
  return new Promise((resolve, reject) => {
    if (org && startDate && endDate) {
      return PartnerCtrl.getOrgsAllPartners(org)
        .then(partners => {
          let counts = {
            total: _.filter(partners, function(data) {
              return data.active
            }).length,
            added: _.filter(partners, function(data) {
              return data.createdAt >= startDate && data.createdAt <= endDate;
            }).length
          };
          return resolve(counts);
        })
        .catch(err => {
          console.log('Error in organization.controller.getPartnersCount 1');
          return reject(err);
        })
    } else {
      console.log('Error in organization.controller.getPartnersCount 2');
      let err = {type: 'EntityNotFound', code: 422, msg: 'org not found.'};
      return reject(err);
    }
  });
}

function finalData(data) {
  return new Promise((resolve, reject) => {
    return resolve(data);
  });
}


function parseExtrenalURL(userId, dtype, role, orgId, deviceId) {
  return function(item) {
    return new Promise((resolve, reject) => {
      let hasActioLink = false;
      if (item.status == 'published') {
        if (item.data && item.data.actionlink && item.data.actionlink.dest && validator.isURL(item.data.actionlink.dest)) {
          item.data.actionlink.dest = createEncryptedURL(item._id, 0, userId, deviceId, 'actionlink', item.data.actionlink.dest, orgId);
          hasActioLink = item.data.actionlink.type && (item.data.actionlink.type == 'wrapped' || item.data.actionlink.type == 'customtab');
        }

        if (item.data && item.data.links && item.data.links.length) {
          item.data.links.forEach((link, index) => {
            if (link.dest && validator.isURL(link.dest)) {
              link.dest = createEncryptedURL(item._id, (index + 1), userId, deviceId, 'link', link.dest, orgId);
            }
          })
        }
      }


      if (userId && item && dtype && dtype == 'web' && role && (role == 'organization-admin' || role == 'superadmin') && item.status == 'published') {
        return LinkAnalyticsCtrl.getLinkStats(item._id, 0, !hasActioLink)
          .then(stats => {
            if (!stats.hasOwnProperty('skipped') && !stats.skipped) {
              item.data.actionlink.stats = stats;
            }
            if (item.data && item.data.links && item.data.links.length) {
              let i = 0;
              return async.mapSeries(item.data.links, function(link, callback) {
                ++i;
                let hasLink = link.type && (link.type == 'wrapped' || link.type == 'customtab');
                return LinkAnalyticsCtrl.getLinkStats(item._id, i, !hasLink)
                  .then(inStats => {
                    if (!inStats.hasOwnProperty('skipped') && !inStats.skipped) {
                      link.stats = inStats;
                    }
                    return callback(null, inStats);
                  })
                  .catch(err => {
                    return callback(err);
                  })
              }, function(inErr, inDone) {
                if (inErr) {
                  return reject(item);
                }
                return resolve(item);
              })
            } else {
              return resolve(item);
            }
          })
          .catch(err => {
            return reject(item);
          })
      } else {
        return resolve(item);
      }
    });
  }
}

function getUsersByAudienceLink(audiences) {
  return new Promise((resolve, reject) => {
    if (audiences && audiences.length) {
      return AudienceLinkCtrl.getPartnersByAudienceLinkIds(audiences)
        .then(allPartners => {
          let partners = _.uniq(_.map(allPartners, '_id'));
          let allUsers = _.uniq(_.flatten(_.map(allPartners, 'allUsers')));
          return resolve({partner: partners.length, user: allUsers.length});
        })
        .catch(err => {
          console.log('Error in organization.controller.getUsersByAudienceLink 1');
          return reject(err);
        })
    } else {
      let err = {type: 'NoAudience', code: 422, msg: 'No Audience found'};
      console.log('Error in organization.controller.getUsersByAudienceLink 2');
      return reject(err);
    }
  });
}

function getOrgUsers(organization) {
  return new Promise((resolve, reject) => {
    return PartnerCtrl.getOrgPartners(organization)
      .then(allPartners => {
        let partners = _.uniq(_.map(allPartners, '_id'));
        let allUsers = _.uniq(_.flatten(_.map(allPartners, 'allUsers')));
        return resolve({partner: partners.length, user: allUsers.length});
      })
      .catch(err => {
        console.log('Error in organization.controller.getOrgUsers 1');
        return reject(err);
      })
  });
}

function getUsersCount() {
  return function(item) {
    return new Promise((resolve, reject) => {
      if (item && item.visibility && item.visibility.audiences && item.visibility.audiences.length) {
        return getUsersByAudienceLink(item.visibility.audiences)
          .then(allUsers => {
            return resolve({"meta": {"code": 200}, "data": item, "stats": {"reach": {"partners": allUsers.partner, "users": allUsers.user}}});
          })
          .catch(err => {
            console.log('Error in organization.controller.getUsersCount 1');
            return reject(err);
          })
      } else {
        return getOrgUsers(item.organization)
          .then(allUsers => {
            return resolve({"meta": {"code": 200}, "data": item, "stats": {"reach": {"partners": allUsers.partner, "users": allUsers.user}}});
          })
          .catch(err => {
            console.log('Error in organization.controller.getUsersCount 2');
            return reject(err);
          })
      }
    });
  }
}

function sendWebhookRequest(org, partnerId) {
  return function(data) {
    return new Promise((resolve, reject) => {
      return PartnerCtrl.getFullPartnerByPartnerId(partnerId)
        .then(partner => {
          let webhookData = {
            event: 'user_created',
            payload: {
              ...data,
              partner: partner,
              org: org
            }
          };
          wh.sendWebhookRequest('webhook1', webhookData);
          // if (data.hasOwnProperty('onboard') && data.onboard) {
          //   webhookData.event = 'user_created';
          //   wh.sendWebhookRequest('webhook1', webhookData);
          // }
          return resolve({fulldata: webhookData, data: data});
        })
        .catch(err => {
          console.log('Error in organization.controller.sendWebhookRequest 1');
          return reject(err);
        })
    });
  }
}

export function sendSMSIfOnboard(activationToken = null) {
  return function(_data) {
    return new Promise((resolve, reject) => {
      let data = JSON.parse(JSON.stringify(_data));
      if (data.data && data.fulldata) {
        let user = JSON.parse(JSON.stringify(data.data));
        if (!activationToken && user.activationToken) {
          activationToken = user.activationToken;
        }
        user.activationToken = activationToken;
        let org = data.fulldata.payload.org;
        let partner = data.fulldata.payload.partner;
        if (user.hasOwnProperty('onboard') && user.onboard && user.phone && user.phone.length && org.smsInvite && org.smsInvite.length) {
          let parseData = {
            user: user,
            organization: org,
            partner: partner
          };

          let smsData = parseSMSTemplate(org.smsInvite, parseData);
          let smsArray = smsData.map(sms => {
            return {
              from: `+1${sms.from}`,
              to: `+1${user.phone}`,
              body: sms.text,
              mediaUrl: sms.media
            }
          });
          return sms.sendSMS(smsArray)
            .then(result => {
              if (result.hasOwnProperty('error') && result.error) {
                data.data.smsError = result.error;
              }
              if (result.hasOwnProperty('sent') && result.sent) {
                data.data.smsSent = result.sent;
              }
              return resolve(data.data);
            })
            .catch(err => {
              console.log('Error in organization.controller.sendSMSIfOnboard 1');
              data.data.smsError = err;
              return resolve(data.data);
            })
        } else {
          return resolve(data.data);
        }
      } else {
        return resolve(data.data);
      }
    });
  }
}

// get userFeed
export function userFeed(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}])
    .then(() => {
      let user;
      let shdParseHandleBars = true;
      let searchText = null;
      let allowedSorts = ['date', 'date_published'];
      let filter = {status: {$ne: 'draft'}};
      if (req.body && req.body.filter) {
        filter = req.body.filter;
      }
      let sortBy = 'date_published', order = 'asc', sortObj = {};
      if (req.query.sort && allowedSorts.indexOf(req.query.sort.trim()) > -1) {
        sortBy = req.query.sort;
      }
      if (req.query.order) {
        order = req.query.order.trim();
      }

      order = ( order === 'asc' ? 1 : -1);
      sortObj[sortBy] = order;

      if (req.params.limit) {
        req.params.limit = parseInt(req.params.limit);
      }

      if (!req.params.orgid) {
        let err = {type: 'ParamsMissing', code: 422, msg: 'Organization id required'};
        return ResponseHelpers.respondWithCustomError(res, err);
      }

      if (req.params.start < 1) {
        let err = {type: 'InvalidParams', code: 422, msg: 'Start value can not be less than 1'};
        return ResponseHelpers.respondWithCustomError(res, err);
      }

      if (req.params.start != 'last' && !validator.isInt(req.params.start)) {
        let err = {type: 'InvalidParams', code: 422, msg: 'Start value is invalid'};
        return ResponseHelpers.respondWithCustomError(res, err);
      }
      if (req.params.limit < 1) {
        let err = {type: 'InvalidParams', code: 422, msg: 'Limit value must be greater than 0'};
        return ResponseHelpers.respondWithCustomError(res, err);
      }
      if (req.query && req.query.text && (req.query.text.trim()).length < 1) {
        let err = {type: 'InvalidParams', code: 422, msg: 'text value must be greater than 0'};
        return ResponseHelpers.respondWithCustomError(res, err);
      } else if (req.query && req.query.text && (req.query.text.trim()).length > 1) {
        searchText = validator.trim(validator.escape(req.query.text));
      }

      if (req.query.parse && req.query.parse == 'false') {
        shdParseHandleBars = false;
      }

      if (req.params.userid) {
        user = req.params.userid;
      } else {
        user = req.user && req.user._id ? req.user._id : null;
      }

      return Organization.findById(req.params.orgid)
        .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Organization not found.'}))
        .then(sendUserFeed(req.params.start, req.params.limit, searchText, sortObj, filter, user))
        .then(parseHandleBars(req, shdParseHandleBars))
        .then(parseThumbNails())
        .then(parseExtrenalURLs(user, req.params.orgid, req.deviceId))
        .then(ResponseHelpers.respondWithPaginatedAndPageResult(res))
        .catch(ResponseHelpers.respondWithErrorEntity(res))
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res))
}

// get orgFeed
export function orgFeed(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}])
    .then(() => {
      let searchText = null;
      let allowedSorts = ['date', 'date_published'];
      let sortBy = 'date', order = 'asc', sortObj = {};
      let filter = null;
      if (req.body && req.body.filter) {
        filter = req.body.filter;
      }
      if (req.query.sort && allowedSorts.indexOf(req.query.sort.trim()) > -1) {
        sortBy = req.query.sort;
      }
      if (req.query.order) {
        order = req.query.order.trim();
      }

      order = ( order === 'asc' ? 1 : -1);
      sortObj[sortBy] = order;

      if (req.params.limit) {
        req.params.limit = parseInt(req.params.limit);
      }

      if (!req.params.orgid) {
        let err = {type: 'ParamsMissing', code: 422, msg: 'Organization id required'};
        return ResponseHelpers.respondWithCustomError(res, err);
      }

      if (req.params.start < 1) {
        let err = {type: 'InvalidParams', code: 422, msg: 'Start value can not be less than 1'};
        return ResponseHelpers.respondWithCustomError(res, err);
      }

      if (req.params.start != 'last' && !validator.isInt(req.params.start)) {
        let err = {type: 'InvalidParams', code: 422, msg: 'Start value is invalid'};
        return ResponseHelpers.respondWithCustomError(res, err);
      }

      if (req.params.limit < 1) {
        let err = {type: 'InvalidParams', code: 422, msg: 'Limit value must be greater than 0'};
        return ResponseHelpers.respondWithCustomError(res, err);
      }

      if (req.query && req.query.text && (req.query.text.trim()).length < 1) {
        let err = {type: 'InvalidParams', code: 422, msg: 'text value must be greater than 0'};
        return ResponseHelpers.respondWithCustomError(res, err);
      } else if (req.query && req.query.text && (req.query.text.trim()).length > 1) {
        searchText = validator.trim(validator.escape(req.query.text));
      }

      req.params.start = req.params.start || 0;
      req.params.limit = req.params.limit || 2;

      return Organization.findById(req.params.orgid)
        .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Organization not found.'}))
        .then(sendOrgFeed(req.params.start, req.params.limit, searchText, sortObj, filter))
        .then(ResponseHelpers.respondWithPaginatedAndPageResult(res))
        .catch(ResponseHelpers.respondWithErrorEntity(res))
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res))
}

// get Single Org Card
export function getSingleOrgCard(req, res) {
  let user, dtype, role, deviceId;
  if (req.user) {
    user = req.user._id;
  }
  if (req.user && req.user.role) {
    role = req.user.role;
  }

  if (req.deviceId) {
    deviceId = req.deviceId;
  }
  return DeviceCtrl.getDeviceData(deviceId)
    .then(device => {
      if (device && device.dtype) {
        dtype = device.dtype;
      }
      return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}, {value: req.params.cardid, type: 'card'}])
        .then(() => {
          return Organization.findById(req.params.orgid)
            .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Organization not found.'}))
            .then(getSingleOrgFeed(req.params.cardid))
            .then(parseExtrenalURL(user, dtype, role, req.params.orgid, deviceId))
            .then(getUsersCount())
            .then(ResponseHelpers.respondRawResults(res))
            .catch(ResponseHelpers.respondWithErrorEntity(res))
        })
        .catch(ResponseHelpers.respondWithErrorEntity(res))
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res))
}

// get Audience Stats
export function getAudienceStats(req, res) {
  let _audiences = req.params.ids.split(',');
  let searchAll = _audiences.filter(_audience => _audience == '0').length;
  if (!searchAll) {
    return getUsersByAudienceLink(_audiences)
      .then(reformatResponse())
      .then(ResponseHelpers.respondRawResults(res))
      .catch(ResponseHelpers.respondWithErrorEntity(res));
  } else {
    return getOrgUsers(req.params.orgid)
      .then(reformatResponse())
      .then(ResponseHelpers.respondRawResults(res))
      .catch(ResponseHelpers.respondWithErrorEntity(res));
  }
}

//Gets Sidebars of the organization
export function getOrgSidebars(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}])
    .then(() => {
      let shdParseHandleBars = true;
      if (req.query.parse && req.query.parse != 'true') {
        shdParseHandleBars = false;
      }
      return Organization.findById(req.params.orgid)
        .select('-_id sidebars style')
        // .populate('sidebars')
        .lean()
        .exec()
        .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Organization not found.'}))
        .then(sidebars => {
          if (shdParseHandleBars && sidebars.sidebars) {
            let prependData = {
              "title": "",
              "type": "$config",
              "icon": "",
              "options": {}
            };
            if (sidebars.style) {
              prependData.options = sidebars.style;
            }
            sidebars.sidebars.unshift(prependData);
          }
          return {items: sidebars.sidebars};
        })
        .then(parseHandleBars(req, shdParseHandleBars, shdParseHandleBars))
        .then(sidebars => {
          return res.status(200).json(sidebars);
        })
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

//Gets es_index of the organization
export function getEsIndex(orgId) {
  return new Promise((resolve, reject) => {
    return Organization.findById(orgId)
      .select('-_id es_index')
      .exec()
      .then(org => {
        return resolve(org);
      })
      .catch(err => {
        console.log('Error in organization.controller.getEsIndex 1');
        return reject(err);
      });
  })
}

//Gets organization
export function getOrgById(orgId) {
  return new Promise((resolve, reject) => {
    return Organization.findById(orgId)
      .exec()
      .then(org => {
        return resolve(org);
      })
      .catch(err => {
        console.log('Error in organization.controller.getOrgById 1');
        return reject(err);
      });
  })
}

//Gets organization
export function getOrgDataById(orgId) {
  return new Promise((resolve, reject) => {
    return Organization.findById(orgId)
      .populate('androidKey iosKey')
      .exec()
      .then(org => {
        return resolve(org);
      })
      .catch(err => {
        console.log('Error in organization.controller.getOrgById 1');
        return reject(err);
      });
  })
}

//Creates/Updates Sidebars of the organization
export function createUpdateOrgSidebars(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}])
    .then(() => {
      if (req.body.active) {
        delete req.body.active;
      }
      if (req.body._id) {
        delete req.body._id;
      }
      return Organization.findById(req.params.orgid)
        .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Organization not found.'}))
        .then(updateSidebarDirectlyInOrg(req.body))
        // .then(createUpdateSidebar(req.body))
        // .then(ResponseHelpers.respondWithResult(res))
        .then(sidebars => {
          return res.status(200).json({items: sidebars});
        })
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

//Createsa new User
export function createUser(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}])
    .then(() => {
      let creator = req.user;
      let apiKey;
      if (req.body.active) {
        delete req.body.active;
      }
      if (req.body._id) {
        delete req.body._id;
      }
      if (req.body.password) {
        delete req.body.password;
      }
      if (req.body.activationToken) {
        delete req.body.activationToken;
      }
      if (req.body.salt) {
        delete req.body.salt;
      }
      if (req.body.inviteLink) {
        delete req.body.inviteLink;
      }
      // if (req.body.role) {
      //   delete req.body.role;
      // }

      if (req.apiKey && req.apiKey._id) {
        apiKey = req.apiKey._id;
      }

      let toValidate = ['name', 'email', 'onboard', 'type', 'role', 'phone'];
      let stripEntities = ['username', 'salt', 'activationToken', '__v', 'createdBy', 'updatedBy', 'createdAt', 'updatedAt'];
      return sanitizeEntities(req.body, toValidate)
        .then(validateEntities(toValidate))
        .then(data => {
          return Organization.findOne({_id: req.params.orgid})
            .then(org => {
              if (org.apps) {
                if (!org.apps.android) {
                  let err = {type: 'AndroidPackageError', code: 422, msg: 'Organization doesn\'t have Android Package'};
                  return ResponseHelpers.respondWithCustomError(res, err)
                }
                if (!org.apps.ios) {
                  let err = {type: 'iOSPackageError', code: 422, msg: 'Organization doesn\'t have iOS Package'};
                  return ResponseHelpers.respondWithCustomError(res, err)
                }
                if (!org.apps.iosAppStoreId) {
                  let err = {type: 'iOSAppStoreIdError', code: 422, msg: 'Organization doesn\'t have iOS AppStoreId'};
                  return ResponseHelpers.respondWithCustomError(res, err)
                }
              } else {
                let err = {type: 'AppsPackageError', code: 422, msg: 'Organization doesn\'t have any Apps'};
                return ResponseHelpers.respondWithCustomError(res, err)
              }

              if (org.firebase) {
                if (!org.firebase.domain) {
                  let err = {type: 'FirebaseDomainError', code: 422, msg: 'Organization doesn\'t have Firebase Domain'};
                  return ResponseHelpers.respondWithCustomError(res, err)
                }
                if (!org.firebase.apiKey) {
                  let err = {type: 'FirebaseApiKeyError', code: 422, msg: 'Organization doesn\'t have Firebase Api Key'};
                  return ResponseHelpers.respondWithCustomError(res, err)
                }
              } else {
                let err = {type: 'FirebaseConfigError', code: 422, msg: 'Organization doesn\'t have firebase config'};
                return ResponseHelpers.respondWithCustomError(res, err)
              }

              if (org.sendgridId) {
                return Mailer.checkSendGridId(org.sendgridId)
                  .then(() => {
                    return checkIfUserExistsInOrg(req.params.orgid, data.email, data.phone)
                      .then(createNewUser(org, data, creator, apiKey, req.params.partnerId))
                      .then(saveUserInPartner(req.params.orgid, req.params.partnerId))
                      .then(sendEmailIfOnboard(org))
                      .then(sendWebhookRequest(org, req.params.partnerId))
                      .then(sendSMSIfOnboard())
                      .then(ResponseHelpers.respondIfSendgridError(res))
                      .then(ResponseHelpers.stripResults(stripEntities))
                      .then(ResponseHelpers.respondWithResult(res))
                      .catch(ResponseHelpers.respondWithErrorEntity(res));
                  })
                  .catch(ResponseHelpers.respondIfSendgridError(res));
              } else {
                let err = {type: 'SendGridError', code: 422, msg: 'Organization doesn\'t have sendgridId'};
                return ResponseHelpers.respondWithCustomError(res, err)
              }
            })
        })
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

export function checkUserExistanceInOrgByEmail() {
  return compose()
    .use(function(req, res, next) {
      return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}])
        .then(() => {
          if (!req.body.email) {
            console.log('Error in organization.controller.checkUserExistanceInOrgByEmail 1');
            let err = {type: 'MissingParams', code: 422, msg: 'email is required'};
            return ResponseHelpers.respondWithCustomError(res, err);
          } else {
            return UserCtrl.getUserByEmail(req.body.email)
              .then(user => {
                if (user) {
                  console.log(`User has role ${user.role}, skipping organization check`);
                  next();
                } else {
                  return PartnerCtrl.checkUser(req.params.orgid, req.body.email.toLowerCase())
                    .then(exists => {
                      if (!exists) {
                        console.log('Error in organization.controller.checkUserExistanceInOrgByEmail 2');
                        let err = {type: 'EntityNotFound', code: 404, msg: 'User not found in organization.'};
                        return ResponseHelpers.respondWithCustomError(res, err);
                      }
                      next();
                    })
                    .catch(err => next(err));
                }
              })
              .catch(err => next(err));
          }
        })
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    });
}

export function checkUserExistanceInOrgByID() {
  return compose()
    .use(function(req, res, next) {
      return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}])
        .then(() => {
          if (!req.params.id) {
            console.log('Error in organization.controller.checkUserExistanceInOrgByID 1');
            let err = {type: 'MissingParams', code: 422, msg: 'id is required'};
            return ResponseHelpers.respondWithCustomError(res, err);
          } else {
            return UserCtrl.getUserByID(req.params.id)
              .then(user => {
                if (user) {
                  console.log(req.params.orgid, user.email.toLowerCase());
                  return PartnerCtrl.checkUser(req.params.orgid, user.email.toLowerCase())
                    .then(exists => {
                      console.log(exists);
                      if (!exists) {
                        console.log('Error in organization.controller.checkUserExistanceInOrgByID 2');
                        let err = {type: 'EntityNotFound', code: 404, msg: 'User not found in organization.'};
                        return ResponseHelpers.respondWithCustomError(res, err);
                      }
                      next();
                    })
                    .catch(err => next(err));
                } else {
                  console.log('Error in organization.controller.checkUserExistanceInOrgByID 3');
                  let err = {type: 'EntityNotFound', code: 404, msg: 'User not found in organization.'};
                  return ResponseHelpers.respondWithCustomError(res, err);
                }
              })
              .catch(err => next(err));
          }
        })
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    });
}

export function checkUserExistanceInOrgByToken() {
  return compose()
    .use(function(req, res, next) {
      return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}])
        .then(() => {
          if (!req.headers.authorization) {
            console.log('Error in organization.controller.checkUserExistanceInOrgByToken 1');
            let err = {type: 'UNAUTHORIZED', code: 401, msg: 'Missing Bearer Token'};
            return ResponseHelpers.respondWithCustomError(res, err);
          } else if (!req.user && !req.user.email) {
            console.log('Error in organization.controller.checkUserExistanceInOrgByToken 2');
            let err = {type: 'UNAUTHORIZED', code: 401, msg: 'Bearer Token is Invalid'};
            return ResponseHelpers.respondWithCustomError(res, err);
          } else {
            return UserCtrl.getUserByEmail(req.user.email)
              .then(user => {
                if (user) {
                  console.log(`User has role ${user.role}, skipping organization check`);
                  next();
                } else {
                  return PartnerCtrl.checkUser(req.params.orgid, req.user.email)
                    .then(exists => {
                      if (!exists) {
                        console.log('Error in organization.controller.checkUserExistanceInOrgByToken 2');
                        let err = {type: 'EntityNotFound', code: 404, msg: 'User not found in organization.'};
                        return ResponseHelpers.respondWithCustomError(res, err);
                      }
                      next();
                    })
                    .catch(err => next(err));
                }
              })
              .catch(err => next(err));
          }
        })
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    });
}

export function checkOrgExistance() {
  return compose()
    .use(function(req, res, next) {
      return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}])
        .then(() => {
          return Organization.findOne({_id: req.params.orgid})
            .then(exists => {
              console.log(exists);
              if (!exists) {
                console.log('Error in organization.controller.checkOrgExistance 1');
                let err = {type: 'EntityNotFound', code: 404, msg: 'Organization not found'};
                return ResponseHelpers.respondWithCustomError(res, err);
              }
              req.orgId = req.params.orgid.toString();
              next();
            })
            .catch(err => next(err));
        })
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    });
}

/*Generic APIs*/

// Gets a list of Organizations
export function index(req, res) {
  return Organization.find().exec()
    .then(ResponseHelpers.respondWithResult(res))
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

//Get Org Config
export function getConfig(req, res) {
  return ResponseHelpers.handleValidObjectIds([])
    .then(() => {
      return Organization.findOne({code: req.params.id})
        .select('config _id register')
        .exec()
        .then(ResponseHelpers.respondWithResult(res))
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}


function getPartnerDetails(id) {
  return function(org) {
    return new Promise((resolve, reject) => {
      return PartnerCtrl.getFullPartnerWithoutUsers(id, org._id)
        .then(partner => {
          return resolve({org: org, partner: partner});
        })
        .catch(err => {
          console.log('Error in organization.controller.getPartnerDetails');
          return reject(err);
        })
    });
  }
}

export function getOrgWithConfig(req, res) {
  let validate = req.params.partnerId ? [{value: req.params.partnerId, type: 'partner'}] : [];
  return ResponseHelpers.handleValidObjectIds(validate)
    .then(() => {
      return Organization.findOne({code: req.params.id})
        .populate('androidKey iosKey')
        .exec()
        .then(getPartnerDetails(req.params.partnerId))
        .then(ResponseHelpers.respondWithResult(res))
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

//Get Org Key/Secret
export function getKeys(req, res) {
  return APIKeyCtrl.getKeySecret(req.params.id)
    .then(ResponseHelpers.respondWithResult(res))
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Gets a single Organization from the DB
export function show(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.id, type: 'organization'}])
    .then(() => {
      return Organization.findById(req.params.id).exec()
        .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Organization not found.'}))
        .then(ResponseHelpers.respondWithResult(res))
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// getIndexMappings
export function getIndexMappings(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}])
    .then(() => {
      return getEsIndex(req.params.orgid)
        .then(es.getMappings())
        .then(ResponseHelpers.respondRawResults(res))
        .catch(ResponseHelpers.respondRawErrors(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

export function getOrganizationByCode(code) {
  return new Promise((resolve, reject) => {
    return Organization.findOne({code: code})
      .then(org => {
        return resolve(org);
      })
      .catch(err => {
        console.log('Error in organization.controller.getOrganizationByCode 2');
        return reject(err);
      })
  })
}

// Gets a single Organization by code from the DB
export function getOrgByCode(req, res) {
  let pver = null;
  let dos = null;
  if (req.query.pver) {
    pver = req.query.pver;
    if (!req.query.dos || (req.query.dos && req.query.dos.length < 1)) {
      console.log('Error in organization.controller.getOrgByCode 1');
      let err = {type: 'MissingParams', code: 422, msg: 'dos is required'};
      return ResponseHelpers.respondWithCustomError(res, err);
    } else {
      dos = req.query.dos;
    }
  }
  return Organization.findOne({code: req.params.code})
    .lean()
    .exec()
    .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Organization not found.'}))
    .then(getVersionIfPverExists(pver, dos))
    .then(org => {
      let prependData = {
        "title": "",
        "type": "$config",
        "icon": "",
        "options": {}
      };
      if (org.style) {
        prependData.options = org.style;
      }
      org.sidebars.unshift(prependData);
      if (org.version) {
        if (org.version.upgrade == 'force') {
          let appLink = config.appLinkAndroid;
          if (dos && dos.toLowerCase() == 'ios') {
            appLink = config.appLinkIos;
          }
          delete org.version;
          let msg = {
            meta: {
              code: 200,
              action: "logout",
              message: {type: 'info', title: 'Upgrade Available', text: 'Upgrade is available', link: appLink}
            },
            data: org
          };
          return ResponseHelpers.respondWithSpecialMsg(res, msg);
        } else {
          return ResponseHelpers.respondWithCustomResult(res, org);
        }
      } else {
        if (pver || dos) {
          let err = {type: 'VersionNotExists', code: 404, msg: 'App version does not exists'};
          return ResponseHelpers.respondWithCustomError(res, err);
        } else {
          return ResponseHelpers.respondWithCustomResult(res, org);
        }
      }
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Creates a new Organization in the DB
export function create(req, res) {
  if (req.body.createdAt) {
    delete req.body.createdAt;
  }
  if (req.body.updatedAt) {
    delete req.body.updatedAt;
  }
  if (req.body.code) {
    delete req.body.code;
  }
  req.body.createdBy = req.user._id;
  let toValidate = ['name', 'level', 'sendgridId', 'sendgridIdForPassword', /*'androidKey', 'iosKey',*/ 'import', 'apps.android', 'apps.ios', 'apps.iosAppStoreId', 'firebase.domain', 'firebase.apiKey', 'smsInvite'];
  return sanitizeEntities(req.body, toValidate)
    .then(validateEntities(toValidate))
    .then(data => {
      return getNextOrgSequence()
        .then(seq => {
          let hashids = new Hashids('', 6, 'abcdefghijklmnopqrstuvwxyz0123456789');
          let org = data;
          org.code = hashids.encode(seq);
          return getOrgSidebarsIfHasToImport(org)
            .then(createEsIndex())
            .then(createOrg())
            .then(duplicateObjects(org))
            .then(org => {
              return APIKeyCtrl.createApiKeyForOrg(org._id, req.user)
                .then(getOrgInstanceWithAPIKey(org))
                .then(ResponseHelpers.respondWithResult(res, 201))
                .catch(ResponseHelpers.respondWithErrorEntity(res));
            })
            .catch(ResponseHelpers.respondWithErrorEntity(res));
        })
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Updates an existing Organization in the DB
export function update(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.id, type: 'organization'}])
    .then(() => {
      if (req.body._id) {
        delete req.body._id;
      }
      if (req.body.createdAt) {
        delete req.body.createdAt;
      }
      if (req.body.updatedAt) {
        delete req.body.updatedAt;
      }
      if (req.body.code) {
        delete req.body.code;
      }
      req.body.updatedAt = moment();
      req.body.updatedBy = req.user._id;
      let toValidate = ['name', 'level', 'androidKey', 'iosKey', 'sendgridId', 'sendgridIdForPassword', 'apps.android', 'apps.ios', 'apps.iosAppStoreId', 'firebase.domain', 'firebase.apiKey', 'smsInvite'];
      return sanitizeEntities(req.body, toValidate)
        .then(validateEntities(toValidate, true))
        .then(data => {
          return Organization.findById(req.params.id).exec()
            .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Organization not found.'}))
            .then(saveUpdates(data))
            .then(ResponseHelpers.respondWithResult(res))
            .catch(ResponseHelpers.respondWithErrorEntity(res));
        })
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

export function saveStyle(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}])
    .then(() => {
      let style = {};
      if (req.body.style) {
        style = req.body.style;
      }
      if (!req.body.style) {
        let err = {type: 'MissingParams', code: 422, msg: 'Missing style'};
        console.log(err);
        console.log('Error in organization.controller.saveStyle 1');
        return ResponseHelpers.respondWithCustomError(res, err);
      }
      let schemaFile = fs.readFileSync(path.resolve(__dirname + '/organizationStyleSchema.txt'), 'utf8');
      let schema = JSON.parse(schemaFile);
      let valid = tv4.validate(req.body.style, schema);
      if (!valid && tv4.error && tv4.error.message) {
        let err = {type: 'SchemaValidationError', code: tv4.error.code, msg: {message: tv4.error.message, params: tv4.error.params, dataPath: tv4.error.dataPath, schemaPath: tv4.error.schemaPath, subErrors: tv4.error.subErrors}};
        console.log(err);
        console.log('Error in organization.controller.saveStyle 2');
        return ResponseHelpers.respondWithCustomError(res, err);
      }
      req.body.updatedAt = moment();
      req.body.updatedBy = req.user._id;
      console.log(style);
      return Organization.update({_id: req.params.orgid}, {$set: {style: style}}).exec()
        .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Organization not found.'}))
        .then(getOrganizationDetails(req.params.orgid))
        .then(ResponseHelpers.respondWithResult(res))
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

export function getStyle(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}])
    .then(() => {
      return Organization.findOne({_id: req.params.orgid})
        .select('style')
        .exec()
        .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Organization not found.'}))
        .then(reformatStyle())
        .then(ResponseHelpers.respondWithResult(res))
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

export function getPartnerTypes(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}])
    .then(() => {
      return Organization.findOne({_id: req.params.orgid})
        .select('partner_types')
        .exec()
        .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Organization not found.'}))
        .then(reformatTypes())
        .then(ResponseHelpers.respondWithResult(res))
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

export function postPartnerTypes(req, res) {
  if (!req.body.name && !req.body.name.length) {
    let err = {type: 'MissingParams', code: 422, msg: 'Missing partner_types'};
    return ResponseHelpers.respondWithCustomError(res, err);
  }
  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}])
    .then(() => {
      return Organization.findOne({_id: req.params.orgid})
        .select('partner_types')
        .lean()
        .exec()
        .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Organization not found.'}))
        .then(reformatTypes())
        .then(_partnerTypes => {
          let partnerTypes = _partnerTypes.map(_p => {
            return _p.toLowerCase();
          });
          if (partnerTypes.indexOf(req.body.name.trim().toLowerCase()) < 0) {
            partnerTypes.push(req.body.name);
          } else {
            let err = {type: 'AlreadyExists', code: 409, msg: "partner_type already exists"};
            console.log(err);
            console.log('Error in organization.controller.postPartnerTypes 2');
            return ResponseHelpers.respondWithCustomResult(res, err)
          }
          return Organization.update({_id: req.params.orgid}, {$set: {partner_types: partnerTypes}})
            .then(result => {
              return partnerTypes;
            })
            .then(ResponseHelpers.respondWithResult(res))
            .catch(ResponseHelpers.respondWithErrorEntity(res));
        })
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

export function getStats(req, res) {
  let err = {};
  if (!req.query.start) {
    err = {type: 'MissingParams', code: 422, msg: 'Start date is missing'};
    return ResponseHelpers.respondWithCustomError(res, err);
  } else if (req.query.start && !validator.isDate(req.query.start)) {
    err = {type: 'MissingParams', code: 422, msg: 'Start date is invalid. It should be in MM/DD/YYYY format'};
    return ResponseHelpers.respondWithCustomError(res, err);
  } else if (!req.query.end) {
    err = {type: 'MissingParams', code: 422, msg: 'End date is missing'};
    return ResponseHelpers.respondWithCustomError(res, err);
  } else if (req.query.end && !validator.isDate(req.query.end)) {
    err = {type: 'MissingParams', code: 422, msg: 'End date is invalid. It should be in MM/DD/YYYY format'};
    return ResponseHelpers.respondWithCustomError(res, err);
  }

  let startDate, endDate;
  try {
    startDate = moment(req.query.start, 'MM/DD/YYYY');
  } catch (e) {
    err = {type: 'MissingParams', code: 422, msg: 'Start date is invalid. It should be in MM/DD/YYYY format'};
    return ResponseHelpers.respondWithCustomError(res, err);
  }
  try {
    endDate = moment(req.query.end, 'MM/DD/YYYY');
  } catch (e) {
    err = {type: 'MissingParams', code: 422, msg: 'End date is invalid. It should be in MM/DD/YYYY format'};
    return ResponseHelpers.respondWithCustomError(res, err);
  }

  console.log('Getting stats from:' + startDate.format() + ' to ' + endDate.format());

  return ResponseHelpers.handleValidObjectIds([{value: req.params.orgid, type: 'organization'}])
    .then(() => {
      return getNotificationsCount(req.params.orgid, startDate, endDate)
        .then(count => {
          let notificationsCount = count;
          return allUsersCount(req.params.orgid, startDate, endDate)
            .then(usersCount => {
              return getPartnersCount(req.params.orgid, startDate, endDate)
                .then(partnersCount => {
                  let _finalData = {
                    "window": {
                      "start": startDate.format('MM/DD/YYYY'),
                      "end": endDate.format('MM/DD/YYYY')
                    },
                    "data": {
                      "notifications": {
                        "sent": notificationsCount
                      },
                      "users": {
                        "total": usersCount.total,
                        "invited": usersCount.invited,
                        "accepted": usersCount.accepted
                      },
                      "partners": {
                        "total": partnersCount.total,
                        "added": partnersCount.added
                      }
                    }
                  };
                  return finalData(_finalData)
                    .then(ResponseHelpers.respondWithResult(res))
                    .catch(ResponseHelpers.respondWithErrorEntity(res));
                })
                .catch(ResponseHelpers.respondWithErrorEntity(res));
            })
            .catch(ResponseHelpers.respondWithErrorEntity(res));
        })
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

// Deletes a Organization from the DB
export function destroy(req, res) {
  return ResponseHelpers.handleValidObjectIds([{value: req.params.id, type: 'organization'}])
    .then(() => {
      return Organization.findById(req.params.id).exec()
        .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'Organization not found.'}))
        .then(removeEntity())
        .then(ResponseHelpers.respondWithResult(res, 204))
        .catch(ResponseHelpers.respondWithErrorEntity(res));
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}


export function createBulkUser(req, res) {
  let ret = 0;
  let onboard = req.body.onboard;
  let type = req.body.type;
  let partnerType = req.body.partner_type;
  let orgid = req.params.orgid;
  return ResponseHelpers.handleValidObjectIds([{value: orgid, type: 'organization'}])
    .then(() => {
      return Organization.findOne({_id: orgid})
        .then(org => {
          if (org.sendgridId) {
            return Mailer.checkSendGridId(org.sendgridId)
              .then(() => {
                return request.get(req.query.file, function(error, response, body) {
                  if (!error && response.statusCode === 200) {
                    return parse(body, {columns: true}, function(err, _output) {
                      if (err) {
                        return ResponseHelpers.respondWithCustomError(res, err);
                      }
                      let errors = [];
                      let success = [];
                      let output = _output.filter(row => row['User Name'].trim().length && row['User Email'].trim().length && row['Partner ID'].trim().length && row['Partner Name'].trim().length && row['Partner Email'].trim().length);
                      return async.mapSeries(output, function(newUser, cb) {
                        let allData = {
                          name: newUser['User Name'].trim(),
                          email: newUser['User Email'].trim().toLowerCase().replace(/\?/g, ''),
                          phone: newUser['User Phone'].trim(),
                          partnerId: newUser['Partner ID'].trim(),
                          partnerName: newUser['Partner Name'].trim(),
                          partnerEmail: newUser['Partner Email'].trim().toLowerCase().replace(/\?/g, ''),
                        };
                        if (allData.phone) {
                          allData.phone = allData.phone.replace(/[^\d]/gi, '');
                        }

                        if (!allData.partnerName && !allData.partnerId && !allData.partnerEmail && !orgid && !partnerType && !allData.name && !allData.email && !type) {
                          errors.push({err: 'Missing params', record: newUser});
                          return cb();
                        }
                        let toValidate = ['phone', 'name', 'email', 'partnerId', 'partnerName', 'partnerEmail'];
                        return sanitizeEntities(allData, toValidate)
                          .then(validateEntities(toValidate))
                          .then(_data => {
                            console.log(_data);
                            let usr = {
                              name: _data.name,
                              email: _data.email,
                              phone: _data.phone,
                              onboard: onboard,
                              type: type
                            };

                            let ptnr = {
                              name: _data.partnerName,
                              partnerId: _data.partnerId,
                              email: _data.partnerEmail,
                              organization: orgid,
                              type: partnerType
                            };
                            return PartnerCtrl.getCount(ptnr)
                              .then(PartnerCtrl.saveInDBIfNewOrSkip(ptnr))
                              .then(data => {
                                return Partner.findOne({organization: orgid, partnerId: data.partnerId})
                                  .populate('organization')
                                  .select('name email partnerId organization type active')
                                  .then(PartnerCtrl.savePartnerInES(data, true))
                                  .then(_p => {
                                    console.log(_p);
                                    return checkIfUserExistsInOrg(orgid, usr.email, usr.phone)
                                      .then(createNewUser(org, usr, null, null, ptnr.partnerId))
                                      .then(saveUserInPartner(orgid, ptnr.partnerId))
                                      .then(sendEmailIfOnboard(org, true))
                                      .then(sendWebhookRequest(org, ptnr.partnerId))
                                      .then(sendSMSIfOnboard())
                                      .then(() => {
                                        success.push({record: newUser});
                                        ++ret;
                                        return cb();
                                      })
                                      .catch(err => {
                                        errors.push({err: err, record: newUser});
                                        ++ret;
                                        return cb();
                                      });
                                  })
                                  .catch(err => {
                                    console.log(err);
                                    errors.push({err: err, record: newUser});
                                    ++ret;
                                    return cb();
                                  });

                              })
                              .catch(err => {
                                errors.push({err: err, record: newUser});
                                ++ret;
                                return cb();
                              });
                          })
                          .catch(err => {
                            errors.push({err: err, record: newUser});
                            ++ret;
                            return cb();
                          });

                      }, function(err, done) {
                        if (err) {
                          return ResponseHelpers.respondWithCustomError(res, err);
                        }
                        return ResponseHelpers.respondWithCustomResult(res, {count: {errors: errors.length, success: success.length}, errors: errors, success: success}, 200);
                      });
                    });
                  }
                });
              })
              .catch(ResponseHelpers.respondIfSendgridError(res));
          }
          else {
            let err = {type: 'SendGridError', code: 422, msg: 'Organization doesn\'t have sendgridId'};
            return ResponseHelpers.respondWithCustomError(res, err)
          }
        })
    })
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}


function getOrg(org) {
  return new Promise((resolve, reject) => {
    return resolve(org);
  })
}


function getP(partner) {
  return new Promise((resolve, reject) => {
    return resolve(partner);
  })
}


/*Resync ES Indexes*/
export function resyncAll(req, res) {
  let query = {};
  if (req.query.org) {
    query = {_id: req.query.org}
  }
  return Organization.find(query)
    .then(orgs => {
      return new Promise((resolve, reject) => {
        return async.mapSeries(orgs, function(org, cb) {
          return getOrg(org)
            .then(deleteEsIndex())
            .then(createEsIndex())
            .then(_org => {
              return Partner.find({organization: org._id})
                .then(partners => {
                  return async.mapSeries(partners, function(partner, callback) {
                    let _p = {
                      partner_name: partner.name,
                      partner_id: partner.partnerId,
                      email: partner.email,
                      partner_type: partner.type,
                      organization: partner.organization,
                      active: partner.active,
                      all_users: partner.allUsers
                    };
                    let final = {};
                    if (partner.extended) {
                      final = merge_options(_p, partner.extended);
                    } else {
                      final = _p;
                    }
                    return es.createDocument(org.es_index, final)
                      .then(esObj => {
                        console.log(JSON.stringify(esObj));
                        return callback(null, esObj);
                      })
                      .catch(err => {
                        return callback(err);
                      })
                  }, function(err, done) {
                    if (err) {
                      return cb(err);
                    }
                    return cb(null, done);
                  });
                })
                .catch(err => {
                  cb(err);
                });
            })
            .catch(err => {
              cb(err);
            });
        }, function(err, done) {
          if (err) {
            return reject(err);
          }
          return resolve(done);
        })
      })
    })
    .then(ResponseHelpers.respondWithResult(res, 201))
    .catch(ResponseHelpers.respondWithErrorEntity(res));
}

function getUserEntity(user) {
  return new Promise((resolve, reject) => {
    return resolve(user);
  })
}

export function updateBulkPhones(req, res) {
  let ret = 0;
  request.get(req.query.file, function(error, response, body) {
    if (!error && response.statusCode === 200) {
      return parse(body, {columns: true}, function(err, _output) {
        if (err) {
          return ResponseHelpers.respondWithCustomError(res, err);
        }
        let errors = [];
        let updated = [];
        let smsSent = [];
        let smsError = [];
        let output = _output.filter(row => row['Email'].trim().length && row['Phone'].trim().length);
        return async.mapLimit(output, 10, function(newUser, cb) {
          let user = {
            email: newUser['Email'].trim(),
            phone: newUser['Phone'].trim()
          };
          if (!user.email && !user.email.length && !user.phone && !user.phone.length) {
            ++ret;
            errors.push({err: 'Missing params', record: newUser});
            return cb();
          } else {
            return UserCtrl.getUserByOnlyEmail(user.email)
              .then(_u => {
                if (!_u) {
                  ++ret;
                  errors.push({err: 'User does not exists', record: newUser});
                  return cb();
                }
                user.phone = user.phone.replace(/[^\d]/gi, '');
                user.updatedAt = moment();
                console.log('Email: ' + user.email);
                console.log('Phone: ' + user.phone);
                let toValidate = ['phone', 'email'];
                return sanitizeEntities(user, toValidate)
                  .then(validateEntities(toValidate))
                  .then(data => {
                    return UserCtrl.updatePhone(data)
                      .then(_user => {
                        console.log('updated : ' + JSON.stringify(data));
                        updated.push({record: user});
                        if (_user && _user.status === 'Accepted') {
                          ++ret;
                          return cb();
                        } else {
                          if (_user && !_user.activationToken && !_user.activationToken.length) {
                            let err = {type: 'InvalidActivationToken', code: 422, msg: 'Invalid Activation Token'};
                            errors.push({err: err, record: user});
                            ++ret;
                            return cb();
                          } else {
                            let activationToken = null;
                            if (_user.activationToken) {
                              activationToken = _user.decrypt(_user.activationToken);
                            }
                            return PartnerCtrl.getUsersPartner(_user._id)
                              .then(_partner => {
                                let org = _partner.organization;
                                let partner = _partner._id;
                                return getUserEntity(_user)
                                  .then(UserCtrl.collectFullData(_user, org, partner))
                                  .then(sendSMSIfOnboard(activationToken))
                                  .then(smsData => {
                                    ++ret;
                                    if (smsData.hasOwnProperty('smsError') && smsData.smsError) {
                                      smsError.push({smsError: smsData.smsError, record: user});
                                    } else if (smsData.hasOwnProperty('smsSent') && smsData.smsSent) {
                                      smsSent.push({smsSent: smsData.smsSent, record: user});
                                    }
                                    return cb();
                                  })
                                  .catch(err => {
                                    errors.push({err: err, record: user});
                                    ++ret;
                                    return cb();
                                  });
                              })
                              .catch(err => {
                                errors.push({err: err, record: user});
                                ++ret;
                                return cb();
                              });
                          }
                        }
                      })
                      .catch(err => {
                        errors.push({err: err, record: user});
                        ++ret;
                        return cb();
                      });
                  })
                  .catch(err => {
                    errors.push({err: err, record: user});
                    ++ret;
                    return cb();
                  });
              })
              .catch(err => {
                errors.push({err: err, record: user});
                ++ret;
                return cb();
              });
          }
        }, function(err, done) {
          if (err) {
            return ResponseHelpers.respondWithCustomError(res, err);
          }
          console.log('Phone Update Failed: ' + errors.length);
          console.log('Phone Update Succeed: ' + (ret - errors.length));
          console.log('smsSent Failed: ' + (smsError.length));
          console.log('smsSent Succeed: ' + (smsSent.length));
          return ResponseHelpers.respondWithCustomResult(res, {count: {errors: errors.length, phoneUpdated: updated.length, smsSent: smsSent.length, smsError: smsError.length}, errors: errors, phoneUpdated: updated, smsSent: smsSent, smsError: smsError}, 200);
        });
      });
    } else {
      return ResponseHelpers.respondWithCustomResult(res, error);
    }
  });
}

function merge_options(obj1, obj2) {
  var obj3 = {};
  for (var attrname in obj1) {
    obj3[attrname] = obj1[attrname];
  }
  for (var attrname in obj2) {
    obj3[attrname] = obj2[attrname];
  }
  return obj3;
}
