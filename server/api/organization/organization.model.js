'use strict';

import mongoose from 'mongoose';
mongoose.Promise = require('bluebird');
import {Schema} from 'mongoose';

var OrganizationSchema = new mongoose.Schema({
  name: String,
  image: String,
  // sidebars: [{
  //   type: mongoose.Schema.Types.ObjectId,
  //   ref: 'Sidebar'
  // }],
  code: String,
  sidebars: [],
  intro_video: String,  //url
  level: {
    type: String,
    enum: ['production', 'sandbox']
  },
  partner_types: [],  //'distributor','reseller','dealer'
  user_types: [],  //'Sales Rep','Sales Manager'
  es_index: String, //url
  __v: {type: Number, select: false},
  androidKey: {
    type: Schema.Types.ObjectId,
    ref: 'PushKeys'
  },
  iosKey: {
    type: Schema.Types.ObjectId,
    ref: 'PushKeys'
  },
  sendgridId: String,
  // sendgridIdForPassword: String,  //template id for sending password
  style: {},
  apps: {
    android: String,
    ios: String,
    iosAppStoreId: String
  },
  firebase: {
    domain: String,
    apiKey: String
  },
  landing: {
    template: {
      type: String
    }
  },
  register: {
    template: {
      type: String
    }
  },
  active: {type: Boolean, default: true, select: false},
  config: {
    registration: {
      partnerLookup: {
        type: Boolean,
        default: false
      },
      partnerCreate: {
        type: Boolean,
        default: false
      },
      allowed: {
        type: Boolean,
        default: false
      },
      partnerType: {
        type: String
      },
      userType: {
        type: String
      }
    }
  },
  smsInvite: [{
    from: {
      type: String  //shdnt have country code
    },
    text: {
      type: String
    },
    media: {
      type: String
    }
  }],
  createdAt: {
    type: Date,
    default: Date.now
  },
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  updatedBy: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  updatedAt: Date
});

export default mongoose.model('Organization', OrganizationSchema);
