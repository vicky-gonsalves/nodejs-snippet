"use strict";
import config from '../../config/environment';
var sg = require('sendgrid')(config.sendgrid.key);
var rq = require('request');

export function checkSendGridId(id) {
  return new Promise(function(resolve, reject) {
    let request1 = sg.emptyRequest({
      method: 'GET',
      path: '/v3/templates/' + id
    });

    return sg.API(request1)
      .then(response => {
        console.log(response);
        console.log('VALID Sendgrid Template id ' + id);
        return resolve('VALID')
      })
      .catch(error => {
        console.log('Invalid Sendgrid Template id ' + id);
        console.log(error);
        return reject(error)
      });
  });
}

export function sendMail(options) {
  return new Promise(function(resolve, reject) {
    let data = options.data;
    let from = options.from;
    let to = options.to;
    let subject = options.subject;
    let sendgridId = options.sendgridId;

    let reqBody = {
      method: 'POST',
      path: '/v3/mail/send',
      body: {
        personalizations: [
          {
            to: [
              {
                email: to,
              },
            ],
            substitutions: {
              ':organization._id': data.organization._id ? data.organization._id : undefined,
              ':organization.code': data.organization.code ? data.organization.code : undefined,
              ':organization.name': data.organization.name ? data.organization.name : undefined,
              ':partner.name': data.partner.name ? data.partner.name : undefined,
              ':partner._id': data.partner._id ? data.partner._id : undefined,
              ':partner.email': data.partner.email ? data.partner.email : undefined,
              ':name': data.name ? data.name : undefined,
              ':email': data.email ? data.email : undefined,
              ':role': data.role ? data.role : undefined,
              ':type': data.type ? data.type : undefined,
              ':password': data.password ? data.password : undefined,
              ':registration_qr': data.qrPath ? data.qrPath : undefined,
              ':registration_token': data.token ? (data.token).toUpperCase() : undefined,
              ':invite_link': data.inviteLink ? data.inviteLink : undefined
            },
            subject: subject
          },
        ],
        from: {
          email: from,
        },
        content: [
          {
            type: 'text/html',
            value: data.qrPath ? `<a href="${data.qrPath}"><img src="${data.qrPath}" alt="QR CODE" /></a>` : ' ',
          },
        ],
        template_id: sendgridId
      },
    };

    if (options.customArgs) {
      reqBody.body.personalizations[0].custom_args = options.customArgs;
    }

    let request = sg.emptyRequest(reqBody);

    return sg.API(request)
      .then(response => {
        return resolve(response);
      })
      .catch(error => {
        console.log('Error in sendMail 1');
        console.log(error.response.body);
        return reject(error);
      });
  });
}
