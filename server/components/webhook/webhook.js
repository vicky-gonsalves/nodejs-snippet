"use strict";

import path from 'path';
import config from "../../config/environment";
const WebHooks = require('node-webhooks');
let db = __dirname + '/../../config/webhooks.json';
if ('http://localhost:9000' === config.DOMAIN) {
  db = __dirname + '/../../config/webhooks_dev.json';
}
const webHooks = new WebHooks({db: path.normalize(db)});
const emitter = webHooks.getEmitter();
let webhooks = [];
if (config.webhook && config.webhook.length) {
  webhooks = JSON.parse(config.webhook);
}
webhooks.forEach(webhook => {
  webHooks.add('webhook1', webhook).then(function() {
    console.log('Webhook Attached: ' + webhook);
  }).catch(function(err) {
    console.log(err)
  });
});


emitter.on('*.success', function(shortname, statusCode, body) {
  console.log('Success on trigger webHook' + shortname + 'with status code', statusCode, 'and body', body)
});

emitter.on('*.failure', function(shortname, statusCode, body) {
  console.error('Error on trigger webHook' + shortname + 'with status code', statusCode, 'and body', body)
});


export function sendWebhookRequest(webhook, data) {
  webHooks.trigger(webhook, data);
}
