"use strict";
import * as config from '../../config/environment';
const elasticsearch = require('elasticsearch');
const Promise = require('bluebird');
Promise.config({
  // Enables all warnings except forgotten return statements.
  warnings: {
    wForgottenReturn: false
  }
});
let client = new elasticsearch.Client({
  host: config.elasticSearchURL,
  // log: 'trace'
});
client.ping({
  // ping usually has a 3000ms timeout
  requestTimeout: Infinity,

  // undocumented params are appended to the query string
  pretty: "elasticsearch!"
}, function(error) {
  if (error) {
    console.trace('elasticsearch cluster is down!');
  } else {
    console.log('All is well');
  }
});

export function perFormEsSearch(query) {
  return function(org) {
    return new Promise((resolve, reject) => {
      if (org && org.es_index) {
        client.indices.refresh({force: true})
          .then(resp => {
            return client.search({
              index: org.es_index,
              ignore: [404],
              body: query
            }).then(resp => {
              let size = 9000;
              if (resp && !resp.error && resp.hits && resp.hits.hasOwnProperty('total') && resp.hits.total) {
                size = resp.hits.total;
              }
              console.log("SIZE:" + size);
              return client.search({
                index: org.es_index,
                ignore: [404],
                body: query,
                size: size
              }).then(resp => {
                console.log('ES resp');
                console.log(JSON.stringify(resp));
                return resolve(resp);
              }, err => {
                console.log('Error in es.controller.perFormEsSearch 3');
                return resolve(err.body);
              });
            }, err => {
              console.log('Error in es.controller.perFormEsSearch 2');
              return resolve(err.body);
            });
          }, err => {
            console.log('Error in es.controller.perFormEsSearch 1');
            return resolve(err.body);
          })

      } else {
        console.log('Error in es.controller.perFormEsSearch 2');
        return reject({type: 'EntityNotFound', code: 404, msg: 'es_index not found'});
      }
    });
  }
}

export function getMappings() {
  return function(org) {
    return new Promise((resolve, reject) => {
      if (org && org.es_index) {
        client.indices.refresh({force: true})
          .then(resp => {
            return client.indices.getMapping({
              index: org.es_index,
              ignore: [404]
            }).then(resp => {
              console.log('ES resp');
              console.log(JSON.stringify(resp));
              return resolve(resp);
            }, err => {
              console.log('Error in es.controller.getMappings 2');
              return resolve(err.body);
            });
          }, err => {
            console.log('Error in es.controller.getMappings 1');
            return resolve(err.body);
          })
      } else {
        console.log('Error in es.controller.getMappings 2');
        return reject({type: 'EntityNotFound', code: 404, msg: 'es_index not found'});
      }
    });
  }
}

export function perFormEsSearchByEsIndex(query, es_index) {
  return new Promise((resolve, reject) => {
    if (query && es_index) {
      client.indices.refresh({force: true})
        .then(resp => {
          client.indices.clearCache({
            query: true,
            index: es_index
          })
            .then(() => {
              return client.search({
                index: es_index,
                ignore: [404],
                body: query
              }).then(resp => {
                let size = 9000;
                if (resp && !resp.error && resp.hits && resp.hits.hasOwnProperty('total') && resp.hits.total) {
                  size = resp.hits.total;
                }
                return client.search({
                  index: es_index,
                  ignore: [404],
                  body: query,
                  size: size
                }).then(resp => {
                  console.log('ES resp');
                  console.log(JSON.stringify(resp));
                  return resolve(resp);
                }, err => {
                  console.log('Error in es.controller.perFormEsSearchByEsIndex 1');
                  return resolve(err.body);
                });
              }, err => {
                console.log('Error in es.controller.perFormEsSearchByEsIndex 2');
                return resolve(err.body);
              });
            }, err => {
              console.log('Error in es.controller.perFormEsSearchByEsIndex 3');
              return resolve(err.body);
            })
        }, err => {
          console.log('Error in es.controller.perFormEsSearch 4');
          return resolve(err.body);
        })
    } else {
      console.log('Error in es.controller.perFormEsSearchByEsIndex 5');
      return reject({type: 'EntityNotFound', code: 404, msg: 'es_index/query not found'});
    }
  });
}

export function createIndex(es_index) {
  return new Promise((resolve, reject) => {
    if (es_index && es_index.length) {
      let params = {
        body: {
          mappings: {}
        },
        index: es_index
      };
      params.body.mappings[config.elasticSearchDocument] = {
        "properties": {
          "partner_id": {"type": "string", "index": "not_analyzed"}
        }
      };
      return client.indices.create(params)
        .then(resp => {
          console.log('ES resp');
          console.log(JSON.stringify(resp));
          return resolve(resp);
        }, err => {
          console.log('Error in es.controller.createIndex 1');
          return resolve(err.body);
        });
    } else {
      console.log('Error in es.controller.createIndex 2');
      return reject({type: 'EntityNotFound', code: 404, msg: 'es_index not found'});
    }
  });
}

export function deleteIndex(es_index) {
  return new Promise((resolve, reject) => {
    if (es_index && es_index.length) {
      let params = {
        index: es_index
      };
      return client.indices.delete(params)
        .then(resp => {
          console.log('Index Deleted: ' + es_index);
          console.log('ES resp');
          console.log(JSON.stringify(resp));
          return resolve(resp);
        }, err => {
          console.log('Error in es.controller.createIndex 1');
          return resolve(err.body);
        });
    } else {
      console.log('Error in es.controller.createIndex 2');
      return reject({type: 'EntityNotFound', code: 404, msg: 'es_index not found'});
    }
  });
}

export function createDocument(es_index, _body) {
  return new Promise((resolve, reject) => {
    if (es_index && es_index.length && _body) {
      let body = JSON.parse(JSON.stringify(_body));
      if (body._id) {
        delete  body._id;
      }
      if (body.partnerId) {
        body.partner_id = body.partnerId.toString();
        delete body.partnerId;
      }
      if (body.name) {
        body.partner_name = body.name.toString();
        delete body.name;
      }
      if (body.type) {
        body.partner_type = body.type.toString();
        delete body.type;
      }
      if (body.allUsers) {
        body.all_users = body.allUsers.toString();
        delete body.allUsers;
      }
      client.indices.refresh({force: true})
        .then(resp => {
          console.log(body);
          return client.create({
            index: es_index,
            type: config.elasticSearchDocument,
            body: body
          }).then(resp => {
            console.log('ES resp');
            console.log(JSON.stringify(resp));
            return resolve(resp);
          })
            .catch(err => {
              console.log('Error in es.controller.createDocument 3');
              return resolve(err);
            });
        })
        .catch(err => {
          console.log('Error in es.controller.createDocument 1');
          return resolve(err.body);
        });
    } else {
      console.log('Error in es.controller.createDocument 2');
      return reject({type: 'EntityNotFound', code: 404, msg: 'es_index not found'});
    }
  });
}

export function updateDocument(es_index, id, _body, partnerID) {
  return new Promise((resolve, reject) => {
    if (es_index && es_index.length && _body) {
      let body = JSON.parse(JSON.stringify(_body));
      if (body._id) {
        delete  body._id;
      }
      if (body.partnerId) {
        body.partner_id = body.partnerId.toString();
        delete body.partnerId;
      }
      if (body.name) {
        body.partner_name = body.name.toString();
        delete body.name;
      }
      if (body.type) {
        body.partner_type = body.type.toString();
        delete body.type;
      }
      if (body.allUsers) {
        body.all_users = body.allUsers.toString();
        delete body.allUsers;
      }
      return client.indices.refresh({force: true})
        .then(resp => {
          return client.update({
            index: es_index,
            type: config.elasticSearchDocument,
            id: id,
            refresh: "wait_for",
            versionType: "internal",
            body: {
              doc: _body
            }
          }).then(resp2 => {
            let q = {
              "query": {
                "match": {
                  "partner_id": partnerID
                }
              }
            };
            console.log('QUERY:' + JSON.stringify(q));
            setTimeout(() => {
              return perFormEsSearchByEsIndex(q, es_index)
                .then(response => {
                  console.log('ES resp partner: ');
                  console.log(JSON.stringify(response));
                  return resolve(response);
                })
                .catch(err => {
                  console.log(err);
                  console.log('Error in es.controller.updateDocument 4');
                  return reject({type: 'EntityNotFound', code: 404, msg: err});
                });
            }, 1000);
          })
            .catch(err => {
              console.log('Error in es.controller.updateDocument 1');
              return resolve(err.body);
            });
        })
        .catch(err => {
          console.log(err);
          console.log('Error in es.controller.updateDocument 3');
          return reject({type: 'EntityNotFound', code: 404, msg: 'es_index not found'});
        });
    } else {
      console.log('Error in es.controller.updateDocument 2');
      return reject({type: 'EntityNotFound', code: 404, msg: 'es_index not found'});
    }
  });
}

export function deleteDocument(es_index, query) {
  return new Promise((resolve, reject) => {
    if (es_index && es_index.length && query) {
      return client.deleteByQuery({
        index: es_index,
        type: config.elasticSearchDocument,
        body: {
          query: query
        }
      }).then(resp => {
        console.log('ES resp');
        console.log(JSON.stringify(resp));
        return resolve(resp);
      }, err => {
        console.log('Error in es.controller.deleteDocument 1');
        return resolve(err.body);
      });
    } else {
      console.log('Error in es.controller.deleteDocument 2');
      return reject({type: 'EntityNotFound', code: 404, msg: 'es_index not found'});
    }
  });
}
