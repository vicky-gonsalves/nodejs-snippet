"use strict";
import _ from "lodash";
import * as NotificationLogCtrl from "../../api/notificationLog/notificationLog.controller";
import * as PartnerCtrl from "../../api/partner/partner.controller";
import * as DeviceCtrl from "../../api/device/device.controller";
import * as config from "../../config/environment";
const gcm = require('node-gcm');
const Promise = require('bluebird');
const async = require('async');
const request = require('request');

//Organization Specific Keys  -- Configure these constants in order to allow organization specific push keys and then setup orgMappings
const useOrgSpecificKeysForGCM = config.useOrgSpecificKeyAndroid == true || config.useOrgSpecificKeyAndroid == 'true';  //For GCM Only
const useOrgSpecificKeysForAPN = config.useOrgSpecificKeyiOs == true || config.useOrgSpecificKeyiOs == 'true';  //For APN Only

let gcmKey = config.defaultAndroidKey;  //Dev Key --influents-dev-api
let cert_and_key = require('fs').readFileSync(config.defaultiOSKey); //Production Key --influents-dev-api


function getUserData(token, org, err = null, success = null) {
  return new Promise((resolve, reject) => {
    return DeviceCtrl.getDeviceInfoByPushToken(token)
      .then(device => {
        if (device && device.user) {
          return PartnerCtrl.getPartnerByOrgAndUserId(org, device.user._id)
            .then(partner => {
              let obj = {
                partner: partner,
                device: device
              };
              if (err) {
                obj.err = err;
              }
              if (success) {
                obj.success = success;
              }
              return resolve(obj);
            })
            .catch(err => {
              console.log('Error in gcm.controller.getUserData 1');
              console.log(err);
              return reject(err);
            })
        } else {
          let err = {type: 'TokenNotExists', code: 404, msg: 'User token does not exists: ' + token};
          console.log('Error in gcm.controller.getUserData 2');
          console.log(err);
          return reject(err);
        }
      })
  })
}

function createErrorLog(allErrors) {
  return function(data) {
    return new Promise((resolve, reject) => {
      if (!allErrors.length) {
        let error = {partner: {}};
        //If no partner exists in log
        error.partner[data.partner._id] = {};
        error.partner[data.partner._id].users = [];
        let user = JSON.parse(JSON.stringify(data.device.user));
        let device = JSON.parse(JSON.stringify(data.device));
        delete device.user;
        device.error = data.err;
        device.success = data.success;
        user.devices = [device];
        error.partner[data.partner._id].users.push(user);
        allErrors.push(error);
        return resolve(allErrors);
      } else {
        allErrors.forEach(error => {
          console.log(error);
          if (error.partner && error.partner._id && data.partner && data.partner._id && error.partner._id == data.partner._id) {
            //If partner exists in log
            if (error.partner[data.partner._id] && error.partner[data.partner._id].users) {
              //If users exists in partner
              error.partner[data.partner._id].users.forEach(uentity => {
                if (uentity._id && uentity.devices && uentity.devices.length && uentity._id == data.device.user._id) {
                  uentity.devices.forEach(dentity => {
                    if (dentity._id && dentity._id == data.device._id) {
                      //If device exists in partner
                      dentity.error = data.err;
                      dentity.success = data.success;
                    }
                  })
                } else {
                  //If no device in users
                  let user = JSON.parse(JSON.stringify(data.device.user));
                  let device = JSON.parse(JSON.stringify(data.device));
                  delete device.user;
                  device.error = data.err;
                  device.success = data.success;
                  user.devices = [device];
                  uentity.users.push(user);
                }
              })
            } else {
              //If no user in partner
              error.partner[data.partner._id].users = [];
              let user = JSON.parse(JSON.stringify(data.device.user));
              let device = JSON.parse(JSON.stringify(data.device));
              delete device.user;
              device.error = data.err;
              device.success = data.success;
              user.devices = [device];
              error.partner[data.partner._id].users.push(user);
            }
          } else {
            //If no partner exists in log
            error.partner[data.partner._id] = {};
            error.partner[data.partner._id].users = [];
            let user = JSON.parse(JSON.stringify(data.device.user));
            let device = JSON.parse(JSON.stringify(data.device));
            delete device.user;
            device.error = data.err;
            device.success = data.success;
            user.devices = [device];
            error.partner[data.partner._id].users.push(user);
            allErrors.push(error);
          }
        });
        return resolve(allErrors);
      }
    })
  }
}

function saveLog(data) {
  return new Promise((resolve, reject) => {
    if (data && data.card) {
      return NotificationLogCtrl.saveLog(data)
        .then(data => {
          return resolve(data);
        })
        .catch(err => {
          console.log(err);
          console.log('Error in gcm.comtroller.saveLog 1');
          return reject(err);
        })
    } else {
      return resolve(data);
    }
  })
}

export function sendNotification(options) {
  return new Promise((resolve, reject) => {
    if (options && options.devices) {
      console.log("Devices:");
      console.log(JSON.stringify(options.devices));
      let iosPushTokens = options.devices.filter(device => device.dos && device.dos.toLowerCase() == 'ios' && device.dpushtoken && device.dpushtoken.length).map(device => device.dpushtoken);
      let androidPushTokens = options.devices.filter(device => device.dos && device.dos.toLowerCase() == 'android' && device.dpushtoken && device.dpushtoken.length).map(device => device.dpushtoken);
      let browserPushTokens = options.devices.filter(device => device.dtype && device.dtype.toLowerCase() == 'browser extension' && device.dpushtoken && device.dpushtoken.length).map(device => device.dpushtoken);
      let message = new gcm.Message();
      async.parallel({
        androidPush: function(callback) {
          if (androidPushTokens.length) {
            let trimmedMsg = _.cloneDeep(options.msg);
            trimmedMsg.organization = options.msg.organization._id.toString();
            message.addData('message', JSON.stringify(trimmedMsg));
            console.log('Android Message Data:');
            console.log(JSON.stringify(trimmedMsg));
            let key = gcmKey;
            // if (useOrgSpecificKeysForGCM && orgMappings.android[options.msg.organization.toString()]) {
            //   key = orgMappings.android[options.msg.organization.toString()];
            // }
            if (useOrgSpecificKeysForGCM && options.msg.organization && options.msg.organization.androidKey && options.msg.organization.androidKey.key) {
              key = options.msg.organization.androidKey.key;

              console.log("Android key: " + key);
              console.log("org: " + JSON.stringify(options.msg.organization));
              let sender = new gcm.Sender(key);
              sender.send(message, {registrationTokens: androidPushTokens}, function(err, response) {
                if (err) {
                  console.error('Error in gcm.controller.sendNotification 1');
                  console.error(err);
                  return callback(err);
                } else {
                  console.log('Response in gcm.controller.sendNotification 2');
                  console.log(response);
                  return callback(null, response);
                }
              });
            } else {
              console.error('Error in gcm.controller.sendNotification 11');
              let err = {type: 'KeyNotFound', code: 400, msg: 'Organization doesn\'t have android push key'};
              console.error(err);
              return callback(err);
            }
          } else {
            return callback(null, null);
          }
        },
        iosPush: function(callback) {
          if (iosPushTokens.length) {
            let allErrors = [];
            async.each(iosPushTokens, (token, cb) => {
              let payload = {
                aps: {
                  "badge": 1,
                  "card_id": options.msg._id.toString(),
                  "sound": "ping.aiff",
                  "alert": {
                    "title": options.msg.data.title,
                    "body": options.msg.data.description,
                    "action-loc-key": "OPEN"
                  }
                }
              };

              let iOSLimit = 248;
              let charCount = (JSON.stringify(payload.aps)).length;
              if (charCount > iOSLimit) {
                let charsToBeTrimmed = charCount - iOSLimit;
                if (payload.aps.alert.body.length > charsToBeTrimmed) {
                  payload.aps.alert.body = payload.aps.alert.body.substring(0, payload.aps.alert.body.length - charsToBeTrimmed);
                }
              }
              charCount = (JSON.stringify(payload.aps)).length;
              if (charCount > iOSLimit) {
                let charsToBeTrimmed = charCount - iOSLimit;
                if (payload.aps.alert.title.length > charsToBeTrimmed) {
                  payload.aps.alert.title = payload.aps.alert.title.substring(0, payload.aps.title.body.length - charsToBeTrimmed);
                }
              }

              if (useOrgSpecificKeysForAPN && options.msg.organization && options.msg.organization.iosKey && options.msg.organization.iosKey.key) {
                request.get(options.msg.organization.iosKey.key, function(error, response, body) {
                  if (!error && response.statusCode == 200) {
                    cert_and_key = body;
                    console.log("iOS key: " + cert_and_key);
                    console.log("org: " + JSON.stringify(options.msg.organization));
                    let notifier = require('node_apns').services.Notifier({cert: cert_and_key, key: cert_and_key}, false /* development = true, production = false */);
                    let Notification = require('node_apns').Notification;
                    let errorLog = {
                      card: options.msg._id,
                      data: [],
                      payload: payload,
                      organization: options.msg.organization._id
                    };
                    console.log("DEVICE TOKEN: " + token);
                    console.log('iOS Message Data:');
                    console.log(JSON.stringify(payload));
                    notifier.notify(Notification(token, payload),
                      function(err) {
                        if (err) {
                          console.log(err);
                          let errString = err;
                          if (typeof err != 'string') {
                            errString = 'Invalid Notification';
                          }
                          return getUserData(token, options.msg.organization._id, errString)
                            .then(createErrorLog([]))
                            // .then(saveLog())
                            .then(data => {
                              let allErrors = data;
                              errorLog.data = allErrors;
                              return saveLog(errorLog)
                                .then(done => {
                                  return cb();
                                })
                                .catch(err => {
                                  return cb();
                                })
                            })
                            .catch(err => {
                              return cb();
                            })
                        }
                        return getUserData(token, options.msg.organization._id, null, true)
                          .then(createErrorLog([]))
                          .then(data => {
                            let allErrors = data;
                            errorLog.data = allErrors;
                            return saveLog(errorLog)
                              .then(done => {
                                return cb();
                              })
                              .catch(err => {
                                return cb();
                              })
                          })
                          .catch(err => {
                            return cb();
                          })
                      }
                    );
                  } else {
                    return cb(error);
                  }
                });
              } else {
                console.error('Error in gcm.controller.sendNotification 12');
                let err = {type: 'KeyNotFound', code: 400, msg: 'Organization doesn\'t have iOS push key'};
                let errorLog = {
                  card: options.msg._id,
                  data: [],
                  payload: payload,
                  organization: options.msg.organization._id
                };
                let errString = err;
                return getUserData(token, options.msg.organization._id, errString)
                  .then(createErrorLog([]))
                  // .then(saveLog())
                  .then(data => {
                    allErrors = data;
                    errorLog.data = allErrors;
                    return saveLog(errorLog)
                      .then(done => {
                        return cb();
                      })
                      .catch(err => {
                        return cb();
                      })
                  })
                  .catch(err => {
                    return cb();
                  })
              }
            }, (err, result) => {
              if (err) {
                return callback(err);
              }
              return callback(null, result);
            });
          } else {
            return callback(null, null);
          }
        }
      }, function(err, response) {
        // response is now equals to: {one: 1, two: 2}
        if (err) {
          console.error('Error in gcm.controller.sendNotification 3');
          console.error(err);
          return resolve(err);
        } else {
          console.log('Response in gcm.controller.sendNotification 4');
          console.log(response);
          return resolve(response);
        }
      });
    } else {
      console.error('Error in gcm.controller.sendNotification 5');
      return reject({type: 'EntityNotFound', code: 404, msg: 'options not found'});
    }
  });
}

export function sendLogoutNotification(options) {
  return new Promise((resolve, reject) => {
    if (options && options.devices) {
      console.log(options);
      console.log("Devices:");
      console.log(JSON.stringify(options.devices));
      let iosPushTokens = options.devices.filter(device => device.dos && device.dos.toLowerCase() == 'ios' && device.dpushtoken && device.dpushtoken.length).map(device => device.dpushtoken);
      let androidPushTokens = options.devices.filter(device => device.dos && device.dos.toLowerCase() == 'android' && device.dpushtoken && device.dpushtoken.length).map(device => device.dpushtoken);
      let message = new gcm.Message();
      async.parallel({
        androidPush: function(callback) {
          if (androidPushTokens.length) {
            message.addData('message', JSON.stringify({"auth": "logout"}));
            console.log(JSON.stringify(options.msg.organization));
            let key = gcmKey;
            if (useOrgSpecificKeysForGCM && options.msg.organization && options.msg.organization.androidKey && options.msg.organization.androidKey.key) {
              key = options.msg.organization.androidKey.key;
            }
            console.log("Android key: " + key);
            console.log("org: " + JSON.stringify(options.msg.organization));
            let sender = new gcm.Sender(key);
            sender.send(message, {registrationTokens: androidPushTokens}, function(err, response) {
              if (err) {
                console.error('Error in gcm.controller.sendLogoutNotification 1');
                console.error(err);
                return callback(err);
              } else {
                console.log('Response in gcm.controller.sendLogoutNotification 2');
                console.log(response);
                return callback(null, response);
              }
            });
          } else {
            return callback(null, null);
          }
        },
        iosPush: function(callback) {
          if (iosPushTokens.length) {
            let allErrors = [];
            async.each(iosPushTokens, (token, cb) => {
              let payload = {
                aps: {
                  "badge": 1,
                  "auth": "logout",
                  "sound": "ping.aiff",
                  "alert": {
                    "title": "",
                    "body": "",
                    "action-loc-key": "OPEN"
                  }
                }
              };

              let iOSLimit = 248;
              let charCount = (JSON.stringify(payload.aps)).length;
              if (charCount > iOSLimit) {
                let charsToBeTrimmed = charCount - iOSLimit;
                if (payload.aps.alert.body.length > charsToBeTrimmed) {
                  payload.aps.alert.body = payload.aps.alert.body.substring(0, payload.aps.alert.body.length - charsToBeTrimmed);
                }
              }
              charCount = (JSON.stringify(payload.aps)).length;
              if (charCount > iOSLimit) {
                let charsToBeTrimmed = charCount - iOSLimit;
                if (payload.aps.alert.title.length > charsToBeTrimmed) {
                  payload.aps.alert.title = payload.aps.alert.title.substring(0, payload.aps.title.body.length - charsToBeTrimmed);
                }
              }

              if (useOrgSpecificKeysForAPN && options.msg.organization && options.msg.organization.iosKey && options.msg.organization.iosKey.key) {
                request.get(options.msg.organization.iosKey.key, function(error, response, body) {
                  if (!error && response.statusCode == 200) {
                    cert_and_key = body;
                    console.log("iOS key: " + cert_and_key);
                    console.log("org: " + JSON.stringify(options.msg.organization));
                    let notifier = require('node_apns').services.Notifier({cert: cert_and_key, key: cert_and_key}, false /* development = true, production = false */);
                    let Notification = require('node_apns').Notification;
                    let errorLog = {
                      card: options.msg._id,
                      data: [],
                      payload: payload,
                      organization: options.msg.organization._id
                    };
                    console.log("DEVICE TOKEN: " + token);
                    console.log('iOS Message Data:');
                    console.log(JSON.stringify(payload));
                    notifier.notify(Notification(token, payload),
                      function(err) {
                        if (err) {
                          let errString = err;
                          if (typeof err != 'string') {
                            errString = 'Invalid Notification';
                          }
                          return getUserData(token, options.msg.organization._id, errString)
                            .then(createErrorLog([]))
                            // .then(saveLog())
                            .then(data => {
                              let allErrors = data;
                              errorLog.data = allErrors;
                              return saveLog(errorLog)
                                .then(done => {
                                  return cb(null, done);
                                })
                                .catch(err => {
                                  return cb();
                                })
                            })
                            .catch(err => {
                              return cb();
                            })
                        }
                        return getUserData(token, options.msg.organization._id, null, true)
                          .then(createErrorLog([]))
                          .then(data => {
                            let allErrors = data;
                            errorLog.data = allErrors;
                            return saveLog(errorLog)
                              .then(done => {
                                return cb(null, done);
                              })
                              .catch(err => {
                                return cb();
                              })
                          })
                          .catch(err => {
                            return cb();
                          })
                      }
                    );
                  } else {
                    return callback(error);
                  }
                })
              }
            }, (err, result) => {
              if (err) {
                let errorLog = {
                  card: options.msg._id,
                  data: [],
                  payload: payload,
                  organization: options.msg.organization._id
                };
                let errString = err;
                return getUserData(token, options.msg.organization._id, errString)
                  .then(createErrorLog([]))
                  // .then(saveLog())
                  .then(data => {
                    let allErrors = data;
                    errorLog.data = allErrors;
                    return saveLog(errorLog)
                      .then(done => {
                        return callback(err);
                      })
                      .catch(err => {
                        return callback(err);
                      })
                  })
                  .catch(err => {
                    return callback(err);
                  })
              }
              return callback(null, result);
            });
          } else {
            return callback(null, null);
          }
        }
      }, function(err, response) {
        // response is now equals to: {one: 1, two: 2}
        if (err) {
          console.error('Error in gcm.controller.sendLogoutNotification 3');
          console.error(err);
          return resolve(err);
        } else {
          console.log('Response in gcm.controller.sendLogoutNotification 4');
          console.log(response);
          return resolve(response);
        }
      });
    } else {
      console.error('Error in gcm.controller.sendLogoutNotification 5');
      return reject({type: 'EntityNotFound', code: 404, msg: 'options not found'});
    }
  });
}
