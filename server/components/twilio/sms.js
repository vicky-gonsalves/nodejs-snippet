"use strict";

const async = require('async');
import config from "../../config/environment";
const Promise = require('bluebird');
const twilio = require('twilio');
const client = new twilio(config.twilio.accountSid, config.twilio.authToken);
Promise.config({
  // Enables all warnings except forgotten return statements.
  warnings: {
    wForgottenReturn: false
  }
});

export function sendSMS(smsArray) {
  return new Promise(function(resolve, reject) {
    let er = [];
    let msgs = [];
    return async.mapSeries(smsArray, function(sms, cb) {
      if (!sms.from && !sms.from.length) {
        sms.from = config.twilio.phoneNumber;
      }
      if (!sms.body && !sms.body.trim().length && !sms.mediaUrl && !sms.mediaUrl.trim().length) {
        let smsError = {
          msgBody: sms,
          error: {message: 'sms body and mediaUrl is blank'}
        };
        er.push(smsError);
        console.log(smsError);
        return cb();
      }
      if (!sms.mediaUrl && !sms.mediaUrl.trim().length) {
        delete sms.mediaUrl;
      }
      else {
        sms.mediaUrl = encodeURI(sms.mediaUrl);
      }
      console.log(sms);
      return client.messages.create(sms)
        .then((message) => {
          console.log('SMS SENT:' + message.sid);
          console.log(JSON.stringify(sms));
          let smsSent = {
            msgBody: sms,
            sid: message.sid
          };
          msgs.push(smsSent);
          cb();
        })
        .catch(err => {
          let smsError = {
            msgBody: sms,
            error: err
          };
          er.push(smsError);
          console.log(smsError);
          cb();
        })
    }, function(error, done) {
      if (error) {
        console.log('Error in twilio.sms.sendSMS');
        console.log(error);
        return resolve({error: error});
      } else {
        return resolve({sent: msgs, error: er});
      }
    });
  });
}
