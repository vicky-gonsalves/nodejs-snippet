'use strict';

import * as config from '../config/environment';
import _ from 'lodash';
import compose from 'composable-middleware';
import Log from '../api/log/log.model';
import Device from '../api/device/device.model';
import Organization from '../api/organization/organization.model';
const async = require('async');
const winston = require('winston');
const Elasticsearch = require('winston-elasticsearch');
const validator = require('validator');
const Promise = require('bluebird');
const ResponseHelpers = require('../components/responseHelpers/responseHelpers');
let logger;

Promise.config({
  // Enables all warnings except forgotten return statements.
  warnings: {
    wForgottenReturn: false
  }
});

const esTransportOpts = {
  name: 'es-log',
  level: 'info',
  clientOpts: {
    host: config.loggerURL
  }
};

if (config.logger == true || config.logger == 'true') {
  logger = new winston.Logger({
    transports: [
      new Elasticsearch(esTransportOpts)
    ]
  });
}

function saveUpdates(updates) {
  return function(entity) {
    var updated = _.merge(entity, updates);
    return updated.save()
      .then(updated => {
        return updated;
      });
  };
}

function sanitizeEntities(data, toValidate = []) {
  return new Promise((resolve, reject)=> {
    try {
      let sanitizedData = JSON.parse(JSON.stringify(data));
      if (data.dpushtoken && toValidate.indexOf('dpushtoken') > -1) {
        sanitizedData.dpushtoken = validator.trim(data.dpushtoken);
      }

      if (data.did && toValidate.indexOf('did') > -1) {
        sanitizedData.did = validator.trim(data.did);
      }

      if (data.dtype && toValidate.indexOf('dtype') > -1) {
        sanitizedData.dtype = validator.trim(data.dtype);
      }

      if (data.dmake && toValidate.indexOf('dmake') > -1) {
        sanitizedData.dmake = validator.trim(data.dmake);
      }

      if (data.dmodel && toValidate.indexOf('dmodel') > -1) {
        sanitizedData.dmodel = validator.trim(data.dmodel);
      }

      if (data.dres && toValidate.indexOf('dres') > -1) {
        sanitizedData.dres = validator.trim(data.dres);
      }

      if (data.dos && toValidate.indexOf('dos') > -1) {
        sanitizedData.dos = validator.trim(data.dos);
      }

      if (data.dosv && toValidate.indexOf('dosv') > -1) {
        sanitizedData.dosv = validator.trim(data.dosv);
      }

      return resolve(sanitizedData);
    } catch (e) {
      console.log('Error in logger.service.sanitizeEntities 1');
      return reject(e);
    }
  })
}

function validateEntities(toValidate = []) {
  return function(data) {
    return new Promise((resolve, reject)=> {
      if (data) {
        let errors = {type: 'InvalidParams', code: 422, msg: []};
        //Validation
        if (toValidate.indexOf('dpushtoken') > -1) {
          if (!(data.dpushtoken) || validator.isNull(data.dpushtoken)) {
            errors.msg.push({dpushtoken: 'dpushtoken is required'});
          }
        }
        if (toValidate.indexOf('did') > -1) {
          if (!(data.did) || validator.isNull(data.did)) {
            errors.msg.push({did: 'did is required'});
          }
        }
        if (toValidate.indexOf('dtype') > -1) {
          if (!(data.dtype) || validator.isNull(data.dtype)) {
            errors.msg.push({dtype: 'dtype is required'});
          }
        }
        if (toValidate.indexOf('dmake') > -1) {
          if (!(data.dmake) || validator.isNull(data.dmake)) {
            errors.msg.push({dmake: 'dmake is required'});
          }
        }
        if (toValidate.indexOf('dmodel') > -1) {
          if (!(data.dmodel) || validator.isNull(data.dmodel)) {
            errors.msg.push({dmodel: 'dmodel is required'});
          }
        }
        if (toValidate.indexOf('dres') > -1) {
          if (!(data.dres) || validator.isNull(data.dres)) {
            errors.msg.push({dres: 'dres is required'});
          }
        }
        if (toValidate.indexOf('dos') > -1) {
          if (!(data.dos) || validator.isNull(data.dos)) {
            errors.msg.push({dos: 'dos is required'});
          }
        }
        if (toValidate.indexOf('dosv') > -1) {
          if (!(data.dosv) || validator.isNull(data.dosv)) {
            errors.msg.push({dosv: 'dosv is required'});
          }
        }
        if (toValidate.indexOf('pver') > -1) {
          if (!(data.pver) || validator.isNull(data.pver)) {
            errors.msg.push({pver: 'pver is required'});
          }
        }


        if (errors.msg.length > 0) {
          console.log('Error in logger.service.validateEntities 1');
          return reject(errors);
        } else {
          return resolve(data);
        }
      } else {
        console.log('Error in logger.service.validateEntities 2');
        let err = {type: 'EntityNotFound', code: 422, msg: 'Entity not found.'};
        return reject(err);
      }
    });
  }
}

function checkIfDeviceExists(did) {
  return new Promise((resolve, reject)=> {
    Device.findOne({did: did}).count().exec()
      .then(count=> {
        if (count > 0) {
          return resolve(true);
        } else {
          return resolve(false);
        }
      })
      .catch(err => {
        console.log('Error in logger.service.checkIfDeviceExists 1');
        return reject(err);
      });
  });
}

export function putDeviceLog() {
  return compose()
    .use(function(req, res, next) {
      if (req.body) {
        let deviceLog = {
          dpushtoken: req.body.dpushtoken,
          did: req.body.did,
          dtype: req.body.dtype,
          dmake: req.body.dmake,
          dmodel: req.body.dmodel,
          dres: req.body.dres,
          dos: req.body.dos,
          dosv: req.body.dosv,
          pver: req.body.pver
        };
        if (req.user) {
          deviceLog.user = req.user._id;
        }
        let toValidate = [/*'dpushtoken',*/ 'did', 'dtype', 'dmake', 'dmodel', 'dres', 'dos', 'dosv', 'pver'];
        return sanitizeEntities(deviceLog, toValidate)
          .then(validateEntities(toValidate))
          .then(data=> {
            return checkIfDeviceExists(data.did)
              .then(exists=> {
                if (exists) {
                  if (req.body._id) {
                    delete req.body._id;
                  }
                  data.lastLoggedInAt = Date.now();
                  return Device.findOne({did: data.did}).exec()
                    .then(ResponseHelpers.handleEntityNotFound({type: 'EntityNotFound', code: 404, msg: 'did not found.'}))
                    .then(saveUpdates(data))
                    .then(device => {
                      delete req.body.dpushtoken;
                      delete req.body.did;
                      delete req.body.dtype;
                      delete req.body.dmake;
                      delete req.body.dmodel;
                      delete req.body.dres;
                      delete req.body.dos;
                      delete req.body.dosv;
                      delete req.body.pver;
                      req.dos = data.dos;
                      req.did = data.did;
                      req.pver = data.pver;
                      req.deviceId = device._id.toString();
                      next();
                    })
                    .catch(err => {
                      console.log('Error in logger.service.putDeviceLog 1');
                      return ResponseHelpers.respondWithCustomError(res, err);
                    });
                } else {
                  return Device.create(data)
                    .then(device => {
                      delete req.body.dpushtoken;
                      delete req.body.did;
                      delete req.body.dtype;
                      delete req.body.dmake;
                      delete req.body.dmodel;
                      delete req.body.dres;
                      delete req.body.dos;
                      delete req.body.dosv;
                      delete req.body.pver;
                      req.did = data.did;
                      req.deviceId = device._id.toString();
                      next();
                    })
                    .catch(err => {
                      console.log('Error in logger.service.putDeviceLog 2');
                      return ResponseHelpers.respondWithCustomError(res, err);
                    });
                }
              })
              .catch(err => {
                console.log('Error in logger.service.putDeviceLog 3');
                return ResponseHelpers.respondWithCustomError(res, err);
              });
          })
          .catch(err => {
            console.log('Error in logger.service.putDeviceLog 4');
            return ResponseHelpers.respondWithCustomError(res, err);
          });
      } else {
        next();
      }
    });
}

export function putOtherLog() {
  return compose()
    .use(function(req, res, next) {
      if (req.body && req.body.info) {
        let log = {
          info: req.body.info
        };
        if (req.user) {
          log.user = req.user
        }
        return Log.create(log)
          .then(() => {
            delete req.body.info;
            next();
          })
          .catch(err => {
            console.log('Error in logger.service.putOtherLog 1');
            return ResponseHelpers.respondWithCustomError(res, err);
          });
      } else {
        next();
      }
    });
}


export function saveRequestData(data, saveLogs = true) {
  return compose()
    .use(function(req, res, next) {
      if (saveLogs) {
        req.requestData = data;
        req.saveLogs = true;
      }
      next();
    })
}

export function putESLog(req, res) {
  if ((config.logger == true || config.logger == 'true') && req.saveLogs) {
    let requestType = null;
    if (req.requestData.type) {
      requestType = req.requestData.type;
    }
    let ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress || req.socket.remoteAddress || req.connection.socket.remoteAddress;
    let log = {
      meta: {
        ip: ip,
        url: (req.protocol + '://' + req.get('host') + req.originalUrl),
        params: req.params,
        method: req.method,
        body: req.body,
        qs: req.query,
        headers: req.headers
      },
      type: requestType,
      success: (res.statusCode == 200 || res.statusCode == 201)
    };
    if (req.user) {
      let user = _.clone(req.user);
      delete user.password;
      delete user.activationToken;
      delete user.salt;

      log.user = user;
    }

    async.parallel({
      getDevice: function(cb) {
        if (req.deviceId) {
          return Device.findById(req.deviceId)
            .select('-__v')
            .lean()
            .exec()
            .then(device=> {
              log.device = device;
              cb();
            })
            .catch(err=> {
              log.device = {error: err};
              cb(err);
            })
        } else {
          cb();
        }
      },

      getOrg: function(cb) {
        if (req.deviceId) {
          return Organization.findById(req.orgId)
            .select('_id name')
            .lean()
            .exec()
            .then(org=> {
              log.org = org;
              cb();
            })
            .catch(err=> {
              log.org = {error: err};
              cb(err);
            })
        } else {
          cb();
        }
      }


    }, function(err, result) {
      if (err) {
        console.log('Error in logger.service.putESLog 1');
        console.log(err);
      }
      logger.info('API Logs', log);
    });
  }
}
